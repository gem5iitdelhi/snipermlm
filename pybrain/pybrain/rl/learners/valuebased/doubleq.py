__author__ = 'Rahul Jain'
#Hasselt, Hado V. "Double Q-learning." Advances in Neural Information Processing Systems. 2010.

from pybrain.rl.learners.valuebased.valuebased import ValueBasedLearner
from pybrain.rl.learners.valuebased import DoubleSharedActionValueTable
class DoubleQ(ValueBasedLearner):

    offPolicy = True
    batchMode = True

    def __init__(self, alpha=0.5, gamma=0.99):
#        assert(isinstance(self.module, DoubleSharedActionValueTable))
        print "[DoubleQ] init with alpha: ", alpha, " gamma: ", gamma
        ValueBasedLearner.__init__(self)

        self.alpha = alpha
        self.gamma = gamma

        self.laststate = None
        self.lastaction = None
        self.updateCnt = 0

    def learn(self):
        """ Learn on the current dataset, either for many timesteps and
            even episodes (batchMode = True) or for a single timestep
            (batchMode = False). Batch mode is possible, because Q-Learning
            is an off-policy method.

            In batchMode, the algorithm goes through all the samples in the
            history and performs an update on each of them. if batchMode is
            False, only the last data sample is considered. The user himself
            has to make sure to keep the dataset consistent with the agent's
            history.
        """
        if self.batchMode:
            samples = self.dataset
        else:
            samples = [[self.dataset.getSample()]]
        
#        print "[DoubleQ] samples: ", len(samples)
        for seq in samples:
            for state, action, reward in seq:

                state = int(state)
                action = int(action)
#                print "[Batch-Q] ", state, action, reward
                # first learning call has no last state: skip
                if self.laststate == None:
                    self.lastaction = action
                    self.laststate = state
                    self.lastreward = reward
                    continue
                if self.updateCnt%2 == 0:
                  qvalue = self.module.A.getValue(self.laststate, self.lastaction)
                  maxAction = self.module.A.getMaxAction(state)
                  qvalue_b = self.module.B.getValue(state, maxAction)
                  self.module.A.updateValue(self.laststate, self.lastaction, qvalue + self.alpha * (self.lastreward + self.gamma * qvalue_b - qvalue))
                else:
                  qvalue = self.module.B.getValue(self.laststate, self.lastaction)
                  maxAction = self.module.B.getMaxAction(state)
                  qvalue_a = self.module.A.getValue(state, maxAction)
                  self.module.B.updateValue(self.laststate, self.lastaction, qvalue + self.alpha * (self.lastreward + self.gamma * qvalue_a - qvalue))
                
                self.updateCnt += 1
                # move state to oldstate
                self.laststate = state
                self.lastaction = action
                self.lastreward = reward

