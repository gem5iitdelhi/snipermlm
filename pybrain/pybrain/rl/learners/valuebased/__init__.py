from interface import ActionValueTable, ActionValueNetwork, SharedActionValueTable, DoubleSharedActionValueTable
from nfq import NFQ
from q import Q
from doubleq import DoubleQ
from qlambda import QLambda
from sarsa import SARSA
