__author__ = 'Thomas Rueckstiess, ruecksti@in.tum.de'

from pybrain.rl.learners.valuebased.valuebased import ValueBasedLearner


class Q(ValueBasedLearner):

    offPolicy = True
    batchMode = True

    def __init__(self, alpha=0.5, gamma=0.99):
        print "[Q] init with alpha: ", alpha, " gamma: ", gamma
        ValueBasedLearner.__init__(self)

        self.alpha = alpha
        self.gamma = gamma

        self.laststate = None
        self.lastaction = None
        self.lastsmartUpdates = None

    def learn(self):
        """ Learn on the current dataset, either for many timesteps and
            even episodes (batchMode = True) or for a single timestep
            (batchMode = False). Batch mode is possible, because Q-Learning
            is an off-policy method.

            In batchMode, the algorithm goes through all the samples in the
            history and performs an update on each of them. if batchMode is
            False, only the last data sample is considered. The user himself
            has to make sure to keep the dataset consistent with the agent's
            history.
        """
        if self.batchMode:
            samples = self.dataset
        else:
            samples = [[self.dataset.getSample()]]

        for seq in samples:
            for state, action, reward in seq:

                state = int(state)
                action = int(action)

                # first learning call has no last state: skip
                if self.laststate == None:
                    self.lastaction = action
                    self.laststate = state
                    self.lastreward = reward
                    self.lastsmartUpdates = self.smartUpdates
                    continue

                qvalue = self.module.getValue(self.laststate, self.lastaction)
                maxnext = self.module.getValue(state, self.module.getMaxAction(state))
                new_qvalue = qvalue + self.alpha * (self.lastreward + self.gamma * maxnext - qvalue)
                self.module.updateValue(self.laststate, self.lastaction, new_qvalue)

                #secondary Q Updates
                if self.lastsmartUpdates != None:
                  self.smartUpdatesUser(state)
                elif self.multiAgent > 0.0: #Apply all action update
                  #Err = self.lastreward/qvalue
                  #self.smartUpdateRatio(Err)
                  self.smartUpdateCorrection(new_qvalue)

                # move state to oldstate
                self.laststate = state
                self.lastaction = action
                self.lastreward = reward
                self.lastsmartUpdates = self.smartUpdates
    
    
    #only user specified (maybe via jointactions) updates 
    def smartUpdatesUser(self, state):
      for s in self.lastsmartUpdates:
        for a in self.lastsmartUpdates[s]:
#          print "User Sec-Q-Updates for s,a: ", s,a
          qvalue = self.module.getValue(s, a)
          maxAction = self.module.getMaxAction(state)
          maxnext = self.module.getValue(state, maxAction)
          #do not update for long term reward
          self.module.updateValue(s, a, qvalue + self.alpha * (self.lastreward + self.gamma * maxnext - qvalue))
    
    #ratio based smart updates
    #Not working as this leads to unreasonably large or small qvalues
    def smartUpdateRatio(self, Err):
      assert(self.gamma == 0.0)
      aList = []
      #TODO
      if Err < 1 and Err > 0.8: #apply to all actions, +4 ipc should be used to scale others
        for a in range(11):
          if a != self.lastaction:
            aList.append(a)

      elif Err > 1 and Err < 10: #only propagate to higher cache sizes
        if self.lastaction in [1,2,5,6,7]:  
          aList.append(0)
          if self.lastaction in [1,2]:
            aList += range(8,11)        #these are higher than 
          if self.lastaction in [5,6,7]:
            aList += range(3,5)        #these are higher than 
        if self.lastaction>=1 and self.lastaction<=4:
          for a in range(1,5):
            if a>self.lastaction: aList.append(a)
        elif self.lastaction>=5 and self.lastaction<=10:
          for a in range(5,11):
            if a>self.lastaction: aList.append(a)
        elif self.lastaction == 0:
          aList = aList + range(3,5) + range(8,11)

      print "Smart-Q-Updates for s,a: ", self.laststate, self.lastaction,  aList, self.lastreward, qvalue
      for a in aList:
          qvalue = self.module.getValue(self.laststate, a)
          self.module.updateValue(self.laststate, a, qvalue + self.multiAgent * (qvalue*Err - qvalue))
                    
    #updates upstream (high cache sizes) if their qvalue is less
    #updates downstream if their qvalue is more
    def smartUpdateCorrection(self, new_qvalue):
#      assert(self.gamma == 0.0)
      upList = []
      doList = []
      
      if self.lastaction in [1,2,5,6,7]:  
        upList.append(0)
        if self.lastaction in [1,2]:
          upList += range(8,11)        #these are higher than 
        if self.lastaction in [5,6,7]:
          upList += range(3,5)        #these are higher than 
      elif self.lastaction in [3,4,8,9,10]:
        doList.append(0)
        if self.lastaction in [3,4]:
          doList += range(5,8)        #these are lower 
        if self.lastaction in [8,9,10]:
          doList += range(1,3)        #these are lower
      
      if self.lastaction>=1 and self.lastaction<=4:
        for a in range(1,5):
          if a>self.lastaction: upList.append(a)
          elif a<self.lastaction: doList.append(a)
      elif self.lastaction>=5 and self.lastaction<=10:
        for a in range(5,11):
          if a>self.lastaction: upList.append(a)
          elif a<self.lastaction: doList.append(a)
      elif self.lastaction == 0:
        upList = upList + range(3,5) + range(8,11)
        doList = doList + range(1,3) + range(5,8)
      
#      print "UpDo Smart-Q-Updates for s,a: ", self.laststate, self.lastaction, upList, doList, self.lastreward, new_qvalue
      s = self.laststate
      for a in upList:
        q = self.module.getValue(s, a)
        if q < self.lastreward:
          #maxAction = self.module.getMaxAction(state)
          #maxnext = self.module.getValue(state, maxAction)
          #do not update for long term reward
          self.module.updateValue(s, a, q + self.alpha * (self.lastreward - q))
          #self.module.updateValue(self.laststate, a, new_qvalue)
      
      #for a in doList:
      #  q = self.module.getValue(self.laststate, a)
      #  if q > self.lastreward:
      #    self.module.updateValue(s, a, q + self.alpha * (self.lastreward - q))
          #self.module.updateValue(self.laststate, a, new_qvalue)
