__author__ = 'Thomas Rueckstiess, ruecksti@in.tum.de'

from pybrain.utilities import abstractMethod
from pybrain.structure.modules import Table, Module, TanhLayer, LinearLayer, BiasUnit
from pybrain.structure.connections import FullConnection
from pybrain.structure.networks import FeedForwardNetwork
from pybrain.structure.parametercontainer import ParameterContainer
from pybrain.tools.shortcuts import buildNetwork
from pybrain.utilities import one_to_n

from scipy import argmax, array, r_, asarray, where
from random import choice
import threading

class ActionValueInterface(object):
    """ Interface for different ActionValue modules, like the
        ActionValueTable or the ActionValueNetwork.
    """

    numActions = None

    def getMaxAction(self, state, multi=False):
        abstractMethod()

    def getActionValues(self, state):
        abstractMethod()


class ActionValueTable(Table, ActionValueInterface):
    """ A special table that is used for Value Estimation methods
        in Reinforcement Learning. This table is used for value-based
        TD algorithms like Q or SARSA.
    """

    def __init__(self, numStates, numActions, name=None):
        Module.__init__(self, 1, 1, name)
        ParameterContainer.__init__(self, numStates * numActions)
        self.numRows = numStates
        self.numColumns = numActions
        self.multiAgentAction = None
    
    def setMultiAgentMaxAction(self, action):
      #print "setMultiAgentMaxAction ", action
      self.multiAgentAction = action

    @property
    def numActions(self):
        return self.numColumns

    def _forwardImplementation(self, inbuf, outbuf):
        """ Take a vector of length 1 (the state coordinate) and return
            the action with the maximum value over all actions for this state.
        """
        #we need the joint multi action only when taking the action on the env
        #for updates to Q-Table, we still want the true max action, so multi is False by default for calls for update
        outbuf[0] = self.getMaxAction(inbuf[0], True)

    def getMaxAction(self, state, multi=False):
        if multi and self.multiAgentAction != None:
          #print "MA getMaxAction ", self.name, state, self.multiAgentAction
          return self.multiAgentAction
        
        """ Return the action with the maximal value for the given state. """
        values = self.params.reshape(self.numRows, self.numColumns)[state, :].flatten()
        action = where(values == max(values))[0]
        action = choice(action)
        #print "getMaxAction ", self.name, state, action
        return action

    def getActionValues(self, state):
        return self.params.reshape(self.numRows, self.numColumns)[state, :].flatten()

    def initialize(self, value=0.0):
        """ Initialize the whole table with the given value. """
        self._params[:] = value
   
    def initializeState(self, state, values):
      """Initialize a state"""
      self._params[state*self.numColumns:(state+1)*self.numColumns] = values

    def printQTable(self):
      print "Q-Table ", self.numRows, self.numColumns
      for i in range(self.numRows):
        print "S=", i, " Q-Actions:", ["{0:0.2f}".format(i) for i in self._params[i*self.numColumns:(i+1)*self.numColumns]]

class SharedActionValueTable(ActionValueTable):
    def __init__(self, numStates, numActions, name=None):
        ActionValueTable.__init__(self, numStates, numActions, name)
        self.tbSemp = [threading.Semaphore(1) for i in range(numStates*numActions)]
        self.cnt = 0

    def updateValue(self, row, column, value):
        self.tbSemp[row*self.numColumns+column].acquire()
        ActionValueTable.updateValue(self, row, column, value)
        self.cnt = self.cnt + 1
#        print "[SharedActionValueTable] updateValue ", self.cnt, row, column, value
        self.tbSemp[row*self.numColumns+column].release()
#        self.tbSemp.release()

#for Double-Q Learning, 2010, better learning
class DoubleSharedActionValueTable(SharedActionValueTable):
    def __init__(self, numStates, numActions, name=""):
        SharedActionValueTable.__init__(self, numStates, numActions, name+'Base')
        self.A=SharedActionValueTable(numStates, numActions, name+'A')
        self.B=SharedActionValueTable(numStates, numActions, name+'B')
    
    def initialize(self, value=0.0):
        """ Initialize the whole table with the given value. """
        SharedActionValueTable.initialize(self,-1)
        self.A.initialize(value)
        self.B.initialize(value)

    def updateValueA(self, row, column, value):
      self.updateValue('A', row, column, value)

    def updateValueB(self, row, column, value):
      self.updateValue('B', row, column, value)

    def updateValue(self, T, row, column, value):
      if   T=='A': self.A.updateValue(row, column, value)
      elif T=='B': self.B.updateValue(row, column, value)

    def setMultiAgentMaxAction(self, action):
      #print "DQ setMultiAgentMaxAction ", action
      self.multiAgentAction = action

      self.A.setMultiAgentMaxAction(action)
      self.B.setMultiAgentMaxAction(action)
    
    def getMaxAction(self, state, multi=False):
        #HACK for multiagent setup
        if multi and self.multiAgentAction != None:
#          print "getMaxAction ", self.name, state, self.multiAgentAction
          return self.multiAgentAction

        """ Return the action with the maximal value for the given state. """
        values = self.getActionValues(state) 
        action = where(values == max(values))[0]
        action = choice(action)
#        print "getMaxAction ", self.name, state, action
#        print "A:", actA, "B:", actB, "maxAction:", action
        return action

    def getActionValues(self, state):
        actA =  self.A.getActionValues(state)
        actB =  self.B.getActionValues(state)
        values = [(actA[i]+actB[i])/2.0 for i in range(0, self.numActions)]
        return values 

    def printQTable(self):
      print "Q-Table ", self.numRows, self.numColumns
      for i in range(self.numRows):
        print "S=", i, " Q-Actions:",\
        ["{0:0.2f}".format(r) for r in self.A._params[i*self.numColumns:(i+1)*self.numColumns]],\
        ["{0:0.2f}".format(r) for r in self.B._params[i*self.numColumns:(i+1)*self.numColumns]]

class ActionValueNetwork(Module, ActionValueInterface):
    """ A network that approximates action values for continuous state /
        discrete action RL environments. To receive the maximum action
        for a given state, a forward pass is executed for all discrete
        actions, and the maximal action is returned. This network is used
        for the NFQ algorithm. """

    def __init__(self, dimState, numActions, name=None):
        Module.__init__(self, dimState, 1, name)
        self.network = buildNetwork(dimState + numActions, dimState + numActions, 1)
        self.numActions = numActions

    def _forwardImplementation(self, inbuf, outbuf):
        """ takes the state vector and return the discrete action with
            the maximum value over all actions for this state.
        """
        outbuf[0] = self.getMaxAction(asarray(inbuf))

    def getMaxAction(self, state):
        """ Return the action with the maximal value for the given state. """
        return argmax(self.getActionValues(state))

    def getActionValues(self, state):
        """ Run forward activation for each of the actions and returns all values. """
        values = array([self.network.activate(r_[state, one_to_n(i, self.numActions)]) for i in range(self.numActions)])
        return values

    def getValue(self, state, action):
        return self.network.activate(r_[state, one_to_n(action, self.numActions)])
