#!/usr/bin/python
import sys, copy
import random
from csActions import *

#class csActions(csActionsDvfs):      #Coordinated MLM
#class csActions(csActions11):         #MLC
# CACHES=['l1_dcache', 'l2_cache', 'l3_cache']


#class defining valid cache stack actions
class cachestackactions:
    SHARED_WAY=-1
    def __init__(self, np, cacheInfo, csactions_cls):
      self.np = np
      self.cacheInfo = cacheInfo
      csActionsClass = globals()[csactions_cls]
      self.csActions = csActionsClass(np, cacheInfo)
      self._initializeAssignment()

    def _initializeAssignment(self):
      for cache_level in self.csActions.CACHES: 
        print cache_level, ":", self.cacheInfo[cache_level]['wayAssignment']

      self._computeCacheWayReconfigActions()
      print "_computeCacheWayReconfigActions", self._Actions
    
    #each cache level can have -1,0,1 action
    def _computeCacheWayReconfigActions(self):
      self._Actions= self.csActions._Actions

    def _getAction(self, actionNum):
      return self._Actions[actionNum]
    
    def getCacheStackAssignment(self, cachestackActions):
      actionVecs = [self._getAction(actionNum) for actionNum in cachestackActions]
#      print "getCacheStackAssignment: ", cachestackActions, actionVecs
      for cache_level in self.csActions.CACHES:
        cacheIdx = self.csActions.CACHES.index(cache_level)
        for cache_num in range(self.cacheInfo[cache_level]['num']):
          bcore = cache_num*self.cacheInfo[cache_level]['sharedCores']
          ncore = self.cacheInfo[cache_level]['sharedCores']
          self._updateCacheWayAssignment( cache_level, cache_num,
                                            [actionVecs[i][cacheIdx] for i in range(bcore, bcore+ncore)]
                                          );
#      print "getCacheStackAssignment: ", self.cacheInfo['l3_cache']['wayAssignment'], self.cacheInfo['l2_cache']['wayAssignment'], self.cacheInfo['l1_dcache']['wayAssignment']
      #TODO: Core Offsets, hacky for now
      if len(actionVecs[i]) == self.csActions.CORE_DVFS_IDX+1:
        coffsets = [actionVecs[i][self.csActions.CORE_DVFS_IDX] for i in range(self.np)]
#        print "getCacheStackAssignment: coffset", coffsets
      else:
        coffsets = [0]*self.np
      
      return self.cacheInfo['l3_cache']['wayAssignment'], self.cacheInfo['l2_cache']['wayAssignment'], self.cacheInfo['l1_dcache']['wayAssignment'], coffsets

    #updates the way assignment
    #nfixed: min fixed shared ways
    #wayAssign: current way assignment, lidx and ridx for the assignment to be updated
    #coreList: cores participating
    #coreActionList: core actions
    def _updateCacheWayAssignment(self, cache_level, cache_num, coreActionList):
      min_shared = self.cacheInfo[cache_level]['min_shared']    
      nfixed = self.csActions.FIXED[cache_level]      #these ways do not participate in reassignment
      wayAssign = self.cacheInfo[cache_level]['wayAssignment']
      lidx = cache_num*self.cacheInfo[cache_level]['associativity']
      ridx = (cache_num+1)*self.cacheInfo[cache_level]['associativity']
      bcore = cache_num*self.cacheInfo[cache_level]['sharedCores']
      ncore = self.cacheInfo[cache_level]['sharedCores']

      #initial ways are always fixed shared
      if min_shared > 0:
        assert(min_shared == nfixed)
        for w in wayAssign[lidx:lidx+nfixed]:
          assert(w==self.SHARED_WAY)
      elif min_shared == 0: #No fixed shared case
        assert(nfixed == ncore)
        for i in range(ncore):
          wayAssign[i+lidx] = bcore+i 
      else:
        assert(0)
      
#      print "[_updateCacheWayAssignment] ", nfixed, wayAssign, lidx, ridx, bcore, ncore, coreActionList
      #count current cache assignments
      wayCount = [0]*(ncore+1)
      for w in wayAssign[lidx+nfixed:ridx+1]:
        if w==self.SHARED_WAY: wayCount[-1] += 1
        else: wayCount[w-bcore] += 1

      #mark willing cache ways as shared
      for i in range(len(coreActionList)):
#        assert(coreActionList[i] in [-1,0,1,2,3,4,6]) #currently allowed values
        if coreActionList[i]<0 and wayCount[i] > 0:
          if wayCount[i]+coreActionList[i] >=0:    released = abs(coreActionList[i]) 
          else:                                    released = wayCount[i]
          wayCount[i]  -= released
          wayCount[-1] += released #increase shared count
          diter = cacheWayIter(i, lidx+nfixed, ridx, ncore, False)
          while not diter.end() and released > 0:
            idx = diter.next()
            if wayAssign[idx]==i+bcore:
              wayAssign[idx]=self.SHARED_WAY
              released -= 1

      #allocate cache ways if possible
      from random import shuffle
      idxs = range(len(coreActionList))
      shuffle(idxs)
      for i in idxs:
        while coreActionList[i]>0 and wayCount[-1] > 0:
          aiter = cacheWayIter(i, lidx+nfixed, ridx, ncore, True)
          while not aiter.end():
            idx = aiter.next()
            if wayAssign[idx]==self.SHARED_WAY:
              wayAssign[idx]=i+bcore
              break
          
          wayCount[i]  += 1
          wayCount[-1] -= 1 #decr shared count
          coreActionList[i] -= 1  #decr to signify that one way assigned
#      print idxs, wayCount

    def getCacheWayAssignment4Action(self, actionNum):
        actionVec = self._getAction(actionNum)
        assert(sum(actionVec) == 0)
        #TODO not sure if there is a better way to do this
        #first release the ways i.e -1, then allot them
        for c in range(len(actionVec)):
            if actionVec[c] == -1:
              for w in range(self.fixshared, self.ways):
                if self.wayAssignment[w] == self.baseCore+c:
                    self.wayAssignment[w] = -5000  #tmp marker
                    break

        for c in range(len(actionVec)):
            if actionVec[c] == 1:
              for w in range(self.fixshared, self.ways):
                if self.wayAssignment[w] == -5000:
                    self.wayAssignment[w] = self.baseCore+c 
                    break

        print "getCacheWayAssignment4Action",self.Num, self.baseCore, actionVec, self.wayAssignment
        assert(sum(self.wayAssignment) >= -1*self.fixshared)   #hacky way to make sure no unallotted way
        return self.wayAssignment

class cacheWayIter:
    def __init__(self, c, lidx, ridx, ncore, alloc=True):
      self.cidx=-1
      self.idx = self.getIdxList(c, lidx, ridx, ncore, alloc)
#      print "[cacheWayIter]", self.idx

    def getIdxList(self, c, lidx, ridx, ncore, alloc):
      midpoint = lidx-1+(ridx-lidx)/2
      if(ncore==2 or ncore==4):  # TODO starting with 2 or 4 cores
        if (c==0 and not alloc) or (c==ncore-1 and alloc):
          return range(ridx-1, lidx-1, -1)
        elif (c==ncore-1 and not alloc) or (c==0 and alloc):
          return range(lidx, ridx, 1)
        elif (c==1 and not alloc) or (c==ncore-2 and alloc):
          return range(midpoint+1, ridx, 1) + range(lidx, midpoint+1, 1)
        elif (c==ncore-2 and not alloc) or (c==1 and alloc):
          return range(midpoint, lidx-1, -1) + range(ridx-1, midpoint, -1)
      elif ncore==8:
        if c < ncore/2:
          return self.getIdxList(c, lidx, midpoint+1, ncore/2, alloc) + self.getIdxList(0, midpoint+1, ridx, ncore/2, alloc)
        else:
          return self.getIdxList(c-ncore/2, midpoint+1, ridx, ncore/2, alloc) + self.getIdxList(c-ncore/2, lidx, midpoint+1, ncore/2, alloc)
      else:
        if c < ncore/2:
          return self.getIdxList(c, lidx, midpoint+1, ncore/2, alloc) + self.getIdxList(0, midpoint+1, ridx, ncore/2, alloc)
        else:
          return self.getIdxList(c-ncore/2, midpoint+1, ridx, ncore/2, alloc) + self.getIdxList(c-ncore/2, lidx, midpoint+1, ncore/2, alloc)
        #assert(0)

    def next(self):
      self.cidx += 1
      return self.idx[self.cidx]

    def end(self):
      return self.cidx >= len(self.idx)-1

if __name__ == '__main__':
    np = int(sys.argv[1])
    ways = int(sys.argv[2])
    varshared = False
    oneReconf = False
    s =cacheactions(1, np, ways, int(.25*ways), varshared, oneReconf)
    print "Iteration:", len(s._Actions)
    print "Formula: ", getNumValidActions(np)

    cacheWayIter(0, 0, 8, np, True)
    cacheWayIter(0, 0, 8, np, False)
    cacheWayIter(1, 0, 8, np, True)
    cacheWayIter(1, 0, 8, np, False)
    cacheWayIter(2, 0, 8, np, True)
    cacheWayIter(2, 0, 8, np, False)
    cacheWayIter(0, 0, ways, np, False)
    cacheWayIter(0, 0, ways, np, True)
    cacheWayIter(3, 0, ways, np, False)
    cacheWayIter(3, 0, ways, np, True)
    cacheWayIter(np-1, 0, ways, np, False)
    cacheWayIter(np-2, 0, ways, np, True)
    cacheWayIter(np-2, 0, ways, np, False)
    cacheWayIter(np-1, 0, ways, np, True)
