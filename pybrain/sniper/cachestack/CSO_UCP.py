#!/usr/bin/python
#for the given list of experiments, find the optimal joint actions
#also set these actions as the max actions for each of the controller/module/Q-Table
#TODO: User to make sure the experiments and agents are homogeneous
from pybrain.optimization import StochasticHillClimber, HillClimber, RandomSearch
from cs_search_vars import jointCacheActions as jca
import random, sys
from scipy import argmax, array, r_, asarray, where
from random import choice
from cs_search_vars import getCacheInfoStub
from pybrain.structure.evolvables.evolvable import Evolvable
from cachestackactions import cachestackactions

from timeit import timeit

class QTable:
  def __init__(self, numStates, numActions):
    self.values = []
    for s in range(numStates):
      self.values.append([4*random.random() for a in range(numActions)])

  def getActionValues(self, state):
    return self.values[state]

class jointCacheActions(Evolvable):
  def __init__(self, x=None):
    if x == None: self.randomize() #[0]*self.NP  #0 action for all cores
    else: self.x = x

  @classmethod
  def seed(cls):         return cls()
#  def __init__(self, x): self.x = self.__makeValid(x)
  def mutate(self):      self.x = self.__deltachange()
  def copy(self):        return jointCacheActions(self.x)
  def randomize(self):   self.x = self.__randomWayAssign() 
  def __repr__(self):    return '<-%d->'+",".join([str(i) for i in self.x])
  def action(self, core):  return self.x[core]
  
  #random cache way assigment
  def __randomWayAssign(self):
    return self.x
    
  def __deltachange(self):
    return self.x

  def __makeValid1(self, v):
    return v

  def makeValid(self):
    self.x = self.__makeValid(self.x)

class MultiAgentOpt:
  def __init__(self, n, cachestackactions, epsilon=0.20, decay=0.99):
#    print "[MultiAgentOpt] for agents=", n
    self.NumStates = 9
    self.NumActions = 11
    self.n = n
    self.csa = cachestackactions
    
    self.modules = [QTable(self.NumStates, self.NumActions) for i in range(self.n)]  #Q-Table

    self.epsilon = epsilon
    self.decay = decay

  def findOptimalJointActions(self):
    maxEval = 50*self.n/4 #50 for 4-core, 100 for 8-core etc 
    self.agent_states = [random.randint(0, self.NumStates-1) for i in range(self.n)]
   
    max_action = []
    for i in range(self.n):
      action = random.randint(0, self.NumActions-1)
      max_action.append(action)
    
#    print "max_action ", max_action
    #jactions = jointCacheActions(max_action)
    jactions = jca(self.csa.csActions, max_action)
    jactions.makeValid()
    
    if random.random() < self.epsilon:
      jactions.randomize()
    else:
      varList = [jactions,]
      l = HillClimber(self.sumQValues, varList, maxEvaluations = maxEval)
#      l = StochasticHillClimber(self.sumQValues, varList, maxEvaluations = 200)
#      l = RandomSearch(self.sumQValues, varList, maxEvaluations = 200)
#      l.verbose = True
      l.mustMaximize = True     #maximize the objective
      best = l.learn()
#      print "findOptimalJointActions ", self.agent_states, best
      for i in range(self.n):
        state = self.agent_states[i]
#        print " qVal =", self.modules[i].getActionValues(state)
      jactions = best[0][0]
    
    self.epsilon *= self.decay
    self.agent_states = []  #HACK: Verification, just to make sure we do not end up using the current states in next iteration
    return jactions

  def sumQValues(self, varList):
    jactions = varList[0]
    sumQ = 0.0
    for i in range(self.n):
      state = self.agent_states[i]
      action = jactions.action(i)
      qVal = self.modules[i].getActionValues(state)[action]
      sumQ += qVal
    return sumQ

class UCP:
  def __init__(self, np, w):
    self.np = np
    self.w = w
    self.counters = []
    for c in range(np):
      self.counters.append([random.randint(0, 5000*(self.w-i)) for i in range(self.w)])
      for j in range(1, w):
        self.counters[c][j] += self.counters[c][j-1]

  def partition(self):
    shared_cores = self.np
    associativity = self.w
    balance = associativity-shared_cores;
    allocations = [0]*shared_cores 

    while (balance>0):
      winner=0;
      winner_mu=0;
      ways_req = [0]*shared_cores
      max_mu = [0]*shared_cores;
      for i in range(shared_cores):
        ways_req[i] = 1;  #must make progress incase of 0 marginal utility
        max_mu[i], ways_req[i] = self.getMaxMarginalUtility(i, allocations[i], balance)
        if (max_mu[i] >= winner_mu):
          winner_mu = max_mu[i];
          winner = i;
      
      #winner found, update data 
      allocations[winner] += ways_req[winner];
      balance -= ways_req[winner];
    return allocations 
        
  def getMaxMarginalUtility(self, core, alloc, balance):
    max_mu=0.0;
    for i in range(1, balance+1):
      #get marginal utility if way allocation is increased
      mu = (self.counters[core][alloc+i] - self.counters[core][alloc])/i
      if(mu>max_mu):
        max_mu = mu
        reqWays = i
    return max_mu, reqWays;

def print_time(time_dict):
  for np in [4,8,16,32]:
    print "\\addplot+[sharp plot] coordinates {",
    for w in [8,16,32,64]:
      print " (" + str(w) + "," + "{0:.2f}".format(time_dict[np][w]) + ") ",
    print "};"

if __name__ == '__main__':
  #np = int(sys.argv[1])
  #ways = int(sys.argv[2]) #UCP
  count = 10000
 
  ucp_time = {}
  cso_time = {}

  for np in [4,8,16,32]:
    cso_time[np] = {}
    ucp_time[np] = {}
    for w in [8,16,32,64]:
      cso_time[np][w] = 0
      ucp_time[np][w] = 0
     
      if np>=w: continue

      ucpSetup = 'from __main__ import UCP; ucp = UCP({0:d}, {1:d})'.format( np, w )
#      print ucpSetup
      #ucp = UCP(np, ways)
      ucp_time[np][w] = timeit(stmt='ucp.partition()', setup=ucpSetup, number=count)

      csoSetup = 'from __main__ import MultiAgentOpt;from cs_search_vars import getCacheInfoStub;from cachestackactions import cachestackactions; cachestack = cachestackactions({0:d}, getCacheInfoStub({0:d})); maOpt = MultiAgentOpt({0:d}, cachestack)'.format(np)
#      print csoSetup
      #cachestack = cachestackactions(np, getCacheInfoStub(np))
      #maOpt = MultiAgentOpt(np, cachestack)
#      cso_time[np][w] = timeit(stmt='maOpt.findOptimalJointActions()', setup=csoSetup, number=count)

  for np in [4,8,16,32]:
    for w in [8,16,32,64]:
      if np==4 and w==8: continue
      #Normalize
#      print ucp_time[np][w], ucp_time[4][8]
      ucp_time[np][w] = ucp_time[np][w]/ucp_time[4][8]
#      cso_time[np][w] = cso_time[np][w]/cso_time[4][8]
  ucp_time[4][8] = 1.0
#  cso_time[4][8] = 1.0

  print_time(ucp_time)
#  print_time(cso_time)
