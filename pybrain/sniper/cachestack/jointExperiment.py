#for the given list of experiments, find the optimal joint actions
#also set these actions as the max actions for each of the controller/module/Q-Table
#TODO: User to make sure the experiments and agents are homogeneous
from pybrain.optimization import StochasticHillClimber, HillClimber, RandomSearch
from cs_search_vars import jointCacheActions
import random
from scipy import argmax, array, r_, asarray, where
from random import choice


class MultiAgentOpt:
  def __init__(self, experiments, cachestackactions, epsilon=0.20, decay=0.99, smartUpdate=0.0):
    print "[MultiAgentOpt] for agents=", len(experiments)
    self.experiments = experiments
    self.n = len(experiments)
    self.csa = cachestackactions
    self.modules = [e.agent.module for e in experiments]  #Q-Table

    self.epsilon = epsilon
    self.decay = decay
    self.smart = smartUpdate
    
    self.agent_sacrifice = [1.0]*self.n #this is a list keeping track of agent sacrifice done due to joint actions

    self.histJA = {}

  def __getAgentStates(self):
    self.agent_states = [exp.task.getObservation() for exp in self.experiments]
  
  def __getAgentRewards(self):
    self.agent_rewards = [exp.task.getReward() for exp in self.experiments]

  #joint action based on all agents max_action, usually this will be an invalid action
  def __getMaxJointAction(self):
    max_action = []
    for i in range(self.n):
      state = self.agent_states[i]
      values = self.modules[i].getActionValues(state)
      self.max_action_values.append(max(values))      #max expected IPC/utility in state
      action = where(values == max(values))[0]
      action = choice(action)
      max_action.append(action)
#    return max_action

  def findOptimalJointActions(self):
    self.max_action_values = []   #represents the max utility in a state
    self.agent_states = []  #HACK: Verification, just to make sure we do not end up using old state data
    self.agent_rewards = []

    self.__getAgentStates()
    self.__getAgentRewards()
#    print "findOptimalJointActions: ", self.agent_states, self.agent_rewards, self.max_action_values
    
    max_action = self.__getMaxJointAction()
    jactions = jointCacheActions(self.csa.csActions, max_action)
#    print "max_action, jactions ", max_action, jactions
    jactions.makeValid()
#    print "jactions.makeValid() ", jactions
    if random.random() < self.epsilon:
      max_action = self.__getMaxJointAction()
      jactions = jointCacheActions(self.csa.csActions, max_action)
#      jactions.randomize()
    else:
      varList = [jactions,]

#      l = HillClimber(self.sacrificeQValues, varList, maxEvaluations = int(1000*self.n/4))
      l = HillClimber(self.searchEval, varList, maxEvaluations = int(250*self.n/4))
#      l = HillClimber(self.weightedQValues, varList, maxEvaluations = int(5000*self.n/4))


#      l = StochasticHillClimber(self.sumQValues, varList, maxEvaluations = 200)
#      l = RandomSearch(self.sumQValues, varList, maxEvaluations = 200)
#      l.verbose = True
      l.mustMaximize = True     #maximize the objective
      best = l.learn()
      print "findOptimalJointActions best, max ", self.agent_states, best, self.searchEval(best[0]), sum(self.max_action_values)
      for i in range(self.n):
        state = self.agent_states[i]
#        print " qVal =", self.modules[i].getActionValues(state)
      jactions = best[0][0]
    
    self.epsilon *= self.decay
    self.__setActions(jactions)

  def searchEval(self, varList):
    jactions = varList[0]
    key = "-".join(str(jactions.action(i)) for i in range(self.n))
    if key not in self.histJA.keys():
      self.histJA[key] = 0
    self.histJA[key] += 1

    return self.sumQValues(varList);
#    return self.sacrificeQValues(varList);
#    return self.weightedQValues(varList);

  def sumQValues(self, varList):
    jactions = varList[0]
    sumQ = 0.0
    for i in range(self.n):
      state = self.agent_states[i]
      action = jactions.action(i)
      qVal = self.modules[i].getActionValues(state)[action]
      sumQ += qVal
#    print "sumQValues: jactions=", jactions, sumQ
    return sumQ
  
  def sumInvQValues(self, varList):
    jactions = varList[0]
    sumQ = 0.0
    for i in range(self.n):
      state = self.agent_states[i]
      action = jactions.action(i)
      qVal = self.modules[i].getActionValues(state)[action]
      sumQ += 1.0/qVal
#    print "sumQValues: jactions=", jactions, sumQ
    return sumQ
  
  def weightedQValues(self, varList):
    jactions = varList[0]
    sumQ = 0.0
    for i in range(self.n):
      state = self.agent_states[i]
      action = jactions.action(i)
      qVal = self.modules[i].getActionValues(state)[action]
      #the assumption is that the max action represents the best IPC that can be achieved in this state
      #which is taken as standalone IPC and hence we can compute a weighted speedup
      sumQ += (qVal/self.max_action_values[i])
#    print "sumQValues: jactions=", jactions, sumQ
    return sumQ

  def sacrificeQValues(self, varList):
    jactions = varList[0]
    sumQ = 0.0
    for i in range(self.n):
      state = self.agent_states[i]
      action = jactions.action(i)
      qVal = self.modules[i].getActionValues(state)[action]
      sumQ = sumQ + (self.agent_sacrifice[i]*qVal)
#    print "sumQValues: jactions=", jactions, sumQ
    return sumQ

  def updateAgentSacrifice(self, actions):
    for i in range(self.n):
      state = self.agent_states[i]
      action = actions.action(i)
      qVal = self.modules[i].getActionValues(state)[action]
      max_qVal = self.max_action_values[i]
      self.agent_sacrifice[i] = self.agent_sacrifice[i]*max_qVal/qVal

    #sacrifice factor is a non decreasing value with each iteration, so this can explode
    if(max(self.agent_sacrifice)/min(self.agent_sacrifice) > 10000.0):
      print "ZZZZZZZZZZZZZZZZZZ updateAgentSacrifice " , self.agent_sacrifice
      
    if(max(self.agent_sacrifice) > 100.0):
      min_s = min(self.agent_sacrifice)
      self.agent_sacrifice = [v/min_s for v in self.agent_sacrifice]
      print "normalizing updateAgentSacrifice ", self.agent_sacrifice

  #set these optimal joint actions on individual agents so that when the Q-Learning algo calls getMaxAction, these respective actions are returned
  def __setActions(self, actions):
    #compute agent sacrifice
#    self.updateAgentSacrifice(actions)
    
#    print "[MultiAgentOpt] setting optimal actions ", actions
    for i in range(self.n):
      self.modules[i].setMultiAgentMaxAction(actions.action(i))

      if self.smart < 0.0:
        state = self.agent_states[i][0]
        smartUpdates = dict()
        smartUpdates[state] = []
        if actions.action(i) == 3:
          smartUpdates[state].append(4)
        elif actions.action(i) == 8:
          smartUpdates[state].append(9)
          smartUpdates[state].append(10)
        elif actions.action(i) == 9:
          smartUpdates[state].append(10)
        print "finding smartupdates for ", actions.action(i), smartUpdates
        self.experiments[i].agent.learner.setSmartUpdates(smartUpdates)  

  def printHistJA(self):
    print "printHistJA ", len(self.histJA.keys())
    total = 0
    for key in self.histJA.keys():
      total += self.histJA[key]
    print "printHistJA total JA evaluted:", total
    
    for key in self.histJA.keys():
      if self.histJA[key] > 0.01*total: #more than 1%
        print key, "->", self.histJA[key] 
