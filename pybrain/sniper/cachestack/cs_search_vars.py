import random
from pybrain.structure.evolvables.evolvable import Evolvable

#from cachestackactions import csActions
from cachestackactions import cachestackactions
import sys

#class for defining cache partitioning for NP cores
#the variables evolve around valid actions from cachestackactions class
class jointCacheActions(Evolvable):
  def __init__(self, csActions, x=None):
    self.csActions = csActions
    self.NP = csActions.np
    if x == None: self.randomize() #[0]*self.NP  #0 action for all cores
    else: self.x = x

  @classmethod
  def seed(cls, csActions):      return cls(csActions)
#  def __init__(self, x): self.x = self.__makeValid(x)
  def mutate(self):      self.x = self.__deltachange()
  def copy(self):        return jointCacheActions(self.csActions, self.x)
  def randomize(self):   self.x = self.__randomWayAssign() 
  def __repr__(self):    return '<-%d->'+",".join([str(i) for i in self.x])
  def action(self, core):  return self.x[core]
  
  #random cache way assigment
  def __randomWayAssign(self):
    return self.csActions.randomActions()
    
  def __deltachange(self):
    return self.csActions.deltachange(self.x)

  def __makeValid(self, v):
    return self.csActions.makeValid(v)

  def makeValid(self):
    self.x = self.__makeValid(self.x)

#Testing stub
def getCacheInfoStub(np):
  cacheInfo = {}
  for cache in ['l1_icache', 'l1_dcache', 'l2_cache',]:
    cacheInfo[cache] = {}
    cacheInfo[cache]['associativity']=8
    cacheInfo[cache]['min_shared']=2
    cacheInfo[cache]['sharedCores']=2
    cacheInfo[cache]['num']=np/cacheInfo[cache]['sharedCores']
    cacheInfo[cache]['wayAssignment']=[-1]*cacheInfo[cache]['associativity']*cacheInfo[cache]['num']

  cache = 'l3_cache'
  cacheInfo[cache] = {}
  cacheInfo[cache]['associativity']=16
  cacheInfo[cache]['min_shared']=4
  cacheInfo[cache]['sharedCores']=4
  cacheInfo[cache]['num']=np/cacheInfo[cache]['sharedCores']
  cacheInfo[cache]['wayAssignment']=[-1]*cacheInfo[cache]['associativity']*cacheInfo[cache]['num']

  return cacheInfo

def np4_UT(csa):
  print "csa.makeValid([10,10,10,10]) ", csa.makeValid([10,10,10,10])
  print "csa.makeValid([10,8,10,10]) ", csa.makeValid([10,8,10,10])
  
def UT(csa):
  jac = jointCacheActions.seed(csa)
  print "seed ", jac
  jac.randomize()
  print "randomize ", jac
  jac.mutate()
  print "mutate ", jac

  for i in range(10):
    jac.randomize()
    print "randomize ", jac
  
def reg_Shared(cachestack, np, num):
  csa = cachestack.csActions
  #shared=True
  for i in range(num):
    actionNumList = []
    for n in range(np):
      actionNumList.append(random.randint(0, csa.numActions-1))
    print "wayAssignment FIXED ", csa.cacheInfo['l2_cache']['wayAssignment'], csa.cacheInfo['l3_cache']['wayAssignment']
    print "csa.makeValid ",actionNumList
    sys.stdout.flush()
    valid = csa.makeValid(actionNumList, True)
    print valid
    cachestack.getCacheStackAssignment(valid)

def np4_UT_NoFixed(csa):
  #shared=False
  csa.cacheInfo['l2_cache']['min_shared'] = 0
  csa.cacheInfo['l3_cache']['min_shared'] = 0
  
  csa.cacheInfo['l2_cache']['wayAssignment']=[0,1,0,0,0,1,1,1,2,3,2,2,2,3,3,3]
  csa.cacheInfo['l3_cache']['wayAssignment']=[0,1,2,3,0,1,1,1,2,2,2,2,3,3,3,3]
  
  print "wayAssignment ", csa.cacheInfo['l2_cache']['wayAssignment'], csa.cacheInfo['l3_cache']['wayAssignment']
  assert (csa.makeValid([2,2,5,5], True) == [2,2,5,5])
  
  csa.cacheInfo['l2_cache']['wayAssignment']=[0,1,0,0,0,1,1,1,2,3,2,2,2,2,2,2]
  csa.cacheInfo['l3_cache']['wayAssignment']=[0,1,2,3,0,1,1,1,2,2,2,2,3,3,3,3]
  print "wayAssignment ", csa.cacheInfo['l2_cache']['wayAssignment'], csa.cacheInfo['l3_cache']['wayAssignment']
  assert (csa.makeValid([0,0,3,2], True) == [0,0,0,0])
  

def reg_NoFixed(cachestack, np, num):
  csa = cachestack.csActions
  for i in range(num):
    actionNumList = []
    for n in range(np):
      actionNumList.append(random.randint(0, csa.numActions-1))
    print "wayAssignment NOFIXED ", csa.cacheInfo['l2_cache']['wayAssignment'], csa.cacheInfo['l3_cache']['wayAssignment']
    print "csa.makeValid ",actionNumList
    sys.stdout.flush()
    valid = csa.makeValid(actionNumList, True)
    print valid
    cachestack.getCacheStackAssignment(valid)

if __name__ == '__main__':
  np = 8
  cachestack = cachestackactions(np, getCacheInfoStub(np))
  csa = cachestack.csActions

  UT(csa)
  if np==4: 
    np4_UT(csa)
  
  num=50000
  reg_Shared(cachestack, np, num)
  print "=========================================================================="
  
  if np==4: 
    np4_UT_NoFixed(csa)
  
  reg_NoFixed(cachestack, np, num)
