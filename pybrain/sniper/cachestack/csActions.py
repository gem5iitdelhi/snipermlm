import random, sys
class csActionsBase:
  CACHES=['l1_dcache', 'l2_cache', 'l3_cache']
  CORE_DVFS_IDX=3 #index for storing core dvfs offset in the ActionVector
  l2_actions = []
  l3_actions = []
  _Actions = [ [0,0,0],  #no change
                ] + l2_actions + l3_actions
  numActions = len(_Actions)
  def __init__(self, np, cacheInfo):
    print "Actions: ", self.numActions
    
    if len(self.l2_actions) > 0:
      print "Total l2_actions: ", self.l2_actions
      assert(cacheInfo['l1_dcache']['sharedCores'] > 1 and cacheInfo['l2_cache']['sharedCores'] > 1)

    self.np = np
    self.cacheInfo = cacheInfo
    self.FIXED = {}
    for level in self.CACHES:
      fixed = self.cacheInfo[level]['min_shared']
      if fixed == 0:  fixed = self.cacheInfo[level]['sharedCores']
      self.FIXED[level] = fixed

  def randomActions(self):
    #create random action vector
    actionNumList = [random.randint(0, self.numActions-1) for i in range(self.np)]
    actionNumList = self.makeValid(actionNumList)
    return actionNumList

  def deltachange(self, actionNumList):
    idx = random.randint(0, self.np-1)  #both limits included
    old = actionNumList[idx]
    new = (old + random.randint(-4,4))%self.numActions
    actionNumList[idx] = new
    actionNumList = self.makeValid(actionNumList)
    return actionNumList

  def __makeValidL2(self, actions, shared):
    level = 'l2_cache'
    actionSpace = self.l2_actions
    if shared == True:
      return self.__makeValidShared(actions, level, actionSpace)
    else:
      return self.__makeValidNoShared(actions, level, actionSpace)

  def __makeValidL3(self, actions, shared):
    level = 'l3_cache'
    actionSpace = self.l3_actions
    if shared == True:
      return self.__makeValidShared(actions, level, actionSpace)
    else:
      return self.__makeValidNoShared(actions, level, actionSpace)

  #input is list of action vectors, output is vector of action numbers
  def makeValid(self, actionNumList, shared=True):
    actions = [self._Actions[actionNumList[i]] for i in range(self.np)]
#    print "[makeValid] input ", actions, self.np, shared
    actions = self.__makeValidL2(actions, shared)
#    print "[makeValidL2] output ", actions, self.np
    actions = self.__makeValidL3(actions, shared)
#    print "[makeValidL3] output ", actions, self.np

#    print "[makeValid] output ", actions
    #convert to actionNumList
    actionNumList = [self._Actions.index(actions[i]) for i in range(self.np)]
    return actionNumList
  
  def __makeValidNoShared(self, actions, level, actionSpace):
    assert(0) #NOT WORKING
    assert(self.cacheInfo[level]['wayAssignment'].count(-1) == 0 and 
            self.cacheInfo[level]['min_shared'] == 0)
#    print "[__makeValidNoShared] input ", level, actions
    actions = self.__makeReleaseValid(actions, level, actionSpace)

#    print "[__makeReleaseValid] output ", level, actions
    #At this point we are sure that all way releases can be fulfilled
    actions = self.__makeDemandValid(actions, level, actionSpace)
#    print "[__makeDemandValid] output ", level, actions
    actions = self.__matchDemandWithRelease(actions, level, actionSpace)
#    print "[__matchDemandWithRelease] input ", level, actions
    
    return actions
    
  def __makeValidShared(self, actions, level, actionSpace):
#    print "[__makeValidShared] input ", level, actions
    actions = self.__makeReleaseValid(actions, level, actionSpace)
#    print "[__makeReleaseValid] output ", level, actions
#    sys.stdout.flush()
    #At this point we are sure that all way releases can be fulfilled
    actions = self.__makeDemandValid(actions, level, actionSpace)
#    print "[__makeDemandValid] output ", level, actions
    return actions
  
  def getWayCount(self, level, num):
    lidx = self.cacheInfo[level]['associativity']*num+self.FIXED[level]
    ridx = self.cacheInfo[level]['associativity']*(num+1)
    wayAssign = self.cacheInfo[level]['wayAssignment'][lidx:ridx]
    wayCount = [wayAssign.count(core) for core in range(self.np)]
    wayCount.append(wayAssign.count(-1))
#    print "getWayCount ", wayCount
    assert(sum(wayCount) == self.cacheInfo[level]['associativity']-self.FIXED[level])
    return wayCount

  #assumes that releases can be fulfilled
  def __makeDemandValid(self, actions, level, actionSpace):
    cacheIdx = self.CACHES.index(level)
    for num in range(self.cacheInfo[level]['num']):
      wayCount = self.getWayCount(level, num)
      cores = range(num*self.cacheInfo[level]['sharedCores'], (num+1)*self.cacheInfo[level]['sharedCores'])
      action_sum = sum([actions[core][cacheIdx] for core in cores])
      i = 0
      while action_sum - wayCount[-1] > 0:
        if i%self.cacheInfo[level]['sharedCores'] == 0:
          random.shuffle(cores)
          i = 0
  #        print "action_sum, wayCount[-1]", action_sum, wayCount[-1]
        #more way demand
        #core = num*self.cacheInfo[level]['sharedCores']+random.randint(0,self.cacheInfo[level]['sharedCores']-1)
        core = cores[i]
        i += 1
        cs_action = actions[core]     #[L1D, L2,L3]
        action = cs_action[cacheIdx]  #-1/0/+1 etc
        if action > 0:
          if action == 1:
            cs_action = self._Actions[0]
          else:
            aidx = actionSpace.index(cs_action)
            cs_action = actionSpace[aidx-1]   #reduce the number of ways demanded
        actions[core] = cs_action
        action_sum = sum([actions[core][cacheIdx] for core in range(num*self.cacheInfo[level]['sharedCores'], (num+1)*self.cacheInfo[level]['sharedCores'])])

#    print "__makeValid actions", actions
    return actions
  
  def __makeReleaseValid(self, actions, level, actionSpace):
    cacheIdx = self.CACHES.index(level)
    for num in range(self.cacheInfo[level]['num']):
      wayCount = self.getWayCount(level, num)
#      print "__makeValid wayCount cache ", level, wayCount
      #check if way releases can be fulfilled
      for i in range(self.cacheInfo[level]['sharedCores']):
        core = num*self.cacheInfo[level]['sharedCores']+i
        cs_action = actions[core]     #[L1D, L2,L3]
        action = cs_action[cacheIdx]  #-1/0/+1 etc
        if action < 0:
          if wayCount[core] == 0:
            cs_action = self._Actions[0]
          else:
            while wayCount[core] < abs(action): #core do not have enough ways to release
              aidx = actionSpace.index(cs_action)
              cs_action = actionSpace[aidx+1]   #reduce the number of ways to be released
              action = cs_action[cacheIdx]  #-1/0/+1 etc
        actions[core] = cs_action
    return actions

  #assumes release is more than demand
  def __matchDemandWithRelease(self, actions, level, actionSpace):
    cacheIdx = self.CACHES.index(level)
    for num in range(self.cacheInfo[level]['num']):
      action_sum = sum([actions[core][cacheIdx] for core in range(num*self.cacheInfo[level]['sharedCores'], (num+1)*self.cacheInfo[level]['sharedCores'])])
      while(action_sum != 0): #increase demand
        #action_sum can only -ve here
        assert(action_sum < 0)
        cores = [num*self.cacheInfo[level]['sharedCores']+c for c in range(self.cacheInfo[level]['sharedCores'])]
#TODO        random.shuffle(cores)
#        core = num*self.cacheInfo[level]['sharedCores']+random.randint(0,self.cacheInfo[level]['sharedCores']-1)
        for core in cores:
          cs_action = actions[core]     #[L1D, L2,L3]
          try:
            actionSpace.index(cs_action)
          except:
            continue  #action not part of this actionspace

          action = cs_action[cacheIdx]  #-1/0/+1 etc
          if action > 0:
            if action == actionSpace[-1][cacheIdx]: #already max action, cannot increase demand
              continue
            else:
              aidx = actionSpace.index(cs_action)
              cs_action = actionSpace[aidx+1]   #incr the number of ways demanded
              old_action = actions[core]
              actions[core] = cs_action
              action_sum = sum([actions[core][cacheIdx] for core in range(num*self.cacheInfo[level]['sharedCores'], (num+1)*self.cacheInfo[level]['sharedCores'])])
              if(action_sum>0): #infeasible action
                actions[core] = old_action
          action_sum = sum([actions[core][cacheIdx] for core in range(num*self.cacheInfo[level]['sharedCores'], (num+1)*self.cacheInfo[level]['sharedCores'])])
    
    return actions

class csActions11(csActionsBase):
  l2_actions = [
      [0,-2,0], [0,-1,0],
      [0,1,0],  [0,2,0],
      ]
  l3_actions = [
      [0,0,-4], [0,0,-2], [0,0,-1],
      [0,0,1],  [0,0,2],  [0,0,4],
      ]
  _Actions = [ [0,0,0],  #no change
                ] + l2_actions + l3_actions
  numActions = len(_Actions)

class csActionsDvfs(csActionsBase):
  l2_actions = [
      ]
  l3_actions = [
      [0,0,-4,0], [0,0,-2,0], [0,0,-1,0],
      [0,0,1,0],  [0,0,2,0],  [0,0,4,0],
      ]
  core_dvfs_actions = [
      [0,0,0,-1], [0,0,0,1],
      ]
  _Actions = [ [0,0,0,0],  #no change
                ] + l2_actions + l3_actions \
                  + core_dvfs_actions
  numActions = len(_Actions)

class csActions5:
  l2_actions = [ [0,-1,0], [0,1,0],]
  l3_actions = [ [0,-1,0], [0,1,0],]
  _Actions = [ [0,0,0],  #no change
                ] + l2_actions + l3_actions
  numActions = len(_Actions)

  def __init__(self, np, cacheInfo):
    self.np = np
    self.cacheInfo = cacheInfo
    self.FIXED = {}
    for level in self.CACHES:
      fixed = self.cacheInfo[level]['min_shared']
      if fixed == 0:  fixed = self.cacheInfo[level]['sharedCores']
      self.FIXED[level] = fixed

  def randomActions(self):
    #create random action vector
    actionNumList = [random.randint(0, self.numActions-1) for i in range(self.np)]
    actionNumList = self.makeValid(actionNumList)
    return actionNumList

  def deltachange(self, actionNumList):
    idx = random.randint(0, self.np-1)  #both limits included
    old = actionNumList[idx]
    new = (old + random.randint(-4,4))%self.numActions
    actionNumList[idx] = new
    actionNumList = self.makeValid(actionNumList)
    return actionNumList

  def __makeValidL2(self, actions, shared):
    level = 'l2_cache'
    actionSpace = self.l2_actions
    if shared == True:
      return self.__makeValidShared(actions, level, actionSpace)
    else:
      return self.__makeValidNoShared(actions, level, actionSpace)

  def __makeValidL3(self, actions, shared):
    level = 'l3_cache'
    actionSpace = self.l3_actions
    if shared == True:
      return self.__makeValidShared(actions, level, actionSpace)
    else:
      return self.__makeValidNoShared(actions, level, actionSpace)

  #input is list of action vectors, output is vector of action numbers
  def makeValid(self, actionNumList, shared=True):
    actions = [self._Actions[actionNumList[i]] for i in range(self.np)]
#    print "[makeValid] input ", actions, self.np, shared
    actions = self.__makeValidL2(actions, shared)
    print "[makeValidL2] output ", actions, self.np
    actions = self.__makeValidL3(actions, shared)
    print "[makeValidL3] output ", actions, self.np

#    print "[makeValid] output ", actions
    #convert to actionNumList
    actionNumList = [self._Actions.index(actions[i]) for i in range(self.np)]
    return actionNumList
  
  def __makeValidNoShared(self, actions, level, actionSpace):
    assert(0) #NOT WORKING
    assert(self.cacheInfo[level]['wayAssignment'].count(-1) == 0 and 
            self.cacheInfo[level]['min_shared'] == 0)
#    print "[__makeValidNoShared] input ", level, actions
    actions = self.__makeReleaseValid(actions, level, actionSpace)

#    print "[__makeReleaseValid] output ", level, actions
    #At this point we are sure that all way releases can be fulfilled
    actions = self.__makeDemandValid(actions, level, actionSpace)
#    print "[__makeDemandValid] output ", level, actions
    actions = self.__matchDemandWithRelease(actions, level, actionSpace)
#    print "[__matchDemandWithRelease] input ", level, actions
    
    return actions
    
  def __makeValidShared(self, actions, level, actionSpace):
#    print "[__makeValidShared] input ", level, actions
    actions = self.__makeReleaseValid(actions, level, actionSpace)
#    print "[__makeReleaseValid] output ", level, actions
#    sys.stdout.flush()
    #At this point we are sure that all way releases can be fulfilled
    actions = self.__makeDemandValid(actions, level, actionSpace)
    print "[__makeDemandValid] output ", level, actions
    return actions
  
  def getWayCount(self, level, num):
    lidx = self.cacheInfo[level]['associativity']*num
    ridx = self.cacheInfo[level]['associativity']*(num+1)
    wayAssign = self.cacheInfo[level]['wayAssignment'][lidx:ridx]
    wayCount = [wayAssign.count(core) for core in range(self.np)]
    wayCount.append(wayAssign.count(-1)-self.FIXED[level])
#    print "getWayCount ", wayCount
    assert(sum(wayCount) == self.cacheInfo[level]['associativity']-self.FIXED[level])
    return wayCount

  #assumes that releases can be fulfilled
  def __makeDemandValid(self, actions, level, actionSpace):
    cacheIdx = self.CACHES.index(level)
    for num in range(self.cacheInfo[level]['num']):
      wayCount = self.getWayCount(level, num)
      cores = range(num*self.cacheInfo[level]['sharedCores'], (num+1)*self.cacheInfo[level]['sharedCores'])
      action_sum = sum([actions[core][cacheIdx] for core in cores])
      i = 0
      while action_sum - wayCount[-1] > 0:
        if i%self.cacheInfo[level]['sharedCores'] == 0:
          random.shuffle(cores)
          i = 0
  #        print "action_sum, wayCount[-1]", action_sum, wayCount[-1]
        #more way demand
        #core = num*self.cacheInfo[level]['sharedCores']+random.randint(0,self.cacheInfo[level]['sharedCores']-1)
        core = cores[i]
        i += 1
        cs_action = actions[core]     #[L1D, L2,L3]
        action = cs_action[cacheIdx]  #-1/0/+1 etc
        if action > 0:
          if action == 1:
            cs_action = self._Actions[0]
          else:
            aidx = actionSpace.index(cs_action)
            cs_action = actionSpace[aidx-1]   #reduce the number of ways demanded
        actions[core] = cs_action
        action_sum = sum([actions[core][cacheIdx] for core in range(num*self.cacheInfo[level]['sharedCores'], (num+1)*self.cacheInfo[level]['sharedCores'])])

#    print "__makeValid actions", actions
    return actions
  
  def __makeReleaseValid(self, actions, level, actionSpace):
    cacheIdx = self.CACHES.index(level)
    for num in range(self.cacheInfo[level]['num']):
      wayCount = self.getWayCount(level, num)
#      print "__makeValid wayCount cache ", level, wayCount
      #check if way releases can be fulfilled
      for i in range(self.cacheInfo[level]['sharedCores']):
        core = num*self.cacheInfo[level]['sharedCores']+i
        cs_action = actions[core]     #[L1D, L2,L3]
        action = cs_action[cacheIdx]  #-1/0/+1 etc
        if action < 0:
          if wayCount[core] == 0:
            cs_action = self._Actions[0]
          else:
            while wayCount[core] < abs(action): #core do not have enough ways to release
              aidx = actionSpace.index(cs_action)
              cs_action = actionSpace[aidx+1]   #reduce the number of ways to be released
              action = cs_action[cacheIdx]  #-1/0/+1 etc
        actions[core] = cs_action
    return actions

  #assumes release is more than demand
  def __matchDemandWithRelease(self, actions, level, actionSpace):
    cacheIdx = self.CACHES.index(level)
    for num in range(self.cacheInfo[level]['num']):
      action_sum = sum([actions[core][cacheIdx] for core in range(num*self.cacheInfo[level]['sharedCores'], (num+1)*self.cacheInfo[level]['sharedCores'])])
      while(action_sum != 0): #increase demand
        #action_sum can only -ve here
        assert(action_sum < 0)
        cores = [num*self.cacheInfo[level]['sharedCores']+c for c in range(self.cacheInfo[level]['sharedCores'])]
#TODO        random.shuffle(cores)
#        core = num*self.cacheInfo[level]['sharedCores']+random.randint(0,self.cacheInfo[level]['sharedCores']-1)
        for core in cores:
          cs_action = actions[core]     #[L1D, L2,L3]
          try:
            actionSpace.index(cs_action)
          except:
            continue  #action not part of this actionspace

          action = cs_action[cacheIdx]  #-1/0/+1 etc
          if action > 0:
            if action == actionSpace[-1][cacheIdx]: #already max action, cannot increase demand
              continue
            else:
              aidx = actionSpace.index(cs_action)
              cs_action = actionSpace[aidx+1]   #incr the number of ways demanded
              old_action = actions[core]
              actions[core] = cs_action
              action_sum = sum([actions[core][cacheIdx] for core in range(num*self.cacheInfo[level]['sharedCores'], (num+1)*self.cacheInfo[level]['sharedCores'])])
              if(action_sum>0): #infeasible action
                actions[core] = old_action
          action_sum = sum([actions[core][cacheIdx] for core in range(num*self.cacheInfo[level]['sharedCores'], (num+1)*self.cacheInfo[level]['sharedCores'])])
    
    return actions

