__author__ = 'Rahul Jain'

from pybrain.rl.environments import Task
from scipy import array

class MDPSniperTask(Task):
    """ This is a MDP task for the MazeEnvironment. The state is fully observable,
        giving the agent the current position of perseus. Reward is given on reaching
        the goal, otherwise no reward. """

    def getReward(self):
        """ compute and return the current reward (i.e. corresponding to the last action performed) """
        reward = self.env.getReward()
#        print "[MDPSniperTask] getReward ", reward
        return reward

    def performAction(self, action):
        """ The action vector is stripped and the only element is cast to integer and given
            to the super class.
        """
#        print "[MDPSniperTask] performAction ", action 
        self.env.performAction(int(action[0]))

    def getObservation(self):
        """ The agent receives its position in the maze, to make this a fully observable
            MDP problem.
        """
        obs = self.env.getSensors() 
#        print "[MDPSniperTask] getObservation ", obs
        return obs



