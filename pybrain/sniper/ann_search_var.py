import random
from pybrain.structure.evolvables.evolvable import Evolvable

#class cooptVar(Evolvable):



#class for defining DVFS variable
class dvfsVar(Evolvable):
  MAX_IDX=4
  @classmethod
  def seed(cls):         return cls(0)
  def __init__(self, x): self.x = max(0, min(x, self.MAX_IDX))
  def mutate(self):      self.x = max(0, min(self.x + random.randint(-1, 1), self.MAX_IDX))
  def copy(self):        return dvfsVar(self.x)
  def randomize(self):   self.x = random.randint(0, self.MAX_IDX)
  def __repr__(self):    return '<-%d->'+str(self.x)
  def dvfsIdx(self):     return self.x

#class for defining cache way assignment variable
class cachewayVar(Evolvable):
  MAX_WAYS=16
  NP = 4
  @classmethod
  def seed(cls):      return cls([cls.MAX_WAYS/cls.NP]*cls.NP)  #TODO  works only if MAX_WAYS%NP = 0
  def __init__(self, x): self.x = self.__makeValid(x)
  def mutate(self):      self.__deltachange()
  def copy(self):        return cachewayVar(self.x)
  def randomize(self):   self.x = self.__randomWayAssign(0, self.MAX_WAYS) 
  def __repr__(self):    return '<-%d->'+",".join([str(i) for i in self.x])
  def numWays(self, core):  return self.x[core]
  
  #random cache way assigment
  def __randomWayAssign(self, shared, associativity):
    exways = associativity-shared
    r2 = random.randint(2,exways-2)
    r1 = random.randint(1, r2-1)
    r3 = random.randint(1, exways-r2-1)
    r2 = r2-r1
    r4 = exways - r1 -r2 -r3
    assert(r1+r2+r3+r4 == exways)
    #print '[__random cachewayVar]', r1, r2, r3, r4
    return [r1, r2, r3, r4,]

  def __deltachange(self):
    posIdx = 0
    negIdx = 0
    while posIdx == negIdx or self.x[negIdx] <= 1 or self.x[posIdx] > self.MAX_WAYS-self.NP:
      posIdx = random.randint(0, self.NP-1)
      negIdx = random.randint(0, self.NP-1)
    self.x[posIdx] += 1
    self.x[negIdx] -= 1
    assert(sum(self.x) == self.MAX_WAYS)

  def __makeValid(self, v):
    #first make sure each core assigned between 1 and 13 ways
    for i in range(len(v)):
      v[i] = max(1, min(v[i], self.MAX_WAYS-self.NP+1))

    #compensation value
    if sum(v) > self.MAX_WAYS: c=-1
    else: c=1

    while sum(v) != self.MAX_WAYS:
      i = random.randint(0, self.NP-1)
      if   c == -1 and v[i]>1: v[i] = v[i] + c
      elif c ==  1 and v[i]<=self.MAX_WAYS-self.NP: v[i] = v[i] + c
    return v

#class for defining DVFS variable for cores+uncore
class dvfsVars(Evolvable):
  NP=4
  dvfsTable = [3.2, 3.0, 2.7, 2.4, 2.0]
  MAX_IDX=len(dvfsTable)-1
  MAX_POWER=(NP+1)*MAX_IDX
  POWER_BUDGET=10

  @classmethod
  #def seed(cls):         return cls([cls.POWER_BUDGET/(cls.NP+1)]*(cls.NP+1))  #TODO
  def seed(cls):         return cls([0]*(cls.NP+1))  #TODO
  def __init__(self, x): self.x = self.__makeValid(x)
  def mutate(self):      self.__deltachange()
  def copy(self):        return dvfsVars(self.x)
  def randomize(self):   self.x = self.__randomDvfs() 
  def __repr__(self):    return '<-%d->'+",".join([str(i) for i in self.x])
  def dvfsLevel(self, core): return self.dvfsTable[ self.x[core] ]
  
  
  def __makeValid(self, v):
    #first make sure each core assigned at least lowest frequency
    for i in range(len(v)):
      v[i] = max(0, min(v[i], self.MAX_IDX))
    return v
  
  def __randomDvfs(self):
    dvfsList = [random.randint(0, self.MAX_IDX)]*(self.NP+1)
    return self.__makeValid(dvfsList)

  def __deltachange(self):
    for i in range(2):
      idx = random.randint(0, self.NP)
      self.x[idx] += random.randint(-1,1)
    self.x = self.__makeValid(self.x)
