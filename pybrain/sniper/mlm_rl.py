#!/usr/bin/python
############################################################################
# Attempt to build a MDP based Self Adapting System Unit to be used with Sniper 
#
############################################################################

__author__ = 'Rahul Jain'

"""
A reinforcement learning (RL) task in pybrain always consists of a few
components that interact with each other: Environment, Agent, Task, and
Experiment. In this tutorial we will go through each of them, create
the instances and explain what they do.

But first of all, we need to import some general packages and the RL
components from PyBrain:
"""

#from scipy import * #@UnusedWildImport
#import pylab
import os
from random import random, choice
from sysenv import SystemEnv
from mdp import MDPSniperTask
from pybrain.rl.learners.valuebased import *
from pybrain.rl.agents import LearningAgent
from pybrain.rl.learners import Q, SARSA, QLambda, DoubleQ
from pybrain.rl.learners.valuebased.linearfa import *
from pybrain.rl.agents.linearfa import LinearFA_Agent
from pybrain.rl.explorers import *
from pybrain.rl.experiments import Experiment
import time, sys
from getmetricSniper import *
from mdpconfig import mdp_options
from util.cacheactionslib import cacheactions
from util.cacheactionslib import rwscacheactions
from util.cacheactionslib import rwpcacheactions
#from util.cacheactionslib import cachestackactions
from cachestack.cachestackactions import cachestackactions

from cachestack.jointExperiment import MultiAgentOpt
import pickle

class mlm_rl:
    def __init__(self, getmetric, np=1, mdpIdx=0, picklefile=None):
        self.np = np
        self.getmetric = getmetric
        self.config = mdp_options[mdpIdx]
        self.StateVecLen = 1

        if picklefile != None:
          self.cacheInfo = pickle.load(open(picklefile, 'r'))
          errfile = open(os.path.dirname(picklefile)+"/mlm.stderr","w")
          sys.stderr = errfile
        else:
          self.cacheInfo = getCacheInfoStub()

        self.setup()

    def __del__(self):
      print "[MLM][__del__] Resetting stderr"
      self.printEnd()

    def printEnd(self):
      if self.isMultiAgent():
        self.maOpt.printHistJA()
      
      for n in range(self.np):
        if self.config['coreDvfs'] == True:
          exp = self.experiments['coreDvfs'][n]
          cntlr = exp.agent.module
          print "Core ", n
          cntlr.printQTable()
      sys.stderr = sys.__stderr__

    def setup(self):
      self.OnlineRL = True
      self.experiments = {}
      
      if self.isCacheStackOpt():
        self.cachestack = cachestackactions(self.np, self.cacheInfo, self.config['actioncls'])
        #HACK TODO
        #for Approximate Q Learning, state is actually a vector of features (continous values) 
        #and not a encoded number as in case of Tablular Q-Learning
        if 'statelen' in self.config: self.StateVecLen = int(self.config['statelen'])

      self.coreSetup()
      self.uncoreSetup()
      self.l3cacheSetup()
      self.l2cacheSetup()
      self.l1dcacheSetup()
      self.l1icacheSetup()
     
      if self.isMultiAgent():
        self.maOpt = MultiAgentOpt(self.experiments['coreDvfs'], self.cachestack, epsilon=0.1, decay=0.985, smartUpdate=self.getSmartUpdateFactor())
   
    def isMultiAgent(self):
      return 'multiagent' in self.config and self.config['multiagent'] == True  #TODO HACK
    
    def getSmartUpdateFactor(self):
      if 'smartupdates' in self.config: #TODO HACK
        return float(self.config['smartupdates'])
      else:
        return 0.0

    def isCacheStackOpt(self):
      #TODO HACK for CacheStack Optimization
      #currently its run as a core mdp and later this cachestack optimizer would do the cache assignments
      return 'cachestack' in self.config and self.config['cachestack'] == True  #TODO HACK for CacheStack Optimization

    #Read Write Shutdown
    def isRWSOpt(self):
      return 'rwsopt' in self.config and self.config['rwsopt'] == True  #TODO HACK
    
    def isNFQ(self):
      if 'learner' in self.config: 
        return self.config['learner'] == 'NFQ'
      return False
    
    def coreSetup(self):
      if self.config['coreDvfs'] == True:
        self.experiments['coreDvfs'] = []
        coreCntrl = self.createController(self.config['coreNumStates'], self.config['coreNumActions'])
        #HACK TODO
        if 'actionfn' in self.config:     actionfn = self.config['actionfn']
        elif self.config['coreNumActions'] == 3: actionfn = "getCoreDvfsActions"
        elif self.config['coreNumActions'] == 5: actionfn = "getCoreDvfsFreqIndex"
        else: assert(0)

        for c in range(self.np):
          args = dict()
          args['core'] = c
          args['slen'] = self.StateVecLen
          args['tlen'] = self.StateVecLen*self.np
          args['rlen'] = self.np
          args['actionfn'] = actionfn 
          args['actionfnarg'] = self.config['coreNumActions']
          args['statefn'] = self.getmetric.getCoreDvfsMetrics
          args['rewardfn'] = self.getmetric.getCoreDvfsReward
          self.experiments['coreDvfs'].append(self.createExperiment(coreCntrl, args))
          if self.config['coreDvfsCentral'] == False:
            coreCntrl = self.createController(self.config['coreNumStates'], self.config['coreNumActions'])
      
    def uncoreSetup(self):
        if self.config['uncoreDvfs'] == True:
            #HACK TODO
            if self.config['uncoreNumActions'] == 3: actionfn = "getUnCoreDvfsActions"
            elif self.config['uncoreNumActions'] == 5: actionfn = "getUnCoreDvfsFreqIndex"

            self.experiments['uncoreDvfs'] = []
            args = dict()
            args['uncore'] = 0
            args['slen'] = self.StateVecLen
            args['tlen'] = self.StateVecLen 
            args['rlen'] = 1 
            args['actionfn'] = actionfn
            args['actionfnarg'] = None 
            args['statefn'] = self.getmetric.getUnCoreDvfsMetrics
            args['rewardfn'] = self.getmetric.getUnCoreDvfsReward
            uncoreCntrl = self.createController(self.config['uncoreNumStates'], self.config['uncoreNumActions'])
            self.experiments['uncoreDvfs'].append(self.createExperiment(uncoreCntrl, args))
    
    def isBatchL3Mdp(self):
      return 'batchL3Mdp' in self.config and self.config['batchL3Mdp'] == True  #TODO HACK

    def l3cacheSetup(self):
        self.l3assoc = self.cacheInfo['l3_cache']['associativity']
        self.l3sharedcores = self.cacheInfo['l3_cache']['sharedCores']
        self.noL3s = self.np/self.l3sharedcores

        #HACKY creating multiple MDP's per L3 in groups of 4-cores
        #DATE2017 Experiments
        #We are fooling the setup in thinking that there are 2 L3's each shared by 4 cores (instead of 1 L3 shared by 8-cores)
        if self.isBatchL3Mdp():
          assert(self.l3sharedcores <= 16)    #making sure it works for 8 shared cores
          self.l3sharedcores = 4 
          self.noL3s = self.np/self.l3sharedcores
          self.l3assoc = self.cacheInfo['l3_cache']['associativity']/self.noL3s

        print "batchL3MDP: ", self.isBatchL3Mdp(), self.l3sharedcores, self.noL3s, self.l3assoc
          
        if self.config['l3wayreconfig'] == True:
            self.l3stateinfo = []
            self.experiments['l3wayreconfig'] = []
            #supporting only dist MDP fo L3
            #l3wayCntrl = self.createController(self.config['l3wayNumStates'], self.config['l3wayNumActions'])
            for l3Num in range(self.noL3s):
                #dist mdp
                l3wayCntrl = self.createController(self.config['l3wayNumStates'], self.config['l3wayNumActions'])
                actionmod = __import__('util.cacheactionslib')
                cacheactions_class_ = getattr(actionmod, self.config['cacheactionsclass'])
                if self.isRWSOpt():
                  virtual_shared_cores = 3 #R,W,S are the 3 virtual cores competing for cache ways
                  #base core should be same for all L3
                  #no shared ways i.e -1
                  l3stateinfo = cacheactions_class_(self.l3assoc)
                else:
                  l3stateinfo = cacheactions_class_(l3Num, self.l3sharedcores, self.l3assoc, int(self.l3assoc*self.config['shared']))
                self.l3stateinfo.append(l3stateinfo)
                args = dict()
                args['l3way'] = l3Num 
                args['slen'] = self.StateVecLen      #stateVec is encoded to a number
                args['tlen'] = self.noL3s*self.StateVecLen 
                args['rlen'] = self.noL3s   #reward is per L3 
                args['actionfn'] = "getL3WayReconfigActions"
                args['actionfnarg'] = self.config['l3wayNumActions']
                args['statefn'] = self.getmetric.getL3wayReconfigMetrics
                args['rewardfn'] = self.getmetric.getL3wayReconfigReward
                self.experiments['l3wayreconfig'].append(self.createExperiment(l3wayCntrl, args))
                print "setupL3 : ", args
    
    def l2cacheSetup(self):
        self.l2assoc = self.cacheInfo['l2_cache']['associativity']
        self.l2sharedcores = self.cacheInfo['l2_cache']['sharedCores']
        self.noL2s = self.np/self.l2sharedcores

        if self.config['l2wayreconfig'] == True:
            self.l2stateinfo = []
            self.experiments['l2wayreconfig'] = []
            #supporting only dist MDP fo L2
            l2wayCntrl = self.createController(self.config['l2wayNumStates'], self.config['l2wayNumActions'])
            for l2Num in range(self.noL2s):
                #dist mdp
                l2wayCntrl = self.createController(self.config['l2wayNumStates'], self.config['l2wayNumActions'])
                l2stateinfo = cacheactions(l2Num, self.l2sharedcores, self.l2assoc, int(self.l2assoc*self.config['shared']))
                self.l2stateinfo.append(l2stateinfo)
                args = dict()
                args['l2way'] = l2Num 
                args['slen'] = self.StateVecLen      #stateVec is encoded to a number
                args['tlen'] = self.noL2s*self.StateVecLen 
                args['rlen'] = self.noL2s   #reward is per L2
                args['actionfn'] = "getL2WayReconfigActions"
                args['actionfnarg'] = self.config['l2wayNumActions']
                args['statefn'] = self.getmetric.getL2wayReconfigMetrics
                args['rewardfn'] = self.getmetric.getL2wayReconfigReward
                self.experiments['l2wayreconfig'].append(self.createExperiment(l2wayCntrl, args))

    def l1dcacheSetup(self):
        self.l1dassoc = self.cacheInfo['l1_dcache']['associativity']
        self.l1dsharedcores = self.cacheInfo['l1_dcache']['sharedCores']
        self.noL1Ds = self.np/self.l1dsharedcores

        if self.config['l1dwayreconfig'] == True:
            self.l1dstateinfo = []
            self.experiments['l1dwayreconfig'] = []
            #supporting only dist MDP fo L1D
            l1dwayCntrl = self.createController(self.config['l1dwayNumStates'], self.config['l1dwayNumActions'])
            for l1dNum in range(self.noL1Ds):
                #dist mdp
                l1dwayCntrl = self.createController(self.config['l1dwayNumStates'], self.config['l1dwayNumActions'])
                l1dstateinfo = cacheactions(l1dNum, self.l1dsharedcores, self.l1dassoc, int(self.l1dassoc*self.config['shared']))
                self.l1dstateinfo.append(l1dstateinfo)
                args = dict()
                args['l1dway'] = l1dNum 
                args['slen'] = self.StateVecLen      #stateVec is encoded to a number
                args['tlen'] = self.noL1Ds*self.StateVecLen 
                args['rlen'] = self.noL1Ds   #reward is per L1D
                args['actionfn'] = "getL1DWayReconfigActions"
                args['actionfnarg'] = self.config['l1dwayNumActions']
                args['statefn'] = self.getmetric.getL1DwayReconfigMetrics
                args['rewardfn'] = self.getmetric.getL1DwayReconfigReward
                self.experiments['l1dwayreconfig'].append(self.createExperiment(l1dwayCntrl, args))

    def l1icacheSetup(self):
        self.l1iassoc = self.cacheInfo['l1_icache']['associativity'] 
        self.l1isharedcores = self.cacheInfo['l1_icache']['sharedCores']
        self.noL1Is = self.np/self.l1isharedcores

        if self.config['l1iwayreconfig'] == True:
            self.l1istateinfo = []
            self.experiments['l1iwayreconfig'] = []
            #supporting only dist MDP fo L1I
            l1iwayCntrl = self.createController(self.config['l1iwayNumStates'], self.config['l1iwayNumActions'])
            for l1iNum in range(self.noL1Is):
                #dist mdp
                l1iwayCntrl = self.createController(self.config['l1iwayNumStates'], self.config['l1iwayNumActions'])
                l1istateinfo = cacheactions(l1iNum, self.l1isharedcores, self.l1iassoc, int(self.l1iassoc*self.config['shared']))
                self.l1istateinfo.append(l1istateinfo)
                args = dict()
                args['l1iway'] = l1iNum 
                args['slen'] = self.StateVecLen      #stateVec is encoded to a number
                args['tlen'] = self.noL1Is*self.StateVecLen 
                args['rlen'] = self.noL1Is   #reward is per L1I
                args['actionfn'] = "getL1IWayReconfigActions"
                args['actionfnarg'] = self.config['l1iwayNumActions']
                args['statefn'] = self.getmetric.getL1IwayReconfigMetrics
                args['rewardfn'] = self.getmetric.getL1IwayReconfigReward
                self.experiments['l1iwayreconfig'].append(self.createExperiment(l1iwayCntrl, args))

    def _oneStepAction(self, exp):
        exp.doInteractions(1)
        if self.OnlineRL:
          #learn every step
          exp.agent.learn()
        elif self.isNFQ() or (not self.OnlineRL): #Batch Mode
          LPTS=10
          #learn when enough data points collected
          if len(exp.agent.learner.dataset)%LPTS==0: 
            print "[Batch-Learn] calling learn with dataset:", len(exp.agent.learner.dataset)
            exp.agent.learn()
          if len(exp.agent.learner.dataset) >= 5*LPTS+1: 
            exp.agent.reset()

#        print "[MLM doOneStep] ", cntrl.params
        env = exp.task.env
#        print "[MLM doOneStep] getAction ", env.getAction()
        return env.getAction()

    def createController(self, nstates, nactions):
        """
        The controller in PyBrain is a module, that takes states as inputs and
        transforms them into actions. For value-based methods, like the
        Q-Learning algorithm we will use here, we need a module that implements
        the ActionValueInterface. There are currently two modules in PyBrain
        that do this: The ActionValueTable for discrete actions and the
        ActionValueNetwork for continuous actions. Our maze uses discrete
        actions, so we need a table:
        """

        controller = SharedActionValueTable(nstates, nactions)
        if False and self.isCacheStackOpt():
          initQ = []
          for i in range(nactions):
            ival = sum(self.cachestack._getAction(i))
            initQ.append(ival)
          for state in range(nstates):
            controller.initializeState(state, initQ)
        else:
          controller.initialize(1.)
        return controller

    def doOneStep(self):
        print '[MLM doOneStep] run exp for a step'
        if self.isCacheStackOpt():
          if self.isMultiAgent(): self.maOpt.findOptimalJointActions()  #find jointly optimal actions

          cachestackActions = self.coreOneStep()
#          print "cachestackActions: " , cachestackActions
          l3wayrequest, l2wayrequest, l1dwayrequest, coffsets = self.cachestack.getCacheStackAssignment(cachestackActions)
          #coffsets = [0]*self.np
          #uoffsets = [0]  #same freq on UnCore
          uoffsets = self.uncoreOneStep()   #uncore dvfs with JointMLM
          l1iwayrequest = [-1 for i in range(self.l1iassoc*self.noL1Is)]
        else:
          coffsets = self.coreOneStep()
          uoffsets = self.uncoreOneStep()
          l3wayrequest = self.l3OneStep()
          l2wayrequest = self.l2OneStep()
          l1dwayrequest = self.l1dOneStep()
          l1iwayrequest = self.l1iOneStep()

        return coffsets, uoffsets, l3wayrequest, l2wayrequest, l1dwayrequest, l1iwayrequest

    #Core
    def coreOneStep(self):
        coffsets = []
        for n in range(self.np):
            if self.config['coreDvfs'] == True:
                exp = self.experiments['coreDvfs'][n]
                coffsets.append(self._oneStepAction(exp))
            else:
                coffsets.append(0)
        return coffsets

    #UnCore
    def uncoreOneStep(self):
        if self.config['uncoreDvfs'] == True:
            uoffsets = [self._oneStepAction(self.experiments['uncoreDvfs'][0]), ]
        else:
            uoffsets = [0]  #same freq on UnCore
        return uoffsets

    #L3 Cache Reconfig
    def l3OneStep(self):
        l3wayrequest=[]
        for l3 in range(self.noL3s):
            if self.config['l3wayreconfig'] == True:
                l3wayAction = self._oneStepAction(self.experiments['l3wayreconfig'][l3])
                #map the action to actual L3 Way Assignment
                l3assign = self.l3stateinfo[l3].getCacheWayAssignment4Action(l3wayAction)
                print "[doOneStep]", l3, l3wayAction, l3wayrequest, l3assign
                l3wayrequest.extend(l3assign)
            else:
                l3wayrequest.extend([-1 for i in range(self.l3assoc*self.noL3s)])
        return l3wayrequest

    #L2 Cache Reconfig
    def l2OneStep(self):
        l2wayrequest=[]
        for l2 in range(self.noL2s):
            if self.config['l2wayreconfig'] == True:
                l2wayAction = self._oneStepAction(self.experiments['l2wayreconfig'][l2])
                #map the action to actual L2 Way Assignment
                l2assign = self.l2stateinfo[l2].getCacheWayAssignment4Action(l2wayAction)
#                print "[doOneStep]", l2, l2wayAction, l2wayrequest, l2assign
                l2wayrequest.extend(l2assign)
            else:
                l2wayrequest.extend([-1 for i in range(self.l2assoc*self.noL2s)])
        return l2wayrequest

    #L1D Cache Reconfig
    def l1dOneStep(self):
        l1dwayrequest=[]
        for l1d in range(self.noL1Ds):
            if self.config['l1dwayreconfig'] == True:
                l1dwayAction = self._oneStepAction(self.experiments['l1dwayreconfig'][l1d])
                #map the action to actual L1D Way Assignment
                l1dassign = self.l1dstateinfo[l1d].getCacheWayAssignment4Action(l1dwayAction)
#                print "[doOneStep]", l1d, l1dwayAction, l1dwayrequest, l1dassign
                l1dwayrequest.extend(l1dassign)
            else:
                l1dwayrequest.extend([-1 for i in range(self.l1dassoc*self.noL1Ds)])
        return l1dwayrequest

    #L1I Cache Reconfig
    def l1iOneStep(self):
        l1iwayrequest=[]
        for l1i in range(self.noL1Is):
            if self.config['l1iwayreconfig'] == True:
                l1iwayAction = self._oneStepAction(self.experiments['l1iwayreconfig'][l1i])
                #map the action to actual L1I Way Assignment
                l1iassign = self.l1istateinfo[l1i].getCacheWayAssignment4Action(l1iwayAction)
#                print "[doOneStep]", l1i, l1iwayAction, l1iwayrequest, l1iassign
                l1iwayrequest.extend(l1iassign)
            else:
                l1iwayrequest.extend([-1 for i in range(self.l1iassoc*self.noL1Is)])
        return l1iwayrequest

    #supports a single setup for multiple cores/componenets
    def createExperiment(self, controller, args):
        getmetricfn = args['statefn']
        rewardfn = args['rewardfn']
        """
        The Environment is the world, in which the agent acts. It receives input
        with the .performAction() method and returns an output with
        .getSensors(). All environments in PyBrain are located under
        pybrain/rl/environments.
        """
        environment = SystemEnv(getmetricfn, rewardfn, args)

        """
        Next, we need an agent. The agent is where the learning happens. It can
        interact with the environment with its .getAction() and
        .integrateObservation() methods.

        The agent itself consists of a controller, which maps states to actions,
        a learner, which updates the controller parameters according to the
        interaction it had with the world, and an explorer, which adds some
        explorative behaviour to the actions. All standard agents already have a
        default explorer, so we don't need to take care of that in this
        tutorial.

        The controller in PyBrain is a module, that takes states as inputs and
        transforms them into actions. For value-based methods, like the
        Q-Learning algorithm we will use here, we need a module that implements
        the ActionValueInterface. There are currently two modules in PyBrain
        that do this: The ActionValueTable for discrete actions and the
        ActionValueNetwork for continuous actions. Our maze uses discrete
        actions, so we need a table:
        """

        #NumStates = 3   #tpi is high, same, less than prev interval tpi
        #NumActions = 3  #freq to be inc,dec,same
        #controller = ActionValueTable(NumStates, NumActions)
        #controller.initialize(1.)
        
        #controller = ActionValueNetwork(1, NumActions)

        """
        The table needs the number of states and actions as parameters. The standard
        maze environment comes with the following 4 actions: north, south, east, west.

        Then, we initialize the table with 1 everywhere. This is not always necessary
        but will help converge faster, because unvisited state-action pairs have a
        promising positive value and will be preferred over visited ones that didn't
        lead to the goal.

        Each agent also has a learner component. Several classes of RL learners
        are currently implemented in PyBrain: black box optimizers, direct
        search methods, and value-based learners. The classical Reinforcement
        Learning mostly consists of value-based learning, in which of the most
        well-known algorithms is the Q-Learning algorithm. Let's now create
        the agent and give it the controller and learner as parameters.
        """

        if 'learner' in self.config: 
          learner_class = globals()[self.config['learner']]
          if self.config['learner'] in ['Q_LinFA', 'QLambda_LinFA', 'LSPI', 'GQLambda']:
            learner = learner_class(self.config['coreNumActions'], self.config['statelen'])
            agent = LinearFA_Agent(learner)
          elif self.isNFQ():
            learner = NFQ(maxEpochs=250)
            learner.gamma=0.75
            controller = ActionValueNetwork(dimState=self.config['statelen'], numActions=self.config['coreNumActions'])
            agent = LearningAgent(controller, learner)
          else:                                     
            learner = learner_class()
            agent = LearningAgent(controller, learner)
        else:
          print "no learner specified, using default"
          #alpha: importance of current reward
          #gamma: importance of expected future reward

          import random
          
          if 'alpha' in self.config:      #TODO HACK
            alp = self.config['alpha']
          else:
            alp = 0.5

          learner = Q(alpha=alp, gamma=0.75)   #change the controller as well

          #this assert may not be exhaustive and should be extended as and when other valid combinations are found
          assert( (type(learner) in [Q, SARSA] and type(controller) is SharedActionValueTable)
              or  (type(learner) is DoubleQ and type(controller) is DoubleSharedActionValueTable)
              )

          agent = LearningAgent(controller, learner)
        
        learner.batchMode = not self.OnlineRL
        eps = 0.5
        if self.isMultiAgent(): 
          eps = 0.0 #exploration is done in the joinOpt class
          learner.setMultiAgent(alpha2=self.getSmartUpdateFactor())

        learner.explorer = EpsilonGreedyExplorer(epsilon = eps, decay = 0.98)
        #learner.explorer = EpsilonGreedyExplorer()
        #learner.explorer = BoltzmannExplorer()
        #learner.explorer = DiscreteStateDependentExplorer()
        

        """
        So far, there is no connection between the agent and the environment. In fact,
        in PyBrain, there is a special component that connects environment and agent: the
        task. A task also specifies what the goal is in an environment and how the
        agent is rewarded for its actions. For episodic experiments, the Task also
        decides when an episode is over. Environments usually bring along their own
        tasks. The Maze environment for example has a MDPMazeTask, that we will use.
        MDP stands for "markov decision process" and means here, that the agent knows
        its exact location in the maze. The task receives the environment as parameter.
        """

        task = MDPSniperTask(environment)

        """
        Finally, in order to learn something, we create an experiment, tell it both
        task and agent (it knows the environment through the task) and let it run
        for some number of steps or infinitely, like here:
        """

        experiment = Experiment(task, agent)
        return experiment

    def run_experiment(self):
        n=10000
        exp = self.experiments['coreDvfs'][0]
        cntrl = exp.agent.module
        while n>0:
            exp.doInteractions(1)
            exp.agent.learn()
            exp.agent.reset()
            n = n-1
            print "[MLM iter] ", n, cntrl.params
        #    pylab.pcolor(controller.params.reshape(81,4).max(1).reshape(9,9))
        #    pylab.draw()


        """
        Above, the experiment executes 100 interactions between agent and
        environment, or, to be exact, between the agent and the task. The task
        will process the agent's actions, possibly scale it and hand it over to
        the environment. The environment responds, returns the new state back to
        the task which decides what information should be given to the agent.
        The task also gives a reward value for each step to the agent.

        After 100 steps, we call the agent's .learn() method and then reset it.
        This will make the agent forget the previously executed steps but of
        course it won't undo the changes it learned.

        Then the loop is repeated, until a desired behaviour is learned.

        In order to observe the learning progress, we visualize the controller
        with the last two code lines in the loop. The ActionValueTable consists
        of a scalar value for each state/action pair, in this case 81x4 values.
        A nice way to visualize learning is to only consider the maximum value
        over all actions for each state. This value is called the state-value V
        and is defined as V(s) = max_a Q(s, a).

        We plot the new table after learning and resetting the agent, INSIDE the
        while loop. Running this code, you should see the shape of the maze and
        a change of colors for the free fields. During learning, colors may jump
        and change back and forth, but eventually the learning should converge
        to the true state values, having higher scores (brigher fields) the
        closer they are to the goal.
        """

def setupWithSniper(filename, np, mlm_type, picklefile):
    getmetric = getmetricSniper(filename, np)
    mlm = mlm_rl(getmetric, np, mlm_type, picklefile)
    while not getmetric.mpi.isEnd():
        coffsets, uoffsets, l3wayAssign, l2wayAssign, l1dwayAssign, l1iwayAssign = mlm.doOneStep()
#        print '[setupWithSniper] coffsets, uoffsets, l3ways', coffsets, uoffsets, l3wayAssign, l2wayAssign, l1dwayAssign, l1iwayAssign
        getmetric.setCoreDvfsOffsets(coffsets)
        getmetric.setUnCoreDvfsOffsets(uoffsets)
        getmetric.setL3wayReconfigAssign(l3wayAssign)
        getmetric.setL2wayReconfigAssign(l2wayAssign)
        getmetric.setL1DwayReconfigAssign(l1dwayAssign)
        getmetric.setL1IwayReconfigAssign(l1iwayAssign)
        getmetric.incInterval()
        sys.stdout.flush()
#    mlm.printEnd()

#Testing stub
def getCacheInfoStub():
  cacheInfo = {}
  for cache in ['l1_icache', 'l1_dcache', 'l2_cache', 'l3_cache']:
    cacheInfo[cache] = {}
    for attrib in ['associativity', 'sharedCores',]:
      cacheInfo[cache][attrib]=4
  return cacheInfo


def _main():
    #this is the real flow with sniper
    #this script is run as a sub-process from sniper
    if len(sys.argv) >= 3:
        filename = sys.argv[1]
        np = int(sys.argv[2])
        mlm_type = int(sys.argv[3])
        picklefile = sys.argv[4]
        print filename, np, mlm_type, picklefile
        setupWithSniper(filename, np, mlm_type, picklefile)
    else:
        print 'TEST SETUP: Not a Sniper Setup'
        mlm = mlm_rl(getmetricrandom(), np=1, mdpIdx=10)
        mlm.run_experiment()

if __name__ == '__main__':
  _main()
