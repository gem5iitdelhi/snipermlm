import sys, time
from random import random, choice
from mpi_sniper import MpiSniperMdp
from mpi_cache import mpiCache
from stateAssign import stateAssign

MAXRETRYTIME=1.0
RETRYTIME=0.01
class getmetricSniper():
    def __init__(self, filename, np):
        self.mpi = MpiSniperMdp(filename, np)
        self.interval = 0
        self.mpicache = mpiCache()

    def incInterval(self):
        self.interval = self.interval + 1

    def _doloop(self, metric):
        if metric == None:
            return True
        elif self.mpi.isEnd():
            print "Ending MLM"
            sys.exit(0)
        else:
            return False

    #30-Sep-2015 adding component to specify the cache level, core, uncore etc
    def getMetrics(self, fn, comp_num, slen, tlen, component):    
#        metric = None
#        retry=RETRYTIME
#        while self._doloop(metric):
#            metric = getattr(self.mpi, fn)(self.interval, component, tlen) #read states for all comp
#            time.sleep(retry)
#            retry = retry*2
#            if retry > MAXRETRYTIME: retry = MAXRETRYTIME
#        print "[getMetrics] ", self.interval, fn, component, comp_num, slen, tlen, metric
        metric = self.__getDataVector(fn, slen, tlen, component)               
        return metric[slen*comp_num:slen*(comp_num+1)]  #extract the state for comp_num

    def getReward(self, fn, comp_num, rlen, component):
#        reward = None
#        retry=RETRYTIME
#        while self._doloop(reward):
#            time.sleep(retry)
#            reward = getattr(self.mpi, fn)(self.interval, component, rlen) 
#            retry = retry*2
#            if retry > MAXRETRYTIME: retry = MAXRETRYTIME
        reward = self.__getDataVector(fn, 1, rlen, component)               
        return reward[comp_num]

    def __getDataVector(self, fn, slen, tlen, component):
        cache_key = "_".join([fn, str(self.interval), str(slen), str(tlen), component])
        datavec = self.mpicache.get(cache_key)
                
        retry=RETRYTIME
        while self._doloop(datavec):
            datavec = getattr(self.mpi, fn)(self.interval, component, tlen) #read states for all comp
            if datavec == None:
              time.sleep(retry)
              retry = retry*2
              if retry > MAXRETRYTIME: retry = MAXRETRYTIME
            else: #add to cache
              self.mpicache.put(cache_key, datavec)
        return datavec

    #slightly inefficient since each core needs a file read to the same line
    #each core c has n metrics
    def getCoreDvfsMetrics(self, args):
        c = args['core']
        slen = args['slen']
        tlen = args['tlen']
        return self.getMetrics("readDvfsMetric", c, slen, tlen, self.mpi.Core)

    #slightly inefficient since each core needs a file read to the same line
    def getCoreDvfsReward(self, args):
        c = args['core']
        rlen = args['rlen']
        return self.getReward("readDvfsRewards", c, rlen, self.mpi.Core)

    def setCoreDvfsOffsets(self, offsets):
        self.mpi.writeDvfsOffsets(self.interval, self.mpi.Core, offsets)
        
    def getUnCoreDvfsMetrics(self, args):
        uc = args['uncore']
        slen = args['slen']
        tlen = args['tlen']
        return self.getMetrics("readDvfsMetric", uc, slen, tlen, self.mpi.Uncore)

    def getUnCoreDvfsReward(self, args):
        uc = args['uncore']
        rlen = args['rlen']
        return self.getReward("readDvfsRewards", uc, rlen, self.mpi.Uncore)

    def setUnCoreDvfsOffsets(self, offsets):
        self.mpi.writeDvfsOffsets(self.interval, self.mpi.Uncore, offsets)

    def getL3wayReconfigMetrics(self, args):
        nl3 = args['l3way']
        slen = args['slen']
        tlen = args['tlen']
        return self.getMetrics("readCacheWayReconfigMetric", nl3, slen, tlen, self.mpi.L3) 

    def getL3wayReconfigReward(self, args):
        l3w = args['l3way']
        rlen = args['rlen']
        return self.getReward("readCacheWayReconfigRewards", l3w, rlen, self.mpi.L3)

    def setL3wayReconfigAssign(self, assign):
        self.mpi.writeCacheWayReconfigAssign(self.interval, self.mpi.L3, assign)

    def getL2wayReconfigMetrics(self, args):
        nl2 = args['l2way']
        slen = args['slen']
        tlen = args['tlen']
        return self.getMetrics("readCacheWayReconfigMetric", nl2, slen, tlen, self.mpi.L2) 

    def getL2wayReconfigReward(self, args):
        l2w = args['l2way']
        rlen = args['rlen']
        return self.getReward("readCacheWayReconfigRewards", l2w, rlen, self.mpi.L2)

    def setL2wayReconfigAssign(self, assign):
        self.mpi.writeCacheWayReconfigAssign(self.interval, self.mpi.L2, assign)

    def getL1DwayReconfigMetrics(self, args):
        nl1d = args['l1dway']
        slen = args['slen']
        tlen = args['tlen']
        return self.getMetrics("readCacheWayReconfigMetric", nl1d, slen, tlen, self.mpi.L1D) 

    def getL1DwayReconfigReward(self, args):
        l1dw = args['l1dway']
        rlen = args['rlen']
        return self.getReward("readCacheWayReconfigRewards", l1dw, rlen, self.mpi.L1D)

    def setL1DwayReconfigAssign(self, assign):
        self.mpi.writeCacheWayReconfigAssign(self.interval, self.mpi.L1D, assign)

    def getL1IwayReconfigMetrics(self, args):
        nl1i = args['l1iway']
        slen = args['slen']
        tlen = args['tlen']
        return self.getMetrics("readCacheWayReconfigMetric", nl1i, slen, tlen, self.mpi.L1I) 

    def getL1IwayReconfigReward(self, args):
        l1iw = args['l1iway']
        rlen = args['rlen']
        return self.getReward("readCacheWayReconfigRewards", l1iw, rlen, self.mpi.L1I)

    def setL1IwayReconfigAssign(self, assign):
        self.mpi.writeCacheWayReconfigAssign(self.interval, self.mpi.L1I, assign)

class getmetricrandom():
    def getCoreDvfsMetrics(self, args):
        return [random(),]

    def getCoreDvfsReward(self, args):
        return random()

