__author__ = 'Rahul Jain'

from random import random, choice
from scipy import zeros, array
import sys

from pybrain.utilities import Named
from pybrain.rl.environments.environment import Environment

class SystemReconfigActions():
    def getCoreDvfsActions(self, arg=None):
        return [1,0,-1] #downscale freq, same freq, upscale freq
   
    def getCoreDvfsFreqIndex(self, arg=None):
        return range(0,5)   #5 frequencies are currently being supported, check dvfsmetrics.py
    
    def getUnCoreDvfsFreqIndex(self, arg=None):
        return range(0,5)   #5 frequencies are currently being supported, check dvfsmetrics.py

    def getUnCoreDvfsActions(self, arg=None):
        return [1,0,-1] #downscale freq, same freq, upscale freq

    def getL3WayReconfigActions(self, arg):
        return range(arg)  
    
    def getL2WayReconfigActions(self, arg):
        return range(arg)  
    
    def getL1DWayReconfigActions(self, arg):
        return range(arg)  

    def getL1IWayReconfigActions(self, arg):
        return range(arg)  
    
    def getCacheStackActions(self, arg):
        return range(arg)

class SystemEnv(Environment):
    """ 2D mazes, with actions being the direction of movement (N,E,S,W)
    and observations being the presence of walls in those directions.

    It has a finite number of states, a subset of which are potential starting states (default: all except goal states).
    A maze can have absorbing states, which, when reached end the episode (default: there is one, the goal).

    There is a single agent walking around in the maze (Theseus).
    The movement can succeed or not, or be stochastically determined.
    Running against a wall does not get you anywhere.

    Every state can have an an associated reward (default: 1 on goal, 0 elsewhere).
    The observations can be noisy.
    """
    sysActions = SystemReconfigActions()

    # stochasticity
    stochAction = 0.
    stochObs = 0.

    def __init__(self, statefn, rewardfn, args):
        self.statefn = statefn
        self.rewardfn = rewardfn
        self.args = args

        self.allActions = getattr(self.sysActions, args['actionfn'])(args['actionfnarg'])
        #current decision
        self.currAction = 0

    def reset(self):
        assert(0)
        pass

    def performAction(self, action):
        ''' This returns the action to be performed on the sniper env '''
        if self.stochAction > 0:
            if random() < self.stochAction:
                action = choice(range(len(self.allActions)))
#        print "[SystemEnv] performAction action", action, self.allActions[action]
        self.currAction = self.allActions[action] #return the dvfs decision

    #this is the function called by sniper script to get the decision of MDP
    def getAction(self):
        return self.currAction

    def getSensors(self):
        '''
            This returns the observation or the change in goal (tpi/amat)
            after the action has been performed on the sniper env
        '''
        obs = self.statefn(self.args)
#        print "[SystemEnv] getSensors obs= ", self.args, obs
        return array(obs)

    def getReward(self):
        return self.rewardfn(self.args)

    def __str__(self):
        s = 'TOIMPL' + str(random())
        return s


