'''
sniper uses a very restricted version of python2.7 (SNIPER/python_kit)
This does not allow to import any external libraries like scipy, numpy etc
so this is a very basic message passing interface between sniper and mdp
via a file
'''
import sys, os, stat
import time, datetime
from mpi_db import MpiStore 

def reportCrash():
    import traceback
    exc_type, exc_value, exc_traceback = sys.exc_info()
    print "*** print_tb:"
    traceback.print_tb(exc_traceback, limit=1, file=sys.stdout)
    print "*** print_exception:"
    traceback.print_exception(exc_type, exc_value, exc_traceback,
                             limit=15, file=sys.stdout)
    sys.exit()

class MpiSniperMdp():
    def __init__(self, filename, np, create_db=False):
        self.fname = filename
        self.np = np
        self.fpos = 0
        self.init()
        self.exitCnt = 0  #__doexit takes too much time cummulatively
        print 'MpiSniperMdp created'
        
        self.mpidb = MpiStore(self.fname + '.db', create_db)

    def init(self):
        self.L1I='L1I'
        self.L1D='L1D'
        self.L2='L2'
        self.L3='L3'
        self.CacheLevels = [self.L1I, self.L1D, self.L2, self.L3]
        
        self.Core='C'
        self.Uncore='U'
        self.DvfsComps = [self.Core, self.Uncore]

        self.ANNDP='ANNDP'

    def __writeMsg(self, interval, id, csv):
#        fout = open(self.fname, 'a')
#        self.fpos = fout.tell()
#        fout.write("%d,%s,%s\n" % (interval, id, csv))
#        fout.close()
#        print "__writeMsg ", interval, id, csv
        self.mpidb.writeDb(interval, id, csv)
    
    def __doexit(self):
        #to reduce the call to heavy os.stat call
        self.exitCnt += 1
        if self.exitCnt < 1000: return False
        self.exitCnt = 0  #reset

        age_sec = time.time() - os.stat(self.fname+'.db')[stat.ST_MTIME]
        if age_sec > 10*60: #10 mins, if no writes for very long
            print "[ERROR] no writes to %s for last %s seconds, so exiting the run" %(self.fname, age_sec)
            return True
        return False

    def __readMsg(self, interval, id, n=1, type_cast=int):
      return self.__readMsgDb(interval, id, n, type_cast)

    def __readMsgFile(self, interval, id, n=1, type_cast=int):
        fout = open(self.fname, 'r')
#        fout.seek(self.fpos)
        values = None
        try:
            for line in fout.readlines():
                l=line.strip().split(',')
                if int(l[0]) == interval and l[1] == id:
                    values = [type_cast(float(l[i])) for i in range(2, 2+n)]
                    self.fpos = fout.tell() #since a valid data found, the END tag can only be past this
                elif l[1] == 'END' or self.__doexit(): #force exit the application on finding the END marker
                    fout.close()
                    sys.exit(0)
        except:
            print "Issue with __readMsg ", interval, id, n
            reportCrash()

        fout.close()
    
    def __readMsgDb(self, interval, id, n, type_cast):
        try:
          csv = self.mpidb.readDb(interval, id)
          if csv == None: 
            if self.mpidb.isEnd() or self.__doexit(): #force exit the application on finding the END marker
              sys.exit(0)
            return None
          l=str(csv).strip().split(',')
          csv = [type_cast(float(l[i])) for i in range(0, n)]
        except:
            print "Issue with __readMsg ", interval, id, n
            reportCrash()
        
#        if values!=csv:
#          print "__readMsg ", values, csv, interval, id, n
#          assert(0)
#        return values
#        print "__readMsg ", csv, interval, id, n
        return csv
    
    #Special Markers
    def markEnd(self):
        self.__writeMsg(-1, 'END', ','.join([str(0) for v in range(100)]))
        self.mpidb.markEnd()

    def isEnd(self):
        fout = open(self.fname, 'r')
        fout.seek(self.fpos)
        try:
            for line in fout.readlines():
                l=line.strip().split(',')
                if l[1] == 'END': #force exit the application on finding the END marker
                    fout.close()
                    return True
        except:
            print "Issue with isEnd ", self.fpos
            reportCrash()

        fout.close()
        return False
    
    #Core/Uncore DVFS
    def dvfsAssert(self, comp):
        if comp not in self.DvfsComps:
            print "ERROR (pybrain::mpi_sniper::dvfsAssert): --------------Wrong DVFS Component specified----------------"
            assert(0)

    def writeDvfsMetric(self, interval, comp, values):
        self.dvfsAssert(comp)
        self.__writeMsg(interval, comp+'S', ','.join([str(v) for v in values]))
    
    def readDvfsMetric(self, interval, comp, np):
        self.dvfsAssert(comp)
        return self.__readMsg(interval, comp+'S', np)

    def writeDvfsRewards(self, interval, comp, values):
        self.dvfsAssert(comp)
        self.__writeMsg(interval, comp+'R', ','.join([str(v) for v in values]))
    
    def readDvfsRewards(self, interval, comp, np):
        self.dvfsAssert(comp)
        return self.__readMsg(interval, comp+'R', np, float)

    def writeDvfsOffsets(self, interval, comp, values):
        self.dvfsAssert(comp)
        self.__writeMsg(interval, comp+'O', ','.join([str(v) for v in values]))
    
    def readDvfsOffsets(self, interval, comp, np):
        self.dvfsAssert(comp)
        return self.__readMsg(interval, comp+'O', np)
    
    #Cache Way Reconfig
    #cache param can be L3,L2,L1D,L1I
    def cacheAssert(self, cache):
        if cache not in self.CacheLevels:
            print "ERROR (pybrain::mpi_sniper::cacheAssert): --------------Wrong Cache specified----------------"
            assert(0)

    def writeCacheWayReconfigMetric(self, interval, cache, values):
        self.cacheAssert(cache)
        self.__writeMsg(interval, cache+'S', ','.join([str(v) for v in values]))
    
    def readCacheWayReconfigMetric(self, interval, cache, slen):
        self.cacheAssert(cache)
        return self.__readMsg(interval, cache+'S', slen)

    def writeCacheWayReconfigRewards(self, interval, cache, values):
        self.cacheAssert(cache)
        self.__writeMsg(interval, cache+'R', ','.join([str(v) for v in values]))
    
    def readCacheWayReconfigRewards(self, interval, cache, l3):
        self.cacheAssert(cache)
        return self.__readMsg(interval, cache+'R', l3, float)

    def writeCacheWayReconfigAssign(self, interval, cache, values):
        self.cacheAssert(cache)
        self.__writeMsg(interval, cache+'A', ','.join([str(v) for v in values]))
    
    def readCacheWayReconfigAssign(self, interval, cache, assoc):
        self.cacheAssert(cache)
        return self.__readMsg(interval, cache+'A', assoc)
    
    def writeANNDataPoint(self, interval, values):
        self.__writeMsg(interval, self.ANNDP, ','.join([str(v) for v in values]))
    
    def readANNDataPoint(self, interval, compName, np_inDim):
        assert(compName == self.ANNDP)
        return self.__readMsg(interval, compName, np_inDim, float)
    
