from random import random
from pybrain.optimization import HillClimber
from pybrain.structure.evolvables.evolvable import Evolvable
class SimpleEvo(Evolvable):
  def __init__(self, x): self.x = max(0, min(x, 10))
  def mutate(self):      self.x = max(0, min(self.x + random() - 0.3, 10))
  def copy(self):        return SimpleEvo(self.x)
  def randomize(self):   print "randmize called"; self.x = 10*random()
  def __repr__(self):    return '<-%.2f->'+str(self.x)

def fn(x):
  return x[0].x+x[1].x

x0 = SimpleEvo(1.2)
x1 = SimpleEvo(3.2)

l = HillClimber(fn, [x0, x1], maxEvaluations = 50)
#l = HillClimber(lambda x: x.x, x0, maxEvaluations = 50)
print l.learn()
