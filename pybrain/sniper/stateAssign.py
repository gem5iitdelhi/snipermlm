
class stateAssign:
    def __init__(self, maxStates):
        self._states = dict()
        self._stateNum = maxStates-1

    def __del__(self):
        print "[stateAssign] _stateNum = ", self._stateNum
        print self._states

    def getState(self, stateVec):
        stateKey = "_".join([str(s) for s in stateVec])
        if self._states.get(stateKey) == None:  #does not exist
            self._states[stateKey] = self._stateNum
            self._stateNum -= 1
            if self._stateNum < 0:
                print "[stateAssign] states=", self._stateNum, self._states
                assert(self._stateNum >= -1)    #when the last state is assigned a num, then this would be -1
        return [self._states[stateKey],]

    #creates all possible states of stateVecLength consisting of stateVecElems
    def createAllStates(self, stateVecLength, stateVecElems):
      self.__createAllStates(stateVecLength, stateVecElems, [])

    def __createAllStates(self, stateVecLength, stateVecElems, startVec=[]):
      if stateVecLength == 0: #terminal case
        self.getState(startVec)
      else:
        for e in stateVecElems:
          newSV = startVec + [e,]
          self.__createAllStates(stateVecLength-1, stateVecElems, newSV)

if __name__ == '__main__':
  sa = stateAssign(9)
  sa.createAllStates(2, [0,1,2])
  del sa
