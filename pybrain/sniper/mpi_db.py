import sys, os, sqlite3

class MpiStore:
    def __init__(self, filename, create_db ):
        self.commonInit()
        self.mpidb = filename
        if create_db:
          self.createTables()
    
    def createTables(self): #only sniper should call this
       self.createMpiTable()

    def commonInit(self):
        self.mpiTable = 'mpi_sniper'
        self.INTV = 'interval'
        self.ID = 'id'
        self.csv = 'csv'
        self.mpi_fields = [self.INTV, self.ID, self.csv]

    def createMpiTable(self):
        conn = sqlite3.connect(self.mpidb, timeout=30.0)
#        print "Opened database successfully";
        create_table = 'create table ' + self.mpiTable
        fields = ' ( ' + self.INTV + ' integer, ' +\
                         self.ID + ' string, ' +\
                         self.csv + ' string ' +\
                 ' )'
        create_table += fields
        print create_table

        conn.execute(create_table)
#        print "Table created successfully";
        conn.close()

    def writeDb(self, interval, id, csv):
        #csv = csv.replace(',', ':')
        conn = sqlite3.connect(self.mpidb, timeout=30.0)
        insert_query = 'insert into ' + self.mpiTable 
        fields = ' ( ' + self.INTV +',' + self.ID + ',' + self.csv + ') '
        values = ' values(' + str(interval) + ',"' + id +'","' + csv + '")'
        insert_query += fields + values
        #print "writeDB ", insert_query
        conn.execute(insert_query)
        conn.commit()
        conn.close()

    def readDb(self, interval, id):
        conn = sqlite3.connect(self.mpidb, timeout=30.0)
        fields = self.INTV +',' + self.ID + ',' + self.csv
        query = 'select ' + fields  + ' from ' + self.mpiTable +\
                ' where ' + self.INTV + ' = ' + str(interval) + ' and ' + self.ID + '= "' + id +'"'
        #print "readDb ", query
        cursor = conn.execute(query)
        #print cursor
        row = cursor.fetchone()
        if row == None: return None

        csv = row[2]
        #print "readDb ", interval, id, csv
        conn.close()
        #csv = csv.replace(':', ',')
        return csv

    def markEnd(self):
        self.writeDb(-1000, 'END', 'END')

    def isEnd(self):
        csv = self.readDb(-1000, 'END')
        if csv == 'END': return True
        return False

    def printTable(self):
        conn = sqlite3.connect(self.mpidb, timeout=30.0)
        query = 'select * from ' + self.mpiTable
        cursor = conn.execute(query)
        for r in cursor:
            print r
        conn.close()

if __name__ == '__main__':
  mpidb = MpiStore(sys.argv[1], False)
  mpidb.printTable()
