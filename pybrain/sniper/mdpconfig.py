'''
this is the configuration for various mdp experiments
'''
import copy

#takes variable numbers of dict in arg and combine them into single dict
def create_config(*arg):
    config = {'coreDvfs':False, 'uncoreDvfs':False, 'l3wayreconfig':False, 'l2wayreconfig':False, 'l1dwayreconfig':False, 'l1iwayreconfig':False,}
    for a in arg:
        config.update(a)
    return config

core_options  = {
    'no' : {'coreDvfs':False,'coreNumStates':0, 'coreNumActions':0},
    'juan' : {'coremdp': 'juan', 'corereward': 'epi', 'coreDvfs':True, 'coreDvfsCentral':False, 'coreNumStates':24, 'coreNumActions':3, 
       },
    'tpiedp': {'coremdp': 'tpi', 'corereward': 'edp', 'coreDvfs':True, 'coreDvfsCentral':False, 'coreNumStates':3, 'coreNumActions':3, 
       },
    'tpied2p': {'coremdp': 'tpi', 'corereward': 'ed2p', 'coreDvfs':True, 'coreDvfsCentral':False, 'coreNumStates':3, 'coreNumActions':3,
       },
    'tpitpi': {'coremdp': 'tpi', 'corereward': 'tpi', 'coreDvfs':True, 'coreDvfsCentral':False, 'coreNumStates':3, 'coreNumActions':3, 
       },
    'tpibptpi': {'coremdp': 'tpibp', 'corereward': 'tpi', 'coreDvfs':True, 'coreDvfsCentral':False, 'coreNumStates':15, 'coreNumActions':3, 
       },
    'tpibpepi': {'coremdp': 'tpibp', 'corereward': 'epi', 'coreDvfs':True, 'coreDvfsCentral':False, 'coreNumStates':15, 'coreNumActions':3, 
       },
    'tpil1epi': {'coremdp': 'tpil1', 'corereward': 'epi', 'coreDvfs':True, 'coreDvfsCentral':False, 'coreNumStates':15, 'coreNumActions':3, 
       },
    'tpil2epi': {'coremdp': 'tpil2', 'corereward': 'epi', 'coreDvfs':True, 'coreDvfsCentral':False, 'coreNumStates':15, 'coreNumActions':3, 
       },
    'tpil3epi': {'coremdp': 'tpil3', 'corereward': 'epi', 'coreDvfs':True, 'coreDvfsCentral':False, 'coreNumStates':15, 'coreNumActions':3, 
       },
    'tpi9tpi': {'coremdp': 'tpi9', 'corereward': 'tpi', 'coreDvfs':True, 'coreDvfsCentral':False, 'coreNumStates':9, 'coreNumActions':3, 
       },
    'tpiq10epi': {'coremdp': 'tpiq10', 'corereward': 'epi', 'coreDvfs':True, 'coreDvfsCentral':False, 'coreNumStates':10, 'coreNumActions':3, 
       },
    'tpiq5trepi': {'coremdp': 'tpiq5tr', 'corereward': 'epi', 'coreDvfs':True, 'coreDvfsCentral':False, 'coreNumStates':25, 'coreNumActions':3, 
       },
    'epitpi': {'coremdp': 'epi', 'corereward': 'tpi', 'coreDvfs':True, 'coreDvfsCentral':False, 'coreNumStates':3, 'coreNumActions':3, 
       },
    'edpedp': {'coremdp': 'edp', 'corereward': 'edp', 'coreDvfs':True, 'coreDvfsCentral':False, 'coreNumStates':3, 'coreNumActions':3, 
       },
    'epiedp': {'coremdp': 'epi', 'corereward': 'edp', 'coreDvfs':True, 'coreDvfsCentral':False, 'coreNumStates':3, 'coreNumActions':3, 
       },
    'epied2p': {'coremdp': 'epi', 'corereward': 'ed2p', 'coreDvfs':True, 'coreDvfsCentral':False, 'coreNumStates':3, 'coreNumActions':3, 
       },
    'tpiepi': {'coremdp': 'tpi', 'corereward': 'epi', 'coreDvfs':True, 'coreDvfsCentral':False, 'coreNumStates':3, 'coreNumActions':3, 
       },
    'coretpitpi': {'coremdp': 'coretpi', 'corereward': 'tpi', 'coreDvfs':True, 'coreDvfsCentral':False, 'coreNumStates':7, 'coreNumActions':5, 
       },
    'coretpiedp': {'coremdp': 'coretpi', 'corereward': 'edp', 'coreDvfs':True, 'coreDvfsCentral':False, 'coreNumStates':7, 'coreNumActions':5, 
       },
    'coretpicoretpi': {'coremdp': 'coretpi', 'corereward': 'coretpi', 'coreDvfs':True, 'coreDvfsCentral':False, 'coreNumStates':7, 'coreNumActions':5, 
       },
    #Cooperative MLM: L3 actions are passed to core MDP
    'tpibpl3tpi': {'coremdp': 'tpibpl3', 'corereward': 'tpi', 'coreDvfs':True, 'coreDvfsCentral':False, 'coreNumStates':27, 'coreNumActions':3, 
       },
    'tpibpl3epi': {'coremdp': 'tpibpl3', 'corereward': 'csepitpi', 'coreDvfs':True, 'coreDvfsCentral':False, 'coreNumStates':27, 'coreNumActions':3, 
#TODO    'tpibpl3epi': {'coremdp': 'tpibpl3', 'corereward': 'coreepitpi', 'coreDvfs':True, 'coreDvfsCentral':False, 'coreNumStates':27, 'coreNumActions':3, 
       },
    #CacheStack Optimization, a MDP per core is created
    #TODO HACK: coreDvfs set to true to indicate this opt to be applied
    'csmkpiinstr': {'coremdp': 'cachestackmpki', 'corereward': 'csinstr', 'coreDvfs':True, 'coreDvfsCentral':False, 'coreNumStates':9, 'coreNumActions':11, 'actionfn':'getCacheStackActions', 'cachestack':True, 'multiagent':False,
       },
    'csmkpiireward': {'coremdp': 'cachestackmpki', 'corereward': 'csireward', 'coreDvfs':True, 'coreDvfsCentral':False, 'coreNumStates':27, 'coreNumActions':7, 'actionfn':'getCacheStackActions', 'cachestack':True,
       },
    'csmkpimpki': {'coremdp': 'cachestackmpki', 'corereward': 'csmpkireward', 'coreDvfs':True, 'coreDvfsCentral':False, 'coreNumStates':27, 'coreNumActions':7, 'actionfn':'getCacheStackActions' , 'cachestack':True,
       },
    'cstpimpki': {'coremdp': 'tpi10', 'corereward': 'csmpkireward', 'coreDvfs':True, 'coreDvfsCentral':False, 'coreNumStates':10, 'coreNumActions':9, 'actionfn':'getCacheStackActions' , 'cachestack':True,
       },
    'cstpiinstr': {'coremdp': 'tpi16', 'corereward': 'csinstr', 'coreDvfs':True, 'coreDvfsCentral':False, 'coreNumStates':16, 'coreNumActions':11, 'actionfn':'getCacheStackActions', 'actioncls':'csActions11', 'cachestack':True, 'multiagent':False,
       },
    'cstpi10ipc': {'coremdp': 'tpi10', 'corereward': 'csinstr', 'coreDvfs':True, 'coreDvfsCentral':False, 'coreNumStates':10, 'coreNumActions':11, 'actionfn':'getCacheStackActions', 'actioncls':'csActions11', 'cachestack':True, 'multiagent':False,
       },
    'cstpil3miss': {'coremdp': 'tpi16', 'corereward': 'csl3miss', 'coreDvfs':True, 'coreDvfsCentral':False, 'coreNumStates':16, 'coreNumActions':11, 'actionfn':'getCacheStackActions', 'actioncls':'csActions11', 'cachestack':True, 'multiagent':False,
       },
    'cstpil2miss': {'coremdp': 'tpi16', 'corereward': 'csl2miss', 'coreDvfs':True, 'coreDvfsCentral':False, 'coreNumStates':16, 'coreNumActions':11, 'actionfn':'getCacheStackActions', 'actioncls':'csActions11', 'cachestack':True, 'multiagent':False,
       },
    'cstpiepi': {'coremdp': 'tpi16', 'corereward': 'csepi', 'coreDvfs':True, 'coreDvfsCentral':False, 'coreNumStates':16, 'coreNumActions':11, 'actionfn':'getCacheStackActions', 'actioncls':'csActions11', 'cachestack':True, 'multiagent':False,
       },
    #Coordinated MLM: Core-DVFS+LLC+NoC
    'mlmtpiepi': {'coremdp': 'tpi16', 'corereward': 'sysepitpi', 'coreDvfs':True, 'coreDvfsCentral':False, 'coreNumStates':16, 'coreNumActions':9, 'actionfn':'getCacheStackActions', 'actioncls':'csActionsDvfs', 'cachestack':True, 'multiagent':False,
       },
    'mlmtpitpi': {'coremdp': 'tpi16', 'corereward': 'tpi', 'coreDvfs':True, 'coreDvfsCentral':False, 'coreNumStates':16, 'coreNumActions':9, 'actionfn':'getCacheStackActions', 'actioncls':'csActionsDvfs', 'cachestack':True, 'multiagent':False,
       },
    'mlmtpiedp': {'coremdp': 'tpi16', 'corereward': 'edp', 'coreDvfs':True, 'coreDvfsCentral':False, 'coreNumStates':16, 'coreNumActions':9, 'actionfn':'getCacheStackActions', 'actioncls':'csActionsDvfs', 'cachestack':True, 'multiagent':False,
       },
    'mlmtpi10epi': {'coremdp': 'tpi10', 'corereward': 'sysepitpi', 'coreDvfs':True, 'coreDvfsCentral':False, 'coreNumStates':10, 'coreNumActions':9, 'actionfn':'getCacheStackActions', 'actioncls':'csActionsDvfs', 'cachestack':True, 'multiagent':False,
       },
    'mlmtpitrepi': {'coremdp': 'tpiq5tr', 'corereward': 'sysepitpi', 'coreDvfs':True, 'coreDvfsCentral':False, 'coreNumStates':25, 'coreNumActions':9, 'actionfn':'getCacheStackActions', 'actioncls':'csActionsDvfs', 'cachestack':True, 'multiagent':False,
       },
    'mlmtpitred2p': {'coremdp': 'tpiq5tr', 'corereward': 'sysed2p', 'coreDvfs':True, 'coreDvfsCentral':False, 'coreNumStates':25, 'coreNumActions':9, 'actionfn':'getCacheStackActions', 'actioncls':'csActionsDvfs', 'cachestack':True, 'multiagent':False,
       },
    'mlmtpifepi': {'coremdp': 'tpif', 'corereward': 'sysepitpi', 'coreDvfs':True, 'coreDvfsCentral':False, 'coreNumStates':25, 'coreNumActions':9, 'actionfn':'getCacheStackActions', 'actioncls':'csActionsDvfs', 'cachestack':True, 'multiagent':False,
       },
    'mlmtpifedp': {'coremdp': 'tpif', 'corereward': 'sysedp', 'coreDvfs':True, 'coreDvfsCentral':False, 'coreNumStates':25, 'coreNumActions':9, 'actionfn':'getCacheStackActions', 'actioncls':'csActionsDvfs', 'cachestack':True, 'multiagent':False,
       },
    'mlmtpifed2p': {'coremdp': 'tpif', 'corereward': 'sysed2p', 'coreDvfs':True, 'coreDvfsCentral':False, 'coreNumStates':25, 'coreNumActions':9, 'actionfn':'getCacheStackActions', 'actioncls':'csActionsDvfs', 'cachestack':True, 'multiagent':False,
       },
    'mlmtpifnocepi': {'coremdp': 'tpifnoc', 'corereward': 'sysepitpi', 'coreDvfs':True, 'coreDvfsCentral':False, 'coreNumStates':75, 'coreNumActions':9, 'actionfn':'getCacheStackActions', 'actioncls':'csActionsDvfs', 'cachestack':True, 'multiagent':False,
       },
#    'mlmtpifedp': {'coremdp': 'tpif', 'corereward': 'sysedp', 'coreDvfs':True, 'coreDvfsCentral':False, 'coreNumStates':25, 'coreNumActions':9, 'actionfn':'getCacheStackActions', 'actioncls':'csActionsDvfs', 'cachestack':True, 'multiagent':False,
#       },
    #Approximate Q-Learning: Feature based, state is now a vector of features
    'csfeatureinstr': {'coremdp': 'csfeatures', 'corereward': 'csinstr', 'coreDvfs':True, 'coreDvfsCentral':False, 'coreNumStates':10, 'coreNumActions':9, 'actionfn':'getCacheStackActions', 'cachestack':True, 'statelen':7, 'learner':'Q_LinFA',
       },
    'csfeatureNFQ': {'coremdp': 'csfeatures', 'corereward': 'csinstr', 'coreDvfs':True, 'coreDvfsCentral':False, 'coreNumStates':10, 'coreNumActions':9, 'actionfn':'getCacheStackActions', 'cachestack':True, 'statelen':7, 'learner':'NFQ',
       },
}

uncore_options = {
    'no' : {'uncoreDvfs':False, 'uncoreNumStates':0, 'uncoreNumActions':0 },
    'nw' : {'uncoreDvfs':True, 'uncoremdp': 'nw', 'uncorereward': 'tpi', 'uncoreNumStates':3, 'uncoreNumActions':3 },
    'tpi' : {'uncoreDvfs':True, 'uncoremdp': 'tpi', 'uncorereward': 'tpi', 'uncoreNumStates':7, 'uncoreNumActions':5 },
}

l3cache_options = {
    'no' : {'l3wayreconfig':False, 'l3wayNumStates':0, 'l3wayNumActions':0,},
    #with tpi2, 2 states per core i.e total 2^4 states
    #with tpi3, 81 states
    'tpitpi' : {'l3wayreconfig':True, 'l3waymdp': 'tpi2', 'l3wayreward': 'tpi', 'l3wayNumStates':16, 'l3wayNumActions':19, 'shared':0.25, 'l3waystep':0.125, 'cacheactionsclass':'cacheactions'},
    'tpitpi81' : {'l3wayreconfig':True, 'l3waymdp': 'tpi3', 'l3wayreward': 'tpi', 'l3wayNumStates':81, 'l3wayNumActions':19, 'shared':0.25, 'l3waystep':0.125, 'cacheactionsclass':'cacheactions'},
    'tpiedp' : {'l3wayreconfig':True, 'l3waymdp': 'tpi2', 'l3wayreward': 'edp', 'l3wayNumStates':16, 'l3wayNumActions':19, 'shared':0.25, 'l3waystep':0.125, 'cacheactionsclass':'cacheactions'},
    'tpiepi' : {'l3wayreconfig':True, 'l3waymdp': 'tpi3', 'l3wayreward': 'epi', 'l3wayNumStates':81, 'l3wayNumActions':19, 'shared':0.25, 'l3waystep':0.125, 'cacheactionsclass':'cacheactions'},
    'tpicsepi' : {'l3wayreconfig':True, 'l3waymdp': 'tpi3', 'l3wayreward': 'csepitpi', 'l3wayNumStates':81, 'l3wayNumActions':19, 'shared':0.25, 'l3waystep':0.125, 'cacheactionsclass':'cacheactions'},
    'tpiinstr' : {'l3wayreconfig':True, 'l3waymdp': 'tpi3', 'l3wayreward': 'instr', 'l3wayNumStates':81, 'l3wayNumActions':19, 'shared':0.25, 'l3waystep':0.125, 'cacheactionsclass':'cacheactions'},
    'tpiuncore' : {'l3wayreconfig':True, 'l3waymdp': 'tpi2', 'l3wayreward': 'uncorereq', 'l3wayNumStates':16, 'l3wayNumActions':19, 'shared':0.25, 'l3waystep':0.125, 'cacheactionsclass':'cacheactions'},
    'l1mpkiuncore' : {'l3wayreconfig':True, 'l3waymdp': 'l1mpki3', 'l3wayreward': 'uncorereq', 'l3wayNumStates':81, 'l3wayNumActions':19, 'shared':0.25, 'l3waystep':0.125, 'cacheactionsclass':'cacheactions'},
    'l1mpkitpi' : {'l3wayreconfig':True, 'l3waymdp': 'l1mpki3', 'l3wayreward': 'tpi', 'l3wayNumStates':81, 'l3wayNumActions':19, 'shared':0.25, 'l3waystep':0.125, 'cacheactionsclass':'cacheactions'},
    'l1mpkiepi' : {'l3wayreconfig':True, 'l3waymdp': 'l1mpki3', 'l3wayreward': 'epi', 'l3wayNumStates':81, 'l3wayNumActions':19, 'shared':0.25, 'l3waystep':0.125, 'cacheactionsclass':'cacheactions'},
    'l1mpkihinstr' : {'l3wayreconfig':True, 'l3waymdp': 'l1mpki3', 'l3wayreward': 'hinstr', 'l3wayNumStates':81, 'l3wayNumActions':19, 'shared':0.25, 'l3waystep':0.125, 'cacheactionsclass':'cacheactions'},
    'l3mpkil1miss' : {'l3wayreconfig':True, 'l3waymdp': 'l3mpki3', 'l3wayreward': 'l1miss', 'l3wayNumStates':81, 'l3wayNumActions':19, 'shared':0.25, 'l3waystep':0.125, 'cacheactionsclass':'cacheactions'},
    'amatepi' : {'l3wayreconfig':True, 'l3waymdp': 'amat2', 'l3wayreward': 'epi', 'l3wayNumStates':16, 'l3wayNumActions':19, 'shared':0.25, 'l3waystep':0.125, 'cacheactionsclass':'cacheactions'},
    'rwpinstr' : {'l3wayreconfig':True, 'l3waymdp': 'tpi2', 'l3wayreward': 'instr', 'l3wayNumStates':16, 'l3wayNumActions':56, 'shared':0.0625, 'l3waystep':0.125, 'cacheactionsclass':'rwpcacheactions'},
    'rwpuncore' : {'l3wayreconfig':True, 'l3waymdp': 'tpi2', 'l3wayreward': 'uncorereq', 'l3wayNumStates':16, 'l3wayNumActions':56, 'shared':0.0625, 'l3waystep':0.125, 'cacheactionsclass':'rwpcacheactions'},
    'rwpl3miss' : {'l3wayreconfig':True, 'l3waymdp': 'tpi2', 'l3wayreward': 'l3loadmiss', 'l3wayNumStates':16, 'l3wayNumActions':56, 'shared':0.0625, 'l3waystep':0.125, 'cacheactionsclass':'rwpcacheactions'},
    'rwpl3level' : {'l3wayreconfig':True, 'l3waymdp': 'l3tpi', 'l3wayreward': 'uncorereq', 'l3wayNumStates':16, 'l3wayNumActions':56, 'shared':0.0625, 'l3waystep':0.125, 'cacheactionsclass':'rwpcacheactions'},
    'rwpl3levell3miss' : {'l3wayreconfig':True, 'l3waymdp': 'l3tpi', 'l3wayreward': 'l3loadmiss', 'l3wayNumStates':16, 'l3wayNumActions':56, 'shared':0.0625, 'l3waystep':0.125, 'cacheactionsclass':'rwpcacheactions'},
    'rwpl3level81' : {'l3wayreconfig':True, 'l3waymdp': 'mpki3', 'l3wayreward': 'l3loadmiss', 'l3wayNumStates':81, 'l3wayNumActions':56, 'shared':0.25, 'l3waystep':0.125, 'cacheactionsclass':'rwpcacheactions'},
    'rwpl3missl3miss' : {'l3wayreconfig':True, 'l3waymdp': 'l3loadmiss2', 'l3wayreward': 'l3loadmiss', 'l3wayNumStates':16, 'l3wayNumActions':56, 'shared':0.0625, 'l3waystep':0.125, 'cacheactionsclass':'rwpcacheactions'},
    'rwpl3miss4l3miss' : {'l3wayreconfig':True, 'l3waymdp': 'l3loadmiss4', 'l3wayreward': 'l3loadmiss', 'l3wayNumStates':256, 'l3wayNumActions':56, 'shared':0.0625, 'l3waystep':0.125, 'cacheactionsclass':'rwpcacheactions'},
    'rwps4l3miss4l3miss' : {'l3wayreconfig':True, 'l3waymdp': 'l3loadmiss4', 'l3wayreward': 'l3loadmiss', 'l3wayNumStates':256, 'l3wayNumActions':56, 'shared':0.25, 'l3waystep':0.125, 'cacheactionsclass':'rwpcacheactions'},
    #CacheStack Optimization, MDP at L3
    'csmpkiinstr' : {'l3wayreconfig':True, 'l3waymdp': 'l1mpki3', 'l3wayreward': 'csinstr', 'l3wayNumStates':81, 'l3wayNumActions':19, 'shared':0.25, 'l3waystep':0.125, 'cacheactionsclass':'cacheactions'},
    #RWS
    'rwsmpkiinstr' : {'l3wayreconfig':True, 'rwsopt':True, 'l3waymdp': 'l1mpki3', 'l3wayreward': 'csinstr', 'l3wayNumStates':81, 'l3wayNumActions':5, 'shared':0.0, 'l3waystep':0.0, 'cacheactionsclass':'rwscacheactions'},
}

l2cache_options = {
    'no' : {'l2wayreconfig':False, 'l2wayNumStates':0, 'l2wayNumActions':0,},
    #with tpi3, and 2 logical_cpus, l2 will have 2^3 states i.e 9
    #total actions would be 3, [-1, 1], [0, 0], [1, -1]
    'tpitpi' : {'l2wayreconfig':True, 'l2waymdp': 'tpi3', 'l2wayreward': 'tpi', 'l2wayNumStates':9, 'l2wayNumActions':3, 'shared':0.25, 'l2waystep':0.125},
    'tpiedp' : {'l2wayreconfig':True, 'l2waymdp': 'tpi3', 'l2wayreward': 'edp', 'l2wayNumStates':9, 'l2wayNumActions':3, 'shared':0.25, 'l2waystep':0.125},
    'tpiepi' : {'l2wayreconfig':True, 'l2waymdp': 'tpi3', 'l2wayreward': 'epi', 'l2wayNumStates':9, 'l2wayNumActions':3, 'shared':0.25, 'l2waystep':0.125},
    'tpiinstr' : {'l2wayreconfig':True, 'l2waymdp': 'tpi3', 'l2wayreward': 'instr', 'l2wayNumStates':9, 'l2wayNumActions':3, 'shared':0.25, 'l2waystep':0.125},
    'tpiinstr4' : {'l2wayreconfig':True, 'l2waymdp': 'tpi4', 'l2wayreward': 'instr', 'l2wayNumStates':16, 'l2wayNumActions':3, 'shared':0.25, 'l2waystep':0.125},
    'tpiinstr5' : {'l2wayreconfig':True, 'l2waymdp': 'tpi5', 'l2wayreward': 'instr', 'l2wayNumStates':25, 'l2wayNumActions':3, 'shared':0.25, 'l2waystep':0.125},
    'tpil2misses' : {'l2wayreconfig':True, 'l2waymdp': 'tpi5', 'l2wayreward': 'l2miss', 'l2wayNumStates':25, 'l2wayNumActions':3, 'shared':0.25, 'l2waystep':0.125},
    'tpiuncore' : {'l2wayreconfig':True, 'l2waymdp': 'tpi3', 'l2wayreward': 'uncorereq', 'l2wayNumStates':9, 'l2wayNumActions':3, 'shared':0.25, 'l2waystep':0.125},
    'mpkiinstr' : {'l2wayreconfig':True, 'l2waymdp': 'l2mpki5', 'l2wayreward': 'instr', 'l2wayNumStates':25, 'l2wayNumActions':3, 'shared':0.25, 'l2waystep':0.125},
    'mpkil2misses' : {'l2wayreconfig':True, 'l2waymdp': 'l2mpki5', 'l2wayreward': 'l2miss', 'l2wayNumStates':25, 'l2wayNumActions':3, 'shared':0.25, 'l2waystep':0.125},
    #CacheStack Optimization, MDP at L2
    'csmpkiinstr' : {'l2wayreconfig':True, 'l2waymdp': 'l2mpki5', 'l2wayreward': 'csinstr', 'l2wayNumStates':25, 'l2wayNumActions':3, 'shared':0.25, 'l2waystep':0.125},
}

l1dcache_options = {
    'no' : {'l1dwayreconfig':False, 'l1dwayNumStates':0, 'l1dwayNumActions':0,},
    #with tpi3, and 2 logical_cpus, l1d will have 2^3 states i.e 9
    #total actions would be 3, [-1, 1], [0, 0], [1, -1]
    'tpitpi' : {'l1dwayreconfig':True, 'l1dwaymdp': 'tpi3', 'l1dwayreward': 'tpi', 'l1dwayNumStates':9, 'l1dwayNumActions':3, 'shared':0.25, 'l1dwaystep':0.125},
    'tpiedp' : {'l1dwayreconfig':True, 'l1dwaymdp': 'tpi3', 'l1dwayreward': 'edp', 'l1dwayNumStates':9, 'l1dwayNumActions':3, 'shared':0.25, 'l1dwaystep':0.125},
    'tpiepi' : {'l1dwayreconfig':True, 'l1dwaymdp': 'tpi3', 'l1dwayreward': 'epi', 'l1dwayNumStates':9, 'l1dwayNumActions':3, 'shared':0.25, 'l1dwaystep':0.125},
    'tpiinstr' : {'l1dwayreconfig':True, 'l1dwaymdp': 'tpi3', 'l1dwayreward': 'instr', 'l1dwayNumStates':9, 'l1dwayNumActions':3, 'shared':0.25, 'l1dwaystep':0.125},
    'cstpiinstr' : {'l1dwayreconfig':True, 'l1dwaymdp': 'tpi3', 'l1dwayreward': 'csinstr', 'l1dwayNumStates':9, 'l1dwayNumActions':3, 'shared':0.25, 'l1dwaystep':0.125},
}

l1icache_options = {
    'no' : {'l1iwayreconfig':False, 'l1iwayNumStates':0, 'l1iwayNumActions':0,},
    #with tpi3, and 2 logical_cpus, l1i will have 2^3 states i.e 9
    #total actions would be 3, [-1, 1], [0, 0], [1, -1]
    'tpitpi' : {'l1iwayreconfig':True, 'l1iwaymdp': 'tpi3', 'l1iwayreward': 'tpi', 'l1iwayNumStates':9, 'l1iwayNumActions':3, 'shared':0.25, 'l1iwaystep':0.125},
    'tpiedp' : {'l1iwayreconfig':True, 'l1iwaymdp': 'tpi3', 'l1iwayreward': 'edp', 'l1iwayNumStates':9, 'l1iwayNumActions':3, 'shared':0.25, 'l1iwaystep':0.125},
    'tpiepi' : {'l1iwayreconfig':True, 'l1iwaymdp': 'tpi3', 'l1iwayreward': 'epi', 'l1iwayNumStates':9, 'l1iwayNumActions':3, 'shared':0.25, 'l1iwaystep':0.125},
    'tpiinstr' : {'l1iwayreconfig':True, 'l1iwaymdp': 'tpi3', 'l1iwayreward': 'instr', 'l1iwayNumStates':9, 'l1iwayNumActions':3, 'shared':0.25, 'l1iwaystep':0.125},
}

mdp_options = {
    0: {},  #0 is for no-optimization/baseline case and we should not be accessing any config

    1: create_config(core_options['juan'], {'coreDvfsCentral':False}, uncore_options['no'], l3cache_options['no'],),
    2: create_config(core_options['juan'], {'coreDvfsCentral':True}, uncore_options['no'], l3cache_options['no'],), 
    3: create_config(core_options['juan'], {'coreDvfsCentral':False}, uncore_options['nw'], l3cache_options['no'],),
    4: create_config(core_options['juan'], {'coreDvfsCentral':True}, uncore_options['nw'], l3cache_options['no'],), 
    
    9: create_config(core_options['tpied2p'], {'coreDvfsCentral':False}, uncore_options['no'], l3cache_options['no'],),
    10: create_config(core_options['tpied2p'], {'coreDvfsCentral':True}, uncore_options['no'], l3cache_options['no'],),
    11: create_config(core_options['tpiepi'], {'coreDvfsCentral':True}, uncore_options['no'], l3cache_options['no'],),
    12: create_config(core_options['tpiepi'], {'coreDvfsCentral':False}, uncore_options['no'], l3cache_options['no'],),
    
    13: create_config(core_options['tpitpi'], {'coreDvfsCentral':False}, uncore_options['no'], l3cache_options['no'],),
    14: create_config(core_options['tpitpi'], {'coreDvfsCentral':True}, uncore_options['no'], l3cache_options['no'],),
    
    15: create_config(core_options['tpibptpi'], {'coreDvfsCentral':False}, uncore_options['no'], l3cache_options['no'],),
    16: create_config(core_options['tpibptpi'], {'coreDvfsCentral':True}, uncore_options['no'], l3cache_options['no'],),
    17: create_config(core_options['tpibpepi'], {'coreDvfsCentral':False}, uncore_options['no'], l3cache_options['no'],),
    18: create_config(core_options['tpibpepi'], {'coreDvfsCentral':True}, uncore_options['no'], l3cache_options['no'],),
    19: create_config(core_options['tpiq10epi'], {'coreDvfsCentral':False}, uncore_options['no'], l3cache_options['no'],),
    20: create_config(core_options['tpiq5trepi'], {'coreDvfsCentral':False}, uncore_options['no'], l3cache_options['no'],),
    
    41: create_config(core_options['no'], uncore_options['no'], l3cache_options['tpitpi'],),
    42: create_config(core_options['no'], uncore_options['no'], l3cache_options['tpiedp'],),
    43: create_config(core_options['no'], uncore_options['no'], l3cache_options['tpiepi'],),    #epi could be a good reward for L3 cache
    44: create_config(core_options['no'], uncore_options['no'], l3cache_options['tpiinstr'],),   
    45: create_config(core_options['no'], uncore_options['no'], l3cache_options['tpiuncore'],),   
    46: create_config(core_options['no'], uncore_options['no'], l3cache_options['l1mpkitpi'],),   
    47: create_config(core_options['no'], uncore_options['no'], l3cache_options['l3mpkil1miss'],),   
    48: create_config(core_options['no'], uncore_options['no'], l3cache_options['l1mpkiuncore'],),   
    49: create_config(core_options['no'], uncore_options['no'], l3cache_options['l1mpkiepi'],),   
    
    55: create_config(core_options['no'], uncore_options['no'], l3cache_options['tpiuncore'], {'batchL3Mdp':True},),   
    
    #Cooperative MLM
    69: create_config(core_options['tpibpl3tpi'], {'coreDvfsCentral':True}, uncore_options['nw'], l3cache_options['l1mpkitpi'],),
    70: create_config(core_options['tpibpl3tpi'], {'coreDvfsCentral':True}, uncore_options['tpi'], l3cache_options['l1mpkiuncore'],),   
    71: create_config(core_options['tpibpl3epi'], {'coreDvfsCentral':True}, uncore_options['nw'], l3cache_options['l1mpkiuncore'],),   
    73: create_config(core_options['tpibpl3epi'], {'coreDvfsCentral':False}, uncore_options['nw'], l3cache_options['tpiepi'],),   
    74: create_config(core_options['tpibpl3epi'], {'coreDvfsCentral':True}, uncore_options['nw'], l3cache_options['tpicsepi'],),   
    75: create_config(core_options['tpibpl3epi'], {'coreDvfsCentral':False}, uncore_options['nw'], l3cache_options['tpiuncore'],),
    
    #coMLM with distributed MDP on LLC
    83: create_config(core_options['tpibpl3epi'], {'coreDvfsCentral':False}, uncore_options['nw'], l3cache_options['tpiepi'],{'batchL3Mdp':True}),   

    #110+ l3 rwp
    111: create_config(core_options['no'], uncore_options['no'], l3cache_options['rwpinstr']),
    112: create_config(core_options['no'], uncore_options['no'], l3cache_options['rwpuncore']),
    113: create_config(core_options['no'], uncore_options['no'], l3cache_options['rwpl3level']),
    114: create_config(core_options['no'], uncore_options['no'], l3cache_options['rwpl3levell3miss']),
    115: create_config(core_options['no'], uncore_options['no'], l3cache_options['rwpl3level81']),
    116: create_config(core_options['no'], uncore_options['no'], l3cache_options['rwpl3miss']),
    117: create_config(core_options['no'], uncore_options['no'], l3cache_options['rwpl3missl3miss']),
    118: create_config(core_options['no'], uncore_options['no'], l3cache_options['rwpl3miss4l3miss']),
    119: create_config(core_options['no'], uncore_options['no'], l3cache_options['rwps4l3miss4l3miss']),
    
    #cache stack opt
    201: create_config(core_options['csmkpiinstr'], {'coreDvfsCentral':False}, uncore_options['no'], l3cache_options['no'],),
    202: create_config(core_options['csmkpiireward'], {'coreDvfsCentral':False}, uncore_options['no'], l3cache_options['no'],),
    203: create_config(core_options['csmkpimpki'], {'coreDvfsCentral':False}, uncore_options['no'], l3cache_options['no'],),
    204: create_config(core_options['cstpimpki'], {'coreDvfsCentral':False}, uncore_options['no'], l3cache_options['no'],),
    205: create_config(core_options['cstpiinstr'], {'coreDvfsCentral':False}, uncore_options['no'], l3cache_options['no'],),
    
    221: create_config(core_options['csfeatureinstr'], {'learner':'Q_LinFA'}, {'coreDvfsCentral':False}, uncore_options['no'], l3cache_options['no'],),
    222: create_config(core_options['csfeatureinstr'], {'learner':'QLambda_LinFA'}, {'coreDvfsCentral':False}, uncore_options['no'], l3cache_options['no'],),
    223: create_config(core_options['csfeatureinstr'], {'learner':'LSPI'}, {'coreDvfsCentral':False}, uncore_options['no'], l3cache_options['no'],),
    224: create_config(core_options['csfeatureinstr'], {'learner':'GQLambda'}, {'coreDvfsCentral':False}, uncore_options['no'], l3cache_options['no'],),
    
    231: create_config(core_options['csfeatureNFQ'], {'coreDvfsCentral':False}, uncore_options['no'], l3cache_options['no'],),
    
    #multiagent cache stack
    301: create_config(core_options['csmkpiinstr'], {'coreDvfsCentral':False, 'multiagent':True, 'smartupdates':-1.0}, uncore_options['no'], l3cache_options['no'],),
    305: create_config(core_options['cstpiinstr'], {'coreDvfsCentral':False, 'multiagent':True, 'smartupdates':-1.0}, uncore_options['no'], l3cache_options['no'],),
   
    #place holder for All Action Smart Update
    351: create_config(core_options['csmkpiinstr'], {'coreDvfsCentral':False, 'multiagent':True, 'smartupdates':0.5}, uncore_options['no'], l3cache_options['no'],),
    352: create_config(core_options['csmkpiinstr'], {'coreDvfsCentral':False, 'multiagent':True, 'smartupdates':0.25}, uncore_options['no'], l3cache_options['no'],),
    353: create_config(core_options['csmkpiinstr'], {'coreDvfsCentral':False, 'multiagent':True, 'smartupdates':0.1}, uncore_options['no'], l3cache_options['no'],),
    355: create_config(core_options['cstpiinstr'], {'coreDvfsCentral':False, 'multiagent':True, 'smartupdates':0.5}, uncore_options['no'], l3cache_options['no'],),
    356: create_config(core_options['cstpil3miss'], {'coreDvfsCentral':False, 'multiagent':True, 'smartupdates':0.5}, uncore_options['no'], l3cache_options['no'],),
    357: create_config(core_options['cstpil2miss'], {'coreDvfsCentral':False, 'multiagent':True, 'smartupdates':0.5}, uncore_options['no'], l3cache_options['no'],),
    358: create_config(core_options['cstpiepi'], {'coreDvfsCentral':False, 'multiagent':True, 'smartupdates':0.5}, uncore_options['no'], l3cache_options['no'],),
    365: create_config(core_options['cstpi10ipc'], {'coreDvfsCentral':False, 'multiagent':True, 'smartupdates':0.5}, uncore_options['no'], l3cache_options['no'],),
    
    #Coordinated MLM via joint actions
    #TODO: smartupdates must be 0, >0 means cache smart updates, <0 means user specified updates
    401: create_config(core_options['mlmtpiepi'], {'coreDvfsCentral':False, 'multiagent':True, 'smartupdates':0.0}, uncore_options['no'], l3cache_options['no'],),
    402: create_config(core_options['mlmtpitpi'], {'coreDvfsCentral':False, 'multiagent':True, 'smartupdates':0.0}, uncore_options['no'], l3cache_options['no'],),
    403: create_config(core_options['mlmtpiedp'], {'coreDvfsCentral':False, 'multiagent':True, 'smartupdates':0.0}, uncore_options['no'], l3cache_options['no'],),
    405: create_config(core_options['mlmtpi10epi'], {'coreDvfsCentral':False, 'multiagent':True, 'smartupdates':0.0}, uncore_options['no'], l3cache_options['no'],),
    451: create_config(core_options['mlmtpiepi'], {'coreDvfsCentral':False, 'multiagent':True, 'smartupdates':0.0}, uncore_options['nw'], l3cache_options['no'],),
    455: create_config(core_options['mlmtpi10epi'], {'coreDvfsCentral':False, 'multiagent':True, 'smartupdates':0.0}, uncore_options['nw'], l3cache_options['no'],),
    456: create_config(core_options['mlmtpifepi'], {'coreDvfsCentral':False, 'multiagent':True, 'smartupdates':0.0}, uncore_options['nw'], l3cache_options['no'],),
    457: create_config(core_options['mlmtpifed2p'], {'coreDvfsCentral':False, 'multiagent':True, 'smartupdates':0.0}, uncore_options['nw'], l3cache_options['no'],),
    458: create_config(core_options['mlmtpitrepi'], {'coreDvfsCentral':False, 'multiagent':True, 'smartupdates':0.0}, uncore_options['nw'], l3cache_options['no'],),
    459: create_config(core_options['mlmtpitred2p'], {'coreDvfsCentral':False, 'multiagent':True, 'smartupdates':0.0}, uncore_options['nw'], l3cache_options['no'],),
    
    #JMLM + cooperative uncore mdp
    466: create_config(core_options['mlmtpifnocepi'], {'coreDvfsCentral':False, 'multiagent':True, 'smartupdates':0.0}, uncore_options['nw'], l3cache_options['no'],),
}

#print mdp_options
