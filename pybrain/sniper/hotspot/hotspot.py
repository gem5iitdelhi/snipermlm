#!/usr/bin/python
import sys
sys.path.insert(1, '../') 
import mlm_rl
import cProfile, pstats, StringIO

pr = cProfile.Profile()
pr.enable()

#Code to profile
try:
  mlm_rl.setupWithSniper('tmp.mpi', 8, 301, './tmp.pickle')
except SystemExit:
  print "mlm done"
#end of code

pr.disable()
s = StringIO.StringIO()
sortby = 'cumulative'
ps = pstats.Stats(pr, stream=s).sort_stats(sortby)
ps.print_stats()
print s.getvalue()















if False:
  import hotshot, hotshot.stats, mlm_rl
  prof = hotshot.Profile("stones.prof")
  prof.runcall(mlm_rl.setupWithSniper('tmp.mpi', 8, 301, './tmp.pickle'))
  prof.close()
  stats = hotshot.stats.load("stones.prof")
  stats.strip_dirs()
  stats.sort_stats('time', 'calls')
  stats.print_stats(20)
