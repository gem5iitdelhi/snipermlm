
class mpiCache:
  def __init__(self, size=5):
    self.size = size
    self.keyList = []
    self.cache = {}
    self.hits = 0
    self.misses = 0

  def __del__(self):
    print "[mpiCache] hits, misses ", self.hits, self.misses, self.hits*100.0/(self.hits+self.misses)

  def get(self, key):
    if key in self.cache.keys():
      self.hits += 1
      return self.cache[key]
    else:
      self.misses += 1
      return None

  def put(self, key, data):
    assert(key not in self.cache.keys())
    if len(self.cache) == self.size:
      evict = self.keyList[0]
      self.keyList.pop(0)
      del self.cache[evict]
    
    self.keyList.append(key)
    self.cache[key] = data
