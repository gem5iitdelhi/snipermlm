#!/usr/bin/python
import sys, copy

def combi(n,m):
    #n!/(n-m)!*m!
    c=1
    for i in range(n-m+1,n+1):
        c = c*i
    for i in range(1,m+1):
        c = c/i
#    print n,m,c
    return c

def getNumValidActions(n):
    ac=0
    for m in range(0, (n/2)+1):
        ac = ac + (combi(n,m)*combi(n-m,m))
    return ac

class cacheactions:
    def __init__(self, n, sharedCores, ways, fixshared, varshared=False, onereconfig=False):
        self._Actions = []
        self.Num = n
        self.np = sharedCores
        self.baseCore = n*sharedCores
        self.ways = ways
        self.fixshared = fixshared
        self.vs = varshared
        self.oner = onereconfig
        self._initializeAssignment()
        
        if self.vs == True:
          self._computeCacheWayReconfigActions(self.np+1)
        else:  
          self._computeCacheWayReconfigActions(self.np)
        print "_computeCacheWayReconfigActions", self.Num, self.np, self.baseCore, self.ways, self.fixshared
        print "_computeCacheWayReconfigActions", self._Actions

    def _initializeAssignment(self):
        sharedWays = self.fixshared
        iPerCore = (self.ways-self.fixshared)/self.np
        waysOccupied = sharedWays + iPerCore*self.np
        if waysOccupied < self.ways:
          sharedWays = self.fixshared + self.ways-waysOccupied
        elif self.vs:
          sharedWays = self.fixshared + self.np
          iPerCore = iPerCore-1

        self.wayAssignment = [-1 for w in range(sharedWays)]
        for c in range(self.baseCore, self.baseCore+self.np):
            self.wayAssignment.extend([c for i in range(iPerCore)])
        if len(self.wayAssignment) < self.ways:
            assert(0)
            #iPerCore was non int, fill the rest with baseCore
            self.wayAssignment.extend([int(self.baseCore) for i in range(self.ways-len(self.wayAssignment))])
        
        print "[_initializeAssignment]", self.wayAssignment
        assert(len(self.wayAssignment) == self.ways)

    def _getAction(self, actionNum):
        return self._Actions[actionNum]

    def getCacheWayAssignment4Action(self, actionNum):
        actionVec = self._getAction(actionNum)
        assert(sum(actionVec) == 0)
        #TODO not sure if there is a better way to do this
        #first release the ways i.e -1, then allot them
        for c in range(len(actionVec)):
            if actionVec[c] == -1:
              for w in range(self.fixshared, self.ways):
                if self.wayAssignment[w] == self.baseCore+c:
                    self.wayAssignment[w] = -5000  #tmp marker
                    break

        for c in range(len(actionVec)):
            if actionVec[c] == 1:
              for w in range(self.fixshared, self.ways):
                if self.wayAssignment[w] == -5000:
                    self.wayAssignment[w] = self.baseCore+c 
                    break

        print "getCacheWayAssignment4Action",self.Num, self.baseCore, actionVec, self.wayAssignment
        assert(sum(self.wayAssignment) >= -1*self.fixshared)   #hacky way to make sure no unallotted way
        return self.wayAssignment

    #finds the no of configs possible by distributing ways among np cores
    #each core can get 0 to #ways
    #e.g np=4, ways=8
    #   c1: 0,0,0,8  c2: 1,1,1,5 etc
    def _computeCacheWayReconfigs(self, np, ways,config=[]):
        if ways < 0 or np <=0:
            assert("np or ways must be >0")
        if np==1:
            config.append(ways)
    #        print config
            return 1    #all ways assigned to the only core
        else:
            nconfigs=0;
            for w in range(ways+1):
                tconfig = copy.copy(config)
                tconfig.append(w)
                nconfigs=nconfigs+self._computeCacheWayReconfigs(np-1, ways-w, tconfig)
            return nconfigs

    #each core can have -1,0,1 action
    #combined the sum of action must be zero
    def _computeCacheWayReconfigActions(self, np, actions=[]):
        if np < 0:
            assert("np must be >=0")
        if np==0:
            if sum(actions) == 0 and (self.oner == False or actions.count(-1)==1):
    #            print actions
                self._Actions.append(copy.copy(actions))
                return 1    #all ways assigned to the only core
            else:
                return 0
        else:
            nactions=0;
            for a in [-1,0,1]:
                tact = copy.copy(actions)
                tact.append(a)
                nactions += self._computeCacheWayReconfigActions(np-1, tact)
            return nactions

class rwscacheactions(cacheactions):
    RW=0
    RO=1
    SD=2
    def __init__(self, ways):
      self.Num = 0
      self.np = 3
      self.baseCore = 0
      self.ways = ways
      self.fixshared = 0
      self.vs = False
      self.oner = False
      self.wayAssignment = [self.RW]*(self.ways/2) + [self.RO]*(self.ways/2) 
      print "rwscacheactions.__init__ wayAssignment = ", self.wayAssignment, self._Actions
      self._Actions = [ [0,0,0], 
                        [1,-1,0],[-1,1,0], 
                        [0,1,-1],[0,-1,1],
                      ]
#      self._computeCacheWayReconfigActions(self.np)
      print "_computeCacheWayReconfigActions", self._Actions

    def getCacheWayAssignment4Action(self, actionNum):
      cacheactions.getCacheWayAssignment4Action(self, actionNum)
      #make sure RW has atleast 1 cache way
      if self.wayAssignment.count(self.RW) == 0:
        self.wayAssignment[0] = self.RW
      return self.wayAssignment

class rwpcacheactions:
    def __init__(self, n, sharedCores, ways, fixshared):
        self.Num = n
        self.np = sharedCores
        self.baseCore = n*sharedCores
        self.ways = ways
        self.fixshared = fixshared
        self._initializeAssignment()

    def _initializeAssignment(self):
        self.EXCL_READ=1000
        self.EXCL_WRITE=2000
        print self.EXCL_READ, self.EXCL_WRITE
        self.wayAssignment = [-1 for w in range(self.fixshared)]
        iPerCore = (self.ways-self.fixshared)/self.np
        for c in range(self.baseCore, self.baseCore+self.np):
            self.wayAssignment.extend([self.EXCL_WRITE +c for i in range(iPerCore/2)])
            self.wayAssignment.extend([self.EXCL_READ +c for i in range(iPerCore - iPerCore/2)]) #if iPerCore=3, then this evaluates to 2
        if len(self.wayAssignment) < self.ways:
            #iPerCore was non int, fill the rest with READ starting from baseCore
            for i in range(self.ways-len(self.wayAssignment)):
              self.wayAssignment.append(int(self.EXCL_READ+self.baseCore+i))
        
        print "[_initializeAssignment]", self.wayAssignment
        assert(len(self.wayAssignment) == self.ways)
        self._Actions = []
        self._computeCacheWayReconfigActions(self.np)
        print "_computeCacheWayReconfigActions", self._Actions

    def _getAction(self, actionNum):
        return self._Actions[actionNum]

    # [-1, 0, 0, 0, 0, 1, 0, 0] [-1, -1, -1, -1, 1005, 1000, 1000, 2001, 1001, 1001, 2002, 1002, 1002, 2003, 1003, 1003]
    def getCacheWayAssignment4Action(self, actionNum):
        actionVec = self._getAction(actionNum)
        assert(sum(actionVec) == 0)
        #TODO not sure if there is a better way to do this
        #first release the ways i.e -1, then allot them
        # actionVec: W0 R0 W1 R1 W2 R2 W3 R3
        for c in range(len(actionVec)):
            if actionVec[c] == -1:
              victim = [self.EXCL_WRITE, self.EXCL_READ][c%2]+c/2
            if actionVec[c] == 1:
              victor = [self.EXCL_WRITE, self.EXCL_READ][c%2]+c/2

        for w in range(self.fixshared, self.ways):
          if self.wayAssignment[w] == victim:
              self.wayAssignment[w] = victor  #reassign
              break

        print "getCacheWayAssignment4Action",self.Num, self.baseCore, actionVec, self.wayAssignment, victim, victor
        assert(sum(self.wayAssignment) >= -1*self.fixshared)   #hacky way to make sure no unallotted way
        return self.wayAssignment

    #Atmost 1 core looses and one core gains a way
    #A core could release its R/W for its own W/R
    def _computeCacheWayReconfigActions(self, np):
      if np < 0:
          assert("np must be >=0")
      
      nactions=0;
      for i in range(2*np):
        for j in range(2*np):
          if(i==j): continue
          tact = [0 for c in range(2*np)]
          tact[i] = 1
          tact[j] = -1
          nactions += 1
          self._Actions.append(tact)
      return nactions

class cachestackactions:
    SHARED_WAY=-1
    FIXED=[2,2,4] #L1D, L2, L3i
    CACHES=['l1_dcache', 'l2_cache', 'l3_cache']
    def __init__(self, np, cacheInfo):
      self.np = np
      self.cacheInfo = cacheInfo
      assert(cacheInfo['l1_dcache']['sharedCores'] > 1 and cacheInfo['l2_cache']['sharedCores'] > 1)
      self._initializeAssignment()

    def _initializeAssignment(self):
      for cache_level in self.CACHES: 
        print cache_level, ":", self.cacheInfo[cache_level]['wayAssignment']

      self._computeCacheWayReconfigActions()
      print "_computeCacheWayReconfigActions", self._Actions
    
    #each cache level can have -1,0,1 action
    def _computeCacheWayReconfigActions(self):
      self._Actions= [
                        [0,0,0],  #no change
#                        [1,0,0], [-1,0,0],  #L1D change
                        [0,1,0], [0,-1,0],  #L2 change
                        [0,0,1], [0,0,-1],  #L3 change
                        [0,1,1], [0,1,-1], [0,-1,1], [0,-1,-1],
                        [0,2,0], [0,3,0],   #L2 change
                        [0,0,2], [0,0,4], [0,0,6],  #L3 change
                    ]

    def _getAction(self, actionNum):
      return self._Actions[actionNum]
    
    def getCacheStackAssignment(self, cachestackActions):
      actionVecs = [self._getAction(actionNum) for actionNum in cachestackActions]
#      print "getCacheStackAssignment: ", cachestackActions, actionVecs
      for cache_level in self.CACHES:
        cacheIdx = self.CACHES.index(cache_level)
        for cache_num in range(self.cacheInfo[cache_level]['num']):
          bcore = cache_num*self.cacheInfo[cache_level]['sharedCores']
          ncore = self.cacheInfo[cache_level]['sharedCores']
          self._updateCacheWayAssignment(self.FIXED[cacheIdx],
                                          self.cacheInfo[cache_level]['wayAssignment'],
                                            cache_num*self.cacheInfo[cache_level]['associativity'],
                                            (cache_num+1)*self.cacheInfo[cache_level]['associativity'],
                                            bcore, ncore,
                                            [actionVecs[i][cacheIdx] for i in range(bcore, bcore+ncore)]
                                          );
#      print "getCacheStackAssignment: ", self.cacheInfo['l3_cache']['wayAssignment'], self.cacheInfo['l2_cache']['wayAssignment'], self.cacheInfo['l1_dcache']['wayAssignment']
      return self.cacheInfo['l3_cache']['wayAssignment'], self.cacheInfo['l2_cache']['wayAssignment'], self.cacheInfo['l1_dcache']['wayAssignment']

    #updates the way assignment
    #nfixed: min fixed shared ways
    #wayAssign: current way assignment, lidx and ridx for the assignment to be updated
    #coreList: cores participating
    #coreActionList: core actions
    def _updateCacheWayAssignment(self, nfixed, wayAssign, lidx, ridx, bcore, ncore, coreActionList):
      print "[_updateCacheWayAssignment] ", nfixed, wayAssign, lidx, ridx, bcore, ncore, coreActionList
      #initial ways are always fixed shared
      for w in wayAssign[lidx:lidx+nfixed]:
        assert(w==self.SHARED_WAY)
      
      #count current cache assignments
      wayCount = [0]*(ncore+1)
      for w in wayAssign[lidx+nfixed:ridx+1]:
        if w==self.SHARED_WAY: wayCount[-1] += 1
        else: wayCount[w-bcore] += 1

      #mark willing cache ways as shared
      for i in range(len(coreActionList)):
        assert(coreActionList[i] in [-1,0,1,2,3,4,6]) #currently allowed values
        if coreActionList[i]==-1 and wayCount[i] > 0:
          wayCount[i]  -= 1
          wayCount[-1] += 1 #increase shared count
          diter = cacheWayIter(i, lidx+nfixed, ridx, ncore, False)
          while not diter.end():
            idx = diter.next()
            if wayAssign[idx]==i+bcore:
              wayAssign[idx]=self.SHARED_WAY
              break

      #allocate cache ways if possible
      from random import shuffle
      idxs = range(len(coreActionList))
      shuffle(idxs)
      for i in idxs:
        while coreActionList[i]>0 and wayCount[-1] > 0:
          aiter = cacheWayIter(i, lidx+nfixed, ridx, ncore, True)
          while not aiter.end():
            idx = aiter.next()
            if wayAssign[idx]==self.SHARED_WAY:
              wayAssign[idx]=i+bcore
              break
          
          wayCount[i]  += 1
          wayCount[-1] -= 1 #decr shared count
          coreActionList[i] -= 1  #decr to signify that one way assigned
      print idxs, wayCount

    def getCacheWayAssignment4Action(self, actionNum):
        actionVec = self._getAction(actionNum)
        assert(sum(actionVec) == 0)
        #TODO not sure if there is a better way to do this
        #first release the ways i.e -1, then allot them
        for c in range(len(actionVec)):
            if actionVec[c] == -1:
              for w in range(self.fixshared, self.ways):
                if self.wayAssignment[w] == self.baseCore+c:
                    self.wayAssignment[w] = -5000  #tmp marker
                    break

        for c in range(len(actionVec)):
            if actionVec[c] == 1:
              for w in range(self.fixshared, self.ways):
                if self.wayAssignment[w] == -5000:
                    self.wayAssignment[w] = self.baseCore+c 
                    break

        print "getCacheWayAssignment4Action",self.Num, self.baseCore, actionVec, self.wayAssignment
        assert(sum(self.wayAssignment) >= -1*self.fixshared)   #hacky way to make sure no unallotted way
        return self.wayAssignment

class cacheWayIter:
    def __init__(self, c, lidx, ridx, ncore, alloc=True):
      self.cidx=-1
      self.idx = self.getIdxList(c, lidx, ridx, ncore, alloc)
#      print "[cacheWayIter]", self.idx

    def getIdxList(self, c, lidx, ridx, ncore, alloc):
      midpoint = lidx-1+(ridx-lidx)/2
      if(ncore==2 or ncore==4):  # TODO starting with 2 or 4 cores
        if (c==0 and not alloc) or (c==ncore-1 and alloc):
          return range(ridx-1, lidx-1, -1)
        elif (c==ncore-1 and not alloc) or (c==0 and alloc):
          return range(lidx, ridx, 1)
        elif (c==1 and not alloc) or (c==ncore-2 and alloc):
          return range(midpoint+1, ridx, 1) + range(lidx, midpoint+1, 1)
        elif (c==ncore-2 and not alloc) or (c==1 and alloc):
          return range(midpoint, lidx-1, -1) + range(ridx-1, midpoint, -1)
      elif ncore==8:
        if c < ncore/2:
          return self.getIdxList(c, lidx, midpoint+1, ncore/2, alloc) + self.getIdxList(0, midpoint+1, ridx, ncore/2, alloc)
        else:
          return self.getIdxList(c-ncore/2, midpoint+1, ridx, ncore/2, alloc) + self.getIdxList(c-ncore/2, lidx, midpoint+1, ncore/2, alloc)
      else:
        assert(0)

    def next(self):
      self.cidx += 1
      return self.idx[self.cidx]

    def end(self):
      return self.cidx >= len(self.idx)-1

if __name__ == '__main__':
    np = int(sys.argv[1])
    ways = int(sys.argv[2])
    varshared = False
    oneReconf = False
    #s =cacheactions(0, np, ways, int(.25*ways), varshared, oneReconf)
    #print "Iteration:", len(s._Actions)
    print "Formula: ", getNumValidActions(np)

def returntmp():
    rws = rwscacheactions(ways)
    print rws.getCacheWayAssignment4Action(2)
    print " RWS Iteration:", len(rws._Actions)
    
    cacheWayIter(0, 0, 8, np, True)
    cacheWayIter(0, 0, 8, np, False)
    cacheWayIter(1, 0, 8, np, True)
    cacheWayIter(1, 0, 8, np, False)
    cacheWayIter(2, 0, 8, np, True)
    cacheWayIter(2, 0, 8, np, False)
    cacheWayIter(0, 0, ways, np, False)
    cacheWayIter(0, 0, ways, np, True)
    cacheWayIter(3, 0, ways, np, False)
    cacheWayIter(3, 0, ways, np, True)
    cacheWayIter(np-1, 0, ways, np, False)
    cacheWayIter(np-2, 0, ways, np, True)
    cacheWayIter(np-2, 0, ways, np, False)
    cacheWayIter(np-1, 0, ways, np, True)
