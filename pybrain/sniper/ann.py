#!/usr/bin/python
#Artificial Neural Network
#intention is to create a generic framework which can be used to train any function
#current focus is on ANN based multiple resource management MICRo 2008 paper
import sys, os, time
import logging
import pickle
import random

from pybrain.structure import FeedForwardNetwork
from pybrain.structure import LinearLayer, SigmoidLayer
from pybrain.structure import FullConnection

from pybrain.datasets            import ClassificationDataSet
from pybrain.utilities           import percentError
from pybrain.tools.shortcuts     import buildNetwork
from pybrain.tools.neuralnets    import NNregression, NNclassifier
from pybrain.supervised.trainers import BackpropTrainer
from pybrain.structure.modules   import SoftmaxLayer

from pybrain.optimization import StochasticHillClimber

from pybrain.tools.customxml import NetworkWriter, NetworkReader

from getmetricSniper import *
from ann_search_var import *

ENSEMBLE=1
INPUT=11
MAXEPOCH = 250
CONV_ITER = 5
hidden = [4,]

#reads a csv file, expecting a total of inDim+outDim cvs values
def readData(trnDataCsv, inDim, outDim):
  trndata = ClassificationDataSet(inp=inDim)  #specify the num of inputs and outputs/targets
  for line in open(trnDataCsv, 'r').readlines():
    sample = line.split(",")
    inData = [float(v) for v in sample[0:inDim]]
    outData = [float(v) for v in sample[inDim:]]
    trndata.addSample(inData, outData) 
  return trndata

def setupLogger():
  logger = logging.getLogger()
  logger.setLevel(logging.INFO)

  formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')

  fh = logging.FileHandler('log_filename.txt')
  fh.setLevel(logging.DEBUG)
  fh.setFormatter(formatter)
  logger.addHandler(fh)

#  ch = logging.StreamHandler()
#  ch.setLevel(logging.INFO)
#  ch.setFormatter(formatter)
#  logger.addHandler(ch)

  logger.debug('This is a test log message.')

class AnnEnsemble:
  def __init__(self, datafile, np, mpifile, picklefile):
    self.ANNEnsembleList = []
    self.np = np
    #Train
    for core in range(np):
      self.ANNEnsembleList.append(self.trainANNEnsemble(datafile+str(core)))
    
    #Initialize Activate Phase
    self.getmetric = getmetricSniper(mpifile, np)
    self.mpifile = mpifile
    if picklefile != None:
      self.cacheInfo = pickle.load(open(picklefile, 'r'))
  
  #trains a ensemble of ANNs for a given data file
  def trainANNEnsemble(self, dfile):  
    alldata = readData(dfile, INPUT, 1)
    AnnEnsemble = []
    for e in range(ENSEMBLE):
      print "trainANNEnsemble: ", e, dfile
      tstDS, DS = alldata.splitWithProportion( 0.33 )
      nnr = NNregression(DS, TDS=tstDS, maxepochs=MAXEPOCH)
      nnr.setupNN(hidden=hidden, verbose=False)
      nnr.runTraining(convergence=CONV_ITER)
      AnnEnsemble.append(nnr)
    return AnnEnsemble
  
  #predicts for a List of ANN Ensembles
  def activateANNEnsemble(self):
    os.system('touch ' + self.mpifile)      #HACK TODO: this is required since training can take more than 10 mins
    getmetric = self.getmetric
    while not getmetric.mpi.isEnd():
      coffsets, uoffsets, l3wayAssign, l2wayAssign, l1dwayAssign, l1iwayAssign = self.doOneStep()
      getmetric.setCoreDvfsOffsets(coffsets)
      getmetric.setUnCoreDvfsOffsets(uoffsets)
      getmetric.setL3wayReconfigAssign(l3wayAssign)
      getmetric.setL2wayReconfigAssign(l2wayAssign)
      getmetric.setL1DwayReconfigAssign(l1dwayAssign)
      getmetric.setL1IwayReconfigAssign(l1iwayAssign)
      getmetric.incInterval()
      print '[activateANNEnsemble] coffsets, uoffsets, l3ways', coffsets, uoffsets, l3wayAssign
      sys.stdout.flush()

  #activates the learned ANNs and returns the new recongfiguration after search
  def doOneStep(self):
#    time.sleep(3)
    indim = 12  #1 extra for ipc
    mLen = 8    #TODO make this configurable, l1 metrics length
    dp = []
    for core in range(self.np):
      dp.append(self.getmetric.getMetrics('readANNDataPoint', core, indim, self.np*indim , self.getmetric.mpi.ANNDP))
   
    self.currentConfig = [dp[core][mLen:indim] for core in range(self.np)]
    self.currentActivationMetrics = [dp[core][0:mLen] for core in range(self.np)]   #TODO make this configurable
#    print "doOneStep: dp, currentActivationMetrics ", dp, self.currentActivationMetrics
    return self._searchBest()

  #uses ANN ensemble to predict and find optimal configuration
  def _searchBest(self):
    dvfsIdx = dvfsVars.seed()
    l3waysAssign = cachewayVar.seed()
    varList = [dvfsIdx, l3waysAssign]
    l = StochasticHillClimber(self.predictMetrics, varList, maxEvaluations = 2000)
    l.mustMaximize = True     #maximize the objective
    best = l.learn()
    print "Best Search ", best
    return self.__getReconfig(best)

    #return self.__randomResources()

  #predict optimization metric using varList
  def predictMetrics(self, varList):
#    print "predictMetrics: ", varList
    total_ipc = 0.0
    for core in range(self.np):
      coreDVFS = varList[0].dvfsLevel(core)
      uncoreDVFS = varList[0].dvfsLevel(self.np)
      cacheWays = varList[1].numWays(core)
      total_ipc += self.__predictIPC(core, coreDVFS, uncoreDVFS, cacheWays)
#    print "predictMetrics: ", varList, total_ipc
    return total_ipc

  #predict IPC for a core
  def __predictIPC(self, core, coreDvfsIdx, uncoreDvfsIdx, cacheWays):
    l1metricsList = [self.currentActivationMetrics[core][i] for i in range(len(self.currentActivationMetrics[core]))]
    dp = l1metricsList + [coreDvfsIdx, uncoreDvfsIdx, cacheWays]
    ann = self.ANNEnsembleList[core]
    ipc = 0.0
#    import pdb
#    pdb.set_trace()
    for e in range(ENSEMBLE):
      ipc += float(ann[e].Trainer.module.activate(dp)[0])
#      print "__predictIPC: dp ", core, e, dp, ann[e].Trainer.module.activate(dp)
#    print "__predictIPC: ", ipc
    return ipc/ENSEMBLE

  def __getReconfig(self, best):
    bestReconfig = best[0]
#    print "__getReconfig: ", self.currentConfig
#    print "currentConfig: ", [dvfsVars.dvfsTable.index(self.currentConfig[core][0]) for core in range(self.np)]
    coffsets = [int(bestReconfig[0].x[core] - dvfsVars.dvfsTable.index(self.currentConfig[core][0])) for core in range(self.np)]
    assert(self.currentConfig[0][1] == self.currentConfig[1][1])  #uncore dvfs idx
    uoffsets = [int(bestReconfig[0].x[self.np] - dvfsVars.dvfsTable.index(self.currentConfig[0][1])),]

    #get l3 way assignment
    num = self.cacheInfo['l3_cache']['num']
    assert(num==1)
    associativity = self.cacheInfo['l3_cache']['associativity']
    shared = 0
    l3wayassign = [-1]*shared
    for core in range(self.np):
      l3wayassign += [core]*bestReconfig[1].numWays(core)
    assert(len(l3wayassign) == associativity)

    #get l2 way assignment
    num = self.cacheInfo['l2_cache']['num']
    associativity = self.cacheInfo['l2_cache']['associativity']
    l2wayassign = [-1 for i in range(num*associativity)]

    #get l1d way assignment
    num = self.cacheInfo['l1_dcache']['num']
    associativity = self.cacheInfo['l1_dcache']['associativity']
    l1dwayassign = [-1 for i in range(num*associativity)]

    #get l1i way assignment
    num = self.cacheInfo['l1_icache']['num']
    associativity = self.cacheInfo['l1_icache']['associativity']
    l1iwayassign = [-1 for i in range(num*associativity)]
#    print "__getReconfig: " , coffsets, uoffsets, l3wayassign
    return (coffsets, uoffsets, l3wayassign, l2wayassign, l1dwayassign, l1iwayassign)


  def __randomResources(self):
     import random
     coffsets = [random.randint(-1,1) for core in range(self.np)]
     uoffsets = [random.randint(-1,1),]

     #get l3 way assignment
     num = self.cacheInfo['l3_cache']['num']
     associativity = self.cacheInfo['l3_cache']['associativity']
     shared = 0
     l3wayassign = [-1 for i in range(num*associativity)] 
     
     #get l2 way assignment
     num = self.cacheInfo['l2_cache']['num']
     associativity = self.cacheInfo['l2_cache']['associativity']
     l2wayassign = [-1 for i in range(num*associativity)]

     #get l1d way assignment
     num = self.cacheInfo['l1_dcache']['num']
     associativity = self.cacheInfo['l1_dcache']['associativity']
     l1dwayassign = [-1 for i in range(num*associativity)]

     #get l1i way assignment
     num = self.cacheInfo['l1_icache']['num']
     associativity = self.cacheInfo['l1_icache']['associativity']
     l1iwayassign = [-1 for i in range(num*associativity)]

     return (coffsets, uoffsets, l3wayassign, l2wayassign, l1dwayassign, l1iwayassign)





if __name__ == '__main__':
  setupLogger()
  datafile = sys.argv[1]
  np = int(sys.argv[2])
  mpifile = sys.argv[3]
  picklefile = sys.argv[4]

  ann = AnnEnsemble(datafile, np, mpifile, picklefile)
  ann.activateANNEnsemble()
