#ifndef __DRAM_PERF_MODEL_TIMING_H__
#define __DRAM_PERF_MODEL_TIMING_H__

#include "dram_perf_model.h"
#include "queue_model.h"
#include "fixed_types.h"
#include "subsecond_time.h"
#include "dram_cntlr_interface.h"
#include "DRAMTimingModel.h"

class DramPerfModelTiming : public DramPerfModel
{
   private:
      DramModel dramModel;
      SubsecondTime m_dram_access_cost;
      ComponentBandwidth m_dram_bandwidth;
      bool m_shared_readwrite;
      UInt32 m_ddr3_MHz;  //1333,1600,2000 etc

      SubsecondTime m_total_access_latency;

   public:
      DramPerfModelTiming(core_id_t core_id,
            UInt32 cache_block_size);

      ~DramPerfModelTiming();
      bool setFrequency(UInt32 _ddr3_MHz);
      SubsecondTime getAccessLatency(SubsecondTime pkt_time, UInt64 pkt_size, core_id_t requester, IntPtr address, DramCntlrInterface::access_t access_type, ShmemPerf *perf);
};

#endif /* __DRAM_PERF_MODEL_TIMING_H__ */
