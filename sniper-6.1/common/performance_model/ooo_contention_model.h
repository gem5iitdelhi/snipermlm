#ifndef O3CONTENTION_MODEL_H
#define O3CONTENTION_MODEL_H

#include <vector>
#include "fixed_types.h"
#include "subsecond_time.h"

#include "DRAMTimingConfig.h"
#include "RankBank.h"

class OOOContentionModel: public ContentionModel {
   private:
      RowBufferHistory hist;
   public:
//      OOOContentionModel() {assert(0)};
      OOOContentionModel(String name, core_id_t core_id, UInt32 num_outstanding = 1)
        :hist(1, CacheBandwidthTimingConfig::getInstance()) {}
      
      ~OOOContentionModel(){}

//      uint64_t getCompletionTime(uint64_t t_start, uint64_t t_delay, UInt64 tag = 0) // Support legacy components
      SubsecondTime getCompletionTime(SubsecondTime t_start, SubsecondTime t_delay, UInt64 tag = 0) {
        return hist.getDepartureTime(t_start, 0, true); //0: address, 1:read, both param should not influence the result
      }
//      uint64_t getStartTime(uint64_t t_start);
//      SubsecondTime getStartTime(SubsecondTime t_start);
};

#endif // O3CONTENTION_MODEL_H
