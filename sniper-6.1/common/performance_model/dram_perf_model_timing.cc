#include "dram_perf_model_timing.h"
#include "simulator.h"
#include "config.h"
#include "config.hpp"
#include "stats.h"
#include "shmem_perf.h"

DramPerfModelTiming::DramPerfModelTiming(core_id_t core_id,
      UInt32 cache_block_size):
   DramPerfModel(core_id, cache_block_size),
   m_dram_bandwidth(8 * Sim()->getCfg()->getFloat("perf_model/dram/per_controller_bandwidth")), // Convert bytes to bits
   m_shared_readwrite(Sim()->getCfg()->getBool("perf_model/dram/readwrite/shared")),
   m_ddr3_MHz(Sim()->getCfg()->getInt("perf_model/dram/freq")),
   m_total_access_latency(SubsecondTime::Zero())
{
   m_dram_access_cost = SubsecondTime::FS() * static_cast<uint64_t>(TimeConverter<float>::NStoFS(Sim()->getCfg()->getFloat("perf_model/dram/latency"))); // Operate in fs for higher precision before converting to uint64_t/SubsecondTime

   setFrequency(m_ddr3_MHz);
   registerStatsMetric("dram", core_id, "total-access-latency", &m_total_access_latency);
}

bool DramPerfModelTiming::setFrequency(UInt32 _ddr3_MHz) {
    return dramModel.setFrequency(_ddr3_MHz);
}

DramPerfModelTiming::~DramPerfModelTiming()
{
}

SubsecondTime
DramPerfModelTiming::getAccessLatency(SubsecondTime pkt_time, UInt64 pkt_size, core_id_t requester, IntPtr address, DramCntlrInterface::access_t access_type, ShmemPerf *perf)
{
   // pkt_size is in 'Bytes'
   // m_dram_bandwidth is in 'Bits per clock cycle'
   if ((!m_enabled) ||
         (requester >= (core_id_t) Config::getSingleton()->getApplicationCores()))
   {
      return SubsecondTime::Zero();
   }

   SubsecondTime processing_time = m_dram_bandwidth.getRoundedLatency(8 * pkt_size); // bytes to bits

   // Compute Access Latency which does model queue delay, device access, transfer time based on burst length
   SubsecondTime access_latency;
    if (access_type == DramCntlrInterface::READ)
    {
        access_latency = processing_time + dramModel.getAccessLatency(pkt_time, address, true /*READ*/);
    } else {    //Write returns a zero latency,but processed to capture its effects on READ latency
        access_latency = dramModel.getAccessLatency(pkt_time, address, false /*WRITE*/);
    }

   perf->updateTime(pkt_time);
   perf->updateTime(pkt_time + access_latency, ShmemPerf::DRAM_QUEUE);
   perf->updateTime(pkt_time + access_latency, ShmemPerf::DRAM_BUS);
   perf->updateTime(pkt_time + access_latency, ShmemPerf::DRAM_DEVICE);

   // Update Memory Counters
   m_num_accesses ++;
   m_total_access_latency += access_latency;

   return access_latency;
}
