#ifndef __DYNAMIC_INSERT_POLICY_H
#define __DYNAMIC_INSERT_POLICY_H

/*
Samples Cache Sets to see which policy leads to lesser misses and chooses the one appropriately
Qureshi proposed using LRU vs BIP replacement
*/

#include "qureshi_umon.h"
#include "cache_atd.h"
#include "cache_base.h"
using namespace std;

class DIPCircuit {
public:
  DIPCircuit(String cache_level, String configName, core_id_t master_core_id) {
//    assert(cache_level == "L3");
    cout << "[DIPCircuit] creating for Cache" << cache_level << " master_core_id=" << master_core_id << endl;
    LRU_ATD = createATDs(cache_level + ".lru", configName, master_core_id, "lru");
    BIP_ATD = createATDs(cache_level + ".bip", configName, master_core_id, "bip");
    m_counter = 0;
  }

  ~DIPCircuit() {
    delete LRU_ATD;
    delete BIP_ATD;
  }

  bool isLRUPolicyOptimal() {
    //find if LRU is optimal policy
    return m_counter<0;
  }
/*
  vector<UInt64> getHitCounterValues(bool cleanNDirty) {
    if(cleanNDirty)
      return LRU_ATD->getUmon()->getHitCounterValues();
    else
      return BIP_ATD->getUmon()->getHitCounterValues();
  }
*/

  //Forget previous interval data
  void doIntervalReset() {
    LRU_ATD->getUmon()->intervalReset(0);
    BIP_ATD->getUmon()->intervalReset(0);
  }
  
  void update(Core::mem_op_t mem_op_type, bool hit, CacheState::cstate_t cstate, IntPtr address)
  {
    if(isMiss(LRU_ATD, mem_op_type, hit, address))
      m_counter++;
    if(isMiss(BIP_ATD, mem_op_type, hit, address))
      m_counter--;
  }

private:
  ATD* LRU_ATD;
  ATD* BIP_ATD;

  ATD* createATDs(String cache_level, String configName, core_id_t master_core_id, String policy) {
    UInt32 assoc = Sim()->getCfg()->getInt(configName + "/associativity");
    UInt32 blk_size = Sim()->getCfg()->getInt(configName + "/cache_block_size");
    UInt32 cache_size = Sim()->getCfg()->getInt(configName + "/cache_size")*1024;
    UInt32 num_sets = cache_size/(assoc*blk_size);

    return new ATD (cache_level + ".atd", configName, master_core_id, num_sets, assoc, blk_size,
                    policy,
                    CacheBase::parseAddressHash((Sim()->getCfg()->getString(configName + "/address_hash")))
                   );
  }
  
  bool isMiss(ATD* atd, Core::mem_op_t mem_op_type, bool hit, IntPtr address) {
    bool atd_miss = atd->isSampledSet(address) && !atd->exists(address);
    atd->access(mem_op_type, hit, address);
    return atd_miss;
  }

  SInt32 m_counter;
};

#endif
