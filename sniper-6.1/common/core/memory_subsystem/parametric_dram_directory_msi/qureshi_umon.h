#ifndef __CACHE_ATD_UMON_H
#define __CACHE_ATD_UMON_H
/* based on Qureshi's paper on Utility based cache partitioning */
//Umon: Utility Monitors
#include "config.hpp"
#include "stats.h"

#include <vector>
using namespace std;

class Umon {
  private:
    core_id_t core_id;
    UInt32 associativity;
    UInt64 set_start_index;
    UInt32 static_sample;
    UInt32 num_sets;

    typedef std::vector<UInt64> SetCounter;
    std::vector<SetCounter> Counters;
  public:
    Umon(core_id_t _core_id, UInt32 _associativity, UInt64 _set_start_index, UInt32 _static_sample, UInt32 _num_sets)
      :core_id(_core_id), associativity(_associativity), 
       set_start_index(_set_start_index), static_sample(_static_sample), num_sets(_num_sets)
    {
      Counters.resize(static_sample);
      for(UInt32 i=0; i<static_sample; i++)
        Counters[i].resize(associativity+1,0);
    }

    ~Umon() {
      print();
    }

    core_id_t core() { return core_id; }

    void update(UInt32 set_index, UInt32 lru_pos, Core::mem_op_t mem_op_type) {
      if(lru_pos == UInt32(-1)) lru_pos = associativity;
      Counters[set_index/(num_sets/static_sample)][lru_pos]++;
    }

    void print() {
      printf("core_id %d, associativity %d, num_sets %d\n", core_id, associativity, num_sets);
      vector<UInt64> hitcounters = getHitCounterValues();
    }

    vector<UInt64> getHitCounterValues(bool print=true)
    {
      vector<UInt64> hitcounters(associativity+1,0);
      for(UInt32 i=0; i<static_sample; i++)
        for(UInt32 j=0; j<associativity+1; j++)
          hitcounters[j] += Counters[i][j];
      
      if(print) {
        printf("Counters:\n");
        for(unsigned int j=0; j<associativity; j++) {
          printf("%10lu", hitcounters[j]);
        }
        printf(" misses=%10lu \n",hitcounters[associativity]);
      }

      return hitcounters;
    }
    
    void intervalReset(float x=.5)
    {
      for(UInt32 i=0; i<static_sample; i++)
        for(UInt32 j=0; j<associativity+1; j++)
          Counters[i][j] = (UInt32)(Counters[i][j]*x);
    }

    //From the paper this is the U: change in misses for application p when the number of blocks assigned to it increases from a to b
    UInt64 getAdditionalHitCount(UInt32 a, UInt32 b) {
      assert(b>=a && b<=associativity && a>=1);  //counter[associtivity] corresponds to total misses seen by ATD
      vector<UInt64> hitcounters = getHitCounterValues(false);
      UInt64 U = 0;
      for(UInt32 j=a-1; j<b; j++)
        U += hitcounters[j];

      return U;
    }
    
    UInt64 getHitCount(UInt32 a) {
      assert(a<=associativity && a>=1);  //counter[associtivity] corresponds to total misses seen by ATD
      vector<UInt64> hitcounters = getHitCounterValues(false);
      UInt64 U = 0;
      for(UInt32 j=0; j<a; j++)
        U += hitcounters[j];

      return U;
    }
    
    UInt64 getMissCount(UInt32 a) {
      assert(a<=associativity && a>=1);  //counter[associtivity] corresponds to total misses seen by ATD
      vector<UInt64> hitcounters = getHitCounterValues(false);
      UInt64 U = 0;
      for(UInt32 j=a; j<=associativity; j++)
        U += hitcounters[j];

      return U;
    }

    //MCFQ Paper 2014: Interference Sensitivity Factor
    //A core with high hits on higer ways would be more cache sensitive
    UInt64 getInterferenceSensitivity() {
      vector<UInt64> hitcounters = getHitCounterValues(false);
      UInt64 S = 0;
      for(UInt32 j=0; j<associativity; j++)
        S += j*hitcounters[j];

      return S;
    }
};
#endif
