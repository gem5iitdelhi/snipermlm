#ifndef __CACHE_RWP_CIRCUIT_H
#define __CACHE_RWP_CIRCUIT_H

//Improving Cache Performance by Exploiting Read-Write Disparity -Onur Mutlu
//Divide the cache between clean and dirty lines
//Partition size depends on the number of read requests to clean lines vs dirty lines
//Optimize for maximum read-hits
#include "qureshi_umon.h"
#include "cache_atd.h"
#include "cache_base.h"
using namespace std;

class RWPCircuit {
public:
  RWPCircuit(String cache_level, String configName, core_id_t master_core_id, String replacement_policy) {
//    assert(cache_level == "L3");
    assert(replacement_policy != "dip" && replacement_policy != "vip");
    cleanATD = createATDs(cache_level + ".clean", configName, master_core_id, replacement_policy);
    dirtyATD = createATDs(cache_level + ".dirty", configName, master_core_id, replacement_policy);
  }

  ~RWPCircuit() {
    delete cleanATD;
    delete dirtyATD;
  }

  vector<UInt64> getHitCounterValues(bool cleanNDirty) {
    if(cleanNDirty)
      return cleanATD->getUmon()->getHitCounterValues();
    else
      return dirtyATD->getUmon()->getHitCounterValues();
  }

  void doIntervalReset() {
    cleanATD->getUmon()->intervalReset(0.5);
    dirtyATD->getUmon()->intervalReset(0.5);
  }
  
  void updateRWP(Core::mem_op_t mem_op_type, bool hit, CacheState::cstate_t cstate, IntPtr address)
  {
    if (mem_op_type==Core::WRITE) {
      if(cleanATD->removeIfExists(address)) {
         //if write hit is in clean directory, move the entry to dirty
         //address was in cleanATD and move to dirtyATD
         dirtyATD->access(mem_op_type, hit, address);
      }
      else {
         dirtyATD->access(mem_op_type, hit, address);
      }
/*      else if(!dirtyATD->exists(address)) {
          //write miss, allocate to dirty directory
          dirtyATD->access(mem_op_type, hit, address);
      }*/
    } else {  //Read Request
      if(dirtyATD->exists(address)) {
        //if the address in dirty partition, update dirtyATD
        dirtyATD->access(mem_op_type, hit, address);
      }
      else {
        //if not in dirty then either in clean or make an entry to cleanATD
        cleanATD->access(mem_op_type, hit, address);
      }
    }
  }

private:
  ATD* cleanATD;
  ATD* dirtyATD;

  ATD* createATDs(String cache_level, String configName, core_id_t master_core_id, String replacement_policy) {
    UInt32 assoc = Sim()->getCfg()->getInt(configName + "/associativity");
    UInt32 blk_size = Sim()->getCfg()->getInt(configName + "/cache_block_size");
    UInt32 cache_size = Sim()->getCfg()->getInt(configName + "/cache_size")*1024;
    UInt32 num_sets = cache_size/(assoc*blk_size);

    return new ATD (cache_level + ".atd", configName, master_core_id, num_sets, assoc, blk_size,
                    replacement_policy,
                    CacheBase::parseAddressHash((Sim()->getCfg()->getString(configName + "/address_hash")))
                   );
  }

};

#endif
