#ifndef __DRAM_RANKBANK_H__
#define __DRAM_RANKBANK_H__
#include <utility>
#include <list>
#include <stdint.h>
#include <iostream>
#include <fixed_types.h>
#include <subsecond_time.h>
#include "DRAMTimingConfig.h"

using namespace std;

/*
Maintains a window of history of the open row buffer
Since the requests are OOO, some adjustment to timing would be done even though a transaction was processed
much earlier. Since that latency value cannot be changed, we take this error. But do the adjustment for
future transactions. 
Since the OOO has been observed to be within a very short window, the error effects are very local and due to
adjustment, outside the window the effects are captured accurately.
*/
typedef SInt64 DCycles;
class RowBufferHistory {
private:
    struct histEntry {
      private:
        DCycles arr;
        DCycles dep;
        UInt64 rowN;
        bool sanity(DCycles a, DCycles d) {
//            return a>0 && d>0 && d-a<500000;
            return a>0 && d>0;
        }
        void update(DCycles ad, bool aNd) {
            if (aNd)    arr = ad;
            else        dep = ad;
            assert(sanity(arr, dep));
        }
      public:
        histEntry(DCycles a, DCycles d, UInt64 r):
            arr(a), dep(d), rowN(r) {
                assert(sanity(arr,dep));
        }
        void updateArr(DCycles a) { update(a, true);}
        void updateDep(DCycles d) { update(d, false);}
        DCycles getArr() { return arr; }
        DCycles getDep() { return dep; }
        UInt64 getRow() { return rowN; }
    };

    DRAMTimingConfig* m_timing_config;
    UInt32 histSize;
    UInt32 RowBufferSizeBytes;
    typedef list<histEntry*> HistListT;
    HistListT hist_list;
    HistListT::iterator curr;    
    void removeItems(DCycles t);
    bool isRequestOverlap(HistListT::iterator processed_req, DCycles curr_req_arr); 
    void CompensateNext(HistListT::iterator curr, SInt64 cycles);
    SubsecondTime handleNoHistory(DCycles arr_cycle, UInt64 rowN, bool RnW);
    SubsecondTime handleOOOFrontTransaction(DCycles arr_cycle, UInt64 rowN, HistListT::iterator start,bool RnW);
public:
    RowBufferHistory(UInt32 nRowBuffers, DRAMTimingConfig* timing_config);
    SubsecondTime getDepartureTime(SubsecondTime arrival, IntPtr bankAddr, bool RnW); 
};


/*
A rank consists of muliple chips. Chips consist of multiple banks.
Corresponding Banks from each chip together contribute to a DRAM request
A 64-bit DRAM, would typically involve 8 chips contributing 8-bits each.
So the number of row-buffers in each rank is equal to the number of banks per chip.
All corresponding banks in a Rank respond to single commnd, share the address bits but provide different parts of data

So a RankBank is being defined below, where in a bank means a bank across all chips in a Rank
*/
class RankBank {
public:
    RankBank(string _name, IntPtr startAddr, IntPtr size, UInt32 _RowBufferSizeBytes);
    bool isIdle() { return idle; }
//    bool inRowBuffer(IntPtr addr) { return !isIdle() && inRange(StartEndRowBufAddr, addr); }
    bool inBank(IntPtr addr) { return inRange(StartEndBankAddr, addr); }

    SubsecondTime getAccessLatency(SubsecondTime msg_time, IntPtr addr, bool RnW);

private:
    typedef pair<IntPtr, IntPtr> AddrRangeT;
    AddrRangeT StartEndBankAddr;
//    AddrRangeT StartEndRowBufAddr;
    UInt32 RowBufferSizeBytes;
    bool idle;
    RowBufferHistory hist;
    bool inRange(AddrRangeT addrRange, IntPtr addr) {
        return (addrRange.first <= addr && addrRange.second >= addr);
    }
    string name;
    bool debug;
};
#endif
