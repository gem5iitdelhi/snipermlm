#include "RankBank.h"
#include <math.h>

RankBank::RankBank(string _name, IntPtr startAddr, IntPtr size, UInt32 _RowBufferSizeBytes)
    :hist(_RowBufferSizeBytes, DRAMTimingConfig::getInstance()) {
    RowBufferSizeBytes = _RowBufferSizeBytes;
    name = _name;
    StartEndBankAddr.first = startAddr;
    StartEndBankAddr.second = startAddr+size-1;
    idle = true;
    cout << "[DRAMModel::RankBank] Addr(MB) = " 
         << StartEndBankAddr.first/(1024*1024) << "," << StartEndBankAddr.second/(1024*1024)
         <<endl;
    debug = false;
}

SubsecondTime RankBank::getAccessLatency(SubsecondTime msg_time, IntPtr addr,bool RnW) {
    if(!inBank(addr)) {
        cout << "addr= " << addr
             << " StartEndBankAddr.first = " << StartEndBankAddr.first
             << " StartEndBankAddr.second = " << StartEndBankAddr.second
             << endl;
        assert(0);
    }
    //address is offset into the Bank
    IntPtr bankAddr = addr-StartEndBankAddr.first;
    SubsecondTime depTime = hist.getDepartureTime(msg_time, bankAddr, RnW);
    UInt64 rowN = bankAddr/RowBufferSizeBytes;
    if (debug) {
        cout << "[LATENCY] " << addr << "," 
             << msg_time.getPS()/1000 << "," << depTime.getPS()/1000 << ","
             << name << "," << rowN << endl;
    }
    //Process writes but return zero latency
    return RnW ? depTime-msg_time : SubsecondTime::Zero();
}

RowBufferHistory::RowBufferHistory(UInt32 rbSize, DRAMTimingConfig* timing_config ) {
    histSize = 50000;    //cycles
    RowBufferSizeBytes = rbSize;
    curr = hist_list.end();
    m_timing_config = timing_config;
}

bool RowBufferHistory::isRequestOverlap(HistListT::iterator processed_req, DCycles curr_req_arr) {
    return ((*processed_req)->getDep() >= curr_req_arr && (*processed_req)->getArr() <= curr_req_arr);   //current request overlaps this request
}

void RowBufferHistory::CompensateNext(HistListT::iterator current, SInt64 cycles) {
    if(current == hist_list.end())  return;

    if (cycles < 0 || cycles > 5000) {
//        cout << "ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ CompensateNext " <<  cycles << std::endl;
        cycles = 500;
        //assert(0);
    }
    
//    std::cout<< "CompensateNext " << cycles << std::endl;
    HistListT::iterator next = current; next++;
    if ((next != hist_list.end()) &&
            (*next)->getArr() <= (*current)->getDep()) {    //overlap
        SInt64 overlap = 1+(*current)->getDep()-(*next)->getArr();
        SInt64 comp = min(cycles, overlap);
        (*next)->updateDep((*next)->getDep() + comp);
        (*next)->updateArr((*next)->getArr() + comp);
//        std::cout<< "Recurse ";  
        CompensateNext(next, comp);
    }
}

SubsecondTime RowBufferHistory::handleNoHistory(DCycles arr_cycle, UInt64 rowN, bool RnW) {
    DCycles dep_cycle = arr_cycle + m_timing_config->getCyclesNoRowOpen(RnW);
    hist_list.push_back(new histEntry(arr_cycle, dep_cycle, rowN));
    return dep_cycle*m_timing_config->getCycleTime();
}

SubsecondTime RowBufferHistory::handleOOOFrontTransaction(DCycles arr_cycle, 
                            UInt64 rowN, HistListT::iterator start, bool RnW) {
    assert((*start)->getArr() > arr_cycle);  //current request arrived earlier than start request
    DCycles dep_cycle = 0;
    DCycles overlap = 0;
    if(start!=hist_list.begin()){
        HistListT::iterator prev = start; prev--;
        overlap = (*prev)->getDep()>arr_cycle ? (*prev)->getDep()-arr_cycle+1 : 0;
/*        if(overlap>5000){
            cout << "OVERLAP parr="<< (*prev)->getArr() 
                 << ",pdep="<< (*prev)->getDep()
                 << ",carr=" << arr_cycle
                 << ",sarr="<< (*start)->getArr() 
                 << ",sdep="<< (*start)->getDep()
                 <<",overlap=" << overlap
                 <<endl;
        }*/
    }
    //if ((*start)->getRow() == rowN) { //start request is in the same row
    arr_cycle += overlap;  // add overlap to arr as we do not want this overlap to be counted multiple times
    dep_cycle = arr_cycle + m_timing_config->getCyclesWrongRowOpen(RnW); //current req has to open the row
    //OVERLAP
    if (dep_cycle >= (*start)->getArr()) { //already processed req would have overlapped
        DCycles back_overlap = dep_cycle - (*start)->getArr() + 1;
        if((*start)->getRow() == rowN) { //start request is in the same row
            back_overlap -= m_timing_config->getCyclesActivate(); //account for extra ACT
            (*start)->updateDep((*start)->getDep()+back_overlap);
            //Update Arrival Time
            (*start)->updateArr(arr_cycle);
            if(back_overlap>0) CompensateNext(start, back_overlap);
        } else {    //different row
            hist_list.insert(start, new histEntry(arr_cycle, dep_cycle, rowN));
            CompensateNext(start, back_overlap);
        } 
    } else {//NO OVERLAP
        if( (*start)->getRow() != rowN ||   //different row
            ((*start)->getArr()-dep_cycle) > 5*m_timing_config->getCyclesWrongRowOpen(true)) {
            //same row, but large difference in request timestamp
            //If this gap is more than 5 times the worst case read time, 
            //then its better to split the transactions, so that some other transaction can be scheduled
            //No compensation or adjustment required as no overlap
            hist_list.insert(start, new histEntry(arr_cycle, dep_cycle, rowN));
        } else {//same row, very close timestamps
            //merge the 2 transactions by updating the dep, arr cycles for the current transaction
            DCycles newTxWidth = (*start)->getDep() - arr_cycle;
            assert(newTxWidth>0);
            if (newTxWidth > 50*m_timing_config->getCyclesCorrectRowOpen(true)) {
            //If this merging leads to too big a tx, then to avoid starvation,do not merge
                hist_list.insert(start, new histEntry(arr_cycle, dep_cycle, rowN));
            } else {
            //compensate the dep time as an extra activate has been issued
            //TODO: No downstream compenstation being performed currently, incase of shrinkage
                DCycles compenstate = m_timing_config->getCyclesActivate();
                (*start)->updateDep((*start)->getDep() - compenstate);
                //Update Arrival Time
                (*start)->updateArr(arr_cycle);
            }
        }
    }
//    } else {    //this belongs to a different row and hence should have been processed first
//        dep_cycle = arr_cycle + overlap + m_timing_config->getCyclesWrongRowOpen(RnW); //current req has to open the row
//        start = hist_list.insert(start, new histEntry(arr_cycle, dep_cycle, rowN));
//        CompensateNext(start, overlap+m_timing_config->getCyclesWrongRowOpen(RnW));
//    }
    return dep_cycle*m_timing_config->getCycleTime();
}

SubsecondTime RowBufferHistory::getDepartureTime(SubsecondTime arrival, IntPtr bankAddr, bool RnW) {
/*    cout << "getDepartureTime: " << arrival.getPS() << "," << m_timing_config->getCycleTime().getPS() 
         << " ,hist_list= " << hist_list.size()
         << endl;*/
    DCycles arr_cycle = ceil(arrival.getPS()/m_timing_config->getCycleTime().getPS());
    UInt64 rowN = bankAddr/RowBufferSizeBytes;
    DCycles dep_cycle = 0;
    //First Request in the History
    if (hist_list.empty()) {
        //no history
        return handleNoHistory(arr_cycle, rowN, RnW);
    }

    removeItems(arr_cycle);
    
    //[FRONT]Curr Request newer than all in history
    HistListT::iterator start=hist_list.begin();
    if((*start)->getArr() > arr_cycle) { //current request arrived earlier than start request
        return handleOOOFrontTransaction(arr_cycle, rowN, start, RnW); 
    }

    //[OVERLAP]Search for correct overlapping transactions
    for(; start!=hist_list.end(); ++start) {
        if (isRequestOverlap(start, arr_cycle)) {   //current request overlaps this request
            if ( ((*start)->getRow() == rowN) &&   //correct row was open, schedule this request at the end, precharge is part of next tx
                 ((*start)->getDep() - (*start)->getArr() < 50*m_timing_config->getCyclesWrongRowOpen(true)) ) 
                //also this request is not very long
            {
                dep_cycle = (*start)->getDep() + m_timing_config->getCyclesCorrectRowOpen(RnW);
                (*start)->updateDep(dep_cycle); //Account for this additional processed request
                CompensateNext(start, m_timing_config->getCyclesCorrectRowOpen(RnW));
                return dep_cycle*m_timing_config->getCycleTime();
            }
        } else if((*start)->getArr() > arr_cycle) {
            //first non-overlapping request with arrival later than current request
            return handleOOOFrontTransaction(arr_cycle, rowN, start, RnW);
        }
    }

    //[BACK] transaction that does not overlap and is the latest
    if ((hist_list.back())->getRow() == rowN &&
        (arr_cycle - hist_list.back()->getDep()) > 5*m_timing_config->getCyclesWrongRowOpen(true)) 
    {
        dep_cycle = arr_cycle + m_timing_config->getCyclesCorrectRowOpen(RnW);
        (hist_list.back())->updateDep(dep_cycle); 
    } else {
        dep_cycle = arr_cycle + m_timing_config->getCyclesWrongRowOpen(RnW); //current req has to open the row
        hist_list.push_back(new histEntry(arr_cycle, dep_cycle, rowN));
    }
    return dep_cycle*m_timing_config->getCycleTime();
}

void RowBufferHistory::removeItems(DCycles t) {
    if(histSize == 0)   return; //cleaning disabled
    if(hist_list.size() <= histSize/30)    return;  //each entry is atleast 30 cycles, a rough estimate to when to really try cleaning
    if (t<histSize) return;
    
    t -= histSize;
    HistListT::iterator iter=hist_list.begin();
    UInt64 count=0;
    while ((*iter)->getDep() < t) {   //tx dep is atleast histSize cycles away from current tx
        ++iter;
        ++count;
    }
    --iter;
//    cout << "[INFO][removeItems] removing entries: " << count << endl;
//    cout << "INFO: " << hist_list.front()->getArr() << "," << hist_list.back()->getArr() << "," << t << endl;
    hist_list.erase(hist_list.begin(), iter);

}
