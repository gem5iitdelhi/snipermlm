#ifndef __DRAM_TIMING_MODEL_H__
#define __DRAM_TIMING_MODEL_H__
#include <iostream>
#include <stdint.h>
#include <vector>
#include "RankBank.h"
#include "DRAMTimingConfig.h"
#include "queue_model.h"

using namespace std;

class DramModel {
public:
    DramModel();
    SubsecondTime getAccessLatency(SubsecondTime msg_time, IntPtr addr, bool RnW);
    bool setFrequency(UInt32 _ddr3_MHz) { return DRAMTimingConfig::getInstance()->setClock(_ddr3_MHz); }
private:
    UInt64 DramSizeMb;
    UInt32 NumChannels;
    UInt32 NumDimmsPerChannel;
    UInt32 NumRanksPerDimms; 
    UInt32 NumChipsPerRanks;    
    UInt32 NumBanksPerChip;
    
    UInt32 RowBufferSizeBytes;
    UInt32 RankBankSizeKb;
    UInt32 NumRowBuffers;
    vector<RankBank*> rankBanks;
    QueueModel* data_chan_queue;    
private:
    void init();
};
#endif
