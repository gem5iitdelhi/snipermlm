#include "DRAMTimingModel.h"

DramModel::DramModel() {
    DramSizeMb = 4096;  //4GB
    NumChannels = 1;    //TODO read from config
    NumDimmsPerChannel = 1; //typically 1, a slot of DRAM hosts 1 DIMM
    NumRanksPerDimms = 2;   //typically 2, one on either side of DIMM
    NumChipsPerRanks = 8;
    NumBanksPerChip = 8;
    RowBufferSizeBytes = 8192;
    init();
}

void DramModel::init() {
    RankBankSizeKb = DramSizeMb*1024/(NumChannels*NumDimmsPerChannel*NumRanksPerDimms*NumBanksPerChip); 
    NumRowBuffers = DramSizeMb*1024/RankBankSizeKb; //also equals no of RankBanks
    
    cout << "\n[DramModel] "
         << "\n  DramSizeMb = " << DramSizeMb
         << "\n  NumChannels = " << NumChannels
         << "\n  NumDimmsPerChannel = " << NumDimmsPerChannel
         << "\n  NumRanksPerDimms = " << NumRanksPerDimms
         << "\n  NumChipsPerRanks = " << NumChipsPerRanks
         << "\n  NumBanksPerChip = " << NumBanksPerChip
         << "\n  RowBufferSizeBytes = " << RowBufferSizeBytes
         << "\n  RankBankSizeKb = " << RankBankSizeKb
         << "\n  NumRowsPerBank = " << RankBankSizeKb*1024/RowBufferSizeBytes
         << "\n  NumRowBuffers = " << NumRowBuffers
         << endl;
    cout << "getCyclesCorrectRowOpen(R) " << DRAMTimingConfig::getInstance()->getCyclesCorrectRowOpen(true) << endl;
    cout << "getCyclesCorrectRowOpen(W) " << DRAMTimingConfig::getInstance()->getCyclesCorrectRowOpen(false) << endl;
    cout << "getCyclesNoRowOpen(R) " << DRAMTimingConfig::getInstance()->getCyclesNoRowOpen(true) << endl;
    cout << "getCyclesWrongRowOpen(R) " << DRAMTimingConfig::getInstance()->getCyclesWrongRowOpen(true) << endl;
    cout << "getCyclesPrecharge() " << DRAMTimingConfig::getInstance()->getCyclesPrecharge() << endl;
    cout << "getCyclesActivate() " << DRAMTimingConfig::getInstance()->getCyclesActivate() << endl;
    cout << "getCycleTime() PS " << DRAMTimingConfig::getInstance()->getCycleTime().getPS() << endl;


    for(UInt32 i=0 ; i<NumRowBuffers; ++i) {
        UInt32 rank = i/NumBanksPerChip;
        UInt32 bank = i%NumBanksPerChip;
        string name = to_string(rank) + "," + to_string(bank);
        rankBanks.push_back(new RankBank(name, i*RankBankSizeKb*1024, RankBankSizeKb*1024, RowBufferSizeBytes)); 
    }

    data_chan_queue = QueueModel::create("DramChannelQueue", 0, "windowed_mg1", SubsecondTime::Zero());
}
    
SubsecondTime DramModel::getAccessLatency(SubsecondTime msg_time, IntPtr addr, bool RnW) {
    //HACK: Since sniper is passing virtual address to dram, take the lower bits to get physical address
    //This should work in general for dram latency, since we are more interested in 
    //latency variation due to dram page misses etc
    UInt64 DramSizeB = DramSizeMb*1024*1024;
    IntPtr phyAddr = addr%DramSizeB;
    UInt32 bankN = phyAddr/(RankBankSizeKb*1024);
    SubsecondTime access_latency = rankBanks[bankN]->getAccessLatency(msg_time, phyAddr, RnW);
    SubsecondTime transfer_time = data_chan_queue->computeQueueDelay(msg_time+access_latency, 
                                        (8/2)*DRAMTimingConfig::getInstance()->getCycleTime(), 0);
/*    cout << "[getAccessLatency] " << addr << "," 
         << msg_time.getPS()/1000 << "," << access_latency.getPS()/1000 << "," << transfer_time.getPS()/1000 << "," 
         << bankN << "," << bankN/RowBufferSizeBytes << endl;
*/    access_latency = access_latency + transfer_time;
    return access_latency;
}
