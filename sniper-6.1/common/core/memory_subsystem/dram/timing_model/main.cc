#include "DRAMTimingConfig.h"
#include "DRAMTimingModel.h"
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>

using namespace std;

std::vector<std::string> &split(const std::string &s, char delim, std::vector<std::string> &elems) {
    std::stringstream ss(s);
    std::string item;
    while (std::getline(ss, item, delim)) {
        elems.push_back(item);
    }
    return elems;
}

std::vector<std::string> split(const std::string &s, char delim) {
    std::vector<std::string> elems;
    split(s, delim, elems);
    return elems;
}

int main(int argc, char* argv[]) {
    DRAMTimingConfig::getInstance()->setClock(2000); //DRAM Clock MHz
    DramModel dram;
    UInt64 dramSize = std::stoul("0x100000000", nullptr, 16);
    
    ifstream intrace(argv[1]);
    ofstream outtrace(argv[2]);
    string line;
    while(!intrace.eof())
    {
        intrace >> line;
        vector<string> l = split(line, ',');
        SubsecondTime msg_time = SubsecondTime::NSfromFloat(atoi(l[0].c_str()));
//        IntPtr addr = std::stoul(l[3], nullptr, 16)%dramSize; //atoi(l[2].c_str());
        IntPtr addr = std::stoul(l[3], nullptr, 16); //atoi(l[2].c_str());
        bool RnW = (l[2].compare("R") == 0); 
        outtrace << l[0] << ","
                 << dram.getAccessLatency(msg_time, addr, RnW).getPS()/1000 << ","
                 << l[2] << "," << l[3] << "\n";
    }
    intrace.close();
    outtrace.close();
}
