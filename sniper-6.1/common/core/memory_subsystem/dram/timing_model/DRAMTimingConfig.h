#ifndef __DRAM_TIMING_CONFIG_H__
#define __DRAM_TIMING_CONFIG_H__
#include <subsecond_time.h>
#include <iostream>
using namespace std;

class DRAMTimingConfig {

protected:
//    static DRAMTimingConfig* INSTANCE;
    
    SubsecondTime cycleTime;
//Cmd(i) Cmd(i+1) Param   Cycles
//ACT     WR      tRCD    7
//ACT     RD      tRCD    7
//    UInt32 tRCD;
//ACT     PRE     tRAS    20
//    UInt32 tRAS;
//ACT     ACT(same bank) tRC 21
//    UInt32 tRC;
//ACT     ACT(different bank) tRRD 6
//    UInt32 tRRD;
//RD/WR   RD/WR   tCCD    4
//    UInt32 tCCD;
//RD/WR   PRE     tRTP    4
//    UInt32 tRTP;
//PRE     SRE     tRP     7
//PRE     REF     tRP     7
//PRE     ACT     tRP     7
//    UInt32 tRP;
//REF     REF     tREFI   4160
    UInt32 tREFI;
//REF     ACT     tRFC    59
    UInt32 tRFC;
//SRX     ACT     tXSDLL  512
    UInt32 tXSDLL;

    UInt32 CL; //CAS latency CL  The time between sending a column address to the memory and the beginning of the data in response. This is the time it takes to read the first bit of memory from a DRAM with the correct row already open.
    
    UInt32 tRCD; //Row Address to Column Address Delay TRCD    The number of clock cycles required between the opening of a row of memory and accessing columns within it. The time to read the first bit of memory from a DRAM without an active row is TRCD + CL.

    UInt32 tRP; //Row Precharge Time  TRP The number of clock cycles required between the issuing of the precharge command and opening the next row. The time to read the first bit of memory from a DRAM with the wrong row open is TRP + TRCD + CL.
    
    UInt32 tRAS; //Row Active Time TRAS    The number of clock cycles required between a row active command and issuing the precharge command. This is the time needed to internally refresh the row, and overlapping with TRCD. In SDRAM modules, it is simply TRCD + CL. Otherwise, approximately equal to TRCD + (2 * CL).

    DRAMTimingConfig() {
//        UInt32 DramFreqMHz = 1600;  //this is double data rate frequency, so effective clock is at 800 MHz
        float cyclens = 1.0/0.8;    //1GHz clock time is 1ns
        cycleTime = SubsecondTime::NSfromFloat(cyclens);
  
        //Micron Datasheet for 4GB 1600 SDRAM
        //tRCD-tRP-CL
        tRCD = 11;
        tRP = 11;
        CL = 11;
        //http://en.wikipedia.org/wiki/Memory_timings
        tRAS = tRCD+CL;

        tREFI = 4160;
        tRFC = 59;
        tXSDLL = 512;

        AL=0;   //0
        WR=10;
        BL=8;   //Burst Length, usually 8

        RL=CL+AL;   //CL+AL
        WL=RL-1;   //RL-1
        dataRate=2; //2 for DDR2,DDR3
    };

    UInt32 BL;   //Burst Length, usually 8
    UInt32 AL;   //0
    UInt32 RL;   //CL+AL
    UInt32 WL;   //RL-1
    UInt32 WR;   
    UInt32 dataRate; //2 for DDR2,DDR3

public:
   UInt32 getCyclesCorrectRowOpen(bool RnW) { 
        if (RnW)  //Read
            return RL+1+BL/dataRate;
        else
            return WL+BL/dataRate+WR;
   }
   UInt32 getCyclesWrongRowOpen(bool RnW) { return getCyclesPrecharge() + getCyclesNoRowOpen(RnW); }
   UInt32 getCyclesNoRowOpen(bool RnW) { return getCyclesActivate() + getCyclesCorrectRowOpen(RnW);} 
   UInt32 getCyclesPrecharge() { return tRP; }
   UInt32 getCyclesActivate() { return tRCD; }
   SubsecondTime getCycleTime() { return cycleTime; }
   bool setClock(UInt32 MHz) { 
        cout << "[DRAMConfig] setClock: " << MHz << endl;
        cycleTime = SubsecondTime::PSfromFloat(1000.0*1000.0*2.0/MHz); 
        return true;    //TODO: set only supported frequencies
   }

public:
   static DRAMTimingConfig* getInstance() { 
      static DRAMTimingConfig INSTANCE;
      return &INSTANCE;
   }
};

class CacheBandwidthTimingConfig:public DRAMTimingConfig {
protected:
    CacheBandwidthTimingConfig() {
        float cyclens = 1.0/3.2;    //3.2GHz core/cache clock time
        cycleTime = SubsecondTime::NSfromFloat(cyclens);\
    }
public:
   UInt32 getCyclesCorrectRowOpen(bool RnW) { 
      return 1.0;   //assuming 1 cycle per cache block
   }
   UInt32 getCyclesWrongRowOpen(bool RnW) { return getCyclesPrecharge() + getCyclesNoRowOpen(RnW); }
   UInt32 getCyclesNoRowOpen(bool RnW) { return getCyclesActivate() + getCyclesCorrectRowOpen(RnW);} 
   UInt32 getCyclesPrecharge() { return 0; }
   UInt32 getCyclesActivate() { return 0; }
   SubsecondTime getCycleTime() { return cycleTime; }
   bool setClock(UInt32 MHz) { 
        cout << "[CacheBandwidthTimingConfig] setClock: " << MHz << endl;
        cycleTime = SubsecondTime::PSfromFloat(1000.0*1000.0*2.0/MHz); 
        assert(0);
        return true;    //TODO: set only supported frequencies
   }

public:
   static CacheBandwidthTimingConfig* getInstance() { 
      static CacheBandwidthTimingConfig INSTANCE;
      return &INSTANCE;
   }

};

#endif
