#ifndef CACHE_SET_RECONFIG_H
#define CACHE_SET_RECONFIG_H

#include "cache_set.h"
/* Implements a runtime reconfigurable cache partitioning policy.
To start with each core can be assigned exclusive cache ways
*/
class CacheSetInfoReconfig : public CacheSetInfo
{
   public:
      CacheSetInfoReconfig(String name, String cfgname, core_id_t core_id, UInt32 associativity, UInt8 num_attempts);
      virtual ~CacheSetInfoReconfig();
      void increment(UInt32 index)
      {
         LOG_ASSERT_ERROR(index < m_associativity, "Index(%d) >= Associativity(%d)", index, m_associativity);
         ++m_access[index];
      }
      void incrementAttempt(UInt8 attempt)
      {
         if (m_attempts)
            ++m_attempts[attempt];
         else
            LOG_ASSERT_ERROR(attempt == 0, "No place to store attempt# histogram but attempt != 0");
      }
   private:
      const UInt32 m_associativity;
      UInt64* m_access;
      UInt64* m_attempts;
};

class CacheSetReconfig : public CacheSet
{
   public:
      CacheSetReconfig(CacheBase::ReplacementPolicy _policy, CacheBase::cache_t cache_type,
            UInt32 associativity, UInt32 blocksize, CacheSetInfoReconfig* set_info, UInt8 num_attempts);
      virtual ~CacheSetReconfig();

      virtual UInt32 getReplacementIndex(const CacheReplacementInfo& crInfo);
      UInt32 getLruPosition(UInt32 accessed_index);
      void updateReplacementIndex(UInt32 accessed_index);

   protected:
      const UInt8 m_num_attempts;
      UInt8* m_lru_bits;
      CacheSetInfoReconfig* m_set_info;
      void moveToPolicyPos(UInt32 accessed_index, CacheCntlr *cntlr);
      void moveToMRU(UInt32 accessed_index);
      void moveToLRU(UInt32 accessed_index, UInt8 lruPos=0);
      void moveToBIP(UInt32 accessed_index, UInt8 lruPos=0);
      void moveToDIP(UInt32 accessed_index, CacheCntlr *cntlr);
      void moveToVIP(UInt32 accessed_index, CacheCntlr *cntlr);
      void moveToPIPP(UInt32 accessed_index);
      string getLRUVector();
      
      CacheBase::ReplacementPolicy policy;
      UInt8 vipPos;
};

#endif /* CACHE_SET_RECONFIG_H */
