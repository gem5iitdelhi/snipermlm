#include "cache_set_reconfig.h"
#include "log.h"
#include "stats.h"

#include "core_manager.h"
#include "mem_component.h"
#include "string"
#include "simulator.h"
#include "config.hpp"
#include "cache_cntlr.h"

using namespace std;

// Implements Reconfig replacement policy
/*
LRU: inserts the new line at MRU
LIP: inserts at LRU which gets promoted to MRU on reuse
BIP: LIP with 1/e (e=32 usually) probability of insertion at MRU
DIP: Do BIP or LRU based on some sampling sets 

VIP: Variable LRU Inertion Policy, insert at vth MRU position
  LRU=VIP(v=0)
  LIP=VIP(v=associativity)
*/

CacheSetReconfig::CacheSetReconfig(
      CacheBase::ReplacementPolicy _policy,
      CacheBase::cache_t cache_type,
      UInt32 associativity, UInt32 blocksize, CacheSetInfoReconfig* set_info, UInt8 num_attempts) :
     CacheSet(cache_type, associativity, blocksize)
   , m_num_attempts(num_attempts)
   , m_set_info(set_info)
{
   policy = _policy;
   m_lru_bits = new UInt8[m_associativity];
   for (UInt32 i = 0; i < m_associativity; i++)
      m_lru_bits[i] = i;

   String cfgname = "perf_model/l3_cache";
   if (Sim()->getCfg()->hasKey(cfgname + "/replacement_policy/vip/position"))
     vipPos = Sim()->getCfg()->getInt(cfgname + "/replacement_policy/vip/position");
   else
     vipPos = 0;
}

CacheSetReconfig::~CacheSetReconfig()
{
   delete [] m_lru_bits;
}

UInt32
CacheSetReconfig::getReplacementIndex(const CacheReplacementInfo& crInfo)
{
    CacheCntlr *cntlr = crInfo.cntlr;
    
    list<UInt32> atd_validWays;
    list<UInt32>& validWays = atd_validWays;
    getReconfigData(crInfo, validWays);
  
    assert(validWays.size() > 0); //Cannot be zero

/*    std::cout <<"CacheSetReconfig::getReplacementIndex " << " validways=" << validWays.size() << "--" << crInfo;
    for (list<UInt32>::iterator start = validWays.begin(); start != validWays.end(); ++start)
    {
      std::cout << *start << ",";
    }  
    std::cout << std::endl;
*/
    // First try to find an invalid block
    for (list<UInt32>::iterator start = validWays.begin(); start != validWays.end(); ++start)
    {
        UInt32 i = *start;
        if (!m_cache_block_info_array[i]->isValid())
        {
         // Mark our newly-inserted line as most-recently used
         moveToPolicyPos(i, cntlr);
         return i;
        }
    }

    // Make m_num_attemps attempts at evicting the block at LRU position
    for(UInt8 attempt = 0; attempt < m_num_attempts; ++attempt)
    {
        UInt32 index = 0;
        UInt8 max_bits = 0;
        for (list<UInt32>::iterator start = validWays.begin(); start != validWays.end(); ++start)
        {
            UInt32 i = *start;
            if (m_lru_bits[i] > max_bits && isValidReplacement(i))
            {
                index = i;
                max_bits = m_lru_bits[i];
            }
        }
        LOG_ASSERT_ERROR(index < m_associativity, "Error Finding Reconfig LRU bits");

        bool qbs_reject = false;
        if (attempt < m_num_attempts - 1)
        {
            LOG_ASSERT_ERROR(cntlr != NULL, "CacheCntlr == NULL, QBS can only be used when cntlr is passed in");
            qbs_reject = cntlr->isInLowerLevelCache(m_cache_block_info_array[index]);
        }

        if (qbs_reject)
        {
            // Block is contained in lower-level cache, and we have more tries remaining.
            // Move this block to MRU and try again
            moveToPolicyPos(index, cntlr);
            cntlr->incrementQBSLookupCost();
            continue;
        }
        else
        {
            // Mark our newly-inserted line as most-recently used
            moveToPolicyPos(index, cntlr);
            m_set_info->incrementAttempt(attempt);
            return index;
        }
    }

    LOG_PRINT_ERROR("Should not reach here");
}

//position in LRU list
//0 means MRU
//any block with lower value is more recently used than accessed_index
UInt32 
CacheSetReconfig::getLruPosition(UInt32 accessed_index) 
{
   UInt32 pos=0;
   for (UInt32 i = 0; i < m_associativity; i++)
   {
      if (m_lru_bits[i] < m_lru_bits[accessed_index])
        pos++;
   }
   return pos;  //returning 0 means, accessed_index is MRU
}

void
CacheSetReconfig::updateReplacementIndex(UInt32 accessed_index)
{
//   cout <<"[updateReplacementIndex] accessed_index=" << accessed_index << " " << policy << endl;
   m_set_info->increment(m_lru_bits[accessed_index]);
   //update index should be to MRU  TODO
   //but this function is updatereplacementindex?? so not sure
//   moveToPolicyPos(accessed_index, NULL);
   if (policy==CacheBase::PIPP) moveToPIPP(accessed_index);
   else                         moveToMRU(accessed_index);
}

void
CacheSetReconfig::moveToPolicyPos(UInt32 accessed_index, CacheCntlr *cntlr)
{
/*  UInt32 sum=0;
  for (UInt32 i = 0; i < m_associativity; i++) sum += m_lru_bits[i];
  if (sum != 15*8) {
    cout << "moveToPolicyPos " << sum << " " << accessed_index << " -> " << getLRUVector() << endl;
    assert(0);
  }
*/
  //cout<<"[moveToPolicyPos]" <<accessed_index<<endl;

  switch(policy) {
    case CacheBase::LRU: moveToMRU(accessed_index); break;
    case CacheBase::LIP: moveToLRU(accessed_index); break;
    case CacheBase::BIP: moveToBIP(accessed_index); break;
    case CacheBase::DIP: moveToDIP(accessed_index, cntlr); break;
    case CacheBase::VIP: moveToVIP(accessed_index, cntlr); break;
    case CacheBase::PIPP: moveToPIPP(accessed_index); break;
    default:
      cout << "CacheSetReconfig::moveToPolicyPos Unknown policy=" << policy <<endl;
      assert(0);
  }
}   

void
CacheSetReconfig::moveToDIP(UInt32 accessed_index, CacheCntlr *cntlr)
{
  if(!cntlr) moveToMRU(accessed_index); //HACK: TODO incase cntlr is NULL (from updateReplacementIndex flow, then do MRU
  assert(cntlr->getDIPCircuit());
  //bool isLRU = (dynamic_cast<ParametricDramDirectoryMSI::CacheCntlr*>(cntlr))->getDIPCircuit()->isLRUPolicyOptimal();
  bool isLRU = cntlr->getDIPCircuit()->isLRUPolicyOptimal();
  //DIPCircuit* dip = (DIPCircuit*)cntlr->getDIPCircuit();
  //bool isLRU = dip->isLRUPolicyOptimal();
  //cout << "[moveToDIP] isLRU="<<isLRU<<endl;
  if(isLRU)
    moveToMRU(accessed_index);
  else
    moveToBIP(accessed_index);
}

void
CacheSetReconfig::moveToVIP(UInt32 accessed_index, CacheCntlr *cntlr)
{
  assert(cntlr);
  assert(cntlr->getDIPCircuit());
  bool isLRU = cntlr->getDIPCircuit()->isLRUPolicyOptimal();
  if(isLRU)
    moveToMRU(accessed_index);
  else
    moveToBIP(accessed_index, vipPos);
}

void
CacheSetReconfig::moveToPIPP(UInt32 accessed_index)
{
  assert(m_lru_pos<250);
  moveToLRU(accessed_index, m_lru_pos);
}

void
CacheSetReconfig::moveToBIP(UInt32 accessed_index, UInt8 lruPos)
{
  UInt8 eps = rand()%32;
  if(eps==0)  moveToMRU(accessed_index);
  else        moveToLRU(accessed_index, lruPos);
}

void
CacheSetReconfig::moveToMRU(UInt32 accessed_index)
{
   for (UInt32 i = 0; i < m_associativity; i++)
   {
      if (m_lru_bits[i] < m_lru_bits[accessed_index])
         m_lru_bits[i] ++;
   }
   m_lru_bits[accessed_index] = 0;
}

void
CacheSetReconfig::moveToLRU(UInt32 accessed_index, UInt8 lruPos)
{
   //LRU will have lruPos=0
   //example: assoc=16, accessed_index=8, lruPos=2 => move all lines with pos between 13->9 to 12->8 and move the 8 to 13
   
   moveToMRU(accessed_index); //first move the accessed_index to MRU, and then find its correct location
   for (UInt32 i = 0; i < m_associativity; i++)
   {
      if (m_lru_bits[i] <= m_associativity-1-lruPos && m_lru_bits[i] > m_lru_bits[accessed_index])
         m_lru_bits[i] --;
   }
   m_lru_bits[accessed_index] = m_associativity-1 -lruPos;
}

string
CacheSetReconfig::getLRUVector() {
   string vec=to_string(m_lru_bits[0]);
   for (UInt32 i = 1; i < m_associativity; i++)
      vec += "-" + to_string(m_lru_bits[i]);

   return vec;
}

CacheSetInfoReconfig::CacheSetInfoReconfig(String name, String cfgname, core_id_t core_id, UInt32 associativity, UInt8 num_attempts)
   : m_associativity(associativity)
   , m_attempts(NULL)
{
   m_access = new UInt64[m_associativity];
   for(UInt32 i = 0; i < m_associativity; ++i)
   {
      m_access[i] = 0;
      registerStatsMetric(name, core_id, String("access-mru-")+itostr(i), &m_access[i]);
   }

   if (num_attempts > 1)
   {
      m_attempts = new UInt64[num_attempts];
      for(UInt32 i = 0; i < num_attempts; ++i)
      {
         m_attempts[i] = 0;
         registerStatsMetric(name, core_id, String("qbs-attempt-")+itostr(i), &m_attempts[i]);
      }
   }
};

CacheSetInfoReconfig::~CacheSetInfoReconfig()
{
   delete [] m_access;
   if (m_attempts)
      delete [] m_attempts;
}
/*
void
CacheSetReconfig::setPolicyType(String cfgname)
{
   String cfgpolicy = "lru";
   if (Sim()->getCfg()->hasKey(cfgname + "/replacement_policy/reconfig/policy"))
     cfgpolicy = Sim()->getCfg()->getString(cfgname + "/replacement_policy/reconfig/policy");

   if (cfgpolicy == "lru")
      policy = ReconfigPolicy::LRU;
   else if (cfgpolicy == "lip")
      policy = ReconfigPolicy::LIP;
   else if (cfgpolicy == "bip")
      policy = ReconfigPolicy::BIP;
   else if (cfgpolicy == "dip")
      policy = ReconfigPolicy::DIP;
   else if (cfgpolicy == "vip")
      policy = ReconfigPolicy::VIP;
   else
      LOG_PRINT_ERROR("Unknown replacement policy %s", cfgpolicy.c_str());
}*/
