#ifndef CACHE_REPLACEMENT_INFO_H
#define CACHE_REPLACEMENT_INFO_H

#include <iostream>
#include <shmem_perf_model.h>
#include <core.h>
using namespace std;
//10-Oct-2015  MLM
//Replacement request information
//especially required for reconfig replacement and other complex policies we plan to investigate
class CacheReplacementInfo {
  public:
    CacheReplacementInfo()
      :cntlr(NULL), requester(-1), mem_op_type(Core::INVALID_MEM_OP), thread_num(ShmemPerfModel::_USER_THREAD)
      {}
    
    CacheReplacementInfo(CacheCntlr *_cntlr, core_id_t _requester, Core::mem_op_t _mem_op_type, ShmemPerfModel::Thread_t _thread_num )
      :cntlr(_cntlr), requester(_requester), mem_op_type(_mem_op_type), thread_num(_thread_num)
      {}

    CacheCntlr *cntlr;
    core_id_t requester;
    Core::mem_op_t mem_op_type;
    ShmemPerfModel::Thread_t thread_num;

    friend ostream &operator<<(ostream &output, const CacheReplacementInfo& crInfo)
    {
      output << "crInfo: (req=" << crInfo.requester << " op=" << crInfo.mem_op_type << " T=" << crInfo.thread_num << ")" << endl;
      return output;
    }

};

#endif
