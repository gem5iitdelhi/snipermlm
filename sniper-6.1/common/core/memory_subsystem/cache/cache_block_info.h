#ifndef __CACHE_BLOCK_INFO_H__
#define __CACHE_BLOCK_INFO_H__

#include "fixed_types.h"
#include "cache_state.h"
#include "cache_base.h"
#include "mem_component.h"
#include <map>
#include <iostream>

class CacheBlockInfo
{
   public:
      enum option_t
      {
         PREFETCH,
         WARMUP,
         NUM_OPTIONS
      };

      static const UInt8 BitsUsedOffset = 3;  // Track usage on 1<<BitsUsedOffset granularity (per 64-bit / 8-byte)
      typedef UInt8 BitsUsedType;      // Enough to store one bit per 1<<BitsUsedOffset byte element per cache line (8 8-byte elements for 64-byte cache lines)

   // This can be extended later to include other information
   // for different cache coherence protocols
   private:
      IntPtr m_tag;
      CacheState::cstate_t m_cstate;
      UInt64 m_owner;
      BitsUsedType m_used;
      UInt8 m_options;  // large enough to hold a bitfield for all available option_t's

      static const char* option_names[];

   public:
      CacheBlockInfo(IntPtr tag = ~0,
            CacheState::cstate_t cstate = CacheState::INVALID,
            UInt64 options = 0,
            UInt64 owner = -1);
      virtual ~CacheBlockInfo();

      static CacheBlockInfo* create(CacheBase::cache_t cache_type);

      virtual void invalidate(void);
      virtual void clone(CacheBlockInfo* cache_block_info);

      bool isValid() const { return (m_tag != ((IntPtr) ~0)); }

      IntPtr getTag() const { return m_tag; }
      CacheState::cstate_t getCState() const { return m_cstate; }

      void setTag(IntPtr tag) { m_tag = tag; }
      void setCState(CacheState::cstate_t cstate) { m_cstate = cstate; }

      UInt64 getOwner() const { return m_owner; }
      void setOwner(UInt64 owner) { m_owner = owner; }

      bool hasOption(option_t option) { return m_options & (1 << option); }
      void setOption(option_t option) { m_options |= (1 << option); }
      void clearOption(option_t option) { m_options &= ~(UInt64(1) << option); }

      BitsUsedType getUsage() const { return m_used; };
      bool updateUsage(UInt32 offset, UInt32 size);
      bool updateUsage(BitsUsedType used);

      static const char* getOptionName(option_t option);
      static std::map<CacheBase::cache_t, std::map<UInt64, UInt64>> cb_owners;
      static void IncrCbOwner(CacheBase::cache_t cache_type, UInt64 owner) {
        if(cb_owners.find(cache_type) == cb_owners.end()) {
          cb_owners[cache_type][owner] = 0;
        }
        if(cb_owners[cache_type].find(owner) == cb_owners[cache_type].end()) {
          cb_owners[cache_type][owner] = 0;
        }
        cb_owners[cache_type][owner]++;
     }
     static void printCbOwner() {
      for(std::map<CacheBase::cache_t, std::map<UInt64, UInt64>>::iterator s=cb_owners.begin(); s!=cb_owners.end(); ++s) {
        std::cout << "cache_type: " << s->first;
        for(std::map<UInt64, UInt64>::iterator c=s->second.begin(); c!= s->second.end(); ++c) {
          std::cout << " " << c->first << ":" << c->second;
        }
        std::cout <<std::endl;
      }
      std::cout << std::endl;
     }
};

class DIPCircuit;
class CacheCntlr
{
   public:
      virtual bool isInLowerLevelCache(CacheBlockInfo *block_info) { return false; }
      virtual void incrementQBSLookupCost() {}
      virtual void invalidateCacheWay(UInt32 way) {}   //4-June-2015 to support cache reconfig, a cache-way should be invalidated
      virtual MemComponent::component_t getCacheComponent() { //15-Sep-2015 to support cache reconfig at all levels
        return MemComponent::component_t::INVALID_MEM_COMPONENT; 
      }
      virtual DIPCircuit* getDIPCircuit()=0;
      virtual ~CacheCntlr() {}
};

#endif /* __CACHE_BLOCK_INFO_H__ */
