#include "cache_set.h"
#include "cache_set_lru.h"
#include "cache_set_mru.h"
#include "cache_set_nmru.h"
#include "cache_set_nru.h"
#include "cache_set_plru.h"
#include "cache_set_random.h"
#include "cache_set_reconfig.h"
#include "cache_set_round_robin.h"
#include "cache_set_srrip.h"
#include "cache_base.h"
#include "log.h"
#include "simulator.h"
#include "config.h"
#include "config.hpp"

CacheSet::CacheSet(CacheBase::cache_t cache_type,
      UInt32 associativity, UInt32 blocksize):
      m_associativity(associativity), m_blocksize(blocksize)
      , cacheMgr(Sim()->getCacheManager())
{
   m_cache_block_info_array = new CacheBlockInfo*[m_associativity];
   for (UInt32 i = 0; i < m_associativity; i++)
   {
      m_cache_block_info_array[i] = CacheBlockInfo::create(cache_type);
   }

   if (Sim()->getFaultinjectionManager())
   {
      m_blocks = new char[m_associativity * m_blocksize];
      memset(m_blocks, 0x00, m_associativity * m_blocksize);
   } else {
      m_blocks = NULL;
   }
}

CacheSet::~CacheSet()
{
   for (UInt32 i = 0; i < m_associativity; i++)
      delete m_cache_block_info_array[i];
   delete [] m_cache_block_info_array;
   delete [] m_blocks;
}

void
CacheSet::read_line(UInt32 line_index, UInt32 offset, Byte *out_buff, UInt32 bytes, bool update_replacement)
{
   assert(offset + bytes <= m_blocksize);
   //assert((out_buff == NULL) == (bytes == 0));

   if (out_buff != NULL && m_blocks != NULL)
      memcpy((void*) out_buff, &m_blocks[line_index * m_blocksize + offset], bytes);

   if (update_replacement)
      updateReplacementIndex(line_index);
}

void
CacheSet::write_line(UInt32 line_index, UInt32 offset, Byte *in_buff, UInt32 bytes, bool update_replacement)
{
   assert(offset + bytes <= m_blocksize);
   //assert((in_buff == NULL) == (bytes == 0));

   if (in_buff != NULL && m_blocks != NULL)
      memcpy(&m_blocks[line_index * m_blocksize + offset], (void*) in_buff, bytes);

   if (update_replacement)
      updateReplacementIndex(line_index);
}

CacheBlockInfo*
CacheSet::find(IntPtr tag, UInt32* line_index)
{
   for (SInt32 index = m_associativity-1; index >= 0; index--)
   {
      if (m_cache_block_info_array[index]->getTag() == tag)
      {
         if (line_index != NULL)
            *line_index = index;
         return (m_cache_block_info_array[index]);
      }
   }
   return NULL;
}

UInt32
CacheSet::getCoreOccupancy(core_id_t core_id)
{
//   std::cout << "occu ";
   UInt32 occupancy=0;
   for (SInt32 index = m_associativity-1; index >= 0; index--)
   {
//      std::cout << m_cache_block_info_array[index]->getOwner() << ":";
      if (m_cache_block_info_array[index]->getOwner() == core_id)
        occupancy++;
   }
//   std::cout << std::endl;
   return occupancy;
}

bool
CacheSet::invalidate(IntPtr& tag)
{
   for (SInt32 index = m_associativity-1; index >= 0; index--)
   {
      if (m_cache_block_info_array[index]->getTag() == tag)
      {
         m_cache_block_info_array[index]->invalidate();
         return true;
      }
   }
   return false;
}

void
CacheSet::insert(CacheBlockInfo* cache_block_info, Byte* fill_buff, bool* eviction, CacheBlockInfo* evict_block_info, Byte* evict_buff, const CacheReplacementInfo& crInfo)
{
   // This replacement strategy does not take into account the fact that
   // cache blocks can be voluntarily flushed or invalidated due to another write request
   const UInt32 index = getReplacementIndex(crInfo);
   assert(index < m_associativity);

   assert(eviction != NULL);

   if (m_cache_block_info_array[index]->isValid())
   {
      *eviction = true;
      // FIXME: This is a hack. I dont know if this is the best way to do
      evict_block_info->clone(m_cache_block_info_array[index]);
      if (evict_buff != NULL && m_blocks != NULL)
         memcpy((void*) evict_buff, &m_blocks[index * m_blocksize], m_blocksize);
   }
   else
   {
      *eviction = false;
   }

   // FIXME: This is a hack. I dont know if this is the best way to do
   m_cache_block_info_array[index]->clone(cache_block_info);

   if (fill_buff != NULL && m_blocks != NULL)
      memcpy(&m_blocks[index * m_blocksize], (void*) fill_buff, m_blocksize);
}

char*
CacheSet::getDataPtr(UInt32 line_index, UInt32 offset)
{
   return &m_blocks[line_index * m_blocksize + offset];
}

void
CacheSet::getData(UInt32 line_index, UInt32 offset, Byte* data_buff)
{
  if (data_buff != NULL && m_blocks != NULL)
     memcpy((void*) data_buff, &m_blocks[line_index * m_blocksize], m_blocksize);
}

CacheSet*
CacheSet::createCacheSet(String cfgname, core_id_t core_id,
      String replacement_policy,
      CacheBase::cache_t cache_type,
      UInt32 associativity, UInt32 blocksize, CacheSetInfo* set_info)
{
   CacheBase::ReplacementPolicy policy = parsePolicyType(replacement_policy);
   switch(policy)
   {
      case CacheBase::ROUND_ROBIN:
         return new CacheSetRoundRobin(cache_type, associativity, blocksize);

      case CacheBase::LRU:
      case CacheBase::LRU_QBS:
         return new CacheSetLRU(cache_type, associativity, blocksize, dynamic_cast<CacheSetInfoLRU*>(set_info), getNumQBSAttempts(policy, cfgname, core_id));

      case CacheBase::NRU:
         return new CacheSetNRU(cache_type, associativity, blocksize);

      case CacheBase::MRU:
         return new CacheSetMRU(cache_type, associativity, blocksize);

      case CacheBase::NMRU:
         return new CacheSetNMRU(cache_type, associativity, blocksize);

      case CacheBase::PLRU:
         return new CacheSetPLRU(cache_type, associativity, blocksize);

      case CacheBase::SRRIP:
      case CacheBase::SRRIP_QBS:
         return new CacheSetSRRIP(cfgname, core_id, cache_type, associativity, blocksize, dynamic_cast<CacheSetInfoLRU*>(set_info), getNumQBSAttempts(policy, cfgname, core_id));

      case CacheBase::RANDOM:
         return new CacheSetRandom(cache_type, associativity, blocksize);
      //4-June-2015 L3 cache Reconfiguration 
      case CacheBase::RECONFIG:
         return new CacheSetReconfig(CacheBase::LRU, cache_type, associativity, blocksize, dynamic_cast<CacheSetInfoReconfig*>(set_info), getNumQBSAttempts(policy, cfgname, core_id));
      //7-Nov-2015 Insertion Policies
      case CacheBase::LIP:
         return new CacheSetReconfig(CacheBase::LIP, cache_type, associativity, blocksize, dynamic_cast<CacheSetInfoReconfig*>(set_info), getNumQBSAttempts(policy, cfgname, core_id));
      case CacheBase::BIP:
         return new CacheSetReconfig(CacheBase::BIP, cache_type, associativity, blocksize, dynamic_cast<CacheSetInfoReconfig*>(set_info), getNumQBSAttempts(policy, cfgname, core_id));
      case CacheBase::DIP:
         return new CacheSetReconfig(CacheBase::DIP, cache_type, associativity, blocksize, dynamic_cast<CacheSetInfoReconfig*>(set_info), getNumQBSAttempts(policy, cfgname, core_id));
      case CacheBase::VIP:
         return new CacheSetReconfig(CacheBase::VIP, cache_type, associativity, blocksize, dynamic_cast<CacheSetInfoReconfig*>(set_info), getNumQBSAttempts(policy, cfgname, core_id));
      case CacheBase::PIPP: //29-April-2016 PIPP, MCFQ
         return new CacheSetReconfig(CacheBase::PIPP, cache_type, associativity, blocksize, dynamic_cast<CacheSetInfoReconfig*>(set_info), getNumQBSAttempts(policy, cfgname, core_id));
      default:
         LOG_PRINT_ERROR("Unrecognized Cache Replacement Policy: %i",
               policy);
         break;
   }

   return (CacheSet*) NULL;
}

CacheSetInfo*
CacheSet::createCacheSetInfo(String name, String cfgname, core_id_t core_id, String replacement_policy, UInt32 associativity)
{
   CacheBase::ReplacementPolicy policy = parsePolicyType(replacement_policy);
   switch(policy)
   {
      case CacheBase::LRU:
      case CacheBase::LRU_QBS:
      case CacheBase::SRRIP:
      case CacheBase::SRRIP_QBS:
         return new CacheSetInfoLRU(name, cfgname, core_id, associativity, getNumQBSAttempts(policy, cfgname, core_id));
      case CacheBase::RECONFIG:
      case CacheBase::LIP:
      case CacheBase::BIP:
      case CacheBase::DIP:
      case CacheBase::VIP:
      case CacheBase::PIPP:
         return new CacheSetInfoReconfig(name, cfgname, core_id, associativity, getNumQBSAttempts(policy, cfgname, core_id));
      default:
         return NULL;
   }
}

UInt8
CacheSet::getNumQBSAttempts(CacheBase::ReplacementPolicy policy, String cfgname, core_id_t core_id)
{
   switch(policy)
   {
      case CacheBase::LRU_QBS:
      case CacheBase::SRRIP_QBS:
         return Sim()->getCfg()->getIntArray(cfgname + "/qbs/attempts", core_id);
      default:
         return 1;
   }
}

CacheBase::ReplacementPolicy
CacheSet::parsePolicyType(String policy)
{
   if (policy == "round_robin")
      return CacheBase::ROUND_ROBIN;
   if (policy == "lru")
      return CacheBase::LRU;
   if (policy == "lru_qbs")
      return CacheBase::LRU_QBS;
   if (policy == "nru")
      return CacheBase::NRU;
   if (policy == "mru")
      return CacheBase::MRU;
   if (policy == "nmru")
      return CacheBase::NMRU;
   if (policy == "plru")
      return CacheBase::PLRU;
   if (policy == "srrip")
      return CacheBase::SRRIP;
   if (policy == "srrip_qbs")
      return CacheBase::SRRIP_QBS;
   if (policy == "random")
      return CacheBase::RANDOM;
   if (policy == "reconfig")
      return CacheBase::RECONFIG;
   if (policy == "lip")
      return CacheBase::LIP;
   if (policy == "bip")
      return CacheBase::BIP;
   if (policy == "dip")
      return CacheBase::DIP;
   if (policy == "vip")
      return CacheBase::VIP;
   if (policy == "pipp")
      return CacheBase::PIPP;

   LOG_PRINT_ERROR("Unknown replacement policy %s", policy.c_str());
}

bool CacheSet::isValidReplacement(UInt32 index)
{
   if (m_cache_block_info_array[index]->getCState() == CacheState::SHARED_UPGRADING)
   {
      return false;
   }
   else
   {
      return true;
   }
}

//GetValid ways from the cache manager
void 
CacheSet::getReconfigData(const CacheReplacementInfo& crInfo, list<UInt32>& validWays)
{
    CacheCntlr *cntlr = crInfo.cntlr;
    CacheLevel level = CacheLevel::LMAX;  //TODO
    if(cntlr==NULL) { //TODO   Assumption is that if cntlr is NULL then its from ATD
    //this should be fine in general as other componenets are not being reconfigured, its only caches that must be identified correctly
      level = CacheLevel::TAG_ATD;
      validWays.resize(m_associativity);
      UInt32 i=0;
      for (list<UInt32>::iterator start = validWays.begin(); start != validWays.end(); ++start) {
        *start = i++;
      }
    }
    else {
//    std::cout<< "CacheSetReconfig::getReplacementIndex mem component " << MemComponentString(cntlr->getCacheComponent()) << " " <<  std::endl;
      String config_str="";
      switch (cntlr->getCacheComponent()) {
        case MemComponent::component_t::L1_ICACHE: level = CacheLevel::L1I; config_str="perf_model/l1_icache/"; break;
        case MemComponent::component_t::L1_DCACHE: level = CacheLevel::L1D; config_str="perf_model/l1_dcache/"; break;
        case MemComponent::component_t::L2_CACHE:  level = CacheLevel::L2;  config_str="perf_model/l2_cache/"; break;
        case MemComponent::component_t::L3_CACHE:  level = CacheLevel::L3;  config_str="perf_model/l3_cache/"; break;
        default: 
          LOG_ASSERT_ERROR(false, "CacheSetReconfig::getReplacementIndex, Unhandled Cache Level");
      }
      checkAndInvalidateWays(level);
      
      validWays = cacheMgr->getValidWaysList(level, crInfo.requester, crInfo.mem_op_type);
//      std::cout << "CacheSet::getReconfigData validWays cache, core, op " << level << "," << crInfo.requester << "," << crInfo.mem_op_type
//                << " num=" << validWays.size() << std::endl;

//if(/*level == L3 &&*/ Sim()->getCfg()->getBoolDefault(config_str + "atd/rwp", false)) {
//        validWays = cacheMgr->getValidWaysList(level, crInfo.requester, crInfo.mem_op_type, CacheManager::cache_partition_t::RWP_PART);
//      } else if(/*level == L3 &&*/ Sim()->getCfg()->getBoolDefault(config_str + "atd/corerwp", false)) {
//        validWays = cacheMgr->getValidWaysList(level, crInfo.requester, crInfo.mem_op_type, CacheManager::cache_partition_t::CORE_RWP_PART);
//      } else {
        //LLC-MDP, UCP
//        validWays = cacheMgr->getValidWaysList(level, crInfo.requester, crInfo.mem_op_type, CacheManager::cache_partition_t::CORE_PART);
//      }
      //Get the LRU insertion position incase of variable insertion like PIPP MCQF
      m_lru_pos = cacheMgr->getLruPos(level, crInfo.requester, crInfo.mem_op_type);
    }
}

void CacheSet::checkAndInvalidateWays(CacheLevel level)
{
    //TODO: There is a possiblity that this function is not called between 
    //2 reconfig intervals if no call is made to getReplacementIndex
//    list<UInt32>& invalidWays = cacheMgr->getInvalidWaysList();
//    for(list<UInt32>::iterator start=invalidWays.begin(); start != invalidWays.end(); ++start) {
//       cntlr->invalidateCacheWay(*start);     TODO not invalidating reassigned caches
//    }
    cacheMgr->doneInvalidWays(level);
}

