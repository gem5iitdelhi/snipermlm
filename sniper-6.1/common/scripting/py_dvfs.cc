#include "hooks_py.h"
#include "simulator.h"
#include "dvfs_manager.h"
#include "magic_server.h"
#include <string>
#include <sstream>
#include <vector>
#include <cstdio>

std::vector<SInt32> &split(const std::string &s, char delim, std::vector<SInt32> &elems) {
    std::stringstream ss(s);
    std::string item;
    while (std::getline(ss, item, delim)) {
        elems.push_back(atoi(item.c_str()));
    }
    return elems;
}


std::vector<SInt32> split(const std::string &s, char delim) {
    std::vector<SInt32> elems;
    split(s, delim, elems);
    return elems;
}

static const ComponentPeriod * getDomain(SInt64 domain_id, bool allow_global)
{
   if (domain_id < 0) {
      if (allow_global) {
         switch(domain_id) {
            case -1:
               return Sim()->getDvfsManager()->getGlobalDomain();
            default:
               PyErr_SetString(PyExc_ValueError, "Invalid global domain ID");
               return NULL;
         }
      } else {
         PyErr_SetString(PyExc_ValueError, "Can only set core frequency, not global domains");
         return NULL;
      }
   } else {
      if (domain_id >= Sim()->getConfig()->getApplicationCores()) {
         PyErr_SetString(PyExc_ValueError, "Invalid core ID");
         return NULL;
      }else
         return Sim()->getDvfsManager()->getCoreDomain(domain_id);
   }
   return NULL;
}

static PyObject *
getFrequency(PyObject *self, PyObject *args)
{
   long int domain_id = -999;

   if (!PyArg_ParseTuple(args, "l", &domain_id))
      return NULL;

   const ComponentPeriod *domain = getDomain(domain_id, true);
   if (!domain)
      return NULL;

   UInt64 freq = 1000000000 / domain->getPeriod().getFS();

   return PyInt_FromLong(freq);
}

static PyObject *
setFrequency(PyObject *self, PyObject *args)
{
   long int core_id = -999;
   long int freq_mhz = -1;

   if (!PyArg_ParseTuple(args, "ll", &core_id, &freq_mhz))
      return NULL;

   const ComponentPeriod *domain = getDomain(core_id, false);
   if (!domain)
      return NULL;

   // We're running in a hook so we already have the thread lock, call MagicServer directly
   Sim()->getMagicServer()->setFrequency(core_id, freq_mhz);

   Py_RETURN_NONE;
}

static PyObject *
getUnCoreFrequency(PyObject *self, PyObject *args)
{
   const ComponentPeriod *domain = Sim()->getDvfsManager()->getUnCoreDomain();
   if (!domain)
      return NULL;

   UInt64 freq = 1000000000 / domain->getPeriod().getFS();
   return PyInt_FromLong(freq);
}

static PyObject *
setUnCoreFrequency(PyObject *self, PyObject *args)
{
   long int freq_mhz = -1;

   if (!PyArg_ParseTuple(args, "l", &freq_mhz))
      return NULL;

   const ComponentPeriod *domain = Sim()->getDvfsManager()->getUnCoreDomain();
   if (!domain)
      return NULL;

   // We're running in a hook so we already have the thread lock, call MagicServer directly
   //TODO: Don't know what above means, but no Hook Callback was implemented for CPU and so nothing done for UnCore
   Sim()->getMagicServer()->setUnCoreFrequency(freq_mhz);

   Py_RETURN_NONE;
}

static PyObject *
setL3CacheWayAssignment(PyObject *self, PyObject *args)
{
   const char *val=NULL;   
   if (!PyArg_ParseTuple(args, "s", &val))
      return NULL;
   
   std::vector<SInt32> wayassign = split(std::string(val), ',');
//   printf("setL3CacheWayAssignment: val=%s, size=%lu\n", val, wayassign.size());
   Sim()->getMagicServer()->setL3CacheWayAssignment(wayassign);
   return PyInt_FromLong(1);
}

static PyObject *
getL3CacheWayAssignment(PyObject *self, PyObject *args)
{
   return NULL;
}

static PyObject *
setL2CacheWayAssignment(PyObject *self, PyObject *args)
{
   const char *val=NULL;   
   if (!PyArg_ParseTuple(args, "s", &val))
      return NULL;
   
   std::vector<SInt32> wayassign = split(std::string(val), ',');
//   printf("setL2CacheWayAssignment: val=%s, size=%lu\n", val, wayassign.size());
   Sim()->getMagicServer()->setL2CacheWayAssignment(wayassign);
   return PyInt_FromLong(1);
}

static PyObject *
setL1DCacheWayAssignment(PyObject *self, PyObject *args)
{
   const char *val=NULL;   
   if (!PyArg_ParseTuple(args, "s", &val))
      return NULL;
   
   std::vector<SInt32> wayassign = split(std::string(val), ',');
//   printf("setL1DCacheWayAssignment: val=%s, size=%lu\n", val, wayassign.size());
   Sim()->getMagicServer()->setL1DCacheWayAssignment(wayassign);
   return PyInt_FromLong(1);
}

static PyObject *
setL1ICacheWayAssignment(PyObject *self, PyObject *args)
{
   const char *val=NULL;   
   if (!PyArg_ParseTuple(args, "s", &val))
      return NULL;
   
   std::vector<SInt32> wayassign = split(std::string(val), ',');
//   printf("setL1ICacheWayAssignment: val=%s, size=%lu\n", val, wayassign.size());
   Sim()->getMagicServer()->setL1ICacheWayAssignment(wayassign);
   return PyInt_FromLong(1);
}

static PyObject *
doReadWritePartition(PyObject *self, PyObject *args)
{
   printf("doReadWritePartition\n");
   Sim()->getMagicServer()->doAllLevelReadWritePartition();
   Py_RETURN_NONE;
}

static PyObject *
doUCPartition(PyObject *self, PyObject *args)
{
   const char *val=NULL;   
   if (!PyArg_ParseTuple(args, "s", &val))
      return NULL;
   
   printf("doUCPartition for %s\n", val);
   Sim()->getMagicServer()->doAllLevelUCPartition(std::string(val));
   Py_RETURN_NONE;
}

static PyObject *
doXChangeAllocation(PyObject *self, PyObject *args)
{
   printf("doXChangeAllocation\n");
   Sim()->getMagicServer()->doXChangeAllocation();
   Py_RETURN_NONE;
}

static PyObject *
doL1DShutdown(PyObject *self, PyObject *args)
{
   const char *val=NULL;   
   if (!PyArg_ParseTuple(args, "s", &val))
      return NULL;
   
   std::vector<SInt32> wayassign = split(std::string(val), ',');
   printf("doL1DShutdown: val=%s, size=%lu\n", val, wayassign.size());
   Sim()->getMagicServer()->doL1DShutdown(wayassign);
   Py_RETURN_NONE;
}

static PyMethodDef PyDvfsMethods[] = {
   {"get_frequency",  getFrequency, METH_VARARGS, "Get core or global frequency, in MHz."},
   {"set_frequency",  setFrequency, METH_VARARGS, "Set core frequency, in MHz."},
   {"get_uncore_frequency",  getUnCoreFrequency, METH_VARARGS, "Get uncore frequency, in MHz."},
   {"set_uncore_frequency",  setUnCoreFrequency, METH_VARARGS, "Set uncore frequency, in MHz."},
   {"get_l3_cacheway",  getL3CacheWayAssignment, METH_VARARGS, "CacheReconfig: Get L3 Cache-way assignment"},
   {"set_l3_cacheway",  setL3CacheWayAssignment, METH_VARARGS, "CacheReconfig: Set L3 Cache-way assignment"},
   {"set_l2_cacheway",  setL2CacheWayAssignment, METH_VARARGS, "CacheReconfig: Set L2 Cache-way assignment"},
   {"set_l1d_cacheway",  setL1DCacheWayAssignment, METH_VARARGS, "CacheReconfig: Set L1D Cache-way assignment"},
   {"set_l1i_cacheway",  setL1ICacheWayAssignment, METH_VARARGS, "CacheReconfig: Set L1I Cache-way assignment"},
   {"do_rwp",  doReadWritePartition, METH_VARARGS, "Cache RWP: Read Write based way-partition at all levels possible"},
   {"do_ucp",  doUCPartition, METH_VARARGS, "Cache UCP: UCP based way-partition at all levels possible"},
   {"do_xch",  doXChangeAllocation, METH_VARARGS, "XChange: HPCA 2015 co-allocation paper"},
   {"do_l1d_shutdown",  doL1DShutdown, METH_VARARGS, "CacheReconfig: Set L1D Cache-way shutdown"},
   {NULL, NULL, 0, NULL} /* Sentinel */
};

void HooksPy::PyDvfs::setup(void)
{
   PyObject *pModule = Py_InitModule("sim_dvfs", PyDvfsMethods);

   PyObject *pGlobalConst = PyInt_FromLong(-1);
   PyObject_SetAttrString(pModule, "GLOBAL", pGlobalConst);
   Py_DECREF(pGlobalConst);
}
