//Singleton class to dump out traces for dram power computation using DRAMPower tool
//
#ifndef __SASU_DRAMPOWER_HH__
#define __SASU_DRAMPOWER_HH__

#include <iostream>
#include <fstream>
//#include "zipstream.hpp"
using namespace std;
#include "subsecond_time.h"
#include "shmem_msg.h"

class DRAMPower {
    public:
        static DRAMPower* getInstance();
        void process(SubsecondTime, SubsecondTime, PrL1PrL2DramDirectoryMSI::ShmemMsg*);

    private:
        DRAMPower();
        ~DRAMPower();
        static DRAMPower* INSTANCE;
        static void CleanUp();
        String tracefile = "drampower.trc";
        ofstream tout;
        //zip_ostream zipper;
};
#endif
