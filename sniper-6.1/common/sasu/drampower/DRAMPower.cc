#include "DRAMPower.hh"
#include "cstdlib"
#include "simulator.h"
#include "mem_component.h"

DRAMPower* DRAMPower::INSTANCE;

DRAMPower::DRAMPower() {
    String tracepath = Sim()->getConfig()->formatOutputFileName(tracefile);
    tout.open (tracepath.c_str());
    atexit(&CleanUp);
}

DRAMPower :: ~DRAMPower() {
    tout.close();
}

void DRAMPower::CleanUp() {
    delete INSTANCE;
    INSTANCE = 0;
}

DRAMPower* DRAMPower::getInstance() {
    static bool create = true;
    if(create) {
        DRAMPower::INSTANCE = new DRAMPower();
        create = false;
    }
    return DRAMPower::INSTANCE;
}
        
void DRAMPower::process(SubsecondTime timestamp, SubsecondTime latency, PrL1PrL2DramDirectoryMSI::ShmemMsg* msg ) {
    return;
    PrL1PrL2DramDirectoryMSI::ShmemMsg::msg_t rw = msg->getMsgType();
    IntPtr addr = msg->getAddress();
    string req = (PrL1PrL2DramDirectoryMSI::ShmemMsg::DRAM_READ_REQ == rw) ? "R" : "W";
    tout << std::dec << timestamp.getNS() << ","
         << std::dec << latency.getNS() << ","
         << req << "," 
/*         << "C" << msg->getRequester() <<","
         << "L" << msg->getDataLength() << ","
         << "S=" << MemComponentString(msg->getSenderMemComponent()) << ","
         << "R=" << MemComponentString(msg->getReceiverMemComponent()) << ","
*/         << "0x" << std::hex << addr << ","
         << endl;
}
