#include "system_optimizer.hh"
#include "cstdlib"

SystemOptimizer* SystemOptimizer::INSTANCE;

SystemOptimizer::SystemOptimizer() {
    atexit(&CleanUp);
}

void SystemOptimizer::CleanUp() {
    delete INSTANCE;
    INSTANCE = 0;
}

SystemOptimizer::~SystemOptimizer() {
}

SystemOptimizer* SystemOptimizer::getInstance() {
    static bool create = true;
    if(create) {
        SystemOptimizer::INSTANCE = new SystemOptimizer();
        create = false;
    }
    return SystemOptimizer::INSTANCE;
}
