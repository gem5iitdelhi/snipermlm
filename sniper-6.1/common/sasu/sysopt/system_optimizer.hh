#ifndef __SYSTEM_OPTIMIZER_HH__
#define __SYSTEM_OPTIMIZER_HH__
#include "sasu_type.h"

class SystemOptimizer {
    public:
        static SystemOptimizer* getInstance();
        ~SystemOptimizer();
    private:
        static SystemOptimizer* INSTANCE;
        static void CleanUp();
        SystemOptimizer();
};

#endif
