#ifndef __SASU_HH__
#define __SASU_HH__
#include "system_optimizer.hh"
#include "DRAMPower.hh"

//System Optimization Unit SOU
//Self Adaptation System Unit SASU
class SASU {
    public:
    static SystemOptimizer* getSASU() {
        return SystemOptimizer::getInstance();
    }

    static DRAMPower* getDRAMPower() {
        return DRAMPower::getInstance();
    }
};
#endif
