#ifndef __XCHANGE_MANAGER_H
#define __XCHANGE_MANAGER_H

/*
XChange: A Market-based Approach to Scalable Dynamic Multi-resource Allocation in Multicore Architectures, HPCA 2015
*/

#include "subsecond_time.h"
#include <vector>
#include <list>

#include "core.h"
#include "cache_atd.h"
#include "delta_stats.h"


using namespace std;
class XChangeAgent;
class CacheReconfigInfo;

struct AgentDemands {
  AgentDemands():core_dvfs_idx(0), llc_ways(1) {}
  AgentDemands(UInt32 idx, UInt32 ways):core_dvfs_idx(idx), llc_ways(ways) {}
  UInt32 core_dvfs_idx;
  UInt32 llc_ways;
};

class XChange {
  public:
    XChange(UInt32 np);
    void initXChange(vector<ATD*> atds, CacheReconfigInfo* caches);
    Core2CacheSetWayT getResourceAllocation(vector<float> mlpdata);
  
  private:
    UInt32 m_np;
//    CacheReconfigInfo* m_caches;
    DeltaStats* deltastats;
    UInt32 m_numLLC;
    UInt64 TOTAL_AGENT_BUDGET;  

    float frequency_price;
    float cache_price;
    float max_frequency_resource;
    float max_cache_resource;
    map<int, float> dvfs_table;
    vector<int>  dvfs_idx;
    float prev_qdelay;
    void initDvfsTable();
    
    std::vector<ATD*> getATDs(CacheReconfigInfo* caches);
    void initMetrics();
    void doWealthDistribution();
    void getAgentDemands(AgentDemands*);
    void allocateResources(AgentDemands*);
    void doUncoreDvfs();
    bool hasPriceConverged(float fprice, float curr_fprice, vector<float>& cprice, vector<float>& curr_cprice);

    friend XChangeAgent;
    vector<XChangeAgent*> core_dvfs_players;
    vector<vector<XChangeAgent*>> llc_players;
};

class XChangeAgent {
  public:
    XChangeAgent(core_id_t c, ATD* _atd, DeltaStats* d, XChange* market);
    void setMLP(float mlp) { m_mlp = mlp; }

  private:
    core_id_t core_id;
    ATD* m_atd;
    DeltaStats* deltastats;
    XChange* m_market;
    float m_mlp;
    double MAX_BUDGET;
    double CacheUtility(int a);
    double CacheMarginalUtility(int a, int b);
    double getMemPhase(int a);
    double EDPMarginalUtility(UInt64 f, UInt64 df, double tmem);
    double EDPUtility(UInt64 f, double tmem, bool perf_penalty=true);

    double getPotential();
    AgentDemands getResourceDemands(float freq_price, float llc_price);
    double HillClimbSearchUtility(AgentDemands soln, bool perf_penalty=true);

    friend XChange;
};
#endif /* __XCHANGE_MANAGER_H */
