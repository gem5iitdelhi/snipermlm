#ifndef __CACHE_MANAGER_H
#define __CACHE_MANAGER_H

#include "subsecond_time.h"
#include <vector>
#include <list>

#include <core.h>

class RWPCircuit;
class ATD;
class Umon;
class XChange;

using namespace std;

//3-June-2015 Adding Cache Reconfiguration
typedef std::vector<core_id_t> Core2CacheSetWayT;
enum CacheLevel { L1I=0, L1D, L2, L3, TAG_ATD, LMAX };
class CacheCntlr;

//this stores the cache reconfig information
//each cache level L1-D, L1-I, L2, L3 etc have a single such object,
//irrespective of the number of each cache in the system
class CacheReconfigInfo {
public:
    CacheReconfigInfo(){
       m_ucp_enabled = true;  //enabled by default, if atds not found then disabled later
       for(int i=0;i<3;i++) m_rw_access[i]=0;
       reassignedWays = list<UInt32>();
    }
    ~CacheReconfigInfo(){
      std::cout << "~CacheReconfigInfo: data access info at cache level " << m_level << std::endl;
      std::cout<< m_rw_access[0] << ","<<m_rw_access[1] << "," << m_rw_access[2] << std::endl;
    };
    void initialize(CacheLevel l, UInt32 m_num_app_cores);
    
    enum cache_partition_t {
      NO_PART,    //No partition 
      WAY_SHUTDOWN,   //some ways can be shutdown
      CORE_PART,  //partition among cores
      XCH_PART,   //XChange based LLC partitioning
      RWP_PART,   //read write partition
      CORE_RWP_PART, //read/write partition among cores
      RWS_PART    //Read Write Shutdown partition, not core-wise but data property wise
    };
    
    UInt8 getLruPos(core_id_t core_id, Core::mem_op_t mem_op_type) { return m_lru_positions[core_id];}
    list<UInt32>& getValidWaysList(core_id_t core_id);
    list<UInt32>& getRWPValidWaysList(core_id_t core_id, Core::mem_op_t mem_op_type);
    list<UInt32>& getRWSValidWaysList(core_id_t core_id, Core::mem_op_t mem_op_type);
    list<UInt32>& getCoreRWPValidWaysList(core_id_t core_id, Core::mem_op_t mem_op_type);
    list<UInt32>& getInvalidWaysList() {return reassignedWays;}
    void doneInvalidWays() { /*reassignedWays.clear();*/}
    
    void setWayAssignment(Core2CacheSetWayT& assign);
    Core2CacheSetWayT& getWayAssignment() { return wayAssignment; }
    
    UInt32 numSharedCores() { return m_sharedcores; }
    UInt32 associativity() { return m_assoc; }
private:
    CacheLevel m_level;
    String m_config_str;
    MemComponent::component_t m_mem_component;
    UInt32 m_num_app_cores;
    UInt32 m_num;   //total number of this cache in the system
    UInt32 m_assoc;
    UInt32 m_sharedcores;
    UInt32 m_min_sharedways;
    UInt32 m_max_excl_ways;
//    bool m_rwp;
//    bool m_core_rwp;
    cache_partition_t m_cache_optimization;
    bool m_ucp_enabled;
    UInt64 m_rw_access[3];
    enum CacheReconfigWayT {
      SHUTDOWN=50000, //way is shutdown
      SHARED_WAY=-1,
      SHARED_READ=-2,
      SHARED_WRITE=-3,

      EXCL_READ=1000, //Exl are the base numbers i.e 1001 represents EX Read way for Core 1 etc
      EXCL_WRITE=2000,

      //RWS OPT
      RWS_RW=0,
      RWS_R=1,
      RWS_SD=2
    };
    const core_id_t SharedWay = CacheReconfigWayT::SHARED_WAY;
  
    enum AppCachePropT {
      //This ordering decides the lru positions
      CACHE_THRASH=0,
      CACHE_FRIEND=2,
      CACHE_FIT=3,
      MAX
    };

    Core2CacheSetWayT     wayAssignment;
    vector<list<UInt32>>  cleanNdirtyWays;
    list<UInt32>          reassignedWays;
    vector<list<UInt32>>  core2CacheWays;
    vector<list<UInt32>>  dirtyCore2CacheWays;
    vector<list<UInt32>>& cleanCore2CacheWays=core2CacheWays; //reusing
    vector<UInt8>         m_lru_positions;
    vector<AppCachePropT> m_core_cache_property; 
    vector<double>        m_core_cache_occupancy;
    vector<double>        m_core_miss_overflow; //CacheStack: compute the additional misses which overflow
//    void initialize(); 
    void setMemComponent(CacheLevel l);
    void setCachePartitionOpt();
    void computeValidWays(); 
    void computeRWPValidWays();
    void computeRWSValidWays();
    void computeReconfigValidWays(); 
    void computeCoreRWPValidWays();
    void computeCoreRWSValidWays();
    void printAllocation();
    bool invalidateReassignedWays(Core2CacheSetWayT& assign);

    friend class CacheManager;
    friend class XChange;
};

class CacheManager
{
public:
    CacheManager();
    Core2CacheSetWayT& getWayAssignment(CacheLevel level) { 
        return caches[level].getWayAssignment(); 
    }
    list<UInt32>& getValidWaysList(CacheLevel level, core_id_t core_id, Core::mem_op_t mem_op_type);
    UInt8 getLruPos(CacheLevel level, core_id_t core_id, Core::mem_op_t mem_op_type);
    list<UInt32>& getInvalidWaysList(CacheLevel level) {return caches[level].reassignedWays;}
    void doneInvalidWays(CacheLevel level) { caches[level].doneInvalidWays();}
    std::vector<ATD*> getATDs(CacheLevel level);
    void initXChange();
protected:
   // Make sure all reconfig updates pass through the correct path
   void setWayAssignment(CacheLevel level, Core2CacheSetWayT& assign);
   void doReadWritePartition();
   void doUCPartition(std::string opt);
   void doCacheWayShutdown(CacheLevel level, Core2CacheSetWayT& assign, 
    CacheReconfigInfo::CacheReconfigWayT shutdown=CacheReconfigInfo::CacheReconfigWayT::SHUTDOWN); //shutdown the cache ways marked shutdown
   void doXChangeAllocation();
   friend class MagicServer;

private:
   CacheReconfigInfo caches[CacheLevel::LMAX];
   UInt32 m_num_app_cores;
   bool m_enable_occupancy_profiler;
   bool m_enable_concurrency_profiler;
   vector<double>        m_mshr_concurrency;
   SubsecondTime m_transition_latency;

   XChange *m_xchange_manager;

   void doReadWritePartition(CacheLevel level);
   Core2CacheSetWayT getReadWriteWayPartition(RWPCircuit* rwp, UInt32 associativity);
   
   void doUCPartition(CacheLevel level, std::string opt);
   Core2CacheSetWayT getUCWayPartition(UInt8* allocations, UInt32 associativity, core_id_t master_core_id, UInt32 shared_cores, std::string opt) ;
   void getUtilityPartitionSizes(std::vector<ATD*>& atds, CacheLevel level, UInt8* allocations, std::string opt);
   double getMaxMarginalUtility(Umon* u, UInt32 alloc, UInt32 balance, UInt32& reqWays, CacheLevel level, string opt);
   void updateConcurrencyFactorAndReset(core_id_t, UInt32);
   double getConcurrencyFactor(core_id_t core_id);
   double getAvgOccupancy(core_id_t c, CacheLevel level);
   void setLruPositions(UInt8* allocations, std::vector<ATD*>& atds, CacheLevel level, core_id_t master_core_id, std::string opt);
   void setCoreCacheProperty(CacheLevel level, std::vector<ATD*>& atds, core_id_t master_core_id);
   void setMissOverflow(Core2CacheSetWayT& wayAssignment, std::vector<ATD*>& atds, CacheLevel level, core_id_t master_core_id, std::string opt);
   void resetCircuits(std::vector<ATD*>& atds);
};

#endif /* __CACHE_MANAGER_H */
