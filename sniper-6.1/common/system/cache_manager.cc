#include <cassert>

#include "cache_manager.h"
#include "simulator.h"
#include "core_manager.h"
#include "core.h"
#include "performance_model.h"
#include "instruction.h"
#include "log.h"
#include "config.hpp"
#include "../core/memory_subsystem/parametric_dram_directory_msi/memory_manager.h"
#include "cache_cntlr.h"
#include <limits.h>

#include <cache_atd.h>
#include "onur_rwp.h"
#include "xchange_manager.h"

#include <map>
#include <algorithm> // for copy
#include <iterator> // for ostream_iterator

/* MLM Project
This is a cache manager required for managing dynamic reconfiguration
*/
CacheManager::CacheManager()
{
//  return;
   //LOG_ASSERT_WARNING(false, "RETURNING FROM CACHE MANAGER WITHOUT DOING ANYTHING"); return;
   
   m_transition_latency = SubsecondTime::NS() * Sim()->getCfg()->getInt("dvfs/transition_latency");
   m_num_app_cores = Config::getSingleton()->getApplicationCores();
  
   m_enable_occupancy_profiler = Sim()->getCfg()->getBoolDefault("perf_model/core/occupancy", false); 
   m_enable_concurrency_profiler = Sim()->getCfg()->getBoolDefault("perf_model/core/concurrency", false);
   m_mshr_concurrency.resize(m_num_app_cores, 1);
   
   for(int i=0; i<CacheLevel::LMAX; ++i) {
        CacheLevel level = (CacheLevel)i;
        switch(i) {
            case CacheLevel::TAG_ATD: 
            { //place holder for ATD  HACKY: TODO
              break;
            }
            default:
              caches[level].initialize(level, m_num_app_cores);
        }
   }
  
   //XChange: 6-Aug-2016
   //TODO: only for L3 currently
   if (caches[L3].m_cache_optimization == CacheReconfigInfo::XCH_PART) {
     assert(m_enable_concurrency_profiler == true); //ensure mlp data collection is on
     m_xchange_manager = new XChange(m_num_app_cores);
   } else {
     m_xchange_manager = NULL;
   }
}

void CacheReconfigInfo::setMemComponent(CacheLevel l) {
  UInt32 smt = Sim()->getCfg()->getInt("perf_model/core/logical_cpus");
  m_level = l;
  switch(l) {
      case CacheLevel::L1I:
      {
         m_config_str="perf_model/l1_icache/";
         m_mem_component=MemComponent::L1_ICACHE;
         m_sharedcores = Sim()->getCfg()->getInt(m_config_str + "shared_cores")*smt;
      }
          break;
      case CacheLevel::L1D:
      {
         m_config_str="perf_model/l1_dcache/";
         m_mem_component=MemComponent::L1_DCACHE;
         m_sharedcores = Sim()->getCfg()->getInt(m_config_str + "shared_cores")*smt;
      }
         break;
      case CacheLevel::L2:
      {
         m_config_str="perf_model/l2_cache/";
         m_mem_component=MemComponent::L2_CACHE;
         m_sharedcores = Sim()->getCfg()->getInt(m_config_str + "shared_cores")*smt;
      }
         break;
      case CacheLevel::L3: 
      {
         m_config_str="perf_model/l3_cache/";
         m_mem_component=MemComponent::L3_CACHE;
         //L3 does not know about smt, these shared cores value includes smt cores
         m_sharedcores = Sim()->getCfg()->getInt(m_config_str + "shared_cores"); 
      }
         break;
      case CacheLevel::TAG_ATD: 
      { //place holder for ATD  HACKY: TODO
        break;
      }
      default:
         LOG_ASSERT_ERROR(false, "Unsupported Reconfig Cache Level");
  }
  
}

void CacheReconfigInfo::setCachePartitionOpt() {
  m_cache_optimization = NO_PART;
  String opt_config = m_config_str + "opt";
  if (!Sim()->getCfg()->hasKey(opt_config)) return;

  String opt = Sim()->getCfg()->getString(opt_config);
  if (opt == "nopart")  m_cache_optimization = NO_PART;
  else if (opt == "rwp")  m_cache_optimization = RWP_PART;
  else if (opt == "corerwp")  m_cache_optimization = CORE_RWP_PART;
  else if (opt == "corepart")  m_cache_optimization = CORE_PART; //UCP, MLM
  else if (opt == "rws")  m_cache_optimization = RWS_PART;
  else if (opt == "xchange")  m_cache_optimization = XCH_PART;
}

void CacheReconfigInfo::initialize(CacheLevel l, UInt32 num_cores)
{
   setMemComponent(l);
   m_num_app_cores = num_cores;
   m_assoc = Sim()->getCfg()->getInt(m_config_str + "associativity");
   m_num = 1 + ((m_num_app_cores-1)/m_sharedcores);
   m_min_sharedways = Sim()->getCfg()->hasKey(m_config_str + "min_shared_ways") ? Sim()->getCfg()->getInt(m_config_str + "min_shared_ways") : m_assoc;
   m_max_excl_ways = m_assoc - m_min_sharedways;
   printf("CacheReconfigInfo CacheLevel=%d, #Cores=%d, #Caches=%d, #SharedCores=%d, A=%d, min_shared=%d, max_excl=%d\n", 
                                l,    m_num_app_cores, m_num,      m_sharedcores,m_assoc, m_min_sharedways, m_max_excl_ways);
   
   m_lru_positions.resize(m_num_app_cores, m_assoc-1);
   m_core_cache_property.resize(m_num_app_cores);
   m_core_cache_occupancy.resize(m_num_app_cores, 0.0);
   m_core_miss_overflow.resize(m_num_app_cores, 1.0);

   setCachePartitionOpt();

   //initially all ways are shared
   if (m_cache_optimization == RWP_PART) {
     wayAssignment.resize(m_num*m_assoc, CacheReconfigWayT::SHARED_READ);
     for(UInt32 i=0; i<m_num; i++)
      for(UInt32 j=0; j<m_assoc/2; j++)
        wayAssignment[i*m_assoc+j] = CacheReconfigWayT::SHARED_WRITE;
     
   } else if (m_cache_optimization == CORE_RWP_PART) {
     wayAssignment.resize(m_num*m_assoc, SharedWay);
     //initialize the per core valid ways
     cleanCore2CacheWays.resize(m_num_app_cores);
     dirtyCore2CacheWays.resize(m_num_app_cores);

   } else if (m_cache_optimization == RWS_PART) {
     wayAssignment.resize(m_num*m_assoc, CacheReconfigWayT::RWS_RW);
     
   } else {
     //Initial Cache Assignment
     wayAssignment.resize(m_num*m_assoc, SharedWay);
     //assert(m_max_excl_ways % m_sharedcores == 0);    //We are being strict so that each core gets same cache to start with, need not be
     UInt32 fair_share = m_max_excl_ways/m_sharedcores;
     for(UInt32 i=0; i<m_num; ++i) {
        core_id_t master_core_id=i*m_sharedcores;
        for(UInt32 j=m_min_sharedways; j<m_min_sharedways+fair_share*m_sharedcores; j++) {
          UInt32 idx = i*m_assoc + j;
          wayAssignment[idx] = master_core_id + (j-m_min_sharedways)/fair_share;
        }
     }
     //initialize the per core valid ways
     core2CacheWays.resize(m_num_app_cores);
   }
   
   //initialize the clean and dirty partitions, with last partition being all ways
   cleanNdirtyWays.resize(2*m_num+1); //clean (0,2,4..) and dirty(1,3,5...) per cache
   for(UInt32 i=0; i<m_num*m_assoc; ++i) {
        cleanNdirtyWays[2*m_num].push_back(i%m_assoc);
   }

   computeValidWays();
}

void CacheReconfigInfo::computeValidWays()
{
    if (m_cache_optimization == RWP_PART)
      computeRWPValidWays();
    else if (m_cache_optimization == CORE_RWP_PART)
      computeCoreRWPValidWays();
    else if (m_cache_optimization == RWS_PART)
      computeCoreRWSValidWays();
    else
      computeReconfigValidWays();
    
    printAllocation();
}

void CacheReconfigInfo::computeCoreRWPValidWays()
{
    for(UInt32 c=0; c<m_num_app_cores; ++c) {
        cleanCore2CacheWays[c].clear();
        dirtyCore2CacheWays[c].clear();
    }

    for(UInt32 i=0; i<wayAssignment.size(); ++i) {
        int cacheNum=i/m_assoc;
        core_id_t assigned_core = wayAssignment[i];
        for(UInt32 c=cacheNum*m_sharedcores; c<(cacheNum+1)*m_sharedcores; ++c) {
            if(assigned_core == SharedWay) {
                cleanCore2CacheWays[c].push_back(i%m_assoc);
                dirtyCore2CacheWays[c].push_back(i%m_assoc);
            }
            else if ((UInt32)assigned_core == c+CacheReconfigWayT::EXCL_READ)
                cleanCore2CacheWays[c].push_front(i%m_assoc);
            else if ((UInt32)assigned_core == c+CacheReconfigWayT::EXCL_WRITE)
                dirtyCore2CacheWays[c].push_front(i%m_assoc);
        }
    }
}

void CacheReconfigInfo::computeRWPValidWays()
{
    for(UInt32 n=0; n<m_num; ++n) {
        cleanNdirtyWays[2*n].clear();
        cleanNdirtyWays[2*n+1].clear();
    }
    
    for(UInt32 i=0; i<wayAssignment.size(); ++i) {
        int cacheNum=i/m_assoc;
        if(wayAssignment[i] == CacheReconfigWayT::SHARED_READ) {
          cleanNdirtyWays[2*cacheNum].push_back(i%m_assoc);
        }
        else if(wayAssignment[i] == CacheReconfigWayT::SHARED_WRITE)
        {
          cleanNdirtyWays[2*cacheNum+1].push_back(i%m_assoc);
        }
        else {
          std::cout << "CacheReconfigInfo::computeRWPValidWays CacheReconfigWayT= " << wayAssignment[i] << std::endl;
          assert(0);
        }
    }
}

void CacheReconfigInfo::computeCoreRWSValidWays()
{
    for(UInt32 n=0; n<m_num; ++n) {
        cleanNdirtyWays[2*n].clear();
        cleanNdirtyWays[2*n+1].clear();
    }
    
    for(UInt32 i=0; i<wayAssignment.size(); ++i) {
        int cacheNum=i/m_assoc;
        if(wayAssignment[i] == CacheReconfigWayT::RWS_R) {
          cleanNdirtyWays[2*cacheNum].push_back(i%m_assoc);
        }
        else if(wayAssignment[i] == CacheReconfigWayT::RWS_RW)
        { //RW cache way should participate in read and write requests
          cleanNdirtyWays[2*cacheNum].push_back(i%m_assoc);
          cleanNdirtyWays[2*cacheNum+1].push_back(i%m_assoc);
        }
        else if(wayAssignment[i] == CacheReconfigWayT::RWS_SD) 
        { //shutdown way should not participate in any operation
          std::cout << "CacheReconfigInfo::computeRWSValidWays SHUTDOWN way " << i << std::endl; 
        }
        else {
          std::cout << "CacheReconfigInfo::computeRWSValidWays CacheReconfigWayT= " << wayAssignment[i] << std::endl;
          assert(0);
        }
    }

}

void CacheReconfigInfo::computeReconfigValidWays()
{
    for(UInt32 c=0; c<m_num_app_cores; ++c) {
        core2CacheWays[c].clear();
    }

    for(UInt32 i=0; i<wayAssignment.size(); ++i) {
        int cacheNum=i/m_assoc;
        core_id_t assigned_core = wayAssignment[i];
        for(UInt32 c=cacheNum*m_sharedcores; c<(cacheNum+1)*m_sharedcores; ++c) {
            if(assigned_core == SharedWay)
                core2CacheWays[c].push_back(i%m_assoc);
            else if ((UInt32)assigned_core == c)
                core2CacheWays[c].push_front(i%m_assoc);
        }
    }
}

void CacheReconfigInfo::printAllocation()
{
    if (m_level==L1D || m_level==L1I) return;
    printf("wayAssignment (Cache%d): ", m_level);
    for(UInt32 i=0; i<wayAssignment.size(); ++i)
        printf("%d ,", wayAssignment[i]);
    printf("\n");

    printf("Valid Cache%d Ways: \n", m_level);
    if(m_cache_optimization == RWP_PART 
      || m_cache_optimization == RWS_PART) {
      for(UInt32 c=0; c<2*m_num; ++c) {
          printf("%s: ", c%2==0 ? "Clean" : "Dirty");
          for(list<UInt32>::iterator start=cleanNdirtyWays[c].begin(); start != cleanNdirtyWays[c].end(); ++start)
              printf("%d,", (*start));
          printf("\n");
      }
    } else if(m_cache_optimization == CORE_RWP_PART) {
      for(UInt32 c=0; c<m_num_app_cores; ++c) {
          printf("Core%d: Clean ", c);
          for(list<UInt32>::iterator start=cleanCore2CacheWays[c].begin(); start != cleanCore2CacheWays[c].end(); ++start)
              printf("%d,", (*start));
          printf("\n");
          printf("Core%d: Dirty ", c);
          for(list<UInt32>::iterator start=dirtyCore2CacheWays[c].begin(); start != dirtyCore2CacheWays[c].end(); ++start)
              printf("%d,", (*start));
          printf("\n");
      }
    } else {
      for(UInt32 c=0; c<m_num_app_cores; ++c) {
          printf("Core%d: ", c);
          for(list<UInt32>::iterator start=core2CacheWays[c].begin(); start != core2CacheWays[c].end(); ++start)
              printf("%d,", (*start));
          printf("\n");
      }
    }
  
}

void CacheReconfigInfo::setWayAssignment(Core2CacheSetWayT& assign)
{
    assert(wayAssignment.size() == assign.size());

    //TMP HACK
    if ( m_level==L2 && Sim()->getCfg()->hasKey("perf_model/l2_cache/staticP") ) {
     UInt32 P = Sim()->getCfg()->getInt("perf_model/l2_cache/staticP");
      
      assert(m_num==1);
      for(unsigned int i=0;i<assign.size();i++) {
        if(i<2) assign[i]=SharedWay;
        else if(i<P) assign[i] = 0; //core 0
        else assign[i] = 1; //core 1
      }
    }

    if(invalidateReassignedWays(assign)) {
        std::copy(assign.begin(), assign.end(), wayAssignment.begin() ); 
        computeValidWays();
    }
}

bool CacheReconfigInfo::invalidateReassignedWays(Core2CacheSetWayT& assign)
{
    //assert(reassignedWays.empty()); //must be empty
    if (!reassignedWays.empty()) { //must be empty 
        //TODO: reassigned ways are invalidated and cleared from this list
        //only at getReplacementIndex. If no call is made then this list
        //can be non-empty. Not a concern right now since we are anyway
        //not invalidating ways in the current approach
        printf("CacheReconfigInfo::invalidateReassignedWays -> non-empty reassignedWays %lu\n", reassignedWays.size());
        reassignedWays.clear();
    }
    for(UInt32 i=0; i<wayAssignment.size(); ++i) {
        if(wayAssignment[i] != assign[i]){
            reassignedWays.push_back(i);
        }
    }
    //same assignment as current assignment, nothing to be done
    //return !reassignedWays.empty();
    return true;    //to print the allocation after every interval
}

void CacheManager::setWayAssignment(CacheLevel level, Core2CacheSetWayT& assign)
{
/*  std::cout << "CacheManager::setWayAssignment for Cache " << level
            << " assign ";
  std::copy(assign.begin(), assign.end(), std::ostream_iterator<core_id_t>(std::cout, " "));            
  std::cout << std::endl;*/
  if (caches[level].m_cache_optimization == CacheReconfigInfo::RWS_PART) {
    doCacheWayShutdown(level, assign, CacheReconfigInfo::CacheReconfigWayT::RWS_SD);
  }
  caches[level].setWayAssignment(assign);
}

// core_id, 0-indexed
list<UInt32>& CacheManager::getValidWaysList(CacheLevel level, core_id_t core_id, Core::mem_op_t mem_op_type)
{
  switch(caches[level].m_cache_optimization) {
    case CacheReconfigInfo::NO_PART:
    case CacheReconfigInfo::CORE_PART:
    case CacheReconfigInfo::XCH_PART:
      return caches[level].getValidWaysList(core_id);
    case CacheReconfigInfo::RWP_PART:
      return caches[level].getRWPValidWaysList(core_id, mem_op_type);
    case CacheReconfigInfo::CORE_RWP_PART:
      return caches[level].getCoreRWPValidWaysList(core_id, mem_op_type);
    case CacheReconfigInfo::RWS_PART:
      return caches[level].getRWSValidWaysList(core_id, mem_op_type);
    default:
      assert(0);
  }
}

list<UInt32>& CacheReconfigInfo::getValidWaysList(core_id_t core_id)
{
    return core2CacheWays[core_id];
}

list<UInt32>& CacheReconfigInfo::getRWPValidWaysList(core_id_t core_id, Core::mem_op_t mem_op_type)
{
    UInt32 cache_num = core_id/m_sharedcores;
    if(mem_op_type==Core::WRITE) {//dirty ways
      m_rw_access[0]++;
      return cleanNdirtyWays[2*cache_num+1];
    } else if (mem_op_type==Core::IREAD || mem_op_type==Core::READ || mem_op_type==Core::READ_EX) {
      m_rw_access[1]++;
      return cleanNdirtyWays[2*cache_num]; 
    } else {
      m_rw_access[2]++;
      return cleanNdirtyWays[2*m_num]; 
    }
}

list<UInt32>& CacheReconfigInfo::getRWSValidWaysList(core_id_t core_id, Core::mem_op_t mem_op_type)
{
    UInt32 cache_num = core_id/m_sharedcores;
    if(mem_op_type==Core::WRITE) {//dirty ways
      m_rw_access[0]++;
      return cleanNdirtyWays[2*cache_num+1];
    } else if (mem_op_type==Core::IREAD || mem_op_type==Core::READ || mem_op_type==Core::READ_EX) {
      m_rw_access[1]++;
      return cleanNdirtyWays[2*cache_num]; 
    } else {
      m_rw_access[2]++;
      std::cout << "CacheReconfigInfo::getRWSValidWaysList optype " << mem_op_type << std::endl;
      assert(0);
      return cleanNdirtyWays[2*m_num]; 
    }
}

list<UInt32>& CacheReconfigInfo::getCoreRWPValidWaysList(core_id_t core_id, Core::mem_op_t mem_op_type)
{
//    std::cout << "CacheReconfigInfo::getCoreRWPValidWaysList core,op " << core_id << " " << mem_op_type <<std::endl;
    if(mem_op_type==Core::WRITE) {//dirty ways
//      std::cout <<"Dirty ways " <<  cleanNdirtyWays[2*cache_num+1].size() << std::endl;
      return dirtyCore2CacheWays[core_id]; 
    } else if (mem_op_type==Core::IREAD || mem_op_type==Core::READ || mem_op_type==Core::READ_EX) {
//      std::cout <<"Clean ways " <<  cleanNdirtyWays[2*cache_num].size() << std::endl;
      return cleanCore2CacheWays[core_id]; 
    } else {
      return cleanNdirtyWays[2*m_num];  //pass the default partition of half dirty and half clean
    }
}


UInt8 CacheManager:: getLruPos(CacheLevel level, core_id_t core_id, Core::mem_op_t mem_op_type)
{
    return caches[level].getLruPos(core_id, mem_op_type);
}

void CacheManager::doReadWritePartition() {
  doReadWritePartition(CacheLevel::L1I);
  doReadWritePartition(CacheLevel::L1D);
  doReadWritePartition(CacheLevel::L2);
  doReadWritePartition(CacheLevel::L3);
}

void CacheManager::doReadWritePartition(CacheLevel level)
{
    std::cout << "Calling CacheManager::doReadWritePartition for Cache" << level << std::endl;
    Core2CacheSetWayT allCacheWayAssignment;
    UInt32 num_caches = caches[level].m_num;
    UInt32 shared_cores = caches[level].m_sharedcores;
    UInt32 associativity = caches[level].m_assoc;
    for(UInt32 i=0; i<num_caches; i++) {
      core_id_t master_core_id=i*shared_cores;
      Core* core = Sim()->getCoreManager()->getCoreFromID(master_core_id);
      MemoryManagerBase *mem_manager_base = core->getMemoryManager();
      ParametricDramDirectoryMSI::MemoryManager *mem_manager = (ParametricDramDirectoryMSI::MemoryManager *)(mem_manager_base);
      ParametricDramDirectoryMSI::CacheCntlr* cache_cntlr = mem_manager->getCacheCntlrAt(master_core_id, caches[level].m_mem_component);
     
      //get cache partition for current cache
      RWPCircuit* rwp = cache_cntlr->getRWPCircuit();
      if(!rwp) return;  //TODO no rwp required
      Core2CacheSetWayT wayPartition = getReadWriteWayPartition(rwp, associativity);
      
      //append to a single vector
      allCacheWayAssignment.insert(allCacheWayAssignment.end(), wayPartition.begin(), wayPartition.end());
    }
    //do the reconfig
    setWayAssignment(level, allCacheWayAssignment);
}

Core2CacheSetWayT CacheManager::getReadWriteWayPartition(RWPCircuit* rwp, UInt32 associativity)
{
  Core2CacheSetWayT wayAssignment(associativity, CacheReconfigInfo::CacheReconfigWayT::SHARED_READ); //default all to clean
  vector<UInt64> cleanCounters = rwp->getHitCounterValues(true);
  vector<UInt64> dirtyCounters = rwp->getHitCounterValues(false);
  rwp->doIntervalReset();
  assert(cleanCounters.size() == dirtyCounters.size());
  assert(cleanCounters.size() == associativity+1);  //An additional counter was introduced for MCFQ to track misses in ATD

  //combine hits of the counters
  //if ith-way is allocated then this effectively means,
  //total hits would be the sum of counter values of all ways from 0 to i
  for(UInt32 i=1; i<associativity; i++) {
//    std::cout << cleanCounters[i-1] << " " << dirtyCounters[i-1] <<  " " << rwp << std::endl;
    cleanCounters[i] += cleanCounters[i-1];
    dirtyCounters[i] += dirtyCounters[i-1];
  }

  //now the counters show the total hits if that way is allocated i.e stack property
  //Problem: Find i such that cleanCounters[i]+dirtyCounters[associativity-2-i] is minimum and 0<= i <=14
  UInt64 best_i=0, max_hits=0;
  for(UInt32 i=0; i<=associativity-2; i++){
    //(c,d) = (0,14) (1,13) (2,12) ... (13,1) (14,0)
    UInt32 clean_ways = i;
    UInt32 dirty_ways = associativity-2-i;
    UInt64 hits = cleanCounters[clean_ways]+dirtyCounters[dirty_ways];
    if(hits > max_hits) {
      max_hits=hits;
      best_i=dirty_ways;
    }
  }

  std::cout << "Calling CacheManager::getReadWriteWayPartition best_i " << best_i << std::endl;
  if(Sim()->getCfg()->hasKey("perf_model/l3_cache/atd/staticrwp")) {
    best_i = Sim()->getCfg()->getInt("perf_model/l3_cache/atd/staticrwp");
    std::cout << "overiding best_i with staticrwp " << best_i << std::endl;
  }
  //allocate the first best_i ways to SHARED_WRITE
  for(UInt32 i=0; i<=best_i; i++){
    wayAssignment[i] = CacheReconfigInfo::CacheReconfigWayT::SHARED_WRITE;
  }

  return wayAssignment;
}

//shutdown the ways marked 'shutdown'
void CacheManager::doCacheWayShutdown(CacheLevel level, Core2CacheSetWayT& assign, CacheReconfigInfo::CacheReconfigWayT shutdown) {
  std::cout << "Calling CacheManager::doCacheWayShutdown for Cache" << level << std::endl;
  Core2CacheSetWayT allCacheWayAssignment;
  UInt32 num_caches = caches[level].m_num;
  UInt32 shared_cores = caches[level].m_sharedcores;
  UInt32 associativity = caches[level].m_assoc;
  for(UInt32 i=0; i<num_caches; i++) {
    core_id_t master_core_id=i*shared_cores;
    Core* core = Sim()->getCoreManager()->getCoreFromID(master_core_id);
    MemoryManagerBase *mem_manager_base = core->getMemoryManager();
    ParametricDramDirectoryMSI::MemoryManager *mem_manager = (ParametricDramDirectoryMSI::MemoryManager *)(mem_manager_base);
    ParametricDramDirectoryMSI::CacheCntlr* cache_cntlr = mem_manager->getCacheCntlrAt(master_core_id, caches[level].m_mem_component);
    //Write Back data before marking shutdown
    for(UInt32 j=master_core_id*associativity; j < (master_core_id+1)*associativity; j++) {
      if(assign[j]==shutdown) {
        UInt32 way = j-master_core_id*associativity;
        cache_cntlr->writeBackCacheWay(way, true);  //invalidate all blocks
      }
    }
  }
  //std::cout << "TMP-------------- not setting way shutdown, only doing cache writeback for testing-------------------------------------" << std::endl;
  //do the reconfig
  //setWayAssignment(level, assign);    TODO: tmp not setting way assign, perform only shutdown
}

//called from Magic Server/python script to perform UCP
//assumes umon=true and atds exists for the cache levels where UCP is required
void CacheManager::doUCPartition(std::string opt){
  doUCPartition(CacheLevel::L1I, opt);
  doUCPartition(CacheLevel::L1D, opt);
  doUCPartition(CacheLevel::L2,  opt);
  doUCPartition(CacheLevel::L3,  opt);
}

void CacheManager::doUCPartition(CacheLevel level, std::string opt){
  if(!caches[level].m_ucp_enabled) return;
  std::cout << "[CacheManager] doUCPartition for Cache" << level 
            << " opt=" << opt << std::endl;
  Core2CacheSetWayT allCacheWayAssignment;
  UInt32 num_caches = caches[level].m_num;
  UInt32 shared_cores = caches[level].m_sharedcores;
  UInt32 associativity = caches[level].m_assoc;
  for(UInt32 i=0; i<num_caches; i++) {
    core_id_t master_core_id=i*shared_cores;
    Core* core = Sim()->getCoreManager()->getCoreFromID(master_core_id);
    MemoryManagerBase *mem_manager_base = core->getMemoryManager();
    ParametricDramDirectoryMSI::MemoryManager *mem_manager = (ParametricDramDirectoryMSI::MemoryManager *)(mem_manager_base);
    ParametricDramDirectoryMSI::CacheCntlr* cache_cntlr = mem_manager->getCacheCntlrAt(master_core_id, caches[level].m_mem_component);
   
    //get cache partition for current umon counters
    std::vector<ATD*>& atds = cache_cntlr->getATDs(); //one atd per sharing core
    if(atds.empty()) {
      std::cout << "[CacheManager] atd not found while doing doUCPartition for Cache" << level << std::endl;
      caches[level].m_ucp_enabled = false; 
      return;  //TODO not sure if this is OK, but if atds do not exist then no need to do partitioning for that cache level
    }
    
    //set concurrency factor
    updateConcurrencyFactorAndReset(master_core_id, shared_cores);

    //get partition sizes
    UInt8* allocations = new UInt8[shared_cores];
    cout << "[UCP] doing partitioning for " << opt << " at" << level << endl;
    getUtilityPartitionSizes(atds, level, allocations, opt);
    
    //get way partition assignment of applications
    Core2CacheSetWayT wayPartition = getUCWayPartition(allocations, associativity, master_core_id, shared_cores, opt);
   
    //get insertion positions/lru_positions
    setLruPositions(allocations, atds, level, master_core_id, opt);

    //set miss overflows for cache stack optimization
    setMissOverflow(wayPartition, atds, level, master_core_id, opt); 

    //Reset ATDs
    resetCircuits(atds); 
    
    //append to a single vector
    allCacheWayAssignment.insert(allCacheWayAssignment.end(), wayPartition.begin(), wayPartition.end());
  }
  //do the reconfig
  setWayAssignment(level, allCacheWayAssignment);
}


double CacheManager::getAvgOccupancy(core_id_t c, CacheLevel level) {
    Core* core = Sim()->getCoreManager()->getCoreFromID(c);
    MemoryManagerBase *mem_manager_base = core->getMemoryManager();
    ParametricDramDirectoryMSI::MemoryManager *mem_manager = (ParametricDramDirectoryMSI::MemoryManager *)(mem_manager_base);
    ParametricDramDirectoryMSI::CacheCntlr* cache_cntlr = mem_manager->getCacheCntlrAt(c, caches[level].m_mem_component);
    double occu = cache_cntlr->getAvgOccupancy();
    cache_cntlr->resetOccupancy();
    return occu;
}


//get maximum marginal utility for the core
double CacheManager::getMaxMarginalUtility(Umon* u, UInt32 alloc, UInt32 balance, UInt32& reqWays, CacheLevel level, string opt) {
  double max_mu=0.0;
  for(UInt32 i=1; i<=balance; i++) {
    //get marginal utility if way allocation is increased
    double mu = u->getAdditionalHitCount(alloc, alloc+i)/(double)i;
    if (opt == "MCFQ" || opt == "MLP_UCP") //MLP Aware utility function
      mu = mu/getConcurrencyFactor(u->core());
    if (opt == "CS_UCP" && level == CacheLevel::L3) {
      mu = mu*caches[level-1].m_core_miss_overflow[u->core()];
    }
    if(mu>max_mu) {
      max_mu = mu;
      reqWays = i;
    }
  }
  return max_mu;
}

Core2CacheSetWayT CacheManager::
  getUCWayPartition(UInt8* allocations, UInt32 associativity, core_id_t master_core_id, UInt32 shared_cores, std::string opt) 
{
  Core2CacheSetWayT wayAssignment(associativity, CacheReconfigInfo::CacheReconfigWayT::SHARED_WAY); //default all shared ways
  if (opt == "UCP" || opt == "MLP_UCP" || opt == "CS_UCP") {
    //perform way assignment
    UInt32 next_way=0;
    for(UInt32 i=0; i<shared_cores; i++) {
      UInt32 j=next_way;
      for(; j<next_way+allocations[i]; j++) {
        wayAssignment[j] = master_core_id+i;
      }
      next_way=j;
    }
  }
  return wayAssignment;
}

//sets the m_lru_positions
//currently useful only for PIPP and MCFQ
void CacheManager::
  setLruPositions(UInt8* allocations, std::vector<ATD*>& atds, CacheLevel level, core_id_t master_core_id, std::string opt)
{  
//  UInt32 shared_cores = caches[level].m_sharedcores;
  UInt32 associativity = caches[level].m_assoc;
  if (opt == "MCFQ") {
    //set core cache property like fit, friend, thrashing
    setCoreCacheProperty(level, atds, master_core_id );
  
    vector<map<UInt64, core_id_t>> coreOrderingInfo(CacheReconfigInfo::AppCachePropT::MAX);
    for(UInt32 i=0; i<atds.size(); i++) {
      core_id_t core_id = atds[i]->getUmon()->core();
      CacheReconfigInfo::AppCachePropT coreProp = caches[level].m_core_cache_property[core_id];
      UInt64 coreSens = atds[i]->getUmon()->getInterferenceSensitivity();
      double coreOccupancy = caches[level].m_core_cache_occupancy[master_core_id+i];
      cout << "[MCFQ] (core, prop, sens, alloc, conc, occu) " 
           << core_id << " " << coreProp << " " << coreSens << " " << int(allocations[i]) 
           << " " << getConcurrencyFactor(core_id) << " " << coreOccupancy << endl;
      while(coreOrderingInfo[coreProp].find(coreSens) != coreOrderingInfo[coreProp].end()){
        ++coreSens; //HACK, increment sensitivity to ensure no overwritting
      }
      coreOrderingInfo[coreProp][coreSens] = core_id;
    }
    vector<core_id_t> coreOrdering;
    for(UInt32 p=0; p<UInt32(CacheReconfigInfo::AppCachePropT::MAX); p++){
      for(map<UInt64, core_id_t>::iterator iter  = coreOrderingInfo[p].begin(); 
                                           iter != coreOrderingInfo[p].end(); ++iter) {
        coreOrdering.push_back(iter->second);
      }
    }

    //set lru positions
    cout << "[MCFQ] lru_pos = ";
    UInt32 basePos = 0;
    for(vector<core_id_t>::iterator iter = coreOrdering.begin();
                                    iter!= coreOrdering.end(); ++iter) {
      UInt32 i = *iter;
      UInt32 idealSize = allocations[i];
      UInt32 scaledSize = idealSize;
      if(idealSize >= caches[level].m_core_cache_occupancy[master_core_id+i]) {   //TODO: paper says 80%, but its not working, so taking 100%
        //find the ratio of occupancy/idealsize, and scale by the short fall
        scaledSize = idealSize*(1.0+ (1.0-caches[level].m_core_cache_occupancy[master_core_id+i]/idealSize));
      }
//      if(idealSize < 0.8*caches[level].m_core_cache_occupancy[master_core_id+i]) {
//        scaledSize = caches[level].m_core_cache_occupancy[master_core_id+i];
//      }
      caches[level].m_lru_positions[master_core_id+i] = basePos+scaledSize>associativity? 
                                                          associativity-1 : basePos+scaledSize-1;
      basePos += idealSize;
      cout << i << ":" << int(caches[level].m_lru_positions[master_core_id+i]) << ",";
    }
    cout <<endl;
  }
}

//set the miss overflow factor
void CacheManager::
  setMissOverflow(Core2CacheSetWayT& wayAssignment, std::vector<ATD*>& atds, CacheLevel level, core_id_t master_core_id, std::string opt) {
  if(opt != "CS_UCP") return;

  cout << "[UCP] computing  overflow at " << level << endl;
  UInt32 shared_cores = caches[level].m_sharedcores;
  UInt32 associativity = caches[level].m_assoc;
  Core2CacheSetWayT& prevAllWayAssignment = caches[level].getWayAssignment();
  for(UInt32 i=0; i<shared_cores; i++) {
    Umon* u=atds[i]->getUmon();
    core_id_t c = u->core();
    assert(UInt32(c)==UInt32(master_core_id+i));
    UInt32 prevWays = std::count (prevAllWayAssignment.begin(), prevAllWayAssignment.end(), c);
    prevWays = prevWays==0?1:prevWays;
    UInt32 currWays = std::count (wayAssignment.begin(), wayAssignment.end(), c);
    
    vector<UInt64> hit_counters = u->getHitCounterValues();
    for(UInt32 i=1; i<associativity; i++)
      hit_counters[i] += hit_counters[i-1];

    //Misses=FullCacheMisses+MissesForWaysNotAssigned
    UInt64 prevMisses = hit_counters[associativity]+(hit_counters[associativity-1]-hit_counters[prevWays-1]);
    UInt64 currMisses = hit_counters[associativity]+(hit_counters[associativity-1]-hit_counters[currWays-1]);
    caches[level].m_core_miss_overflow[c] = ((double)currMisses)/prevMisses;
    cout << "[setMissOverflow] c=" << c << " o:"<<caches[level].m_core_miss_overflow[c] 
         << " (prevc, currc, prevm, currm):" << prevWays << "," << currWays << "," << prevMisses << "," << currMisses
         << endl;
  }

}


void CacheManager::
  getUtilityPartitionSizes(std::vector<ATD*>& atds, CacheLevel level, UInt8* allocations, std::string opt) 
{
  UInt32 shared_cores = caches[level].m_sharedcores;
  UInt32 associativity = caches[level].m_assoc;
  UInt32 balance = associativity-shared_cores;
  
  for(UInt32 i=0; i<shared_cores; i++){
    allocations[i] = 1;
  }

  while (balance>0) {
    UInt32 winner=0;
    UInt64 winner_mu=0;
    UInt32 ways_req[shared_cores];
    UInt64 max_mu[shared_cores];
    for(UInt32 i=0; i<shared_cores; i++) {
      ways_req[i] = 1;  //must make progress incase of 0 marginal utility
      max_mu[i] = getMaxMarginalUtility(atds[i]->getUmon(), allocations[i], balance, ways_req[i], level, opt);
//      std::cout << "LA: C" << i << " bal:" << balance << " way_req:" << ways_req[i] 
//                << " max_mu:" << max_mu[i] << std::endl;
      if (max_mu[i] >= winner_mu) {
        winner_mu = max_mu[i];
        winner = i;
      }
    }
//    std::cout << "LA: bal:" << balance << " winner:" << winner << " way_req:" << ways_req[winner] << std::endl;
    //winner found, update data 
    allocations[winner] += ways_req[winner];
    balance -= ways_req[winner];
  }
 
/*  cout << "[getUtilityPartitionSizes] ";
  for(UInt32 i=0; i<shared_cores; i++) {
    cout << i << ":" << int(allocations[i]) << ",";
  }
  cout << endl;
*/
}

//reset various help circuit counters/states
//UMON counters
void CacheManager::resetCircuits(std::vector<ATD*>& atds) 
{
  //Reset umon counters
  for(UInt32 i=0; i<atds.size(); i++) {
//    std::cout << "HitCnt C" << i << " ";
    atds[i]->getUmon()->print();
    atds[i]->getUmon()->intervalReset();
  }
}

void CacheManager::
  updateConcurrencyFactorAndReset(core_id_t master_core_id, UInt32 shared_cores) 
{
    if(!m_enable_concurrency_profiler) return;

    vector<ContentionModel*> mshr_circuits(shared_cores);
    for(UInt32 i=0; i<shared_cores; ++i) {
      core_id_t core_id = master_core_id + i;
      Core* core = Sim()->getCoreManager()->getCoreFromID(core_id);
      MemoryManagerBase *mem_manager_base = core->getMemoryManager();
      ParametricDramDirectoryMSI::MemoryManager *mem_manager = (ParametricDramDirectoryMSI::MemoryManager *)(mem_manager_base);
      CacheLevel level = CacheLevel::L1D;
      ParametricDramDirectoryMSI::CacheCntlr* cache_cntlr = mem_manager->getCacheCntlrAt(core_id, caches[level].m_mem_component);
      ContentionModel& mshr_circuit = cache_cntlr->getMshrCircuit();
      mshr_circuits[i] = &mshr_circuit;
      m_mshr_concurrency[core_id] =  mshr_circuit.getAvgOutstanding();
    }
    for(UInt32 i=0; i<shared_cores; ++i) {
      mshr_circuits[i]->resetAvgOutstanding();
    }
}

double CacheManager::
  getConcurrencyFactor(core_id_t core_id) 
{
    return m_mshr_concurrency[core_id];
}

void CacheManager::
  setCoreCacheProperty(CacheLevel level, std::vector<ATD*>& atds, core_id_t master_core_id) 
{
  UInt32 shared_cores = caches[level].m_sharedcores;
  UInt32 associativity = caches[level].m_assoc;
  for(UInt32 i=0; i<shared_cores; i++)
  {
    vector<UInt64> hit_counters = atds[i]->getUmon()->getHitCounterValues();
    for(UInt32 i=1; i<associativity; i++)
      hit_counters[i] += hit_counters[i-1];
    
    double min_misses = hit_counters[associativity];  //when full cache allocated to core
    double max_misses = hit_counters[associativity-1]-hit_counters[0] + min_misses;  //when only 1 way allocated
    double hr = max_misses > 0.0 ? min_misses/max_misses : 0.0;
    cout << "[MCFQ PROP] min, max, hr " << min_misses << " " << max_misses << " " << hr << endl;
    if (hr > 0.85)
      caches[level].m_core_cache_property[master_core_id+i] = CacheReconfigInfo::AppCachePropT::CACHE_THRASH;
    else if(hr < 0.005)
      caches[level].m_core_cache_property[master_core_id+i] = CacheReconfigInfo::AppCachePropT::CACHE_FIT;
    else
      caches[level].m_core_cache_property[master_core_id+i] = CacheReconfigInfo::AppCachePropT::CACHE_FRIEND;
    
    caches[level].m_core_cache_occupancy[master_core_id+i] = getAvgOccupancy(master_core_id+i, level);
  }
}

std::vector<ATD*> CacheManager::getATDs(CacheLevel level) {
  std::vector<ATD*> all_atds;
  UInt32 num_caches = caches[level].m_num;
  UInt32 shared_cores = caches[level].m_sharedcores;
  UInt32 associativity = caches[level].m_assoc;
  for(UInt32 i=0; i<num_caches; i++) {
    core_id_t master_core_id=i*shared_cores;
    Core* core = Sim()->getCoreManager()->getCoreFromID(master_core_id);
    MemoryManagerBase *mem_manager_base = core->getMemoryManager();
    ParametricDramDirectoryMSI::MemoryManager *mem_manager = (ParametricDramDirectoryMSI::MemoryManager *)(mem_manager_base);
    ParametricDramDirectoryMSI::CacheCntlr* cache_cntlr = mem_manager->getCacheCntlrAt(master_core_id, caches[level].m_mem_component);
   
    //get cache partition for current umon counters
    std::vector<ATD*>& atds = cache_cntlr->getATDs(); //one atd per sharing core
    if(atds.empty()) {
      std::cout << "[CacheManager] atd not found while doing doUCPartition for Cache" << level << std::endl;
      caches[level].m_ucp_enabled = false; 
      return all_atds;  //TODO not sure if this is OK, but if atds do not exist then no need to do partitioning for that cache level
    }
    all_atds.insert(all_atds.end(), atds.begin(), atds.end());
  }
  return all_atds;
}   

void CacheManager::doXChangeAllocation() {
  assert(m_xchange_manager);
  CacheLevel level=L3;
  std::cout << "[CacheManager] doXChangeAllocation " << level << std::endl;
  UInt32 num_caches = caches[level].m_num;
  UInt32 shared_cores = caches[level].m_sharedcores;
  UInt32 associativity = caches[level].m_assoc;
  for(UInt32 i=0; i<num_caches; i++) {
    core_id_t master_core_id=i*shared_cores;
    //set concurrency factor
    updateConcurrencyFactorAndReset(master_core_id, shared_cores);
  }

  vector<float> mlp_data;
  for(UInt32 i=0; i<m_num_app_cores; i++)
    mlp_data.push_back(getConcurrencyFactor(i));
  
  Core2CacheSetWayT allCacheWayAssignment = m_xchange_manager->getResourceAllocation(mlp_data);
  //do the reconfig
  setWayAssignment(level, allCacheWayAssignment);
}

void CacheManager::initXChange() { 
  if(m_xchange_manager) 
    m_xchange_manager->initXChange(getATDs(CacheLevel::L3), caches); 
}
