#ifndef __DELTA_STATS_H
#define __DELTA_STATS_H

#include <vector>
#include <map>
#include <iterator>
#include <utility>
#include <boost/algorithm/string.hpp>
#include "fixed_types.h"

using namespace std;


class DeltaStats {
  public:
    DeltaStats(int np, vector<String> metrics);
    void updateDelta();
    typedef UInt64 StatType;
    StatType getDeltaStat(String metric, core_id_t core_id);
    UInt32 getLLCHitLatency() { return llc_hit_latency; }
    UInt32 getLLCMissLatency() { return llc_miss_latency; }
  private:
    map<String, vector<StatType>> stats;
    map<String, vector<StatType>> delta;
    UInt32 llc_hit_latency;
    UInt32 llc_miss_latency;
};

#endif
