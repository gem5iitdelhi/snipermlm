#ifndef MAGIC_SERVER_H
#define MAGIC_SERVER_H

#include "fixed_types.h"
#include "progress.h"
#include <vector>

class CacheManager;

class MagicServer
{
   public:
      // data type to hold arguments in a HOOK_MAGIC_MARKER callback
      struct MagicMarkerType {
         thread_id_t thread_id;
         core_id_t core_id;
         UInt64 arg0, arg1;
         const char* str;
      };

      MagicServer();
      ~MagicServer();

      UInt64 Magic(thread_id_t thread_id, core_id_t core_id, UInt64 cmd, UInt64 arg0, UInt64 arg1);
      bool inROI(void) const { return m_performance_enabled; }
      static UInt64 getGlobalInstructionCount(void);

      // To be called while holding the thread manager lock
      UInt64 Magic_unlocked(thread_id_t thread_id, core_id_t core_id, UInt64 cmd, UInt64 arg0, UInt64 arg1);
      UInt64 setFrequency(UInt64 core_number, UInt64 freq_in_mhz);
      UInt64 getFrequency(UInt64 core_number);
      
      //21-Mar-2015 Rahul Uncore DVFS
      UInt64 setUnCoreFrequency(UInt64 freq_in_mhz);
      UInt64 getUnCoreFrequency();

      //4-June-2015 Rahul L3 Cache Reconfig
      UInt64 setL3CacheWayAssignment(std::vector<SInt32> assign);
      std::vector<SInt32>& getL3CacheWayAssignment(); 

      //30Sep-2015 Rahul L2, L1 Reconfig
      UInt64 setL2CacheWayAssignment(std::vector<SInt32> assign);
      UInt64 setL1DCacheWayAssignment(std::vector<SInt32> assign);
      UInt64 setL1ICacheWayAssignment(std::vector<SInt32> assign);

      //13Oct-2015 Rahul, RWP -Onur Mutlu 2014
      UInt64 doAllLevelReadWritePartition();

      //27-Mar-2016 Rahul, Cache way shutdown
      UInt64 doL1DShutdown(std::vector<SInt32> assign);
      
      //8April-2016 Rahul, UCP -Quershi 2006
      //29April-2016 Rahul, Adding options to do PIPP, MCFQ
      UInt64 doAllLevelUCPartition(std::string ucp_opt);

      //6Aug-2016 Rahul, XChange HPCA 2015
      UInt64 doXChangeAllocation();

      void enablePerformance();
      void disablePerformance();
      UInt64 setPerformance(bool enabled);

      UInt64 setInstrumentationMode(UInt64 sim_api_opt);

      void setProgress(float progress) { m_progress.setProgress(progress); }

   private:
      bool m_performance_enabled;
      Progress m_progress;
};

#endif // SYNC_SERVER_H
