#include "xchange_manager.h"
#include "magic_server.h"
#include "simulator.h"
#include <cstdlib>

//const bool prints=true;
const bool prints=false;

//map<int, float> XChangeAgent::dvfs_table = map<int, float>();
//vector<int> XChangeAgent::dvfs_idx = vector<int>();

XChange::XChange(UInt32 np):
  m_np(np)
{ 
  TOTAL_AGENT_BUDGET=800*m_np;
  prev_qdelay = 0.0;
}

void XChange::initXChange(vector<ATD*> atds, CacheReconfigInfo* caches)
{
  initMetrics();  
  //TODO: currently only LLC is being partitioned
//  assert(Sim()->getCfg()->getString("perf_model/dram/type") == "timing"); //Currently only for date MLM work

  //ensure MLP data collection is enabled
  assert(Sim()->getCfg()->getBoolDefault("perf_model/core/concurrency", false) == true);

  //initilize the dvfs_table for agents
  initDvfsTable();

  //Set System Budget
  //start with some default price
  frequency_price = 100.0;
  cache_price = 100.0;
  max_frequency_resource = 0.9*m_np*dvfs_idx.size();  //4-core, 45nm, will have a avg freq budget of 3000 MHz
  max_cache_resource = caches[CacheLevel::L3].associativity();  //budget per LLC cache instance

  cout << "[XChange] creating Agents" << endl;

  //Create Agents
  m_numLLC = m_np/caches[CacheLevel::L3].numSharedCores();
  for(unsigned i=0; i<m_np; ++i) {
    XChangeAgent* agent = new XChangeAgent(i, atds[i], deltastats, this);
    core_dvfs_players.push_back(agent);
    UInt32 llc_idx = i/caches[CacheLevel::L3].numSharedCores();
    if(llc_players.size()==llc_idx) llc_players.push_back(vector<XChangeAgent*>());
    llc_players[llc_idx].push_back(agent);
  }
}

void XChange::initMetrics() {
  vector<String> metrics;
  metrics.push_back(String("performance_model.elapsed_time"));     //computetime
  metrics.push_back(String("performance_model.idle_elapsed_time"));         //memtime
  metrics.push_back(String("core.energy-dynamic")),
  metrics.push_back(String("core.energy-static")),
  metrics.push_back(String("L3.loads")),
  metrics.push_back(String("L3.stores")),
  metrics.push_back(String("network.shmem-1.mesh.total-delay")), 
  metrics.push_back(String("network.shmem-1.mesh.packets-in")),

  deltastats = new DeltaStats(m_np, metrics);
}

//computetime: performance_model.nonidle_elapsed_time
//memtime: performance_model.idle_elapsed_time :approx time the cpu waited for data from memory
Core2CacheSetWayT XChange::getResourceAllocation(vector<float> mlpdata)  {
  assert(mlpdata.size() == m_np);
  
  //update delta stats
  deltastats->updateDelta();

  //set new mlp data
  for(unsigned i=0; i<m_np; ++i) {
    core_dvfs_players[i]->setMLP(mlpdata[i]);
  }
  
  cout << "[XCH] getResourceAllocation calling getAgentDemands" << endl;

  //perform wealth distribution
  doWealthDistribution();

  //let market players find the allocation
  AgentDemands demands[m_np];
  getAgentDemands(demands);
  allocateResources(demands);

  //Set the DVFS and cache reconfig
  Core2CacheSetWayT allcacheassignment;
  for(unsigned i=0; i<m_np; ++i) {
    UInt64 freq = dvfs_idx[demands[i].core_dvfs_idx];
    UInt32 llc_ways = demands[i].llc_ways;
    core_id_t core_id = i;
    cout << "[XChange] core" << core_id << " (Freq, Cache) " << freq << "," << llc_ways << endl;
    Sim()->getMagicServer()->setFrequency(core_id, freq);
    allcacheassignment.insert(allcacheassignment.end(), llc_ways, core_id);
  }
  assert(allcacheassignment.size() == max_cache_resource);

  doUncoreDvfs();

  return allcacheassignment;
}

void XChange::allocateResources(AgentDemands* demands) {
  //Fix any over/under cache way assignment
  UInt32 total_llc_ways = 0;
  for(unsigned i=0; i<m_np; ++i) {
    total_llc_ways += demands[i].llc_ways;
  }
  if (total_llc_ways > max_cache_resource) {//cache demand high
    //TODO: Hacky, handles only the case where numLLC=1
    assert(m_numLLC==1);
    UInt32 overassigned = total_llc_ways - max_cache_resource;
    while(overassigned > 0) {
      UInt32 idx = rand()%m_np;
      if(demands[idx].llc_ways > 1) {//ensure atleast one cache way is assigned to each core
        demands[idx].llc_ways--;
        overassigned--;
      }
    }
  } 
  else if(total_llc_ways < max_cache_resource) {
    //cache demand is very low, assign unrequested ways to first core
    //TODO: Hacky, handles only the case where numLLC=1
    assert(m_numLLC==1);
    UInt32 unassigned = max_cache_resource - total_llc_ways;
    while(unassigned > 0) {
      UInt32 idx = rand()%m_np;
      demands[idx].llc_ways++;
      unassigned--;
    }
  }
}

void XChange::getAgentDemands(AgentDemands* demands) {
  float fprice = 0, curr_fprice=frequency_price;
  vector<float> cprice = vector<float>(m_numLLC, 0), curr_cprice = vector<float>(m_numLLC, cache_price);
  UInt64 iters=0;
  for(unsigned i=0; i<m_np; ++i) {
    core_dvfs_players[i]->m_atd->getUmon()->getHitCounterValues(true);
  }
  while(iters < 500*m_np/4 && (hasPriceConverged(fprice, curr_fprice, cprice, curr_cprice) == false))
  {
    ++iters;
    if(prints) cout << iters << "-> " << curr_fprice << "," << curr_cprice[0] << endl;
    fprice = curr_fprice;
    cprice = curr_cprice;
    UInt32 shared_cores = m_np/m_numLLC;
    for(unsigned i=0; i<m_np; ++i) {
//      core_dvfs_players[i]->m_atd->getUmon()->getHitCounterValues(true);
      demands[i] = core_dvfs_players[i]->getResourceDemands(curr_fprice, curr_cprice[i/shared_cores]);
    }

    int total_dvfs = 0;
    for(unsigned i=0; i<m_np; ++i) {
      total_dvfs += demands[i].core_dvfs_idx+1;
    }
    curr_fprice = total_dvfs*fprice/max_frequency_resource; //update frequency price based on demand
    
    for(unsigned j=0; j<m_numLLC; ++j) {
      int total_cache = 0;
      for(unsigned i=j*shared_cores; i<(j+1)*shared_cores; ++i) {
        total_cache += demands[i].llc_ways;
      }
      curr_cprice[j] = total_cache*cprice[j]/max_cache_resource;
    }
  }
  cout << "[XChange::getAgentDemands] prices converged after " << iters << "  at " << fprice << "," << cprice[0] << endl;
}
  
bool XChange::hasPriceConverged(float fprice, float curr_fprice, vector<float>& cprice, vector<float>& curr_cprice) {
  if(abs(fprice-curr_fprice)/fprice >= 0.05) return false;
  for(unsigned int i=0; i<cprice.size(); ++i) {
    if(abs(cprice[i]-curr_cprice[i])/cprice[i] >= 0.05) return false;
  }
  return true;
}  

void XChange::doWealthDistribution() {
  double total_potential=0.0;
  for(unsigned i=0; i<m_np; ++i) {
    if(prints) cout << "potential i->" << core_dvfs_players[i]->getPotential() <<endl;
    total_potential += core_dvfs_players[i]->getPotential();
  }
  float min_budget = frequency_price+cache_price; //min budget for each agent
  float wealth = TOTAL_AGENT_BUDGET - m_np*min_budget;
  for(unsigned i=0; i<m_np; ++i) {
    float budget = min_budget + wealth*core_dvfs_players[i]->getPotential()/total_potential;
    if(prints) cout << "doWealthDistribution i->" << budget <<endl;
    core_dvfs_players[i]->MAX_BUDGET = budget;
  }
}

void XChange::initDvfsTable() {  
  //Initilize the dvfs table
  if (Sim()->getCfg()->getInt("power/technology_node") == 45) { //45 nm dvfs table
    dvfs_table[3200] =1.2;
    dvfs_table[3000] =1.1;
    dvfs_table[2700] =1.0;
    dvfs_table[2400] =0.9;
    dvfs_table[2000] =0.8;
/*    dvfs_table[3000] =1.2;
    dvfs_table[2500] =1.1;
    dvfs_table[2100] =1.0;
    dvfs_table[1700] =0.9;
    dvfs_table[1300] =0.8;*/
  } else if (Sim()->getCfg()->getInt("power/technology_node") == 22) {
    dvfs_table[2000] =1.0;
    dvfs_table[1800] =0.9;
    dvfs_table[1500] =0.8;
    dvfs_table[1000] =0.7;
    dvfs_table[600] =0.6;
  } else {
    assert(0 && "invalid technology_node");
  }

  map<int, float>::iterator start = dvfs_table.begin();
  for(; start != dvfs_table.end(); ++start) {
    dvfs_idx.push_back( (*start).first);
  }
  
  for(unsigned int i=1; i<dvfs_idx.size(); ++i) {
    assert(dvfs_idx[i] > dvfs_idx[i-1]);  //ensure the list is sorted
  }
}

void XChange::doUncoreDvfs() {
  float qdelay = (float)1e-6*(deltastats->getDeltaStat(String("network.shmem-1.mesh.total-delay"), 0));
  DeltaStats::StatType npkts = deltastats->getDeltaStat(String("network.shmem-1.mesh.packets-in"), 0);
  if(npkts==0) return;

  qdelay = qdelay/npkts;
  int uncoreFreq = Sim()->getMagicServer()->getUnCoreFrequency();
  int idx = -1;
  for(unsigned i=0; i<dvfs_idx.size(); ++i) if(dvfs_idx[i] == uncoreFreq) idx=i;
  assert(idx >= 0);
  if(prev_qdelay/qdelay < 0.97) { //incr the freq
    ++idx;  //increase frequency
    if(idx >= dvfs_idx.size())  idx = dvfs_idx.size()-1;
  } else if(qdelay/prev_qdelay < 0.97) {
    --idx;  //decrease frequency
    if(idx < 0)  idx = 0;
  }
  prev_qdelay = qdelay;
  Sim()->getMagicServer()->setUnCoreFrequency(dvfs_idx[idx]);
}

/*****************************************************************************/
XChangeAgent::XChangeAgent(core_id_t c, ATD* _atd, DeltaStats* d, XChange* market) :
  core_id(c),
  m_atd(_atd),
  deltastats(d),
  m_market(market),
  m_mlp(0.0)
{
  //this can be changed for wealth distribution case
  MAX_BUDGET = 800;
}
//cache utility for ways increased from a to b ways
//the additional hits are same as decrese in misses and vice versa
double XChangeAgent::CacheMarginalUtility(int a, int b) {
  UInt64 additionalHits = m_atd->getUmon()->getAdditionalHitCount(a, b);
  double MU = (additionalHits*deltastats->getLLCMissLatency()) - (additionalHits*deltastats->getLLCHitLatency());
  return MU/m_mlp;
}

double XChangeAgent::CacheUtility(int a) {
  UInt64 umon_hits = m_atd->getUmon()->getHitCount(a);
  UInt64 umon_miss = m_atd->getUmon()->getMissCount(a);

  UInt64 access = deltastats->getDeltaStat(String("L3.stores"), core_id) + deltastats->getDeltaStat(String("L3.loads"), core_id);
  double hits = (double)(access*umon_hits)/(umon_hits+umon_miss);
  double miss = (double)(access*umon_miss)/(umon_hits+umon_miss);

  double CU = (hits*deltastats->getLLCHitLatency()) + (miss*deltastats->getLLCMissLatency());
  return CU/m_mlp;
}

double XChangeAgent::getMemPhase(int a) {
//  DeltaStats::StatType tmem = deltastats->getDeltaStat(String("performance_model.idle_elapsed_time"), core_id);         //memtime
  return CacheUtility(a);
}

double XChangeAgent::EDPMarginalUtility(UInt64 f, UInt64 df, double tmem) {
  return EDPUtility(f, tmem)-EDPUtility(df, tmem);
}

//edp utility for f frequency on core
double XChangeAgent::EDPUtility(UInt64 f, double tmem, bool perf_penalty) {
  DeltaStats::StatType t0 = 1e-6*(deltastats->getDeltaStat(String("performance_model.elapsed_time"), core_id) -
                              deltastats->getDeltaStat(String("performance_model.idle_elapsed_time"), core_id));  //ns
  DeltaStats::StatType dE0 = 1e-6*deltastats->getDeltaStat(String("core.energy-dynamic"), core_id);  //nJ
  DeltaStats::StatType sE0 = 1e-6*deltastats->getDeltaStat(String("core.energy-static"), core_id);  //nJ

  //TODO HACKY: start of first interval does not have energy metric
  if(dE0==0) dE0=100;
  if(sE0==0) sE0=dE0/2.0;

  UInt64 f0 = Sim()->getMagicServer()->getFrequency(core_id);
  
  double V0 = m_market->dvfs_table[f0];
  double Vf = m_market->dvfs_table[f];

  double tf = t0*f0/(double)f;

  //XCH_RUNS, compute max freq numbers
  UInt64 fmax = 3200;  //MHz
  double Vmax = m_market->dvfs_table[fmax];
  double tmax = t0*f0/(double)fmax;
  float tr_max = (tmax+tmem)/(tf+tmem); 




  //ensure slow down is not more than 5%
  float tr = (t0+tmem)/(tf+tmem);
  if(prints) cout << "EDPUtility " << f0 << "->" << f << " tr=" << tr << " t0/tmem=" << t0/tmem << endl;
  if( perf_penalty && tr < 0.90 )  return 0.0;          //XCH_RUNS  .95, .98

//  double Pf = (E0*Vf*Vf)/(V0*V0*(tf+tmem));

//XCH_RUNS
//ED2P
//  double Ef = ((dE0*Vf*Vf)/(V0*V0));//+(sE0*Vf/V0);
//  double EDPf = Ef*(tf+tmem)*(tf+tmem)/1e12;   //ns*nJ/1e9 => nJs

//EDP
  double Ef = ((dE0*Vf*Vf)/(V0*V0)) + (sE0*Vf/V0);   //XCH_RUNS: dyn+static
  double EDPf = Ef*(tf+tmem)/1e9;   //ns*nJ/1e9 => nJs
  
//  if(prints) cout << "[XChangeAgent::EDPUtility] " << t0 << "," << f0 << "," << dE0+sE0 << " | "
//                                        << tf << "," << f << "," << Ef << endl;
  
  //since we wish to minimize EDP, the utility would be inverse of EDPf
  return 1.0/EDPf;
}

//perform local search to get best resource demand
AgentDemands XChangeAgent::getResourceDemands(float freq_price, float llc_price) {
//  if(prints) cout << "[XChangeAgent::getResourceDemands] fp, cp, max " << freq_price << "," << llc_price << "," << MAX_BUDGET << endl;
  
  AgentDemands demand;
  //buy min frequency
  demand.core_dvfs_idx = 0; //min frequency
  //buy cache with the rest of the budget
  demand.llc_ways = int((MAX_BUDGET-freq_price)/llc_price);
  if(demand.llc_ways > m_market->max_cache_resource)
    demand.llc_ways = m_market->max_cache_resource;
  else if(demand.llc_ways < 1)
    demand.llc_ways = 1;  //atleast one cache way

  float best_utility = HillClimbSearchUtility(demand);
  AgentDemands best = demand;

  for(unsigned int i=demand.llc_ways; i>0; --i) {
    for(unsigned int j=0; j<m_market->dvfs_idx.size(); ++j) {
      demand.llc_ways = i;
      demand.core_dvfs_idx = j;
      float cost = (llc_price*demand.llc_ways)+(freq_price*(demand.core_dvfs_idx+1));
      if(cost<=MAX_BUDGET) {
        double utility = HillClimbSearchUtility(demand);
        if(utility > best_utility) {
          best_utility = utility;
          best = demand;
        }
      }
    }
  }
  if(prints) cout << "[XChangeAgent::getResourceDemands] " << core_id << " best " << best.core_dvfs_idx << "," << best.llc_ways << "=" << best_utility <<endl;
  return best;
}

//compute the potential for the application to estimate the wealth distribution
double XChangeAgent::getPotential() {
  AgentDemands min(0,1);
  AgentDemands max(m_market->dvfs_idx.size()-1,m_market->max_cache_resource);
  return 1.0-HillClimbSearchUtility(min, false)/HillClimbSearchUtility(max, false);
}

double XChangeAgent::HillClimbSearchUtility(AgentDemands soln, bool perf_penalty) {
  double tmem = getMemPhase(soln.llc_ways);
  UInt64 f = m_market->dvfs_idx[soln.core_dvfs_idx];
  double edp_util = EDPUtility(f, tmem, perf_penalty);
  if(prints) cout << "[XChangeAgent::HillClimbSearchUtility] " << soln.core_dvfs_idx << "," << soln.llc_ways << " tmem= " << tmem << " edp=" << edp_util << endl;
  return edp_util;
}
