#include "delta_stats.h"
#include "config.hpp"
#include "magic_server.h"
#include "simulator.h"
#include "stats.h"
#include <cstring>
using namespace std;

pair<String, String> parseMetrics(String metric) {
  char* p = new char[metric.length()+1];
  std::strcpy (p, metric.c_str());
  String objectName(""), metricName("");
  char* pch = strtok(p, ".");
  vector<String> tokens;
  while (pch != NULL)
  {
    tokens.push_back(String(pch));
    pch = strtok (NULL, ".");
  }

  for(unsigned i=0;i<tokens.size()-1;++i){
    if(i==0)  objectName =  tokens[i];
    else      objectName += "."+tokens[i];
  }
  metricName = tokens[tokens.size()-1];

//  cout << "parseMetrics " << metric << "->" << objectName << ", " << metricName << endl;
  return pair<String, String>(objectName, metricName);
}

DeltaStats::DeltaStats(int np, vector<String> metrics) {
  UInt64 uncoreFreqMhz = Sim()->getMagicServer()->getUnCoreFrequency();
  float llc_tag_latency = Sim()->getCfg()->getInt("perf_model/l3_cache/tags_access_time")*(1000.0/uncoreFreqMhz);
  llc_hit_latency = llc_tag_latency + (Sim()->getCfg()->getInt("perf_model/l3_cache/data_access_time")*(1000.0/uncoreFreqMhz));
  llc_miss_latency = llc_tag_latency + (Sim()->getCfg()->getInt("perf_model/dram/latency")); //1600MHz DRAM op, 26 avg cycles
  
  vector<String>::iterator start = metrics.begin();
  for(; start != metrics.end(); ++start) {
    stats[*start] = vector<StatType>(np, 0); 
    delta[*start] = vector<StatType>(np, 0); 
  }
  
}

void DeltaStats::updateDelta() {
  //update for current uncore freq
  UInt64 uncoreFreqMhz = Sim()->getMagicServer()->getUnCoreFrequency();
  llc_hit_latency = Sim()->getCfg()->getInt("perf_model/l3_cache/data_access_time")*(1000.0/uncoreFreqMhz); //ns
  
  map<String, vector<StatType>>::iterator start = delta.begin();
  for(; start != delta.end(); ++start) {
    String m = (*start).first;
    pair<String, String> metric = parseMetrics(m);
    String objectName = metric.first;
    String metricName = metric.second;
//    cout << "updateDelta " << m << "->" << objectName << ", " << metricName << endl;
    for(unsigned int i=0; i<(*start).second.size(); ++i) {
      StatType value = Sim()->getStatsManager()->getMetricObject(objectName, i, metricName)->recordMetric();
      delta[m][i] = value - stats[m][i];
      stats[m][i] = value;
    }
  }
}

DeltaStats::StatType DeltaStats::getDeltaStat(String metric, core_id_t core_id) {
  return delta[metric][core_id];
}
