#!/usr/bin/python
import sys, copy

class scp_configs:
    def __init__(self):
        self.np = 4
        self.totalways = 16
        self.fixshared = 4
        self.ways = self.totalways-self.fixshared
        self.step = 3
        assert(self.ways%self.step==0)
        self._initializeAssignment()

    def _initializeAssignment(self):
        self._Reconfigs = []
        self._computeCacheWayReconfigs(self.np, self.ways/self.step)
#        print "_computeCacheWayReconfigs", self._Reconfigs

    def _getConfig(self, n):
        return self._Reconfigs[n]

    def getCacheWayAssignment4Config(self, configNum):
        actionVec = self._getConfig(configNum)
        wayAssignment = [-1 for w in range(self.fixshared)]
        
        core=0
        for a in actionVec:
          wayAssignment.extend([core for w in range(a*self.step)])
          core = core + 1

#        print "getCacheWayAssignment4Action", actionVec, wayAssignment
        return wayAssignment

    #finds the no of configs possible by distributing ways among np cores
    #each core can get 0 to #ways
    #e.g np=4, ways=8
    #   c1: 0,0,0,8  c2: 1,1,1,5 etc
    def _computeCacheWayReconfigs(self, np, ways, config=[]):
#        print np, ways, config
        if ways < 0 or np <=0:
            assert("np or ways must be >0")
        if np==1:
            config.append(ways)
            self._Reconfigs.append(copy.copy(config))
            return 1    #all ways assigned to the only core
        else:
            nconfigs=0;
            for w in range(ways+1):
                tconfig = copy.copy(config)
                tconfig.append(w)
                nconfigs=nconfigs+self._computeCacheWayReconfigs(np-1, ways-w, tconfig)
            return nconfigs

if __name__ == '__main__':
    s = scp_configs()
    print "Iteration:", len(s._Reconfigs)
    print s._getConfig(0), s.getCacheWayAssignment4Config(0)
    print s._getConfig(2), s.getCacheWayAssignment4Config(2)
