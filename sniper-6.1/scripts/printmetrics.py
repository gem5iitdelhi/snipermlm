#Not being used or tested or functional
import sys, os, sim
class PrintMetricsConfig:
    def __init__(self, interval=1000000, periodic='time'):
        self.interval = interval
        self.periodic = periodic

class PrintMetrics:
  def __init__(self, config):
    self.config = config

  def setup(self, args):
    print '------------------------------------------'
    self.sd = sim.util.StatsDelta()
    self.stats = {
      'time': [ self.sd.getter('performance_model', core, 'elapsed_time') for core in range(sim.config.ncores) ],
      'ffwd_time': [ self.sd.getter('fastforward_performance_model', core, 'fastforwarded_time') for core in range(sim.config.ncores) ],
      'instrs': [ self.sd.getter('performance_model', core, 'instruction_count') for core in range(sim.config.ncores) ],
      'coreinstrs': [ self.sd.getter('core', core, 'instructions') for core in range(sim.config.ncores) ],
      'l3_dram_queue_time': [self.sd.getter('L3', core, 'uncore-time-dram-queue') for core in range(sim.config.ncores) ],
      'l3_dram_requests': [self.sd.getter('L3', core, 'uncore-requests') for core in range(sim.config.ncores) ],
      'l3_uncore_time': [self.sd.getter('L3', core, 'uncore-totaltime') for core in range(sim.config.ncores) ],
    }
    if self.config.periodic == 'time':
        sim.util.Every(self.config.interval * sim.util.Time.NS, self.periodic, statsdelta = self.sd, roi_only = True)
    elif self.config.periodic == 'inst':
        sim.util.EveryIns(self.config.interval, self.periodicins, roi_only = True)

  def periodic(self, time, time_delta):
    print 'periodic: ', time, time_delta

  def periodicins(self, icount, icount_delta):
    print 'periodicins: ', icount, icount_delta
    self.sd.update()

  def printMetrics(self):
    metrics = {}

    print "PRINTSTATS: time ", [self.stats['time'][core].delta for core in range(sim.config.ncores) ]
    print "PRINTSTATS: coreinstr ", [self.stats['coreinstrs'][core].delta for core in range(sim.config.ncores) ]

    for core in range(sim.config.ncores):
      time_delta = self.stats['time'][core].delta
      cycles = self.stats['time'][core].delta * sim.dvfs.get_frequency(core) / 1e9 # convert fs to cycles
      instrs = self.stats['coreinstrs'][core].delta
      dram_queue_time = self.stats['l3_dram_queue_time'][core].delta
      uncore_time = self.stats['l3_uncore_time'][core].delta
      dram_requests = self.stats['l3_dram_requests'][core].delta
      instr_count = self.stats['instrs'][core].delta
      #metric = dram_queue_time / (dram_requests or 1)
      #metric = uncore_time / (dram_requests or 1)
      #metric = uncore_time / (instr_count or .001)  #uncore time per instr executed
#      print "AMATTRACE: ", instr_count, time_delta
      metric = time_delta / (instr_count or .001)  #tps: time per instruction
      metrics[core] = metric
