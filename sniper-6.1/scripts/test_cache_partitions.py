"""
script to test different cache partitioning strategies
"""

import sys, os, sim

class TestCache:
  def setup(self, args):
    args = dict(enumerate((args or '').split(':')))
    doopt = int(args[0])
    if doopt == 0:
      return

    #commands to perform SCP
    #set_l1d_cacheway
    #set_l2_cacheway
    #set_l3_cacheway

#    sim.dvfs.set_l1d_cacheway("0,0,0,0,0,0,1,1,         2,2,3,3,2,2,2,2")
    if doopt in [1,3]:
      sim.dvfs.set_l2_cacheway ("0 ,1 ,1 ,1 ,1 ,1 ,1 ,1 ,2 ,2 ,2 ,2 ,2 ,2 ,2 ,3")
    if doopt in [2,3]:
      sim.dvfs.set_l3_cacheway ("0 ,0 ,0 ,0 ,0 ,0 ,0 ,1 ,1 ,2 ,2 ,2 ,2 ,2 ,3 ,3")


sim.util.register(TestCache())
