"""
Infra to keep dumping sim and energy stats to a db file per interval
"""

import sys, os, sim
MC_HOME=os.environ['HOME']
sys.path.insert(1, MC_HOME + '/Phd/bitbucket/sniper/sniper-6.1/scripts/getmetrics/')    #TODO
from getmetrics import GetMetrics
from datastore import DataStore
from dvfsenergystats import EnergyStats

MetricTrace=GetMetrics

def reportCrash():
    import traceback
    exc_type, exc_value, exc_traceback = sys.exc_info()
    print "*** print_tb:"
    traceback.print_tb(exc_traceback, limit=1, file=sys.stdout)
    print "*** print_exception:"
    traceback.print_exception(exc_type, exc_value, exc_traceback,
                             limit=15, file=sys.stdout)
    sys.exit()

class StoreMetricsConfig:
    def isTimePeriodic(self):
        return self.INTERVAL == 'time'

    def isInstrPeriodic(self):
        return self.INTERVAL == 'instr'
    
    def __del__(self):
        print '[StoreMetricsConfig:] dtor called, Resetting stderr'
        sys.stderr = sys.__stderr__

    def __init__(self, args):
        self.OPTS = ['NONE', 'MLM', 'RWP', 'SCP', 'ANN', 'WAYDOWN', 'UCP', 'XCH']   #SCP: static cache partitioning
        errfile = open("dvfsmetrics.stderr","w")
        sys.stderr = errfile
        args = dict(enumerate((args or '').split(':')))
        print "[UserArgs]", args
        #Arg0
        self.interval_inst = long(args.get(0, 1000000))
        
        assert(self.interval_inst > 10000)  #make sure the interval is not very small

        #Arg1
        #20-May-2015 HACK TODO: TargetMetric is being used for specifying the MDP configuration in file getmetrics/mdpconfig.py
        self.metricRange = .05    #5% either side
        self.targetMetric = -1.0
        try:
            self.targetMetric = float(args.get(1))
        except:
            self.targetMetric = -1.0
        
        self.doopt = None
        self.subopt = None
        self.periodicEnergy = True
        if self.targetMetric <= 0.0:
          self.doopt = 'None'
        elif self.targetMetric == 1000.0:     #14-Oct-2015 HACK TODO: TargetMetric=1000 being used for specifying Read Write Partitioning of L3
          self.doopt = 'RWP'
        elif self.targetMetric == 1001.0:     #4-Jan-2016 HACK TODO: TargetMetric=1001 being used for specifying static cache partitioning of L3
          self.doopt = 'SCP'
          import scp_configs
          self.scp = scp_configs.scp_configs()
        elif self.targetMetric == 1002.0:     #27-Jan-2016 HACK TODO TargetMetric=1002 being used for specifying ANN based co-opt
          self.doopt = 'ANN'
        elif self.targetMetric == 1003.0:     #27-Mar-2016 HACK TODO TargetMetric=1003 cache way shutdown
          self.doopt = 'WAYDOWN'
        elif self.targetMetric in range(1004, 1030):     #8-Apr-2016 HACK TODO TargetMetric=1004 UCP
          self.doopt = 'UCP'
          if self.targetMetric in range(1004, 1008): 
            self.subopt = 'UCP'  #1004-1007
          elif self.targetMetric == 1008.0:
            self.subopt = 'CS_UCP'
          elif self.targetMetric in range(1010, 1016): #MCFQ
            self.subopt = 'MCFQ'
          elif self.targetMetric in range(1016, 1021): #MLM_UCP
            self.subopt = 'MLP_UCP'
          else:
            self.subopt = 'PIPP'
            print " ============================ NOT SUPPORTED ==================", self.targetMetric
            assert(0)
        elif self.targetMetric == 1030.0:     #6-Aug-2016 HACK TODO TargetMetric=1030 XChange based multiple resource allocation
          self.doopt = 'XCH'
        else:
          self.doopt = 'MLM'
          if self.targetMetric in range(120, 150):  #120: L3, 130: L2, 140:L2+L3 
            self.subopt = 'RWS'

        #Arg2
        self.configid = 0 #now there is only 1 config
        self.sqlitedbpath = str(args.get(2))
        self.ds = DataStore(sim.config.ncores)
        self.ds.setExpDir(self.sqlitedbpath)
        
        #Arg3
        intr_type = int(args.get(3, 1)) #default is 1 i.e instr
        self.INTERVAL=['time', 'instr'][intr_type]
        
        #Arg4
        if self.doopt == 'SCP':
          scp_config = int(args.get(4))
          self.scp_wayassign = ",".join([str(cw) for cw in self.scp.getCacheWayAssignment4Config(scp_config)])
        
        print '[DvfsMetricsConfig] intrv, type, doopt, Tmetric ', self.interval_inst, self.INTERVAL, self.doopt, self.targetMetric
        self.core_dvfs_table, self.uncore_dvfs_table = self.build_dvfs_table(int(sim.config.get('power/technology_node')))

    def build_dvfs_table(self, tech):
      # Build a table of (frequency, voltage) pairs.
      # Frequencies should be from high to low, and end with zero (or the lowest possible frequency)
      self.valid_dvfs_entries = 4 #max index of valid DVFS entries
      if tech == 22:
        return ([ (2000, 1.0), (1800, 0.9), (1500, 0.8), (1000, 0.7), (600, 0.6) ], #core
                [ (2000, 1.0), (1800, 0.9), (1500, 0.8), (1000, 0.7), (600, 0.6) ]  #uncore
               )

      elif tech == 45:
#        return [ (2000, 1.2), (1800, 1.1), (1500, 1.0), (1000, 0.9), (600, 0.8) ]
        return ([ (3200, 1.2), (3000, 1.1), (2700, 1.0), (2400, 0.9), (2000, 0.8), (1, 10.0) ], #(1,0) is for power gating
                [ (3200, 1.2), (3000, 1.1), (2700, 1.0), (2400, 0.9), (2000, 0.8)]
#                [ (2000, 1.2), (1800, 1.1), (1500, 1.0), (1000, 0.9), (600, 0.8)] #(1,0) is for power gating
               )
      else:
        raise ValueError('No DVFS table available for %d nm technology node' % tech)

    def get_dvfs(self, idx, offset, comp='core'):
        i = idx + offset
        if i < 0:
            i = 0
        elif i > self.valid_dvfs_entries:
            i = self.valid_dvfs_entries
        if comp == 'core':
           return i, self.core_dvfs_table[i] 
        elif comp == 'uncore':
            return i, self.uncore_dvfs_table[i]
        else:
            assert ValueError('invalid comp value given as %s' % comp)
   
    def getFreqIdx(self, freq, comp='core'):
        if comp == 'core':
            table = self.core_dvfs_table
        elif comp == 'uncore':
            table = self.uncore_dvfs_table
        else:
            assert ValueError('invalid comp value given as %s' % comp)
        
        for i in range(len(table)):
            f,v = table[i]
            if  f == freq:
                return i

class StoreMetrics:
  def __del__(self):
      print '[StoreMetrics:] dtor called'

  def initSystem(self):
    self.initCores()
    self.initUnCore()
    self.initCaches()

  def initCores(self):
    #2D table of frequency histogram per core
    self.coreFreqHist = [[0 for f in range(len(self.config.core_dvfs_table))]\
                        for n in range(sim.config.ncores)] 

    #current freq index into the histogram
    self.coresIdx = [self.config.getFreqIdx(sim.dvfs.get_frequency(n), 'core')
                            for n in range(sim.config.ncores)]
    print '================================core', self.coresIdx
    #metric data collection per core
    self.metricData = [0 for n in range(sim.config.ncores)]
    for core in range(sim.config.ncores):
      fidx = self.coresIdx[core]
      self.coreFreqHist[core][fidx] += 1
      freq,voltage = self.config.core_dvfs_table[fidx]
      sim.dvfs.set_frequency(core, freq)
  
  def initUnCore(self):
    #2D table of frequency histogram
    self.uncoreFreqHist = [0 for f in range(len(self.config.uncore_dvfs_table))]

    #current freq index into the histogram
    self.uncoresIdx = self.config.getFreqIdx(sim.dvfs.get_uncore_frequency(), 'uncore')
    
    print '================================uncore', self.uncoresIdx
    fidx = self.uncoresIdx
    self.uncoreFreqHist[fidx] += 1
    freq,voltage = self.config.uncore_dvfs_table[fidx]
    sim.dvfs.set_uncore_frequency(freq)

  def initCaches(self):
    #2D table of ways assigned per core
    #TODO assuming associativity of 16
    associativity = long(sim.config.get('perf_model/l3_cache/associativity'))
    self.coreL3wayHist = [[0 for l in range(associativity+1)]\
                        for n in range(sim.config.ncores)] 
    associativity = long(sim.config.get('perf_model/l2_cache/associativity'))
    self.coreL2wayHist = [[0 for l in range(associativity+1)]\
                        for n in range(sim.config.ncores)] 
    associativity = long(sim.config.get('perf_model/l1_dcache/associativity'))
    self.coreL1DwayHist = [[0 for l in range(associativity+1)]\
                        for n in range(sim.config.ncores)] 
    associativity = long(sim.config.get('perf_model/l1_icache/associativity'))
    self.coreL1IwayHist = [[0 for l in range(associativity+1)]\
                        for n in range(sim.config.ncores)] 
#    print "[initCaches] cache histogram init"
    

  def printDvfsTable(self):
#    return
    if self.config.doopt == 'ANN':
      if self.metrictrace.anncoopt.isANNPhaseChange():
        print "[ANN]: Phase change, resetting configuration histrograms"
        #reset cache hist
        self.initCaches()

        #2D table of frequency histogram
        self.uncoreFreqHist = [0 for f in range(len(self.config.uncore_dvfs_table))]
        
        #2D table of frequency histogram per core
        self.coreFreqHist = [[0 for f in range(len(self.config.core_dvfs_table))]\
                            for n in range(sim.config.ncores)] 
#    print 'Core',
#    for f,v in self.config.core_dvfs_table:
#        print '\t%d' % (f),
#    for n in range(sim.config.ncores):
#        print '\nCore%d' % (n), 
#        for f in range(len(self.config.core_dvfs_table)):
#            print '\t%d' % (self.coreFreqHist[n][f]),
#    print '\n'
    
#    print 'Uncore',
#    for f,v in self.config.uncore_dvfs_table:
#        print '\t%d' % (f),
#    print '\nUncore', 
#    for f in range(len(self.config.uncore_dvfs_table)):
#        print '\t%d' % (self.uncoreFreqHist[f]),
#    print '\n'
    
    print 'L3Way',
    associativity = long(sim.config.get('perf_model/l3_cache/associativity'))
    for l in range(associativity+1):
        print '\t%d' % (l),
    for n in range(sim.config.ncores):
        print '\nCore%d' % (n),
        for l in range(associativity+1):
            print '\t%d' % (self.coreL3wayHist[n][l]),
    print '\n'

#    print 'L2Way',
#    associativity = long(sim.config.get('perf_model/l2_cache/associativity'))
#    for l in range(associativity+1):
#        print '\t%d' % (l),
#    for n in range(sim.config.ncores):
#        print '\nCore%d' % (n),
#        for l in range(associativity+1):
#            print '\t%d' % (self.coreL2wayHist[n][l]),
#    print '\n'
    
#    print 'L1DWay',
#    associativity = long(sim.config.get('perf_model/l1_dcache/associativity'))
#    for l in range(associativity+1):
#        print '\t%d' % (l),
#    for n in range(sim.config.ncores):
#        print '\nCore%d' % (n),
#        for l in range(associativity+1):
#            print '\t%d' % (self.coreL1DwayHist[n][l]),
#    print '\n'


  def dumpDvfsTable(self):
    fname = 'dvfsHistogram.stat'
    fout =  file(os.path.join(sim.config.output_dir, fname), 'w')
    #Headers
    fout.write('Core')
    for f,v in self.config.core_dvfs_table:
        fout.write('\t%d' % (f))
    #Values
    for n in range(sim.config.ncores):
        fout.write('\nCore%d' % (n))
        avgFreq = 0
        totIntv = 0
        for f in range(len(self.config.core_dvfs_table)):
            freq, volt = self.config.core_dvfs_table[f]
            totIntv += self.coreFreqHist[n][f]
            avgFreq += (freq*self.coreFreqHist[n][f])
            fout.write('\t%d' % (self.coreFreqHist[n][f]))
        fout.write('\t%1f' % (avgFreq/totIntv))
    fout.write('\n')

    #Headers
    fout.write('UnCore')
    for f,v in self.config.uncore_dvfs_table:
        fout.write('\t%d' % (f))
    #Values
    fout.write('\nUnCore')
    avgFreq = 0
    totIntv = 0
    for f in range(len(self.config.uncore_dvfs_table)):
        freq, volt = self.config.uncore_dvfs_table[f]
        totIntv += self.uncoreFreqHist[f]
        avgFreq += (freq*self.uncoreFreqHist[f])
        fout.write('\t%d' % (self.uncoreFreqHist[f]))
    fout.write('\t%1f' % (avgFreq/totIntv))
    fout.write('\n')

    #dump metricData
    fout.write('\nTotal_Intervals: ' + str(self.count) + ' ' + str(self.energystats.count))
    fout.write('\nAMAT: ' + str(self.metricData))
    sumMetric = sum(self.metricData)
    numSamples = self.count*sim.config.ncores
    fout.write('\nAVG_AMAT=%d' % int(sumMetric/numSamples or 1.0))

    fout.close()

  def setup(self, args):
    self.count = 0
    self.config = StoreMetricsConfig(args)
    self.initSystem()

    self.energystats = EnergyStats(self.config, periodicCall=self.config.periodicEnergy)
    sim.util.register(self.energystats)
    try:
        self.metrictrace = MetricTrace(self.config)
        self.metrictrace.setup(None)
        #sim.util.register(self.metrictrace)
        self.energystats.setGetMetrics(self.metrictrace)

    except:
        print "issue with getmetric setup"
        reportCrash()

    if self.config.isTimePeriodic():
        sim.util.Every(self.config.interval_inst*sim.util.Time.NS, self.periodicins, roi_only = True)
    elif self.config.isInstrPeriodic():
        sim.util.EveryIns(self.config.interval_inst, self.periodicins, roi_only = True)
    else:
        assert(0)
    
    try:
        metricdata = self.metrictrace.getMetrics()
        #print '[__init__] metric data: ', self.count, metricdata
    except:
        print "================ISSUE AT FIRST GETMETRICS CALL==================="
        reportCrash()

  def periodicins(self, icount, icount_delta):
    if icount_delta == 0:
        print "==========================================================="
        print "==========================================================="
        print "==========================================================="
        print "==========================================================="
        print "==========================================================="
        return

    self.count += 1
#    if self.count == 1: return  #ignoring the first interval metric data
    #especially the tpi (time per instruction) showed 100x lesser instructions executed
    #hence the metric being 100x more and hence detorting the target
#    print '[StoreMetrics] periodic: ', self.count, icount, icount_delta
#    self.energystats.update()        #DONOT call this directly, energystats has been registered to be called periodically

    try:
      self.getMetricData()
    except:
      print "Error calling getMetrics"
      reportCrash()
        
    try:
      if self.config.doopt == 'MLM':
        print '[DvfsMetric] doing dvfs for next interval ', self.count, icount, icount_delta
        self.doDvfs()
      elif self.config.doopt == 'RWP':
        print '[RWP] doing rwp for next interval ', self.count, icount, icount_delta
        sim.dvfs.do_rwp()
      elif self.config.doopt == 'UCP':
        print '[UCP] doing ucp for next interval ', self.count, icount, icount_delta  
        sim.dvfs.do_ucp(self.config.subopt)
      elif self.config.doopt == 'SCP':
        print '[SCP] doing scp for next interval ', self.config.scp_wayassign, self.count, icount, icount_delta
        sim.dvfs.set_l3_cacheway(self.config.scp_wayassign)
      elif self.config.doopt == 'ANN':
        print '[ANN] doing coopt for next interval ', self.count, icount, icount_delta
        self.doDvfs()
      elif self.config.doopt == 'XCH':
        print '[XCH] doing coopt for next interval ', self.count, icount, icount_delta
        sim.dvfs.do_xch()
      elif self.config.doopt == 'WAYDOWN':
        assign = ",".join(str(w) for w in [50000]*32)
        print '[WAYDOWN] doing cache way shutdown for next interval ', self.count, icount, icount_delta, assign
        sim.dvfs.do_l1d_shutdown(assign)

    except:
      print "********************************Error calling doDvfs/RWP/UCP"
      reportCrash()

  def getMetricData(self):
    metricdata = self.metrictrace.getMetrics()
#    print '[getMetricData] metric data: ', self.count, metricdata
    metricdata['configid'] = self.config.configid
    metricdata['i'] = self.count
    if sim.config.ncores < 8:
      self.config.ds.writeDb(metricdata)     #TODO Rahul: 3-June-2016 np8 data collection overhead is very high

    metricdata = self.metrictrace.getDvfsMetric()
    for core in range(sim.config.ncores):
        self.metricData[core] += metricdata[core]
  
  def doDvfs(self):
    reconfig = self.metrictrace.getReconfigData()
    core_offsets = reconfig.getCoreDvfsOffsets()
    uncore_offset = reconfig.getUnCoreDvfsOffset()
#    print '[doDvfs] core_offsets:', core_offsets, ' uncore= ', uncore_offset

    #Uncore
    curr_dvfs = self.uncoresIdx
    next_dvfs, (freq, voltage) = self.config.get_dvfs(curr_dvfs, uncore_offset, 'uncore')
    self.uncoreFreqHist[next_dvfs] += 1
    if next_dvfs != curr_dvfs:
        sim.dvfs.set_uncore_frequency(freq)
        self.uncoresIdx = next_dvfs
        reconfig.setUnCoreDvfsAllLevel(next_dvfs) 

    #Core
    for core in range(sim.config.ncores):
      if not self.isCorePowerGated(core):
          curr_dvfs = self.coresIdx[core]
          next_dvfs, (freq, voltage) = self.config.get_dvfs(curr_dvfs, core_offsets[core], 'core')
          self.coreFreqHist[core][next_dvfs] += 1
          if next_dvfs != curr_dvfs:
            sim.dvfs.set_frequency(core, freq)
            self.coresIdx[core] = next_dvfs
            reconfig.setCoreDvfs3Level(core, self.getCoreDvfs3Level(next_dvfs)) 
            reconfig.setCoreDvfsAllLevel(core, next_dvfs) 

    #L3 Cache
    cacheWayAssign = reconfig.getL3wayAssignments()
    arg_str = ",".join([str(cw) for cw in cacheWayAssign])
#    print "[L3 CacheReconfig] cacheWayAssign:", arg_str
    sim.dvfs.set_l3_cacheway(arg_str)
    self.updateCacheHistogram(self.coreL3wayHist, cacheWayAssign)
    
    #L2 Cache
    cacheWayAssign = reconfig.getL2wayAssignments()
    arg_str = ",".join([str(cw) for cw in cacheWayAssign])
#    print "[L2 CacheReconfig] cacheWayAssign:", arg_str
    sim.dvfs.set_l2_cacheway(arg_str)
    self.updateCacheHistogram(self.coreL2wayHist, cacheWayAssign)
   
    #L1D Cache
    cacheWayAssign = reconfig.getL1DwayAssignments()
    arg_str = ",".join([str(cw) for cw in cacheWayAssign])
#    print "[L1D CacheReconfig] cacheWayAssign:", arg_str
    sim.dvfs.set_l1d_cacheway(arg_str)
    self.updateCacheHistogram(self.coreL1DwayHist, cacheWayAssign)

    #L1I Cache
    cacheWayAssign = reconfig.getL1IwayAssignments()
    arg_str = ",".join([str(cw) for cw in cacheWayAssign])
#    print "[L1I CacheReconfig] cacheWayAssign:", arg_str
    sim.dvfs.set_l1i_cacheway(arg_str)
    self.updateCacheHistogram(self.coreL1IwayHist, cacheWayAssign)
    
    #print the reconfig
    self.printDvfsTable()

  def updateCacheHistogram(self, coreCachewayHist, cacheWayAssign):
    #TODO
#    return
    
    #update histogram
    wayAssignSorted = sorted(cacheWayAssign)
    wc=-1
    lw_cnt=0
    for wcs in wayAssignSorted + [-1,]: #-1 added in last to capture histogram of last core
        
        #TODO HACKY
        if wcs >= 2000: wcs=wcs-2000
        if wcs >= 1000: wcs=wcs-1000
        
        if wcs==wc: 
            lw_cnt+=1
        else:#next core assign started
            #print wc, lw_cnt,self.coreL3wayHist
            if wc>=0: coreCachewayHist[wc][lw_cnt] += 1 
            wc=wcs
            lw_cnt=1

  def isL3Reassign(self, core_offsets):
    isPos=False
    isNeg=False
    for core in range(sim.config.ncores):
        if core_offsets[core] == 1: isPos=True
        elif core_offsets[core] == -1: isNeg=True
    return isPos and isNeg

  def getHackL3Assign(self, hackl3way, core_offsets):
    assert(sim.config.ncores == 4)
    pos=-1
    neg=-1
#    for pcore in range(sim.config.ncores):
    while self.isL3Reassign(core_offsets):
        import random
        pcore = random.randint(0,sim.config.ncores-1)
        if core_offsets[pcore] == 1:
            pos = pcore
            #find corresponding neg
            #for ncore in range(neg+1,sim.config.ncores):
            while self.isL3Reassign(core_offsets):
                ncore = random.randint(0,sim.config.ncores-1) 
                if core_offsets[ncore] == -1:
                    neg = ncore
                    try:
                        #pos neg pair found, update hackl3way
                        way = hackl3way.index(neg)
                        hackl3way[way] = pos
                        core_offsets[pcore] = 0
                        core_offsets[ncore] = 0
                        break
                    except:
                        core_offsets[ncore] = 0   #set to 0, so that this is not evaluated again
                        print '[HACK L3] hackl3way does not hv negway', hackl3way, neg 
    print '[HACK L3] returning hackl3way', hackl3way, pos, neg
    return hackl3way

  def isCorePowerGated(self, core):
    return sim.dvfs.get_frequency(core) == '1'
  
  ##-1: MIN Freq, 1:MAX freq, 0:inbetween freq
  def getCoreDvfs3Level(self, dvfs_idx):
    if dvfs_idx == 0: return -1
    elif dvfs_idx == self.config.valid_dvfs_entries: return 1
    else: return 0

  def hook_roi_end(self):
    print 'dvfsmetrics hook_roi_end:',  sim.stats.time() 
    self.energystats.update() #call energy computation before simulation exit
    self.metrictrace.mdp.markEnd()
    print "markEnd called"

  def hook_sim_end(self):
    print 'dvfsmetrics hook_sim_end:',  sim.stats.time() 
    try:
      print "getmetric dtor called"
      del self.metrictrace
    except:
      print "Issue calling getmetric dtor"
    self.dumpDvfsTable()


sim.util.register(StoreMetrics())
