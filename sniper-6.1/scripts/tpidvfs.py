"""
tpi based auto-dvfs
adapted from amatdvfs
"""

import sys, os, sim
from cpitrace import CpiTrace
from amattrace import AmatTrace

PEAK_DYN='Peak Dynamic'
RUNTIME_DYN='Runtime Dynamic'
MetricTrace=AmatTrace
class AmatDvfsConfig:
    def __init__(self, args):
        args = dict(enumerate((args or '').split(':')))
        print "[UserArgs]", args
        self.interval_ns = long(args.get(0, 1000000))
        
        self.metricRange = .05    #5% either side
        self.targetMetric = [-1.0 for core in range(sim.config.ncores)]
        try:
            self.targetMetric = [float(args.get(1)) for core in range(sim.config.ncores)]
            self.dodvfs = True
        except:
            self.targetMetric = [-1.0 for core in range(sim.config.ncores)]

        if self.targetMetric[0] <= 0.0:
            self.dodvfs = False

        print '[AmatDvfsConfig] intrv, doDVFS, Tamat ', self.interval_ns, self.dodvfs, self.targetMetric

        self.dvfs_table = self.build_dvfs_table(int(sim.config.get('power/technology_node')))

    def get_dvfs(self, core, idx, metric):
        offset = 0
        if metric > self.targetMetric[core] * (1.0+self.metricRange): #slow down
            offset = 1
        elif metric < self.targetMetric[core] * (1.0-self.metricRange): #go fast
            offset = -1

        #TODO: TPI based experiment
        #update targetMetric to last interval metric, this should give a good self converging targetMetric
        self.targetMetric[core] = metric

        i = idx + offset
        if i < 0:
            i = 0
        elif i >= len(self.dvfs_table):
            i = len(self.dvfs_table) -1
        return i, self.dvfs_table[i] 

    def build_dvfs_table(self, tech):
      # Build a table of (frequency, voltage) pairs.
      # Frequencies should be from high to low, and end with zero (or the lowest possible frequency)
      if tech == 22:
        return [ (2000, 1.0), (1800, 0.9), (1500, 0.8), (1000, 0.7), (600, 0.6) ]
      elif tech == 45:
        return [ (2000, 1.2), (1800, 1.1), (1500, 1.0), (1000, 0.9), (600, 0.8) ]
      else:
        raise ValueError('No DVFS table available for %d nm technology node' % tech)

class AmatDvfs:
  def initCores(self):
    #2D table of frequency histogram per core
    self.freqHist = [[0 for f in range(len(self.config.dvfs_table))]\
                        for n in range(sim.config.ncores)] 
    #current freq index into the histogram
    self.ncores = [0 for n in range(sim.config.ncores)]
    #metric data collection per core
    self.metricData = [0 for n in range(sim.config.ncores)]
    for core in range(sim.config.ncores):
      fidx = self.ncores[core]
      self.freqHist[core][fidx] += 1
      freq,voltage = self.config.dvfs_table[fidx]    
      sim.dvfs.set_frequency(core, freq)

  def printDvfsTable(self):
    print 'Core',
    for f,v in self.config.dvfs_table:
        print '\t%d' % (f),
    for n in range(sim.config.ncores):
        print '\nCore%d' % (n), 
        for f in range(len(self.config.dvfs_table)):
            print '\t%d' % (self.freqHist[n][f]),
    print '\n'
    
  def dumpDvfsTable(self):
    fname = 'dvfsHistogram.stat'
    fout =  file(os.path.join(sim.config.output_dir, fname), 'w')
    #Headers
    fout.write('Core')
    for f,v in self.config.dvfs_table:
        fout.write('\t%d' % (f))
    #Values
    for n in range(sim.config.ncores):
        fout.write('\nCore%d' % (n))
        avgFreq = 0
        totIntv = 0
        for f in range(len(self.config.dvfs_table)):
            freq, volt = self.config.dvfs_table[f]
            totIntv += self.freqHist[n][f]
            avgFreq += (freq*self.freqHist[n][f])
            fout.write('\t%d' % (self.freqHist[n][f]))
        fout.write('\t%1f' % (avgFreq/totIntv))
    fout.write('\n')

    #dump metricData
    fout.write('\nTotal_Intervals: ' + str(self.count) + ' ' + str(self.energystats.count))
    fout.write('\nAMAT: ' + str(self.metricData))
    sumMetric = sum(self.metricData)
    numSamples = self.count*sim.config.ncores
    fout.write('\nAVG_AMAT=%d' % int(sumMetric/numSamples))

    fout.close()

  def setup(self, args):
    self.count = 0
    self.config = AmatDvfsConfig(args)
    self.initCores()

    self.metrictrace = MetricTrace(self.config)
    sim.util.register(self.metrictrace)

    self.energystats = EnergyStats(self.config)
    sim.util.register(self.energystats)

    sim.util.Every(self.config.interval_ns*sim.util.Time.NS, self.periodic, roi_only = True)

  def periodic(self, time, time_delta):
    if time_delta == 0:
        return

    self.count += 1
    if self.count == 1: return  #ignoring the first interval metric data
    #especially the tpi (time per instruction) showed 100x lesser instructions executed
    #hence the metric being 100x more and hence detorting the target
    print '[AmatDvfs] periodic: ', self.count, time, time_delta
    self.energystats.update()

    self.getMetricData()
    if self.config.dodvfs == True:
        print '[AmatDvfs] doing dvfs for next interval ', self.count, time, time_delta
        self.doDvfs()

  def getMetricData(self):
    metricdata = self.metrictrace.getMetric()
    print '[getMetricData] metric data: ', self.count, metricdata
    for core in range(sim.config.ncores):
        self.metricData[core] += metricdata[core]

  def doDvfs(self):
    metricdata = self.metrictrace.getMetric()
    print '[AmatDvfs] metric data: ', metricdata
    
    for core in range(sim.config.ncores):
      curr_dvfs = self.ncores[core]
      curr_metric = metricdata[core]
      next_dvfs, (freq, voltage) = self.config.get_dvfs(core, curr_dvfs, curr_metric)
      self.freqHist[core][next_dvfs] += 1
      if next_dvfs != curr_dvfs:
        sim.dvfs.set_frequency(core, freq)
        self.ncores[core] = next_dvfs
    
    self.printDvfsTable()

  def hook_roi_end(self):
    print 'hook_roi_end:',  sim.stats.time() 
    self.energystats.update()

  def hook_sim_end(self):
    print 'hook_sim_end:',  sim.stats.time() 
    self.dumpDvfsTable()

class Power:
  def __init__(self, static, dynamic):
    self.s = static
    self.d = dynamic
  def __add__(self, v):
    return Power(self.s + v.s, self.d + v.d)
  def __sub__(self, v):
    return Power(self.s - v.s, self.d - v.d)

class EnergyStats:
  def __init__(self, config):
    self.config = config
    
  def setup(self, args):
    interval_ns = self.config.interval_ns 
    sim.util.Every(interval_ns * sim.util.Time.NS, self.periodic, roi_only = True)
    self.dvfs_table = self.config.dvfs_table

    self.name_last = None 
    self.time_last_power = 0
    self.time_last_energy = 0
    self.in_stats_write = False
    self.count = 0
    self.power = {}
    self.energy = {}
    for metric in ('energy-static', 'energy-dynamic'):
      for core in range(sim.config.ncores):
        sim.stats.register('core', core, metric, self.get_stat)
        sim.stats.register('L1-I', core, metric, self.get_stat)
        sim.stats.register('L1-D', core, metric, self.get_stat)
        sim.stats.register('L2', core, metric, self.get_stat)
      sim.stats.register_per_thread('core-'+metric, 'core', metric)
      sim.stats.register_per_thread('L1-I-'+metric, 'L1-I', metric)
      sim.stats.register_per_thread('L1-D-'+metric, 'L1-D', metric)
      sim.stats.register_per_thread('L2-'+metric, 'L2', metric)
      sim.stats.register('processor', 0, metric, self.get_stat)
      sim.stats.register('dram', 0, metric, self.get_stat)
      sim.stats.register('L3', 0, metric, self.get_stat)
      sim.stats.register('NOC', 0, metric, self.get_stat)

  def periodic(self, time, time_delta):
    self.update()
    return

  def update(self):
    print "[AMATDVFS Power] ", sim.stats.time()
    if sim.stats.time() == self.time_last_power:
      # Time did not advance: don't recompute
      return
    #if not self.power or (sim.stats.time() - self.time_last_power >= 10 * sim.util.Time.US):
    if True:
      self.count += 1
      #print "[AMATDVFS Power] sim_time ", sim.stats.time(), self.count, sim.util.Time.NS
      # Time advanced significantly, or no power result yet: compute power
      #   Save snapshot
      current = 'energystats-temp%s' % ('B' if self.name_last and self.name_last[-1] == 'A' else 'A')
      self.in_stats_write = True
      sim.stats.write(current)
      self.in_stats_write = False
      #   If we also have a previous snapshot: update power
      if self.name_last:
        power = self.run_power(self.name_last, current)
        self.update_power(power)
      #   Clean up previous last
      if self.name_last:
        sim.util.db_delete(self.name_last)
      #   Update new last
      self.name_last = current
      self.time_last_power = sim.stats.time()
      # Increment energy
      self.update_energy()
  
  def get_stat(self, objectName, index, metricName):
#    if not self.in_stats_write:
#      self.update()
    return self.energy.get((objectName, index, metricName), 0L)

  def update_power(self, power):
    def get_power(component, dynamic=RUNTIME_DYN, prefix = ''):
      return Power(component[prefix + 'Subthreshold Leakage'] + component[prefix + 'Gate Leakage'], component[prefix + dynamic])
 
    #all caches are 'Peak Dynamic' power, others are 'Runtime Dynamic'
    #this is inline after tuning the numbers to intel physical power numbers
    for core in range(sim.config.ncores):
      self.power[('L1-I', core)] = get_power(power['Core'][core], PEAK_DYN, 'Instruction Fetch Unit/Instruction Cache/')
      self.power[('L1-D', core)] = get_power(power['Core'][core], PEAK_DYN, 'Load Store Unit/Data Cache/')
      self.power[('L2',   core)] = get_power(power['Core'][core], PEAK_DYN, 'L2/')
#      self.power[('core', core)] = get_power(power['Core'][core], RUNTIME_DYN) - (self.power[('L1-I', core)] + self.power[('L1-D', core)] + self.power[('L2', core)])
      self.power[('core', core)] = get_power(power['Core'][core], RUNTIME_DYN) - (get_power(power['Core'][core], RUNTIME_DYN, 'Instruction Fetch Unit/Instruction Cache/') +\
                                                                                  get_power(power['Core'][core], RUNTIME_DYN, 'Load Store Unit/Data Cache/') +\
                                                                                  get_power(power['Core'][core], RUNTIME_DYN, 'L2/') )
    self.power[('processor', 0)] = get_power(power['Processor'], RUNTIME_DYN)
    self.power[('dram', 0)] = get_power(power['DRAM'], RUNTIME_DYN)
    self.power[('L3', 0)] = get_power(power['L3'][0], PEAK_DYN)
    #self.power[('NOC', 0)] = get_power(power['BUSES'])
    self.power[('NOC', 0)] = get_power(power['NOC'], RUNTIME_DYN)

  def update_energy(self):
    if self.power and sim.stats.time() > self.time_last_energy:
      time_delta = sim.stats.time() - self.time_last_energy
      for (component, core), power in self.power.items():
        if component in ['dram', 'L3', 'NOC']:
            static_scale = 1.0
            dynamic_scale =1.0
        else:    
            #TODO: STARTDVFS once start f,v table is created, then update this
            f0, v0 = self.dvfs_table[0]
            f = sim.dvfs.get_frequency(core)
            v = self.get_vdd_from_freq(f)
            #static scale is linear with v
            static_scale = float(v/v0)
            #dynamic scale is sq with v
            dynamic_scale = float(v/v0)*float(v/v0)*float(float(f)/f0)
            #print f, f0
        #print '[update_energy]', component, core, dynamic_scale, static_scale
        self.energy[(component, core, 'energy-static')] = self.energy.get((component, core, 'energy-static'), 0) + long(time_delta * power.s * static_scale)
        self.energy[(component, core, 'energy-dynamic')] = self.energy.get((component, core, 'energy-dynamic'), 0) + long(time_delta * power.d * dynamic_scale)
      self.time_last_energy = sim.stats.time()

  def get_vdd_from_freq(self, f):
    # Assume self.dvfs_table is sorted from highest frequency to lowest
    for _f, _v in self.dvfs_table:
      if f >= _f:
        return _v
    assert ValueError('Could not find a Vdd for invalid frequency %f' % f)

  def gen_config(self, outputbase):
    #freq = [ sim.dvfs.get_frequency(core) for core in range(sim.config.ncores) ]
    #vdd = [ self.get_vdd_from_freq(f) for f in freq ]
    #TODO: STARTDVFS store the starting f,v in a separate variable, this assumes that all cores starting at dvfs_table index 0
    _f, _v = self.dvfs_table[0]
    freq = [_f]*sim.config.ncores
    vdd  = [_v]*sim.config.ncores
    configfile = outputbase+'.cfg'
    cfg = open(configfile, 'w')
    cfg.write('''
[perf_model/core]
frequency[] = %s
[power]
vdd[] = %s
    ''' % (','.join(map(lambda f: '%f' % (f / 1000.), freq)), ','.join(map(str, vdd))))
    cfg.close()
    return configfile

  def run_power(self, name0, name1):
    outputbase = os.path.join(sim.config.output_dir, 'energystats-temp') #+ str(self.count))

    configfile = self.gen_config(outputbase)

    os.system('unset PYTHONHOME; %s -d %s -o %s -c %s --partial=%s:%s --no-graph --no-text' % (
      os.path.join(os.getenv('SNIPER_ROOT'), 'tools/mcpat.py'),
      sim.config.output_dir,
      outputbase,
      configfile,
      name0, name1
    ))

    result = {}
    execfile(outputbase + '.py', {}, result)
    return result['power']

sim.util.register(AmatDvfs())
