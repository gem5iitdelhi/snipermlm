import sys, os, sqlite3

class bestConfig:
    def __init__(self, c, i, e, t):
        self.c = c
        self.i = i
        self.e = e
        self.t = t
        self.bt = t
        self.RANGE = 0.05   #5% time hit allowed

    def printConfig(self, fout):
        fout.write(str(self.i) + ',' + str(self.e) + ',' + str(self.t) + ',' + self.c)
        fout.write('\n')
    
    #if t is within acceptable limit then consider
    def update(self,c,e,t):
        if self.isBetter(e,t):
            self.c, self.e, self.t = c,e,t
    
    def isBetter(self, e, t):
        return self.isBetterEnergy(e,t)
        #return self.isBetterEDP(e,t)

    def isBetterEDP(self, e, t):
        if t*e < self.t*self.e: return True
        return False

    def isBetterEnergy(self, e, t):
        if t > self.bt*(1.0+self.RANGE): return False
        if e > self.e: return False
        return True

class bestConfigData:
    def __init__(self, dbpath):
        self.bestTable = 'besttable'
        self.dbfile = dbpath + '/best.db'
        self.attribs = ['intv', 'bestcorefreq']

    def createTable(self):
        conn = sqlite3.connect(self.dbfile)
        create_table = 'create table ' + self.bestTable
        fields = ' ( '
        for m in self.attribs:
            fields += m + ' integer,'
        fields = fields[0:-1] + ' )'
        create_table += fields
#        print create_table
        conn.execute(create_table)
        conn.close()

    def writeDb(self, best):
        self.createTable()
        conn = sqlite3.connect(self.dbfile)
        insert_query = 'insert into ' + self.bestTable
        fields = ' ( '
        for m in self.attribs:
            fields += m + ','
        fields = fields[0:-1] + ' )'
        for i,b in best.items():
            values = ' values(' + str(b.i) + ',' + str(b.c) + ')'
#            print insert_query + fields + values
            conn.execute(insert_query + fields + values)
        conn.commit()
        conn.close()
    
    def readDb(self):
        print '[bestConfigData]', self.dbfile
        conn = sqlite3.connect(self.dbfile)
        query = 'select ' + ','.join(self.attribs) +\
                ' from ' + self.bestTable +\
                ' order by intv '
        #print query
        cursor = conn.execute(query)
        js_data = {}
        intv_list = []
        fields = self.attribs[1:]   #no intv
        for f in fields:
            js_data[f] = []

        for row in cursor:
#            print row
            intv_list.append(row[0])
            for f,i in zip(fields, range(len(fields))):
                i = i+1 #first 1st field is interval    TODO
#                print f,i, row[i]
                js_data[f].append(row[i])
            
        conn.close()
        return js_data, intv_list
