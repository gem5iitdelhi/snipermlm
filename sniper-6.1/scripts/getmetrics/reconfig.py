import sys, os, sqlite3
import sim

#class which stores the system reconfiguration decision
class reconfig:
    def __init__(self):
        self.np = sim.config.ncores
        self.coreDvfsOffsets = [0 for n in range(self.np)]
        self.coreDvfs3Levels = [0 for n in range(self.np)]   #-1: MIN Freq, 1:MAX freq, 0:inbetween freq
        self.coreDvfsAllLevels = [0 for n in range(self.np)]   #this is basically the dvfs idx in the dvfs table
        self.uncoreDvfsIndex = 0
        self.uncoreDvfsOffset = 0

        self.__initCaches()

    #Core 
    def getCoreDvfsOffsets(self):
        return self.coreDvfsOffsets

    def setCoreDvfsOffset(self, n, offset):
        self.coreDvfsOffsets[n] = offset

    def setCoreDvfsOffsets(self, offsets):
        self.coreDvfsOffsets = [offsets[n] for n in range(self.np)]
    
    def getCoreDvfs3Level(self, n):
        return self.coreDvfs3Levels[n]

    def setCoreDvfs3Level(self, n, level):
        self.coreDvfs3Levels[n] = level

    def getCoreDvfsAllLevel(self, n):
        return self.coreDvfsAllLevels[n]

    def setCoreDvfsAllLevel(self, n, level):
        self.coreDvfsAllLevels[n] = level

    #UnCore
    def setUnCoreDvfsOffset(self, offset):
        self.uncoreDvfsOffset = offset
    
    def getUnCoreDvfsOffset(self):
        return self.uncoreDvfsOffset
    
    def getUnCoreDvfsAllLevel(self):
        return self.uncoreDvfsIndex

    def setUnCoreDvfsAllLevel(self, level):
        self.uncoreDvfsIndex = level
    
    #Caches
    def __initCaches(self):
        self.cacheInfo = {}
        smt = long(sim.config.get('perf_model/core/logical_cpus'))
        for cache_level in ['l3_cache', 'l2_cache', 'l1_dcache', 'l1_icache']:
          if cache_level == 'l3_cache':
            self.__initCache(cache_level, 1)  #shared cores on L3 unaffected by smt
          else:
            self.__initCache(cache_level, smt)

    def __getDefCoreAssignment(self, cache_level):
      if sim.config.has_key('perf_model/'+cache_level+'/opt') == False: return -1
      elif sim.config.get('perf_model/'+cache_level+'/opt') == "rws":   return 0
      else:                                                             return -1

    def __initCache(self, cache_level, shared_cores_mult):
        cInfo = {}
        cInfo['sharedCores'] = long(sim.config.get('perf_model/'+cache_level+'/shared_cores')) * shared_cores_mult
        cInfo['associativity'] = long(sim.config.get('perf_model/'+cache_level+'/associativity'))
        cInfo['num'] = long((sim.config.ncores-1)/cInfo['sharedCores'])+1
        cInfo['min_shared'] = long(sim.config.get('perf_model/'+cache_level+'/min_shared_ways'))
       
        def_core = self.__getDefCoreAssignment(cache_level)
        cInfo['wayAssignment'] = [def_core for n in range(cInfo['num']*cInfo['associativity'])]
        
        excl_ways = cInfo['associativity'] - cInfo['min_shared']
        #assert(excl_ways % cInfo['sharedCores'] == 0) #//We are being strict so that each core gets same cache to start with, need not be
        fair_share = excl_ways/cInfo['sharedCores']
        for c in range(cInfo['num']):
          mcore = c*cInfo['sharedCores']
          for i in range(cInfo['min_shared'], fair_share*cInfo['sharedCores']+cInfo['min_shared']):
            j = c*cInfo['associativity'] + i
            cInfo['wayAssignment'][j] = mcore + (i-cInfo['min_shared'])/fair_share
        print "[ReconfigInfo] __initCache ", cache_level, cInfo['wayAssignment']

        self.cacheInfo[cache_level] = cInfo 

    def getNumWaysAssigned(self, cache_level, core):
      return self.cacheInfo[cache_level]['wayAssignment'].count(core)

    def getPercentWaysAssigned(self, cache_level, core):
      return self.getNumWaysAssigned(cache_level, core)/float(self.cacheInfo[cache_level]['associativity'])

    def getWaysAssignedUtility(self, cache_level, core):
      shared_ways = self.getNumWaysAssigned(cache_level, -1)  #TODO shared ways are -1
      core_ways = self.getNumWaysAssigned(cache_level, core)
      sharing_cores = self.cacheInfo[cache_level]['sharedCores']
      ways = core_ways + float(shared_ways)/sharing_cores
      #Assuming each additional way to have utility, which is a exponential function
      uTable = { 'l3_cache':1.015, 'l2_cache':1.01, 'l1_dcache':1.10, } #based on single bench runs
      utility = pow(uTable[cache_level], ways)
      return utility 


    def getL3wayAssignments(self):
        return self.cacheInfo['l3_cache']['wayAssignment']
    
    def setL3wayAssignments(self, assigns):
        self.cacheInfo['l3_cache']['wayAssignment'] = assigns
    
    def setL3wayAssignment(self, l3, a, core):
        assoc = self.cacheInfo['l3_cache']['associativity']
        self.cacheInfo['l3_cache']['wayAssignment'][(l3*assoc)+a] = core

    def getL2wayAssignments(self):
        return self.cacheInfo['l2_cache']['wayAssignment']
    
    def setL2wayAssignments(self, assigns):
        self.cacheInfo['l2_cache']['wayAssignment'] = assigns
    
    def setL2wayAssignment(self, l2, a, core):
        assoc = self.cacheInfo['l2_cache']['associativity']
        self.cacheInfo['l2_cache']['wayAssignment'][(l2*assoc)+a] = core
    
    def getL1DwayAssignments(self):
        return self.cacheInfo['l1_dcache']['wayAssignment']
    
    def setL1DwayAssignments(self, assigns):
        self.cacheInfo['l1_dcache']['wayAssignment'] = assigns
    
    def setL1DwayAssignment(self, l1d, a, core):
        assoc = self.cacheInfo['l1_dcache']['associativity']
        self.cacheInfo['l1_dcache']['wayAssignment'][(l1d*assoc)+a] = core
    
    def getL1IwayAssignments(self):
        return self.cacheInfo['l1_icache']['wayAssignment']
    
    def setL1IwayAssignments(self, assigns):
        self.cacheInfo['l1_icache']['wayAssignment'] = assigns
    
    def setL1IwayAssignment(self, l1i, a, core):
        assoc = self.cacheInfo['l1_icache']['associativity']
        self.cacheInfo['l1_icache']['wayAssignment'][(l1i*assoc)+a] = core
    
