import sys, os, sim, time
import metrics
from metrics import getSqlFieldName
import metrics
from reconfig import reconfig
from anncoopt import AnnCoopt

MC_HOME=os.environ['HOME']
sys.path.insert(1, MC_HOME + '/Phd/bitbucket/sniper/pybrain/sniper')
from mpi_sniper import MpiSniperMdp
from mdpconfig import mdp_options
from stateAssign import stateAssign


class GetMetrics:
  def __init__(self, config):
    self.config = config
    self.reconfig = reconfig()
    self.old_reconfig = None
    self.interval = 0
    if self.config.targetMetric > 2000:  #20-May-2015 HACK TODO
        self.metricFunc = 'TPI'
    elif self.config.targetMetric == 0:
        self.metricFunc = 'NODVFS'
    
    if self.config.doopt == 'ANN':
        self.metricFunc = 'ANN' 
#        self.spawnANN()
    elif self.config.doopt == 'SCP':
        self.metricFunc = 'SCP'
    elif self.config.doopt == 'WAYDOWN':
        self.metricFunc = 'WAYDOWN'
    elif self.config.doopt == 'RWP':
        self.metricFunc = 'RWP'
    elif self.config.doopt == 'UCP':
        self.metricFunc = 'UCP'
    elif self.config.doopt == 'MLM':
        self.metricFunc = 'MDP' 
        self.spawnMdp()
    elif self.config.doopt == 'None':
        self.metricFunc = 'None'
    elif self.config.doopt == 'XCH':
        self.metricFunc = 'XCH'
    else:
        assert(0)
 
  def getSniperConfig4MDP(self):
    return self.reconfig.cacheInfo

  def spawnMdp(self):
    #first 100 are reserved for specifying MDP config present in mdpconfig.py
    self.mdpConfig = int(self.config.targetMetric)   #20-May-2015 HACK
    
    #create state assigners
    if mdp_options[self.mdpConfig]['coreDvfs']==True:
        self.coreStateAssigner = stateAssign(mdp_options[self.mdpConfig]['coreNumStates'])  
    if mdp_options[self.mdpConfig]['l3wayreconfig']==True:
        self.l3StateAssigner = stateAssign(mdp_options[self.mdpConfig]['l3wayNumStates'])  
    if mdp_options[self.mdpConfig]['l2wayreconfig']==True:
        self.l2StateAssigner = stateAssign(mdp_options[self.mdpConfig]['l2wayNumStates'])  
    if mdp_options[self.mdpConfig]['l1dwayreconfig']==True:
        self.l1dStateAssigner = stateAssign(mdp_options[self.mdpConfig]['l1dwayNumStates'])  
    if mdp_options[self.mdpConfig]['l1iwayreconfig']==True:
        self.l1iStateAssigner = stateAssign(mdp_options[self.mdpConfig]['l1iwayNumStates'])  
   
    #create a pickle file for passing config params to mlm
    import pickle
    import random
    picklefile = self.config.sqlitedbpath + '/sniperconfig' + str(random.randint(0,1000)) + '.pickle'
    pfile = open(picklefile, 'w')
    pickle.dump(self.getSniperConfig4MDP(), pfile)
    pfile.close()

    mpifile = self.config.sqlitedbpath + '/snipermdp' + str(random.randint(0,1000)) + '.mpi'
    os.system('rm -f ' + mpifile + ';touch ' + mpifile)
    MDPCMD=MC_HOME + '/Phd/bitbucket/sniper/pybrain/sniper/mlm_rl.py ' + \
            mpifile + ' ' +  \
            str(sim.config.ncores) + ' ' + \
            str(self.mdpConfig) + ' ' + \
            str(picklefile) + ' ' + \
            '&'
    print MDPCMD
    os.system(MDPCMD)
    self.mdp = MpiSniperMdp(mpifile, sim.config.ncores, create_db=True)


  def __del__(self):
    print '[GetMetrics] dtor called ', self.metricFunc
    sys.stdout.flush()
    if self.metricFunc == 'MDP':
        self.mdp.markEnd()
        if mdp_options[self.mdpConfig]['coreDvfs']==True:
            print "del coreStateAssigner"
            del self.coreStateAssigner
        if mdp_options[self.mdpConfig]['l3wayreconfig']==True:
            print "del l3StateAssigner"
            del self.l3StateAssigner
        if mdp_options[self.mdpConfig]['l2wayreconfig']==True:
            print "del l2StateAssigner"
            del self.l2StateAssigner
        if mdp_options[self.mdpConfig]['l1dwayreconfig']==True:
            print "del l1dStateAssigner"
            del self.l1dStateAssigner
        if mdp_options[self.mdpConfig]['l1iwayreconfig']==True:
            print "del l1iStateAssigner"
            del self.l1iStateAssigner
    elif self.metricFunc == 'ANN':
        self.anncoopt.mpi.markEnd()

  def setup(self, args):
    self.sd = sim.util.StatsDelta()
    l3sharedcores = self.reconfig.cacheInfo['l3_cache']['sharedCores'] 
    self.stats = {
      #define all metrics in metric_list here
      'time': [ self.sd.getter('performance_model', core, 'elapsed_time') for core in range(sim.config.ncores) ],
      'ffwd_time': [ self.sd.getter('fastforward_performance_model', core, 'fastforwarded_time') for core in range(sim.config.ncores) ],
      'instrs': [ self.sd.getter('performance_model', core, 'instruction_count') for core in range(sim.config.ncores) ],
      'coreinstrs': [ self.sd.getter('core', core, 'instructions') for core in range(sim.config.ncores) ],
      'l3_dram_queue_time': [self.sd.getter('L3', core, 'uncore-time-dram-queue') for core in range(sim.config.ncores) ],
      'l3_dram_requests': [self.sd.getter('L3', core, 'uncore-requests') for core in range(sim.config.ncores) ],
      'l3_uncore_time': [self.sd.getter('L3', core, 'uncore-totaltime') for core in range(sim.config.ncores) ],
      
      'nw_contention_delay': [self.sd.getter('network.shmem-1.mesh', core, 'contention-delay') for core in range(sim.config.ncores) ],
      'nw_linkin_requests': [self.sd.getter('network.shmem-1.mesh.link-in', 0, 'num-requests') for core in range(sim.config.ncores) ],
      'nw_linkout_requests': [self.sd.getter('network.shmem-1.mesh.link-out', 0, 'num-requests') for core in range(sim.config.ncores) ],
      'nw_linkin_qdelay': [self.sd.getter('network.shmem-1.mesh.link-in', 0, 'total-queue-delay') for core in range(sim.config.ncores) ],
      'nw_linkout_qdelay': [self.sd.getter('network.shmem-1.mesh.link-out', 0, 'total-queue-delay') for core in range(sim.config.ncores) ],

      'l1d_loads': [self.sd.getter('L1-D', core, 'loads') for core in range(sim.config.ncores) ],
      'l1d_stores': [self.sd.getter('L1-D', core, 'stores') for core in range(sim.config.ncores) ],
      'l1d_load_miss': [self.sd.getter('L1-D', core, 'load-misses') for core in range(sim.config.ncores) ],
      'l1d_store_miss': [self.sd.getter('L1-D', core, 'store-misses') for core in range(sim.config.ncores) ],
      'l1i_load_miss': [self.sd.getter('L1-I', core, 'load-misses') for core in range(sim.config.ncores) ],
      'l1i_store_miss': [self.sd.getter('L1-I', core, 'store-misses') for core in range(sim.config.ncores) ],
      'l2_load_miss': [self.sd.getter('L2', core, 'load-misses') for core in range(sim.config.ncores) ],
      'l2_store_miss': [self.sd.getter('L2', core, 'store-misses') for core in range(sim.config.ncores) ],
      'l3_load_miss': [self.sd.getter('L3', core, 'load-misses') for core in range(sim.config.ncores) ],
      'l3_store_miss': [self.sd.getter('L3', core, 'store-misses') for core in range(sim.config.ncores) ],
      'l3_uncore_req': [self.sd.getter('L3', core, 'uncore-requests') for core in range(sim.config.ncores) ],
      
      'l1i_lat': [self.sd.getter('L1-I', core, 'total-latency') for core in range(sim.config.ncores) ],
      'l1d_lat': [self.sd.getter('L1-D', core, 'total-latency') for core in range(sim.config.ncores) ],
      'l2_lat': [self.sd.getter('L2', core, 'total-latency') for core in range(sim.config.ncores) ],
      'bp_incorrect': [self.sd.getter('branch_predictor', core, 'num-incorrect') for core in range(sim.config.ncores) ],


      'core_dyn': [self.sd.getter('core', core, 'energy-dynamic') for core in range(sim.config.ncores) ],
      'core_stat': [self.sd.getter('core', core, 'energy-static') for core in range(sim.config.ncores) ],
      'l1i_dyn': [self.sd.getter('L1-I', core, 'energy-dynamic') for core in range(sim.config.ncores) ],
      'l1i_stat': [self.sd.getter('L1-I', core, 'energy-static') for core in range(sim.config.ncores) ],
      'l1d_dyn': [self.sd.getter('L1-D', core, 'energy-dynamic') for core in range(sim.config.ncores) ],
      'l1d_stat': [self.sd.getter('L1-D', core, 'energy-static') for core in range(sim.config.ncores) ],
      'l2_dyn': [self.sd.getter('L2', core, 'energy-dynamic') for core in range(sim.config.ncores) ],
      'l2_stat': [self.sd.getter('L2', core, 'energy-static') for core in range(sim.config.ncores) ],
      'l3_dyn': [self.sd.getter('L3', int(core/l3sharedcores), 'energy-dynamic') for core in range(sim.config.ncores) ],
      'l3_stat': [self.sd.getter('L3', int(core/l3sharedcores), 'energy-static') for core in range(sim.config.ncores) ],
      'noc_dyn': [self.sd.getter('NOC', 0, 'energy-dynamic') for core in range(sim.config.ncores) ],
      'noc_stat': [self.sd.getter('NOC', 0, 'energy-static') for core in range(sim.config.ncores) ],
      'dram_dyn': [self.sd.getter('dram', 0, 'energy-dynamic') for core in range(sim.config.ncores) ],
      'dram_stat': [self.sd.getter('dram', 0, 'energy-static') for core in range(sim.config.ncores) ],

      'cpibase' : [self.sd.getter('rob_timer', core, 'cpiBase') for core in range(sim.config.ncores) ],
      'cpibranch' : [self.sd.getter('rob_timer', core, 'cpiBranchPredictor') for core in range(sim.config.ncores) ],
      'cpirsfull' : [self.sd.getter('rob_timer', core, 'cpiRSFull') for core in range(sim.config.ncores) ],
      'cpil1d' : [self.sd.getter('rob_timer', core, 'cpiDataCacheL1') for core in range(sim.config.ncores) ],
      'cpil2d' : [self.sd.getter('rob_timer', core, 'cpiDataCacheL2') for core in range(sim.config.ncores) ],
      'cpil3d' : [self.sd.getter('rob_timer', core, 'cpiDataCacheL3') for core in range(sim.config.ncores) ],
      'cpidram' : [self.sd.getter('rob_timer', core, 'cpiDataCachedram-local') for core in range(sim.config.ncores) ],
      'cpiunknown' : [self.sd.getter('rob_timer', core, 'cpiDataCacheunknown') for core in range(sim.config.ncores) ],

    }
    #sim.util.EveryIns(self.config.interval_inst, self.periodic, roi_only = True)
    if self.config.doopt == 'ANN':
      self.anncoopt = AnnCoopt(self.stats, self.config.sqlitedbpath, self.reconfig)

  def periodic(self, icount, icount_delta):
    pass

  def getMetrics(self):
    metrics_collect = self.updateSimMetrics()
    metrics_collect.update(self.updateDeltaMetrics())
#    print metrics_collect
    return metrics_collect
    
  def computeAndUpdateMetrics(self, metrics_collect):
    for core in range(sim.config.ncores):
      time_delta = self.stats['time'][core].delta
#      cycles = self.stats['time'][core].delta * sim.dvfs.get_frequency(core) / 1e9 # convert fs to cycles
      instrs = self.stats['coreinstrs'][core].delta
      dram_queue_time = self.stats['l3_dram_queue_time'][core].delta
      uncore_time = self.stats['l3_uncore_time'][core].delta
      dram_requests = self.stats['l3_dram_requests'][core].delta
      instr_count = self.stats['instrs'][core].delta
      #metric = dram_queue_time / (dram_requests or 1)
      #metric = uncore_time / (dram_requests or 1)
      #metric = uncore_time / (instr_count or .001)  #uncore time per instr executed
      #print "AMATTRACE: ", instr_count, time_delta
      tpi = (time_delta or 0.0) / (instr_count or 1.0)  #tps: time per instruction
      metrics_collect[getSqlFieldName('tpi', core)] = tpi
      amat = (uncore_time or 0.0) / (instr_count or 1.0)  #amat per instr
      metrics_collect[getSqlFieldName('amat', core)] = amat

#      print "[computeAndUpdateMetrics] td, id", time_delta, instrs
    

  def updateDeltaMetrics(self):
    metrics_collect = {}
    self.sd.update()    #update the delta
    for k in self.stats.keys():
        if k in metrics.energy_metrics or k in metrics.sim_metrics or k in metrics.mem_metrics or k in metrics.cpi_metrics:
            for core in range(sim.config.ncores):
                m = getSqlFieldName(k, core)
                metrics_collect[m] = self.stats[k][core].delta
        if k in metrics.perinstr_metrics:
            for core in range(sim.config.ncores):
                instr_count = self.stats['instrs'][core].delta
                if instr_count == 0:
                    instr_count = 1
                m = getSqlFieldName(k, core)
                metrics_collect[m] = (self.stats[k][core].delta or 0)/(instr_count or 1)

    self.computeAndUpdateMetrics(metrics_collect)
#    if len(metrics.metrics_list) != sim.config.ncores*len(metrics_collect):
#        print "Issue with metric collection: metrics_list.size, metrics_collect.size", len(metrics.metrics_list), len(metrics_collect)
        #assert(True)
    return metrics_collect

  def updateSimMetrics(self):
    metrics_collect = {}
    for core in range(sim.config.ncores):
        m = getSqlFieldName(metrics.COREFREQ, core)
        metrics_collect[m] = sim.dvfs.get_frequency(core)
        m = getSqlFieldName(metrics.UNCOREFREQ, core)
        metrics_collect[m] = sim.dvfs.get_uncore_frequency()
    return metrics_collect

  def getDvfsMetric(self):
    metrics = {}
    for core in range(sim.config.ncores):
      time_delta = self.stats['time'][core].delta
      cycles = self.stats['time'][core].delta * sim.dvfs.get_frequency(core) / 1e9 # convert fs to cycles
      instrs = self.stats['coreinstrs'][core].delta
      dram_queue_time = self.stats['l3_dram_queue_time'][core].delta
      uncore_time = self.stats['l3_uncore_time'][core].delta
      l1d_latency = self.stats['l1d_lat'][core].delta
      dram_requests = self.stats['l3_dram_requests'][core].delta
      instr_count = self.stats['instrs'][core].delta
      #metric = dram_queue_time / (dram_requests or 1)
      #metric = uncore_time / (dram_requests or 1)
      #metric = time_delta / (instr_count or .001)  #tps: time per instruction
      metric = l1d_latency / (instr_count or 1.0)  #uncore time per instr executed
      #print "AMATTRACE: ",metric, instr_count, time_delta
      metrics[core] = metric
    return metrics

  def getOffsetTPI(self):
    firstIntv = False
    if not hasattr(self, 'coreOffsets'):
        firstIntv = True
        self.coreOffsets = []
        self.coreTpiList = []
        self.coreAmatList = []

    for core in range(sim.config.ncores):
      time_delta = self.stats['time'][core].delta
      l1d_latency = self.stats['l1d_lat'][core].delta
      instr_count = self.stats['instrs'][core].delta
      
      tpi = time_delta/instr_count
      amat = l1d_latency/instr_count
      
      if firstIntv == True:
        self.coreTpiList.append(tpi)
        self.coreAmatList.append(amat)
        self.coreOffsets.append(0)   #no DVFS decision yet
      else:
        print 'Core',core,
        offset = self.getTpiAmatBasedOffset(self.coreTpiList[core], tpi, self.coreAmatList[core], amat, self.coreOffsets[core])
        self.coreTpiList[core] = tpi
        self.coreAmatList[core] = amat
        self.coreOffsets[core] = offset
    print '[computecoreoffset]', self.coreTpiList, self.coreAmatList, self.coreOffsets
    return self.coreOffsets

  def __getUnCoreOffsetTPI(self):
    firstIntv = False
    if not hasattr(self, 'uncoreOffset'):
        firstIntv = True
        self.uncoreOffset = 0
        self.uncoreTpi = 0.0
        self.uncoreAmat = 0.0

    instr_count = float(sum(self.stats['instrs'][core].delta for core in range(sim.config.ncores)))
    tpi = sum([self.stats['time'][core].delta for core in range(sim.config.ncores)])/instr_count
    amat = sum([self.stats['l1d_lat'][core].delta for core in range(sim.config.ncores)])/instr_count

    offset = self.getTpiAmatBasedOffset(self.uncoreTpi, tpi, self.uncoreAmat, amat, self.uncoreOffset)
    self.uncoreTpi = tpi
    self.uncoreAmat = amat
    self.uncoreOffset = -1*offset
    print '[computeuncoreoffset]', self.uncoreTpi, self.uncoreAmat, self.uncoreOffset
    return self.uncoreOffset
  
  def getTpiAmatBasedOffset(self, ptpi, ctpi, pamat, camat, poffset):
    metricRange = self.config.metricRange
    if ctpi <= ptpi*(1.0-metricRange):  #we are on the right path to dvfs
        print 'ctpi <= ptpi'
        return poffset

    elif ctpi >= ptpi*(1.0+metricRange):  #throughput is coming down
        print 'ctpi >= ptpi'
        if camat >= pamat*(1.0+metricRange): #amat dominated tpi
            print 'camat >= pamat'
            return 1
        else: #core dominated tpi
            print 'NOT camat >= pamat'
            return -1

    else: #tpi within range
        print 'ctpi == ptpi'
        return 0

  def getOffsetAMAT(self):
    metricdata = self.getDvfsMetric()
    targetMetric = self.config.targetMetric 
    metricRange = self.config.metricRange
    offsets = []
    for core in range(sim.config.ncores):
        offset = 0
        curr_metric = metricdata[core]
        if curr_metric > targetMetric * (1.0+metricRange): #slow down
          offset = 1
        elif curr_metric < targetMetric * (1.0-metricRange): #go fast
          offset = -1
        offsets.append(offset)
    
    return offsets
  
  #External function to get reconfig values
  def getReconfigData(self):
    if self.metricFunc == 'TPI':
        coreDvfsOffsets = self.getOffsetTPI()
        self.reconfig.setUnCoreDvfsOffset(self.__getUnCoreOffsetTPI())
        self.reconfig.setCoreDvfsOffsets(coreDvfsOffsets)
    elif self.metricFunc == 'AMAT':
        coreDvfsOffsets = self.getOffsetAMAT()
        self.reconfig.setCoreDvfsOffsets(coreDvfsOffsets)
    elif self.metricFunc == 'MDP':
        (coreDvfsOffsets, uncoreDvfsOffsets, l3wayAssignment, l2wayAssignment, l1dwayAssignment, l1iwayAssignment) = self.__getOffsetForMDP()
        if mdp_options[self.mdpConfig]['coreDvfs']==True and mdp_options[self.mdpConfig]['coreNumActions'] == 5:  #HACK for Core
            #the coreoffsets returned by MDP are actually the dvfs index in the dvfs table
            #convert them into offset
            for core in range(sim.config.ncores):
                currIdx = self.reconfig.getCoreDvfsAllLevel(core)
                nextIdxOffset = coreDvfsOffsets[core] - currIdx
                coreDvfsOffsets[core] = nextIdxOffset
        
        if mdp_options[self.mdpConfig]['uncoreDvfs']==True and mdp_options[self.mdpConfig]['uncoreNumActions'] == 5:  #HACK for UnCore
            #the uncoreoffsets returned by MDP are actually the dvfs index in the dvfs table
            #convert them into offset
            currIdx = self.reconfig.getUnCoreDvfsAllLevel()
            nextIdxOffset = uncoreDvfsOffsets[0] - currIdx
            uncoreDvfsOffsets[0] = nextIdxOffset
        
        self.old_reconfig = self.reconfig #30 July: save the old config for Cooperative_MLM
        self.reconfig.setCoreDvfsOffsets(coreDvfsOffsets)
        self.reconfig.setUnCoreDvfsOffset(uncoreDvfsOffsets[0])
        self.reconfig.setL3wayAssignments(l3wayAssignment)
        self.reconfig.setL2wayAssignments(l2wayAssignment)
        self.reconfig.setL1DwayAssignments(l1dwayAssignment)
        self.reconfig.setL1IwayAssignments(l1iwayAssignment)
        self.interval = self.interval + 1
    elif self.metricFunc == 'ANN':
        (coreDvfsOffsets, uncoreDvfsOffsets, l3wayAssignment, l2wayAssignment, l1dwayAssignment, l1iwayAssignment) = self.anncoopt.getOffsetForANN()
        print "getmetric:getReconfigData ", coreDvfsOffsets, uncoreDvfsOffsets, l3wayAssignment
        self.reconfig.setCoreDvfsOffsets(coreDvfsOffsets)
        self.reconfig.setUnCoreDvfsOffset(uncoreDvfsOffsets[0])
        self.reconfig.setL3wayAssignments(l3wayAssignment)
        self.reconfig.setL2wayAssignments(l2wayAssignment)
        self.reconfig.setL1DwayAssignments(l1dwayAssignment)
        self.reconfig.setL1IwayAssignments(l1iwayAssignment)
        self.interval = self.interval + 1

    return self.reconfig

  def __getOffsetForMDP(self):
     firstIntv = self.__computeMdpMetricData()
     if firstIntv == False:
        self.__updateMdpMetricData()
    
#     print self.coreTpiList
     assert(len(self.coreTpiList) == 2)
  
     self.__sendStateRewardToMDPs(firstIntv)
     #time.sleep(1)  #time for mdps to compute the optimal reconfig
     return self.__getReconfigActions()

  def __sendStateRewardToMDPs(self, firstIntv):
    self.__sendDvfsStateReward(firstIntv)
    self.__sendCacheStateReward(firstIntv)
#    print "__sendStateRewardToMDPs done"

  def __sendDvfsStateReward(self, firstIntv):
     #Core DVFS write to file
     states = []
     rewards = []
     #HACK TODO
     #for ApproximateQ Learning, state is actually a vector of features (continous values)
     #and not a encoded number as in case of Tablular Q-Learning
     if 'statelen' in mdp_options[self.mdpConfig]: self.CoreStateVecLen = mdp_options[self.mdpConfig]['statelen']
     else:  self.CoreStateVecLen = 1

     if firstIntv or mdp_options[self.mdpConfig]['coreDvfs'] == False:
         states = [0 for c in range(sim.config.ncores*self.CoreStateVecLen)]
         rewards = [0 for c in range(sim.config.ncores)]
     else:
        for c in range(sim.config.ncores):
            state,reward = self.__getCoreStateAndReward(c)
            rewards.append(reward)
            if type(state) is list: states.extend(state)
            else:                   states.append(state)

     #print "__getCoreOffsetForMDP (S,R) ", states, rewards
     self.mdp.writeDvfsMetric(self.interval, self.mdp.Core, states)
     self.mdp.writeDvfsRewards(self.interval, self.mdp.Core, rewards)
     
     #UnCore write to file
     states = []
     rewards = []
     if firstIntv or mdp_options[self.mdpConfig]['uncoreDvfs'] == False:
        state,reward = 0,0
     else:
        state,reward = self.__getUnCoreStateAndReward()
     states.append(state)
     rewards.append(reward)
     
     #print "__getUnCoreOffsetForMDP (S,R)", states, rewards
     self.mdp.writeDvfsMetric(self.interval, self.mdp.Uncore, states)
     self.mdp.writeDvfsRewards(self.interval, self.mdp.Uncore, rewards)

  def __sendCacheStateReward(self, firstIntv):
    self.__sendL3StateReward(firstIntv)
    self.__sendL2StateReward(firstIntv)
    self.__sendL1DStateReward(firstIntv)
    self.__sendL1IStateReward(firstIntv)

  def isBatchL3Mdp(self):
    return 'batchL3Mdp' in mdp_options[self.mdpConfig] and mdp_options[self.mdpConfig]['batchL3Mdp'] == True  #TODO HACK

  def __sendL3StateReward(self, firstIntv):
  #L3 Way write to file
     states = []
     rewards = []
     num = self.reconfig.cacheInfo['l3_cache']['num']
     sharedCores = self.reconfig.cacheInfo['l3_cache']['sharedCores']
      
     #HACKY creating multiple MDP's per L3 in groups of 4-cores
     #DATE2017 Experiments
     #We are fooling the setup in thinking that there are 2 L3's each shared by 4 cores (instead of 1 L3 shared by 8-cores)
     if self.isBatchL3Mdp():
       assert(sharedCores <=16)    #making sure it works for 8 shared cores
       sharedCores = 4 
       num = sim.config.ncores/sharedCores
     
     if firstIntv or mdp_options[self.mdpConfig]['l3wayreconfig'] == False:
       states = [0 for c in range(num*sharedCores)]
       rewards = [0 for c in range(num)]
     else:
       for n in range(num):
         if 'rwsopt' in mdp_options[self.mdpConfig] and mdp_options[self.mdpConfig]['rwsopt'] == True:
            #state,reward = self.__getL3RwsStateAndReward(n)    TODO: combined state does not look good
            state,reward = self.__getL3wayStateAndReward(n)
         else:
            state,reward = self.__getL3wayStateAndReward(n)
         states.extend(state)
         rewards.append(reward)

#     print "L3 (S,R) ", states, rewards
     self.mdp.writeCacheWayReconfigMetric(self.interval, self.mdp.L3, states)
     self.mdp.writeCacheWayReconfigRewards(self.interval, self.mdp.L3, rewards)
     
  def __sendL2StateReward(self, firstIntv):
  #L2 Way write to file
     states = []
     rewards = []
     num = self.reconfig.cacheInfo['l2_cache']['num']
     sharedCores = self.reconfig.cacheInfo['l2_cache']['sharedCores']
     if firstIntv or mdp_options[self.mdpConfig]['l2wayreconfig'] == False:
       states = [0 for c in range(num*sharedCores)]
       rewards = [0 for c in range(num)]
     else:
       for n in range(num):
         state,reward = self.__getL2wayStateAndReward(n)
         states.extend(state)
         rewards.append(reward)

#     print "L2 (S,R) ", states, rewards
     self.mdp.writeCacheWayReconfigMetric(self.interval, self.mdp.L2, states)
     self.mdp.writeCacheWayReconfigRewards(self.interval, self.mdp.L2, rewards)
     
  def __sendL1DStateReward(self, firstIntv):
  #L1D Way write to file
     states = []
     rewards = []
     num = self.reconfig.cacheInfo['l1_dcache']['num']
     sharedCores = self.reconfig.cacheInfo['l1_dcache']['sharedCores']
     if firstIntv or mdp_options[self.mdpConfig]['l1dwayreconfig'] == False:
       states = [0 for c in range(num*sharedCores)]
       rewards = [0 for c in range(num)]
     else:
       for n in range(num):
         state,reward = self.__getL1DwayStateAndReward(n)
         states.extend(state)
         rewards.append(reward)

#     print "L1D (S,R) ", states, rewards
     self.mdp.writeCacheWayReconfigMetric(self.interval, self.mdp.L1D, states)
     self.mdp.writeCacheWayReconfigRewards(self.interval, self.mdp.L1D, rewards)
     
  def __sendL1IStateReward(self, firstIntv):
  #L1I Way write to file
     states = []
     rewards = []
     num = self.reconfig.cacheInfo['l1_icache']['num']
     sharedCores = self.reconfig.cacheInfo['l1_icache']['sharedCores']
     if firstIntv or mdp_options[self.mdpConfig]['l1iwayreconfig'] == False:
       states = [0 for c in range(num*sharedCores)]
       rewards = [0 for c in range(num)]
     else:
       for n in range(num):
         state,reward = self.__getL1IwayStateAndReward(n)
         states.extend(state)
         rewards.append(reward)

#     print "L1I (S,R) ", states, rewards
     self.mdp.writeCacheWayReconfigMetric(self.interval, self.mdp.L1I, states)
     self.mdp.writeCacheWayReconfigRewards(self.interval, self.mdp.L1I, rewards)

  def __getReconfigActions(self):
     #get offsets
     coffsets = None
     while coffsets == None:
        coffsets = self.mdp.readDvfsOffsets(self.interval, self.mdp.Core, self.reconfig.np)
        if coffsets == None: 
          time.sleep(1)

     #get offsets
     uoffsets = None
     while uoffsets == None:
#        time.sleep(1)
        uoffsets = self.mdp.readDvfsOffsets(self.interval, self.mdp.Uncore, 1)
        if uoffsets == None: 
          time.sleep(1)

     #get l3 way assignment
     l3wayassign = None
     num = self.reconfig.cacheInfo['l3_cache']['num']
     associativity = self.reconfig.cacheInfo['l3_cache']['associativity']
     while l3wayassign == None:
#        time.sleep(1)
        l3wayassign = self.mdp.readCacheWayReconfigAssign(self.interval, self.mdp.L3, num*associativity)
        if l3wayassign == None: 
          time.sleep(1)
     
     #get l2 way assignment
     l2wayassign = None
     num = self.reconfig.cacheInfo['l2_cache']['num']
     associativity = self.reconfig.cacheInfo['l2_cache']['associativity']
     while l2wayassign == None:
#        time.sleep(1)
        l2wayassign = self.mdp.readCacheWayReconfigAssign(self.interval, self.mdp.L2, num*associativity)
        if l2wayassign == None: 
          time.sleep(1)
     
     #get l1d way assignment
     l1dwayassign = None
     num = self.reconfig.cacheInfo['l1_dcache']['num']
     associativity = self.reconfig.cacheInfo['l1_dcache']['associativity']
     while l1dwayassign == None:
#        time.sleep(1)
        l1dwayassign = self.mdp.readCacheWayReconfigAssign(self.interval, self.mdp.L1D, num*associativity)
        if l1dwayassign == None: 
          time.sleep(1)

     #get l1i way assignment
     l1iwayassign = None
     num = self.reconfig.cacheInfo['l1_icache']['num']
     associativity = self.reconfig.cacheInfo['l1_icache']['associativity']
     while l1iwayassign == None:
#        time.sleep(1)
        l1iwayassign = self.mdp.readCacheWayReconfigAssign(self.interval, self.mdp.L1I, num*associativity)
        if l1iwayassign == None: 
          time.sleep(1)

     return (coffsets, uoffsets, l3wayassign, l2wayassign, l1dwayassign, l1iwayassign)

  def __coreEnergy(self, core):
    #taking static energy into account is important when its used in reward function
    #at a very low throughput/low core freq, dynamic energy could be very small per instr
    compList = ['core_dyn',
      'core_stat',
      'l1i_dyn',
      'l1i_stat',
      'l1d_dyn',
      'l1d_stat',
      'l2_dyn',
      'l2_stat',
      ]
    ergy=0.0
    for comp in compList:
        ergy += self.stats[comp][core].delta
    return ergy

  def __uncoreEnergy(self, core):
#    assert(0)   #make sure to analyse if dyn and static both are required or not
    total_l2_miss = sum(self.l2LoadMissList[1])+sum(self.l2StoreMissList[1])
    l2_miss = self.l2LoadMissList[1][core] + self.l2StoreMissList[1][core]

    #For a single L3, NOC is between L2s and L3, so NOC and L3 energy is proportional to l2_miss for the core vs total l2 misses
    assert(self.reconfig.cacheInfo['l3_cache']['num']==1)
    noc_ergy=(self.stats['noc_dyn'][0].delta+self.stats['noc_stat'][0].delta)*l2_miss/(total_l2_miss or 1.0)
    l3_idx = 0 #core/self.reconfig.cacheInfo['l3_cache']['sharedCores']
    l3_ergy = (self.stats['l3_dyn'][l3_idx].delta+self.stats['l3_stat'][l3_idx].delta)*l2_miss/(total_l2_miss or 1.0)
    return noc_ergy+l3_ergy
 
  def __dramEnergy(self, core):
    total_l3_miss = sum(self.l3LoadMissList[1])+sum(self.l3StoreMissList[1])
    l3_miss = self.l3LoadMissList[1][core] + self.l3StoreMissList[1][core]
    dram_ergy = self.stats['dram_dyn'][0].delta*l3_miss/(total_l3_miss or 1.0)
    return dram_ergy

  def __getSystemEnergy(self, core):
    return self.__coreEnergy(core)+ self.__uncoreEnergy(core)
  
  #index 0 stores the prev data, index 1 has the current data
  def __computeMdpMetricData(self):
    firstIntv = False
    if not hasattr(self, 'coreTpiList'):
        firstIntv = True
        self.coreTpiList =    [ [0.0 for c in range(sim.config.ncores)], ]
        self.coreAmatList =   [ [0.0 for c in range(sim.config.ncores)], ]
        self.coreEpiList =    [ [0.0 for c in range(sim.config.ncores)], ]
        self.coreInstrList =  [ [0.0 for c in range(sim.config.ncores)], ]
        self.coreBPList =     [ [0.0 for c in range(sim.config.ncores)], ]
        self.uncoreRequestList = [ [0.0 for c in range(sim.config.ncores)], ]
        self.l3LoadMissList =    [ [0.0 for c in range(sim.config.ncores)], ]
        self.l3StoreMissList =   [ [0.0 for c in range(sim.config.ncores)], ]
        self.l2LoadMissList =    [ [0.0 for c in range(sim.config.ncores)], ]
        self.l2StoreMissList =   [ [0.0 for c in range(sim.config.ncores)], ]
        self.l1LoadMissList =    [ [0.0 for c in range(sim.config.ncores)], ]
        self.l1StoreMissList =   [ [0.0 for c in range(sim.config.ncores)], ]
        self.csUtility =  [ [0.0 for c in range(sim.config.ncores)], [0.0 for c in range(sim.config.ncores)]]
    
    self.l3LoadMissList.append( [self.stats['l3_load_miss'][core].delta for core in range(sim.config.ncores)] )
    self.l3StoreMissList.append( [self.stats['l3_store_miss'][core].delta for core in range(sim.config.ncores)] )
    self.l2LoadMissList.append( [self.stats['l2_load_miss'][core].delta for core in range(sim.config.ncores)] )
    self.l2StoreMissList.append( [self.stats['l2_store_miss'][core].delta for core in range(sim.config.ncores)] )
    self.l1LoadMissList.append( [self.stats['l1d_load_miss'][core].delta + self.stats['l1i_load_miss'][core].delta for core in range(sim.config.ncores)] )
    self.l1StoreMissList.append([self.stats['l1d_store_miss'][core].delta + self.stats['l1i_store_miss'][core].delta for core in range(sim.config.ncores)] )

    currTpiList = []
    currAmatList = []
    currEpiList = []
    currInstrList = []
    currUnCoreRequestList = []
    for core in range(sim.config.ncores):
        time_delta = self.stats['time'][core].delta
        l1d_latency = self.stats['l1d_lat'][core].delta
        instr_count = self.stats['instrs'][core].delta
        sys_energy = self.__getSystemEnergy(core) 
        uncore_request = self.stats['l3_uncore_time'][core].delta

        tpi = time_delta/(instr_count or 1.0)
        amat = l1d_latency/(instr_count or 1.0)
        epi = sys_energy/(instr_count or 1.0)
        #epi = self.__coreEnergy(core)/(instr_count or 1.0)

        currTpiList.append(tpi)
        currAmatList.append(amat)
        currEpiList.append(epi)
        currInstrList.append(instr_count or 1.0)
        currUnCoreRequestList.append(uncore_request)

    self.coreTpiList.append(currTpiList)
    self.coreAmatList.append(currAmatList)
    self.coreEpiList.append(currEpiList)
    self.coreInstrList.append(currInstrList)
    self.coreBPList.append([self.stats['bp_incorrect'][core].delta for core in range(sim.config.ncores)] )
    self.uncoreRequestList.append(currUnCoreRequestList)

    
    return firstIntv
  
  #remove prev data and make curr data as prev
  def __updateMdpMetricData(self):
    self.coreTpiList.pop(0)
    self.coreAmatList.pop(0)
    self.coreEpiList.pop(0)
    self.coreInstrList.pop(0)
    self.coreBPList.pop(0)
    self.uncoreRequestList.pop(0)
    self.l3LoadMissList.pop(0)
    self.l3StoreMissList.pop(0)
    self.l2LoadMissList.pop(0)
    self.l2StoreMissList.pop(0)
    self.l1LoadMissList.pop(0)
    self.l1StoreMissList.pop(0)
    self.csUtility.pop(0); self.csUtility.append([0.0 for c in range(sim.config.ncores)])
  
  def __getCoreState(self, statefn, core):
#    print "__getCoreState", self.coreTpiList
    if statefn == 'tpi' or statefn == 'tpi3':
        state = self.__getDeltaMdpState3(self.coreTpiList[0][core], self.coreTpiList[1][core])
    elif statefn == 'tpi2':
        state = self.__getDeltaMdpState2(self.coreTpiList[0][core], self.coreTpiList[1][core])
    elif statefn == 'tpi4':
        tr = self.coreTpiList[1][core]/(self.coreTpiList[0][core] or 1.0)
        tpilevels = [0.95,1.0,1.05]
        state = self.__getInputLevelState(tr, tpilevels)
        print "[tpi4] ", core, tr, state
    elif statefn == 'tpi5':
        tr = self.coreTpiList[1][core]/(self.coreTpiList[0][core] or 1.0)
        tpilevels = [0.94,0.98,1.02,1.06]
        state = self.__getInputLevelState(tr, tpilevels)
        print "[tpi5] ", core, tr, state
    elif statefn == 'tpibp':
        state = self.__getCoreTpiBrPredState(self.coreTpiList[0][core], self.coreTpiList[1][core], core)
    elif statefn == 'tpil1':
        stateVec = []
        pm = self.coreTpiList[0][core]
        cm = self.coreTpiList[1][core]
        stateVec.append(self.__getRatioMdpState(pm, cm, 5))
        cl1mpki=float( (self.l1StoreMissList[1][core] + self.l1LoadMissList[1][core])*1000.0)/(self.coreInstrList[1][core] or 1.0)
        phase = cl1mpki/1000.0  #approx miss rate [0,1.0]
        mpki_state = self.__getInputLevelState(phase, [0.0025, 0.010])
#        l1_misses = self.stats['l1d_load_miss'][core].delta + self.stats['l1d_store_miss'][core].delta
#        mpki = l1_misses*1000.0/(instrs or 1.0)   #incase of idle cpu
#        mpki_state = int(mpki/30.0)
#        if mpki_state > 2: mpki_state = 2
        stateVec.append(mpki_state)
        return self.coreStateAssigner.getState(stateVec)[0]
    elif statefn == 'tpil2':
        stateVec = []
        pm = self.coreTpiList[0][core]
        cm = self.coreTpiList[1][core]
        stateVec.append(self.__getRatioMdpState(pm, cm, 5))
        cl2mpki=float( (self.l2LoadMissList[1][core]+self.l2StoreMissList[1][core])*1000.0)/(self.coreInstrList[1][core] or 1.0)
        mpki_state = self.__getInputLevelState(cl2mpki, [7, 17])
        stateVec.append(mpki_state)
        return self.coreStateAssigner.getState(stateVec)[0]
    elif statefn == 'tpil3':
        stateVec = []
        pm = self.coreTpiList[0][core]
        cm = self.coreTpiList[1][core]
        stateVec.append(self.__getRatioMdpState(pm, cm, 5))
        cl3mpki=float( (self.l3LoadMissList[1][core]+self.l3StoreMissList[1][core])*1000.0)/(self.coreInstrList[1][core] or 1.0)
        mpki_state = self.__getInputLevelState(cl3mpki, [2, 6])
        stateVec.append(mpki_state)
        return self.coreStateAssigner.getState(stateVec)[0]
    elif statefn == 'tpibpl3':  #Cooperative_MLM
        state = self.__getCoreTpiBP_L3Coop_State(self.coreTpiList[0][core], self.coreTpiList[1][core], core)
    elif statefn == 'tpi9':
        state = self.__getDeltaMdpState9(self.coreTpiList[0][core], self.coreTpiList[1][core], core)
    elif statefn == 'amat2':
        state = self.__getDeltaMdpState2(self.coreAmatList[0][core], self.coreAmatList[1][core])
    elif statefn == 'juan': #CMU Phd Thesis
        state = self.__getJuanMdpState(core)
    elif statefn == 'epi' or statefn == 'energy':
        state = self.__getDeltaMdpState3(self.coreEpiList[0][core], self.coreEpiList[1][core])
    elif statefn == 'edp':
        state = self.__getDeltaMdpState3(self.coreTpiList[0][core]*self.coreEpiList[0][core], 
                                         self.coreTpiList[1][core]*self.coreEpiList[1][core])
    elif statefn == 'ed2p':
        state = self.__getDeltaMdpState3(self.coreTpiList[0][core]*self.coreTpiList[0][core]*self.coreEpiList[0][core],
                                         self.coreTpiList[1][core]*self.coreTpiList[1][core]*self.coreEpiList[1][core])
    elif statefn == 'coretpi':
        state = self.__getCoreTpiState7(self.coreTpiList[1][core])
    elif statefn == 'l3tpi':
        state = self.__getUnCoreTpiState2(self.coreTpiList[1][core])
    elif statefn == 'l3tpi3':
        state = self.__getUnCoreTpiState3(self.coreTpiList[1][core])
    elif statefn == 'l3loadmiss2':
        state = self.__getL3LoadMissState2(self.l3LoadMissList[1][core], self.coreInstrList[1][core])
    elif statefn == 'l3loadmiss4':
        state = self.__getL3LoadMissState4(self.l3LoadMissList[1][core], self.coreInstrList[1][core])
    elif statefn == 'mpki3':
        plmpki=float(self.l3LoadMissList[0][core]*1000.0)/(self.coreInstrList[0][core] or 1.0)
        clmpki=float(self.l3LoadMissList[1][core]*1000.0)/(self.coreInstrList[1][core] or 1.0)
        LowMpki = 0.05
        if clmpki<LowMpki and plmpki<LowMpki:   #if mpki is low
          state = 0  #rate of change in mpki is not a correct metric
        else:
          state = self.__getDeltaMdpState3(plmpki, clmpki)
        print "[mpki3] ", plmpki, clmpki, state
    elif statefn == 'l3mpki3':
        cl2mpki=float( (self.l2StoreMissList[1][core] + self.l2LoadMissList[1][core])*1000.0)/(self.coreInstrList[1][core] or 1.0)
        cl3mpki=float( (self.l3StoreMissList[1][core] + self.l3LoadMissList[1][core])*1000.0)/(self.coreInstrList[1][core] or 1.0)
        LowMpki = 0.10
        if cl2mpki<LowMpki:   #if l2 mpki is low, then l3 mpki will also be low, CPU Intensive Phase
          state = 0
        else:
          phase = cl3mpki/cl2mpki
          state = self.__getInputLevelState(phase, [0.1, 0.9])
        print "[l3mpki3] ", cl2mpki, cl3mpki, state
    elif statefn == 'l2mpki5':
        cl2mpki=float( (self.l2StoreMissList[1][core] + self.l2LoadMissList[1][core])*1000.0)/(self.coreInstrList[1][core] or 1.0)
        state = self.__getInputLevelState(cl2mpki, [7, 15, 25, 40]) #based on actual l2mpki numbers
        print "[l2mpki5] ", cl2mpki, state
    elif statefn == 'l1mpki3':
        #99% hit rate at L1 is approx = 1% misses/1000 instr ~ 10
        cl1mpki=float( (self.l1StoreMissList[1][core] + self.l1LoadMissList[1][core])*1000.0)/(self.coreInstrList[1][core] or 1.0)
        phase = cl1mpki/1000.0  #approx miss rate [0,1.0]
        state = self.__getInputLevelState(phase, [0.0025, 0.010])
        print "[l1mpki3] ", core, cl1mpki, phase, state
    #The below are only for CacheStack Optimization
    elif statefn == 'cachestackmpki':
        state = self.__getCacheStackCoreState(core)
    elif statefn == 'csl3mpki3':
        cl2mpki=float( (self.l2LoadMissList[1][core]+self.l2StoreMissList[1][core])*1000.0)/(self.coreInstrList[1][core] or 1.0)
        cl3mpki=float( (self.l3LoadMissList[1][core]+self.l3StoreMissList[1][core])*1000.0)/(self.coreInstrList[1][core] or 1.0)
        #LowMpki = 20
        #if cl3mpki<LowMpki:   #if l2 mpki is low, then l3 mpki will also be low, CPU Intensive Phase
        #  state = 0
        #else:
        #  phase = cl3mpki/cl2mpki
        #  state = self.__getInputLevelState(phase, [0.25, 0.65])
        state = self.__getInputLevelState(cl3mpki, [2, 6])  #99.5%, 98%
        print "[csl3mpki3] ", cl2mpki, cl3mpki, state
    elif statefn == 'csl2mpki3':
        cl2mpki=float( (self.l2LoadMissList[1][core]+self.l2StoreMissList[1][core])*1000.0)/(self.coreInstrList[1][core] or 1.0)
        cl1mpki=float( (self.l1LoadMissList[1][core]+self.l1StoreMissList[1][core])*1000.0)/(self.coreInstrList[1][core] or 1.0)
        #LowL2Mpki = 20 #99.5% hit
        #if cl2mpki<LowL2Mpki:   #if l1 mpki is low, then l2 mpki will also be low, CPU Intensive Phase
        #  state = 0
        #else:
        #  phase = cl2mpki/cl1mpki
        #  state = self.__getInputLevelState(phase, [0.25, 0.65])  #l2 can service 90% l1 misses, less than 20% l1 misses
        state = self.__getInputLevelState(cl2mpki, [7, 17])  #99.5%, 98%
        print "[csl2mpki3] ", cl1mpki, cl2mpki, state
    elif statefn == 'csl1mpki3':
        #99% hit rate at L1 is approx = 1% misses/1000 instr ~ 10
        cl1mpki=float( (self.l1LoadMissList[1][core])*1000.0)/(self.coreInstrList[1][core] or 1.0)
        state = self.__getInputLevelState(cl1mpki, [20, 50])  #99.5%, 98%
        print "[csl1mpki3] ", core, cl1mpki, state
    elif statefn == 'tpi10':
        tr = self.coreTpiList[1][core]/(self.coreTpiList[0][core] or 1.0)
        state = self.__getInputLevelState(tr, [0.87,0.92,0.95,0.98, 1.0,1.02,1.05,1.08,1.13])
#        print "[tpi10] ", core, tr, state
    elif statefn == 'tpiq10':
        t_ns = self.coreTpiList[1][core]*1e-6  #tpi is in 
        levels = [0.5*i for i in range(1,10)]  #[0.5, 1.0, ...]
        state = self.__getInputLevelState(t_ns, levels)
        print "[tpiq10] ", core, t_ns, state
    elif statefn == 'tpiq5tr':
        stateVec = []
        t_ns = self.coreTpiList[1][core]*1e-6  #tpi is in 
        levels = [0.75*i for i in range(1,5)]  #[0.5, 1.0, ...]
        state = self.__getInputLevelState(t_ns, levels)
        stateVec.append(state)

        tr = self.coreTpiList[1][core]/(self.coreTpiList[0][core] or 1.0)
        tpilevels = [0.94,0.98,1.02,1.06]
        state = self.__getInputLevelState(tr, tpilevels)
        stateVec.append(state)
        
        state = self.coreStateAssigner.getState(stateVec)[0]
        print "[tpiq5tr] ", core, t_ns, tr, state
        return state

    elif statefn == 'tpif':
        stateVec = []
        tr = self.coreTpiList[1][core]/(self.coreTpiList[0][core] or 1.0)
        tpilevels = [0.94,0.98,1.02,1.06]
        stateVec.append(self.__getInputLevelState(tr, tpilevels))
        freq = sim.dvfs.get_frequency(core) 
        stateVec.append(freq)
        return self.coreStateAssigner.getState(stateVec)[0]
    elif statefn == 'tpifnoc':
        return self.__getCoreTpiFreq_UncoreCoop_State(core)
    
    elif statefn == 'tpi16':
        tr = self.coreTpiList[1][core]/(self.coreTpiList[0][core] or 1.0)
        plevels = [1+0.005*pow(2,i) for i in range(0,7)]  #[0.005, 0.01, 0.02, 0.04, 0.08, 0.16, 0.32]
        nlevels = [1-0.005*pow(2,i) for i in range(0,7)]
        tpilevels = sorted(nlevels + [1.0,] + plevels)
        state = self.__getInputLevelState(tr, tpilevels)
#        print "[tpi16] ", core, tr, state
    elif statefn == 'tpi40':
        tr = self.coreTpiList[1][core]/(self.coreTpiList[0][core] or 1.0)
        state = self.__getInputLevelState(tr, [x/100.0 for x in range(81,120,1)])
        print "[tpi40] ", core, tr, state
    elif statefn == 'csfeatures':
        state = self.__getCacheStackFeatures(core)
    else:
        print "Unsupported State Function"
        assert(False)   
    return state
  
  #Shared Core metric
  def __getSharedCoreState(self, statefn, basecore, sharedCores):
    if statefn == 'epi' or statefn == 'energy':
        #state = self.__getDeltaMdpState3(self.coreEpiList[0][core], self.coreEpiList[1][core])
        assert(0)
    elif statefn == 'edp':
        assert(0)
        #state = self.__getDeltaMdpState3(self.coreTpiList[0][core]*self.coreEpiList[0][core], 
        #                                 self.coreTpiList[1][core]*self.coreEpiList[1][core])
    elif statefn == 'l1mpki3':
        assert(0)
        #99% hit rate at L1 is approx = 1% misses/1000 instr ~ 10
        cl1mpki=float( (self.l1LoadMissList[1][core])*1000.0)/(self.coreInstrList[1][core] or 1.0)
        state = self.__getInputLevelState(cl1mpki, [20, 50])  #99.5%, 98%
        print "[csl1mpki3] ", core, cl1mpki, state
    elif statefn == 'l2mpki5':
        load_levels = [7, 15, 25, 40]
        store_levels = [7, 15, 25, 40]
        mpki_load = float(self.l2LoadMissList[1][core]*1000.0)/(self.coreInstrList[1][core] or 1.0)
        mpki_store = float(self.l2StoreMissList[1][core]*1000.0)/(self.coreInstrList[1][core] or 1.0)
        lstate = self.__getInputLevelState(mpki_load, load_levels) 
        sstate = self.__getInputLevelState(mpki_store, store_levels) 
        state = [lstate, sstate,]
        print "[l2mpki5] ", mpki_load, mpki_store, state
    elif statefn == 'l2mpki3':
        assert(0)
        cl2mpki=float( (self.l2LoadMissList[1][core]+self.l2StoreMissList[1][core])*1000.0)/(self.coreInstrList[1][core] or 1.0)
        state = self.__getInputLevelState(cl2mpki, [7, 17])  #99.5%, 98%
        print "[l2mpki3] ", cl2mpki, state
    elif statefn == 'l3mpki5':
        load_levels = [2, 6, 10, 15]
        store_levels = [2, 6, 10, 15]
        mpki_load = float(self.l3LoadMissList[1][core]*1000.0)/(self.coreInstrList[1][core] or 1.0)
        mpki_store = float(self.l3StoreMissList[1][core]*1000.0)/(self.coreInstrList[1][core] or 1.0)
        lstate = self.__getInputLevelState(mpki_load, load_levels) 
        sstate = self.__getInputLevelState(mpki_store, store_levels) 
        state = [lstate, sstate,]
        print "[l3mpki5] ", mpki_load, mpki_store, state
    elif statefn == 'l3mpki3':
        assert(0)
        cl3mpki=float( (self.l3LoadMissList[1][core]+self.l3StoreMissList[1][core])*1000.0)/(self.coreInstrList[1][core] or 1.0)
        state = self.__getInputLevelState(cl3mpki, [2, 6])
        print "[csl3mpki3] ", cl3mpki, state
    else:
        print "Unsupported State Function"
        assert(False)   
    return state

  def __getReward(self, rewardfn, core):
    #REWARD
    if rewardfn == 'edp':
        reward = self._getDeltaMdpReward(self.coreTpiList[0][core]*self.coreEpiList[0][core], 
                                         self.coreTpiList[1][core]*self.coreEpiList[1][core])
    elif rewardfn == 'ed2p':
        reward = self._getDeltaMdpReward(self.coreTpiList[0][core]*self.coreTpiList[0][core]*self.coreEpiList[0][core], 
                                         self.coreTpiList[1][core]*self.coreTpiList[1][core]*self.coreEpiList[1][core])
    elif rewardfn == 'energy' or rewardfn == 'epi':
        reward = self._getDeltaMdpReward(self.coreEpiList[0][core], self.coreEpiList[1][core])/1e08
    elif rewardfn == 'tpi' or rewardfn == 'throughput':
        reward = self._getDeltaMdpReward(self.coreTpiList[0][core], self.coreTpiList[1][core])
    elif rewardfn == 'coretpi':
        #L1 hit is approx 1M, manual inspection of metrics.db tpi shows a good dist around 5M for cores
        #this would be different for UnCore
        reward = self._getDeltaMdpReward(1000000.0, self.coreTpiList[1][core])
    elif rewardfn == 'instr':   #increase instr executed
        reward = self._getDeltaMdpReward(self.coreInstrList[1][core], self.coreInstrList[0][core])
    elif rewardfn == 'csinstr':   #instr executed
# TODO: just instruction based would prefer more cache, no motivation to vacate a cache way
#        reward = self._getDeltaMdpReward(self.coreInstrList[1][core], self.coreInstrList[0][core])

#allocated way utility is imp
#weights determined by latency and divided by 4-issue width due to overlaps
#l3 = 45ns/0.32 = 140 cycles, l2 = 40 cycle l3 access
#        utility = self.coreInstrList[1][core] \
#                  - 35*(self.l3LoadMissList[1][core]+self.l3StoreMissList[1][core])*self.reconfig.getWaysAssignedUtility('l3_cache', core) \
#                  - 10*(self.l2LoadMissList[1][core]+self.l2StoreMissList[1][core])*self.reconfig.getWaysAssignedUtility('l2_cache', core)
#                  - 8  *self.l1LoadMissList[1][core]*self.reconfig.getWaysAssignedUtility('l1_dcache', core)
#        self.csUtility[1][core] = utility
#        reward = self.csUtility[1][core]-self.csUtility[0][core]
#        reward = reward/1000.0
        if self.coreTpiList[1][core] > 0: reward = 1.0e6/self.coreTpiList[1][core]
        else: reward = 1.0
#        print "[csinstr] ",core, reward
    elif rewardfn == 'csl3miss':
#        if self.l3LoadMissList[1][core] > 0: reward = (65.0e3 - self.l3LoadMissList[1][core])/1.0e3   #l3/1e3 is in range 0.001 to 60.0
        if self.l3LoadMissList[1][core] > 0: reward = 1.0e3/(self.l3LoadMissList[1][core]+self.l3StoreMissList[1][core])
        else: reward = 1.0
        print "[csl3miss] ",core, reward
    elif rewardfn == 'csl2miss':
        if self.l2LoadMissList[1][core] > 0: reward = 1.0e4/(self.l2LoadMissList[1][core]+self.l2StoreMissList[1][core])
        else: reward = 1.0
        print "[csl2miss] ",core, reward
    elif rewardfn == 'csepi':
        if self.coreInstrList[1][core] > 1.0: 
          reward = 1e07*self.coreInstrList[1][core]/self.__getSystemEnergy(core)
        else: reward = 1.0
        print "[csepi] ",core, reward
    elif rewardfn == 'csepitpi':
        if self.coreInstrList[1][core] > 1.0 and self.coreInstrList[0][core] > 1.0: 
          tr = self.coreTpiList[0][core]/self.coreTpiList[1][core]
          reward = 1e07*self.coreInstrList[1][core]/(self.__getSystemEnergy(core)*tr)
#TODO          reward = 1e07*self.coreInstrList[1][core]*tr/(self.__getSystemEnergy(core))
        else: reward = 1.0
        print "[csepitpi] ",core, reward
    elif rewardfn == 'coreepitpi':
        if self.coreInstrList[1][core] > 1.0: 
          tr = self.coreTpiList[0][core]/self.coreTpiList[1][core]
          if tr < 0.95: tr =0 #penalize slow down
          tpi = self.coreTpiList[1][core]*1e-6
          edp = self.__coreEnergy(core)*tpi*1e-7
          reward = tr*edp
        else: reward = 1.0
#        print "[coreepitpi] ",core, reward
    elif rewardfn == 'sysepitpi':
        if self.coreInstrList[1][core] > 1.0: 
          tr = self.coreTpiList[0][core]/self.coreTpiList[1][core]
          if tr < 0.95: tr =0 #penalize slow down
          if tr > 5.0: tr =1.0 #usually some initial stages, data not stable
          tpi = self.coreTpiList[1][core]*1e-6  #from fs to ns
          #ergy = (self.__getSystemEnergy(core) + self.__dramEnergy(core))*1e-12 #from fJ to mJ
          ergy = (self.__coreEnergy(core) + self.__dramEnergy(core))*1e-12 #from fJ to mJ
          edp = ergy*tpi
          reward = tr*100.0/(edp or 1.0)
        else: reward = 1.0
        print "[sysepitpi] ",core, reward
    elif rewardfn == 'sysed2p':
        if self.coreInstrList[1][core] > 1.0: 
          tpi = self.coreTpiList[1][core]*1e-6  #from fs to ns
          ergy = (self.__getSystemEnergy(core) + self.__dramEnergy(core))*1e-12 #from fJ to mJ
          #ergy = (self.__coreEnergy(core) + self.__dramEnergy(core))*1e-12 #from fJ to mJ
          ed2p = ergy*tpi*tpi
          reward = 100.0/(ed2p or 1.0)
#          reward = (50.0-ed2p)/10.0
        else: reward = 1.0
        print "[sysed2p] ",core, reward
    elif rewardfn == 'sysedp':
        if self.coreInstrList[1][core] > 1.0: 
          tr = self.coreTpiList[0][core]/self.coreTpiList[1][core]
          if tr < 0.95: tr =0 #penalize slow down
          if tr > 5.0: tr =1.0 #usually some initial stages, data not stable
          tpi = self.coreTpiList[1][core]*1e-6  #from fs to ns
          #ergy = (self.__getSystemEnergy(core) + self.__dramEnergy(core))*1e-12 #from fJ to mJ
          ergy = (self.__coreEnergy(core) + self.__dramEnergy(core))*1e-12 #from fJ to mJ
          edp = ergy*tpi
          reward = (50.0-edp)/10.0
        else: reward = 1.0
        print "[sysedp] ",core, reward, edp, tr
    elif rewardfn == 'csireward':  #reward depends on stp
        #HACK TODO: use tpi as cpi since we are not doing dvfs
        #find the STP taking last interval cpi as base
        stp = sum([self.coreTpiList[0][c]/(self.coreTpiList[1][c] or 1.0) for c in range(sim.config.ncores)])/sim.config.ncores
        if stp > 50.0: stp=1.0  #HACK TODO, the first stp computation is very large
        stp2 = stp*stp    #stp>1 becomes larger
        ireward = self.coreInstrList[1][core] - 15*self.l3LoadMissList[1][core] - 3*self.l2LoadMissList[1][core]
        if ireward >=0: reward = ireward*stp2  #+ve reward should be scaled up as per stp
        else:           reward = ireward/stp2  #-ve reward with high stp should be scaled down, while with low stp would be scaled up
#        print "csireward: ", stp2, stp, ireward, reward
    elif rewardfn == 'csmpkireward':  #reward depends on stp
        #HACK TODO: use tpi as cpi since we are not doing dvfs
        #find the STP taking last interval cpi as base
        stp = sum([self.coreTpiList[0][c]/(self.coreTpiList[1][c] or 1.0) for c in range(sim.config.ncores)])/sim.config.ncores
        if stp > 50.0: stp=1.0  #HACK TODO, the first stp computation is very large
        stp2 = stp    #stp>1 becomes larger
        ireward = 150*(self.l3LoadMissList[0][core] - self.l3LoadMissList[1][core]) +\
                  30 *(self.l2LoadMissList[0][core] - self.l2LoadMissList[1][core]) +\
                  10 *(self.l1LoadMissList[0][core] - self.l1LoadMissList[1][core])
        ireward = ireward/1000.0  #mpki based reward
        if ireward >=0: reward = ireward*stp2  #+ve reward should be scaled up as per stp
        else:           reward = ireward/stp2  #-ve reward with high stp should be scaled down, while with low stp would be scaled up
        print "csimpkireward: ", stp2, stp, ireward, reward
    elif rewardfn == 'hinstr':   #harmonic mean of instr
        inverseTpiSum = sum([1000000.0/(self.coreTpiList[1][c] or 1.0) for c in range(sim.config.ncores)]) #ipc
        hinstr = sim.config.ncores * inverseTpiSum 
        reward = hinstr
        print "reward hinst ", reward
    elif rewardfn == 'uncorereq': #decrease uncorereq, 0:prev, 1:curr
        reward = self._getDeltaMdpReward(self.uncoreRequestList[0][core], self.uncoreRequestList[1][core])
    elif rewardfn == 'l3loadmiss':
#        print "__getReward ", self.l3LoadMissList[0][core], self.l3LoadMissList[1][core]
        reward = self._getDeltaMdpReward(self.l3LoadMissList[0][core], self.l3LoadMissList[1][core])
    elif rewardfn == 'l2miss':
        reward = self._getDeltaMdpReward(self.l2LoadMissList[0][core]+self.l2StoreMissList[0][core], 
                                          self.l2LoadMissList[1][core]+self.l2StoreMissList[1][core])
    elif rewardfn == 'l1miss':
        cl1mpki=float( (self.l1StoreMissList[1][core] + self.l1LoadMissList[1][core])*1000.0)/(self.coreInstrList[1][core] or 1.0)
        reward = 100.0 - cl1mpki
                                          
    else:
        print "Unsupported Reward Function"
        assert(False)   
    return reward
  
  def _getDeltaMdpReward(self, pMetric, cMetric):        
    return (pMetric-cMetric)
#    return float((pMetric-cMetric)/pMetric)*1000        #usually this change is very small percentage
  
  def __getDeltaMdpState3(self, pm, cm):        
    metricRange = self.config.metricRange
    if cm <= pm*(1.0-metricRange): 
        return -1
    elif cm >= pm*(1.0+metricRange):  
        return 1
    else: #within range
        return 0
  
  def __getRatioMdpState(self, pm, cm, mstates=3):        
    metricRange = self.config.metricRange
    if pm == 0: return 0  #TODO: return 0 state if pm=0
    ratio = float(cm)/pm
    low = 1.0 - float(metricRange*int(mstates/2))
    state = 0
    while ratio > low and state < mstates-1:
      state += 1
      low += metricRange
#    print '[RatioMDP] ', state
    return state

  def __getBrPredState(self, core):
    bmp_pki = (1000.0*self.coreBPList[1][core])/self.coreInstrList[1][core]
    levels = [6.0, 2.0]
    state = 0
    for l in levels:
      if l > bmp_pki:
        state += 1
      else:
        break
#    print '[bmp_pki] ', bmp_pki, state
    return state

  def __getCoreTpiBrPredState(self, pm, cm, core):
    stateVec = []
    stateVec.append(self.__getRatioMdpState(pm, cm, 5))
    stateVec.append(self.__getBrPredState(core))
    return self.coreStateAssigner.getState(stateVec)[0]
 
  #30 July 2016: Cooperative_MLM
  #L3 cooperative state, the last L3 action is used to determine the state
  def __getCoreTpiBP_L3Coop_State(self, pm, cm, core):
    stateVec = []
    stateVec.append(self.__getRatioMdpState(pm, cm, 3))
    stateVec.append(self.__getBrPredState(core))
    delta_l3ways = self.reconfig.getNumWaysAssigned('l3_cache', core) - self.old_reconfig.getNumWaysAssigned('l3_cache', core)
    assert(abs(delta_l3ways)<=1)
    stateVec.append(delta_l3ways)
    return self.coreStateAssigner.getState(stateVec)[0]
  
  #15 Oct 2016: JMLM+Cooperative Uncore frequency
  #Unocre cooperative state, the last uncore dvfs action
  def __getCoreTpiFreq_UncoreCoop_State(self, core):
    stateVec = []
    tr = self.coreTpiList[1][core]/(self.coreTpiList[0][core] or 1.0)
    tpilevels = [0.94,0.98,1.02,1.06]
    stateVec.append(self.__getInputLevelState(tr, tpilevels))
    freq = sim.dvfs.get_frequency(core) 
    stateVec.append(freq)
    
    uncore_action = self.reconfig.getUnCoreDvfsOffset()
    stateVec.append(uncore_action)
    print "tpifnoc ", core, tr, freq, uncore_action
    return self.coreStateAssigner.getState(stateVec)[0]
  
  def __getDeltaMdpState9(self, pm, cm, core):        
    metricRange = self.config.metricRange
    stateVec = []
    stateVec.append(self.__getDeltaMdpState3(pm, cm))
    stateVec.append(self.reconfig.getCoreDvfs3Level(core))
    return self.coreStateAssigner.getState(stateVec)[0]

  def __getCoreTpiState7(self, ctpi):        
    baseTpi= 0.25*1000000.0   #1M, L1-hit is 3 cycles @3GHz, L1-Hit = 1ns = 1M fs
    return self.__getTpiLevelState(ctpi, 7, baseTpi, 2.0)
    
  def __getUnCoreTpiState7(self, ctpi):        
    baseTpi= 1.5*1000000.0   
    return self.__getTpiLevelState(ctpi, 7, baseTpi, 1.25)
    
  def __getUnCoreTpiState2(self, ctpi):        
    baseTpi= 2*1000000.0   #based on web2py tpi graphs
    return self.__getTpiLevelState(ctpi, 2, baseTpi, 100.0) #3,3+
  
  def __getL3LoadMissState2(self, cloadmiss, cinstr):
    #we want miss per kilo instr
    #from the web2py graphs 1 MPKI looks like a good factor to divide into 2
    baseMpki = 1.0
    cloadmpki = cloadmiss*1000.0/cinstr
    print "__getL3LoadMissState2  cmpki=", cloadmpki, self.__getTpiLevelState(cloadmpki, 2, baseMpki, 100.0)
    return self.__getTpiLevelState(cloadmpki, 2, baseMpki, 100.0) #1, 1+

  def __getL3LoadMissState4(self, cloadmiss, cinstr):
    #we want miss per kilo instr, 1.0 mpki 2 state not giving very good results
    baseMpki = 0.3
    cloadmpki = cloadmiss*1000.0/cinstr
    state = self.__getUniformLevelState(cloadmpki, 4, baseMpki)  #.3, .6, .9
    print "__getL3LoadMissState4  cmpki=", cloadmpki, state
    return state
  
  def __getUnCoreTpiState3(self, ctpi):        
    baseTpi= 2*1000000.0   #based on web2py tpi graphs
    return self.__getTpiLevelState(ctpi, 3, baseTpi, 2.0) # 2,4,4+
  
  def __getTpiLevelState(self, ctpi, nstates, baseTpi, multiplier):
    tpilevel=baseTpi
    state = 0
    while state+1 < nstates and ctpi > tpilevel:
        tpilevel = multiplier*tpilevel
        state += 1
    return state
 
  def __getInputLevelState(self, metric, levels):
    levels= sorted(levels)
    state = 0
    while state < len(levels) and metric > levels[state]:
        state += 1
#    print "__getInputLevelState ", levels, state
    return state


  def __getUniformLevelState(self, ctpi, nstates, baseTpi):
    tpilevel=baseTpi
    state = 0
    while state+1 < nstates and ctpi > tpilevel:
        tpilevel = tpilevel+baseTpi
        state += 1
    return state
  
  def __getDeltaMdpState2(self, pm, cm):        
    if cm < pm:
        return 0
    elif cm >= pm:  
        return 1

  def __getCoreStateAndReward(self, core):
    statefn = mdp_options[self.mdpConfig]['coremdp']
    state = self.__getCoreState(statefn, core)
    rewardfn = mdp_options[self.mdpConfig]['corereward']
    reward = self.__getReward(rewardfn, core)
    return state, reward 

  def __getUnCoreNwDelayStateReward(self):
    firstIntv = False
    if not hasattr(self, 'nwdelay'):
        firstIntv = True
        self.nwdelay = 0 

    cnwdelay = self.stats['nw_contention_delay'][0].delta
    if cnwdelay < 0:  cnwdelay = 0  #for some unknown reason this can be -ve

    state = self.__getDeltaMdpState3(self.nwdelay, cnwdelay)
    reward = self._getDeltaMdpReward(self.nwdelay, cnwdelay)
    self.nwdelay = cnwdelay

    if firstIntv == True:
        return 0,0
    else:
        return state, reward 
  
  def __getJuanMdpState(self, core):
      cycles = self.stats['time'][core].delta * sim.dvfs.get_frequency(core) / 1e9 # convert fs to cycles
      instrs = self.stats['coreinstrs'][core].delta
      cpi = cycles/(instrs or 1.0)
      cpi_state = int(cpi/2.0)
      if cpi_state > 3: cpi_state = 3

      l1_misses = self.stats['l1d_load_miss'][core].delta + self.stats['l1d_store_miss'][core].delta
      mpki = l1_misses*1000.0/(instrs or 1.0)   #incase of idle cpu
      mpki_state = int(mpki/30.0)
      if mpki_state > 2: mpki_state = 2

      nw_qdelay = self.stats['nw_linkin_qdelay'][0].delta + self.stats['nw_linkout_qdelay'][0].delta
      nw_requests = self.stats['nw_linkin_requests'][0].delta + self.stats['nw_linkout_requests'][0].delta
      bu = nw_qdelay/(nw_requests or 1.0)
      thres = 12000000*.6     #TODO   approx value from wrf out queue delay
      bu_state = int(bu/thres)
      if bu_state > 1: bu_state=1

      state = (mpki_state << 3) | (cpi_state << 1) | bu_state
      print "[__getJuanMdpState] ", cpi, mpki, bu/thres, state
      return state

  def __getUnCoreStateAndReward(self):
    statefn = mdp_options[self.mdpConfig]['uncoremdp']
    rewardfn = mdp_options[self.mdpConfig]['uncorereward']
    if statefn == 'nw':
        return self.__getUnCoreNwDelayStateReward()
    elif statefn == 'tpi':
        inverseTpiSum = sum([1.0/(self.coreTpiList[1][c] or 1.0) for c in range(sim.config.ncores)])
        state = self.__getUnCoreTpiState7(1.0/inverseTpiSum)
        
    #TODO   define a better reward function
    rewardsCore = [self.__getReward(rewardfn, c) for c in range(sim.config.ncores)]
    reward = sum(rewardsCore)/float(len(rewardsCore))
    print "[__getUnCoreStateAndReward] state, reward", state, reward
    return state, reward 

  def __getL3wayStateAndReward(self, n):
    statefn = mdp_options[self.mdpConfig]['l3waymdp']
    sharedCores=self.reconfig.cacheInfo['l3_cache']['sharedCores']
    if self.isBatchL3Mdp(): sharedCores = 4 
    coreIdx = n*sharedCores
    stateVec = [self.__getCoreState(statefn, c) for c in range(coreIdx,coreIdx+sharedCores)]
    state = self.l3StateAssigner.getState(stateVec)
    rewardfn = mdp_options[self.mdpConfig]['l3wayreward']
    #TODO   define a better reward function
    rewardsCore = [self.__getReward(rewardfn, c) for c in range(coreIdx,coreIdx+sharedCores)]
    reward = sum(rewardsCore)/float(len(rewardsCore))
#    print "[__getL3wayStateAndReward] stateVec, state, reward", stateVec, state, reward
    return state, reward 

  def __getL3RwsStateAndReward(self, n):
    statefn = mdp_options[self.mdpConfig]['l3waymdp']
    sharedCores=self.reconfig.cacheInfo['l3_cache']['sharedCores']
    coreIdx = n*sharedCores
    stateVec = self.__getSharedCoreState(statefn, coreIdx, sharedCores)
    state = self.l3StateAssigner.getState(stateVec)
    rewardfn = mdp_options[self.mdpConfig]['l3wayreward']
    #TODO   define a better reward function
    rewardsCore = [self.__getReward(rewardfn, c) for c in range(coreIdx,coreIdx+sharedCores)]
    reward = sum(rewardsCore)/float(len(rewardsCore))
#    print "[__getL3wayStateAndReward] stateVec, state, reward", stateVec, state, reward
    return state, reward 
  
  def __getL2wayStateAndReward(self, n):
    statefn = mdp_options[self.mdpConfig]['l2waymdp']
    sharedCores=self.reconfig.cacheInfo['l2_cache']['sharedCores']
    coreIdx = n*sharedCores
    stateVec = [self.__getCoreState(statefn, c) for c in range(coreIdx,coreIdx+sharedCores)]
    state = self.l2StateAssigner.getState(stateVec)
    rewardfn = mdp_options[self.mdpConfig]['l2wayreward']
    #TODO   define a better reward function
    rewardsCore = [self.__getReward(rewardfn, c) for c in range(coreIdx,coreIdx+sharedCores)]
    reward = sum(rewardsCore)/float(len(rewardsCore))
    print "[__getL2wayStateAndReward] stateVec, state, reward", stateVec, state, reward
    return state, reward 

  def __getL1DwayStateAndReward(self, n):
    statefn = mdp_options[self.mdpConfig]['l1dwaymdp']
    sharedCores=self.reconfig.cacheInfo['l1_dcache']['sharedCores']
    coreIdx = n*sharedCores
    stateVec = [self.__getCoreState(statefn, c) for c in range(coreIdx,coreIdx+sharedCores)]
    state = self.l1dStateAssigner.getState(stateVec)
    rewardfn = mdp_options[self.mdpConfig]['l1dwayreward']
    #TODO   define a better reward function
    rewardsCore = [self.__getReward(rewardfn, c) for c in range(coreIdx,coreIdx+sharedCores)]
    reward = sum(rewardsCore)/float(len(rewardsCore))
    #print "[__getL1DwayStateAndReward] stateVec, state, reward", stateVec, state, reward
    return state, reward 

  def __getL1IwayStateAndReward(self, n):
    statefn = mdp_options[self.mdpConfig]['l1iwaymdp']
    sharedCores=self.reconfig.cacheInfo['l1_icache']['sharedCores']
    coreIdx = n*sharedCores
    stateVec = [self.__getCoreState(statefn, c) for c in range(coreIdx,coreIdx+sharedCores)]
    state = self.l1iStateAssigner.getState(stateVec)
    rewardfn = mdp_options[self.mdpConfig]['l1iwayreward']
    #TODO   define a better reward function
    rewardsCore = [self.__getReward(rewardfn, c) for c in range(coreIdx,coreIdx+sharedCores)]
    reward = sum(rewardsCore)/float(len(rewardsCore))
    #print "[__getL1IwayStateAndReward] stateVec, state, reward", stateVec, state, reward
    return state, reward 
  
  def __getCacheStackCoreState(self, coreIdx):
    #stateVec = [self.__getCoreState(statefn, coreIdx) for statefn in ['csl1mpki3', 'csl2mpki3', 'csl3mpki3'] ]
    stateVec = [self.__getCoreState(statefn, coreIdx) for statefn in ['csl2mpki3', 'csl3mpki3'] ]
    state = self.coreStateAssigner.getState(stateVec)
    return state[0]
  
  def __getCacheStackFeatures(self, core):
    #stateVec = [self.__getCoreState(statefn, coreIdx) for statefn in ['csl1mpki3', 'csl2mpki3', 'csl3mpki3'] ]
    cl1mpki=float(self.l1LoadMissList[1][core]*1000.0)/(self.coreInstrList[1][core] or 1.0)
    cl2mpki=float(self.l2LoadMissList[1][core]*1000.0)/(self.coreInstrList[1][core] or 1.0)
    cl3mpki=float(self.l3LoadMissList[1][core]*1000.0)/(self.coreInstrList[1][core] or 1.0)
    stateVec = [ cl1mpki, cl2mpki, cl3mpki,\
                self.reconfig.getNumWaysAssigned('l3_cache', core), self.reconfig.getNumWaysAssigned('l2_cache', core),
                self.reconfig.getNumWaysAssigned('l3_cache', -1), self.reconfig.getNumWaysAssigned('l2_cache', -1),
                ]
    return stateVec
