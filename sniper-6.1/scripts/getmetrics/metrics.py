import sys
core_energy_metrics = [ 'core_dyn', 'core_stat', \
                        'l1i_dyn', 'l1i_stat', \
                        'l1d_dyn', 'l1d_stat', \
                        'l2_dyn', 'l2_stat', \
                      ]
uncore_energy_metrics = [ 'l3_dyn', 'l3_stat', \
                          'noc_dyn', 'noc_stat', \
                        ]
dram_energy_metrics = ['dram_dyn', 'dram_stat',]

l3_mem_metrics = ['l3_dram_requests', 'l3_load_miss', 'l3_store_miss', 'l3_uncore_req'
            ]
core_mem_metrics = [
                    'l1d_loads', 'l1d_stores', 
                    'l1d_load_miss', 'l1d_store_miss', 
                    'l2_load_miss', 'l2_store_miss',
                    'bp_incorrect',
            ]

sim_metrics = ['time', 'instrs', 'corefreq', 'uncorefreq' ]
perinstr_metrics = ['l3_uncore_time', 'l3_dram_queue_time',
                    'l1i_lat', 'l1d_lat', 'l2_lat',
                    'amat', 'tpi',
              ]
nw_metrics = ['nw_contention_delay', 'nw_linkin_qdelay', 'nw_linkout_qdelay', 'nw_linkin_requests', 'nw_linkout_requests'] 

cpi_metrics = ['cpibase', 'cpibranch', 'cpil1d', 'cpil2d', 'cpil3d', 'cpidram', 'cpirsfull', 'cpiunknown']

normalized_metrics = core_mem_metrics + perinstr_metrics

TIME = 'time'
COREFREQ = 'corefreq'
UNCOREFREQ = 'uncorefreq'
energy_metrics = core_energy_metrics + uncore_energy_metrics + dram_energy_metrics
mem_metrics = core_mem_metrics + l3_mem_metrics

#TODO: used to create the table colomns
metrics_list = sim_metrics + energy_metrics + perinstr_metrics + mem_metrics + cpi_metrics + nw_metrics

def getMetricsFields(np, mlist):
    fields = []
    for m in mlist:
        for n in range(np):
            fields.append(getSqlFieldName(m, n))
    return fields

def getSqlFieldName(statname, ncore):
    sqlname = statname
    for c in ['.', '-']:
        sqlname = sqlname.replace(c,'_')
    return sqlname + '_' + str(ncore)
