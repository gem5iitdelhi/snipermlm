import sys, os
MC_HOME=os.environ['HOME']
sys.path.insert(1, MC_HOME + '/Phd/bitbucket/sniper/sniper-6.1/scripts/getmetrics/')    #TODO
from datastore import DataStore
from bestconfig import bestConfigData
import metrics

class GraphData:
    def __init__(self, np, metricsdb, bestdbpath=None):
        if not os.path.isfile(metricsdb):
            print 'GraphData: metricsdb not found ', np, metricsdb
        self.np = np
        self.ds = DataStore(np)
        self.ds.setDb(metricsdb)
        self.best = None
        if bestdbpath != None:
            bestdb = bestdbpath + '/best.db'
            if not os.path.isfile(bestdb):
                print 'GraphData:bestdb not found ', bestdb, os.path.isfile(bestdb)
            self.best = bestConfigData(bestdbpath)
        #self.ds.printTable()

    def appendBestConfigData(self, js_data, fields, intv_list):
        if self.best == None:
            return

        best_data, best_intv_list = self.best.readDb()
#        print 'appendBestConfigData best_intv_list, intv_list', best_intv_list, intv_list
        if best_intv_list != intv_list: 
            print "Best data interval and metric data interval do not match:"
            print "intv_list: ", intv_list
            print "best_intv_list: ", best_intv_list
            return

        fields += best_data.keys()
        js_data = js_data.update(best_data)

    #scale everything from 0.0->1.0
    def normalizeData(self, js_data):
        ndata = {}
        for k,d in js_data.items():
            dmax = max(d)*1.0
            if dmax == 0:
                dmax = 1    #do not scale
            ndata[k] = [(ed)/dmax for ed in d]
        return ndata

    def getEnergyData(self):
        fields = metrics.getMetricsFields(self.np, metrics.core_energy_metrics)
        fields += metrics.getMetricsFields(1, metrics.uncore_energy_metrics)
#        print 'getEnergyData: ', fields
        js_data, intv_list = self.ds.readDb(fields)
        return js_data, fields, intv_list
    
    def getCoreEnergyData(self):
        fields = metrics.getMetricsFields(self.np, metrics.core_energy_metrics)
        js_data, intv_list = self.ds.readDb(fields)
        return js_data, fields, intv_list
    
    def getSimData(self):
        fields = metrics.getMetricsFields(self.np, metrics.sim_metrics)
        print fields, metrics.sim_metrics
#        fields += metrics.getMetricsFields(1, [metrics.UNCOREFREQ,])
        js_data, intv_list = self.ds.readDb(fields)
        self.appendBestConfigData(js_data, fields, intv_list)
        print js_data, fields, intv_list
        return js_data, fields, intv_list
    
    def getPerInstrData(self):
        fields = metrics.getMetricsFields(self.np, metrics.perinstr_metrics)
        js_data, intv_list = self.ds.readDb(fields)
        return js_data, fields, intv_list

    def getMemData(self):
        fields = metrics.getMetricsFields(self.np, metrics.core_mem_metrics)
        fields += metrics.getMetricsFields(self.np, metrics.l3_mem_metrics)
        js_data, intv_list = self.ds.readDb(fields)
        return js_data, fields, intv_list
        
    def getCpiData(self):
        fields = metrics.getMetricsFields(self.np, metrics.cpi_metrics)
        print 'getCpiData: ', fields
        js_data, intv_list = self.ds.readDb(fields)
        return js_data, fields, intv_list
        
    def getNormalizedData(self):
        fields = metrics.getMetricsFields(self.np, metrics.normalized_metrics)
        js_data, intv_list = self.ds.readDb(fields)
        self.appendBestConfigData(js_data, fields, intv_list)
        return self.normalizeData(js_data), fields, intv_list

    def getTimeData(self):
        fields = metrics.getMetricsFields(self.np, [metrics.TIME,])
        js_data, intv_list = self.ds.readDb(fields)
        return js_data, fields, intv_list
