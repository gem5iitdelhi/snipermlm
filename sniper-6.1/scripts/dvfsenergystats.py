import os, sys, sim
from reportCrash import reportCrash
PEAK_DYN='Peak Dynamic'
RUNTIME_DYN='Runtime Dynamic'
AVG_DYN='PeakRuntimeAvg Dynamic'

class Power:
  def __init__(self, static, dynamic):
    self.s = static
    self.d = dynamic
  def __add__(self, v):
    return Power(self.s + v.s, self.d + v.d)
  def __sub__(self, v):
    return Power(self.s - v.s, self.d - v.d)

class EnergyStats:
  def __init__(self, config, periodicCall=False):
    self.config = config
    self.periodicCall = periodicCall    #periodicCall power computation, call mcpat only first time or periodically 

    self.technology_node = int(sim.config.get('power/technology_node'))
    if self.technology_node == 22:
      self.L1_CACHE_DYN_TYPE = RUNTIME_DYN
      self.L2L3_CACHE_DYN_TYPE = AVG_DYN
    else:  
      self.L1_CACHE_DYN_TYPE = PEAK_DYN
      self.L2L3_CACHE_DYN_TYPE = PEAK_DYN
  
  def setGetMetrics(self, getmetrics):
    self.getmetrics = getmetrics

  def setup(self, args):
    #interval_inst = self.config.interval_inst 
    if self.config.isTimePeriodic():
        sim.util.Every(self.config.interval_inst*sim.util.Time.NS, self.periodicins, roi_only = True)
    elif self.config.isInstrPeriodic():
        sim.util.EveryIns(self.config.interval_inst, self.periodicins, roi_only = True)
    else:
        assert(0)
    #sim.util.EveryIns(interval_inst, self.periodicins, roi_only = True)
    self.core_dvfs_table = self.config.core_dvfs_table
    self.uncore_dvfs_table = self.config.uncore_dvfs_table

    self.name_last = None 
    self.time_last_power = 0
    self.time_last_energy = 0
    self.in_stats_write = False
    self.count = 0
    self.power = {}
    self.energy = {}
    
    try:
      self.registerMetrics()
    except:
      print "Error registering metrics"
      reportCrash()

  def registerMetrics(self):
    for metric in ('energy-static', 'energy-dynamic'):
      l2_cacheSharedCores = long(sim.config.get('perf_model/l2_cache/shared_cores'))
      self.noL2s = long((sim.config.ncores-1)/l2_cacheSharedCores)+1
#      if l2_cacheSharedCores > 1:
#        print 'dvfsmonitor: no of L2s:', self.noL2s
#        for nL2 in range(self.noL2s):
#          sim.stats.register('L2', nL2, metric, self.get_stat)
      
      for core in range(sim.config.ncores):
        sim.stats.register('core', core, metric, self.get_stat)
        sim.stats.register('L1-I', core, metric, self.get_stat)
        sim.stats.register('L1-D', core, metric, self.get_stat)
#        if l2_cacheSharedCores == 1:
        sim.stats.register('L2', core, metric, self.get_stat)
      
      sim.stats.register_per_thread('core-'+metric, 'core', metric)
      sim.stats.register_per_thread('L1-I-'+metric, 'L1-I', metric)
      sim.stats.register_per_thread('L1-D-'+metric, 'L1-D', metric)
      sim.stats.register_per_thread('L2-'+metric, 'L2', metric)
      sim.stats.register('processor', 0, metric, self.get_stat)
      sim.stats.register('dram', 0, metric, self.get_stat)
#      sim.stats.register('L3', 0, metric, self.get_stat)
      sim.stats.register('NOC', 0, metric, self.get_stat)
      sim.stats.register('Memory Controller', 0, metric, self.get_stat)
      sim.stats.register('NIU', 0, metric, self.get_stat)
      sim.stats.register('PCIe', 0, metric, self.get_stat)
      
      l3_cacheSharedCores = long(sim.config.get('perf_model/l3_cache/shared_cores'))
      self.noL3s = long((sim.config.ncores-1)/l3_cacheSharedCores)+1
      print 'dvfsmonitor: no of L3s:', self.noL3s
      for nL3 in range(self.noL3s):
        sim.stats.register('L3', nL3, metric, self.get_stat)

  def periodicins(self, icount, icount_delta):
    if self.count == 0 or self.periodicCall:
      self.update()
    return

  def update(self):
    if long(sim.stats.time()) == long(self.time_last_power):
      # Time did not advance: don't recompute
      return
    
    #if not self.power or (sim.stats.time() - self.time_last_power >= 10 * sim.util.Time.US):
    print "[EnergyStats Update] last_time, time , instr", self.time_last_power, sim.stats.time(), sim.stats.icount()
    try:
      self.count += 1
      #print "[AMATDVFS Power] sim_time ", sim.stats.time(), self.count, sim.util.Time.NS
      # Time advanced significantly, or no power result yet: compute power
      #   Save snapshot
      current = 'energystats-temp%s' % ('B' if self.name_last and self.name_last[-1] == 'A' else 'A')
      self.in_stats_write = True
      sim.stats.write(current)
      self.in_stats_write = False
      #   If we also have a previous snapshot: update power
      if self.name_last:
        power = self.run_power(self.name_last, current)
        self.update_power(power)
      #   Clean up previous last
      if self.name_last:
        sim.util.db_delete(self.name_last)
      #   Update new last
      self.name_last = current
      self.time_last_power = sim.stats.time()
      # Increment energy
      self.update_energy()
    except:
      print "-------ERROR while updating power/energy--------"
      reportCrash()

  def get_stat(self, objectName, index, metricName):
#    if not self.in_stats_write:
#      self.update()
    return self.energy.get((objectName, index, metricName), 0L)

  def update_power(self, power):
    def get_power(component, dynamic=RUNTIME_DYN, prefix = ''):
      if dynamic == AVG_DYN:
        return Power(component[prefix + 'Subthreshold Leakage'] + component[prefix + 'Gate Leakage'], 
                      (component[prefix + RUNTIME_DYN] + component[prefix + PEAK_DYN])/2.0 )
      else:
        return Power(component[prefix + 'Subthreshold Leakage'] + component[prefix + 'Gate Leakage'], component[prefix + dynamic])
 
    #all caches are 'Peak Dynamic' power, others are 'Runtime Dynamic'
    #this is inline after tuning the numbers to intel physical power numbers
    if self.noL2s != sim.config.ncores:
      #L2 is shared, so its power number is no more part of core power tree
      for nL2 in range(self.noL2s):
        self.power[('L2', nL2)] = get_power(power['L2'][nL2], self.L2L3_CACHE_DYN_TYPE)

    for core in range(sim.config.ncores):
      self.power[('L1-I', core)] = get_power(power['Core'][core], self.L1_CACHE_DYN_TYPE, 'Instruction Fetch Unit/Instruction Cache/')
      self.power[('L1-D', core)] = get_power(power['Core'][core], self.L1_CACHE_DYN_TYPE, 'Load Store Unit/Data Cache/')
      
      if self.noL2s == sim.config.ncores:#private L2
        self.power[('L2',   core)] = get_power(power['Core'][core], self.L2L3_CACHE_DYN_TYPE, 'L2/')
#      self.power[('core', core)] = get_power(power['Core'][core], RUNTIME_DYN) - (self.power[('L1-I', core)] + self.power[('L1-D', core)] + self.power[('L2', core)])
      if self.technology_node == 22:
        self.power[('core', core)] = get_power(power['Core'][core], RUNTIME_DYN) - \
                                      (get_power(power['Core'][core], RUNTIME_DYN, 'Instruction Fetch Unit/Instruction Cache/') +\
                                        get_power(power['Core'][core], RUNTIME_DYN, 'Load Store Unit/Data Cache/') +\
                                         get_power(power['Core'][core], RUNTIME_DYN, 'L2/') )
#TODO: 1-Mar-2015, Not sure why the full core enrgy is being considered and we are not subtracting the cache contribution as done in the above commented out equations.
      else:
        self.power[('core', core)] = get_power(power['Core'][core], RUNTIME_DYN)

    self.power[('processor', 0)] = get_power(power['Processor'], RUNTIME_DYN)
    self.power[('dram', 0)] = get_power(power['DRAM'], RUNTIME_DYN)
    for nL3 in range(self.noL3s):
      self.power[('L3', nL3)] = get_power(power['L3'][nL3], self.L2L3_CACHE_DYN_TYPE)
    #self.power[('NOC', 0)] = get_power(power['BUSES'])
    self.power[('NOC', 0)] = get_power(power['NOC'], RUNTIME_DYN)
    self.power[('Memory Controller', 0)] = get_power(power['Memory Controller'], RUNTIME_DYN)
    self.power[('NIU', 0)] = get_power(power['NIU'], RUNTIME_DYN)
    self.power[('PCIe', 0)] = get_power(power['PCIe'], RUNTIME_DYN)

  def update_energy(self):
    if self.power and sim.stats.time() > self.time_last_energy:
      time_delta = sim.stats.time() - self.time_last_energy
      if self.config.subopt == 'RWS':           #TODO Hacky: make this more general, works for now
        reconfig = self.getmetrics.getReconfigData() 
      
      for (component, core), power in self.power.items():
        if component in ['dram', 'Memory Controller', 'NIU', 'PCIe']:
          static_scale = 1.0
          dynamic_scale =1.0
        elif component in ['L3', 'NOC']:
          f0, v0 = self.uncore_dvfs_table[0]
          f = sim.dvfs.get_uncore_frequency()
          v = self.get_vdd_from_freq(f, 'uncore')
          #static scale is linear with v
          static_scale = float(v/v0)
          #dynamic scale is sq with v
          dynamic_scale = float(v/v0)*float(v/v0)*float(float(f)/f0)
        else:    
          f0, v0 = self.core_dvfs_table[0]
          f = sim.dvfs.get_frequency(core)
          v = self.get_vdd_from_freq(f, 'core')
          #static scale is linear with v
          static_scale = float(v/v0)
          #dynamic scale is sq with v
          dynamic_scale = float(v/v0)*float(v/v0)*float(float(f)/f0)
          #print f, f0

        #Cache Reconfig Energy Scaling
        if self.config.subopt == 'RWS':
          RWS_SD=2      #TODO: Hacky value taken from common/system/cache_manager.h, can we somehow have a common place
          if component == 'L3':
            static_scale = static_scale*(1.0-reconfig.getPercentWaysAssigned('l3_cache', RWS_SD))
          elif component == 'L2':
            static_scale = static_scale*(1.0-reconfig.getPercentWaysAssigned('l2_cache', RWS_SD))
        
        #print '[update_energy]', component, core, dynamic_scale, static_scale
        self.energy[(component, core, 'energy-static')] = self.energy.get((component, core, 'energy-static'), 0) + long(time_delta * power.s * static_scale)
        self.energy[(component, core, 'energy-dynamic')] = self.energy.get((component, core, 'energy-dynamic'), 0) + long(time_delta * power.d * dynamic_scale)
      self.time_last_energy = sim.stats.time()

  def get_vdd_from_freq(self, f, comp='core'):
    # Assume self.core_dvfs_table is sorted from highest frequency to lowest
    if comp == 'core':
        table = self.core_dvfs_table
    elif comp == 'uncore':
        table = self.uncore_dvfs_table
    else:
        assert ValueError('invalid comp value given as %s' % comp)
    for _f, _v in table:
      if f == _f:
        return _v
    assert ValueError('Could not find a Vdd for invalid frequency %f' % f)

  def gen_config(self, outputbase):
    #always synthsize the highest config circuit, then scale the energy numbers as per v,f
    _f, _v = self.core_dvfs_table[0]
    freq = [_f]*sim.config.ncores
    vdd  = [_v]*sim.config.ncores
    uncoref, uncorev = self.uncore_dvfs_table[0]
    assert(uncorev == _v)
    configfile = outputbase+'.cfg'
    cfg = open(configfile, 'w')
    cfg.write('''
[perf_model/uncore]
frequency = %s
[perf_model/core]
frequency[] = %s
[power]
vdd[] = %s
    ''' % ((uncoref/1000.), ','.join(map(lambda f: '%f' % (f / 1000.), freq)), ','.join(map(str, vdd))))
    cfg.close()
    return configfile

  def run_power(self, name0, name1):
    outputbase = os.path.join(sim.config.output_dir, 'energystats-temp') #+ str(self.count))

    configfile = self.gen_config(outputbase)

    #TODO: could not fix smt handling in mcpat, for now smt is handled by scaling down static power in analysis scripts
    smt = 1 #TODO long(sim.config.get('perf_model/core/logical_cpus'))
    if smt > 1:                   mcpat = 'tools/smt_mcpat.py'
    elif self.technology_node == 22:   mcpat = 'tools/mcpat_22nm.py'
    else:                         mcpat = 'tools/mcpat.py'

    os.system('unset PYTHONHOME; %s -d %s -o %s -c %s --partial=%s:%s --no-graph --no-text' % (
      os.path.join(os.getenv('SNIPER_ROOT'), mcpat),
      sim.config.output_dir,
      outputbase,
      configfile,
      name0, name1
    ))

    result = {}
    execfile(outputbase + '.py', {}, result)
    return result['power']
